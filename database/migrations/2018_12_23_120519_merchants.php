<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Merchants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchants', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('uid');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('contact_person');
            $table->string('phone');
            $table->string('address');
            $table->string('join_promo');
            $table->string('join_bonus');
            $table->string('available');
            $table->string('rate');
            $table->string('img');
            $table->timestamps();
            $table->integer('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchants');
    }
}
