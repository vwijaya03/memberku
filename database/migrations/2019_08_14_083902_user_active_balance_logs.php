<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserActiveBalanceLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_active_balance_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('uid');
            $table->longText('from_user_uid');
            $table->longText('to_user_uid');
            $table->string('amount');
            $table->string('rate');
            $table->string('type');
            $table->longText('additional_note');
            $table->timestamps();
            $table->integer('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_active_balance_logs');
    }
}
