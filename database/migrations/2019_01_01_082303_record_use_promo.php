<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecordUsePromo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records_use_promo', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('uid');
            $table->longText('promo_uid');
            $table->longText('user_uid')->nullable();
            $table->string('unique_code');
            $table->string('pin')->nullable();
            $table->longText('user_agent');
            $table->string('ip');
            $table->string('promo_type')->nullable();
            $table->timestamps();
            $table->integer('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records_use_promo');
    }
}
