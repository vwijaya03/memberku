<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserClaimGift extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claim_gifts', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('uid');
            $table->longText('user_uid');
            $table->longText('merchant_uid');
            $table->longText('hadiah_uid')->nullable();
            $table->string('nominal');
            $table->string('amount');
            $table->string('no_struk');
            $table->string('no_reg');
            $table->longText('additional_note');
            $table->string('img_struk');
            $table->string('status');
            $table->longText('user_agent');
            $table->string('ip');
            $table->timestamps();
            $table->integer('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claim_gifts');
    }
}
