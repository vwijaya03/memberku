<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UniqueCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unique_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('uid');
            $table->longText('user_uid');
            $table->string('unique_code');
            $table->integer('is_send_email');
            $table->integer('is_send_wa');
            $table->dateTime('expired_date');
            $table->string('click_limit');
            $table->string('is_reset');
            $table->timestamps();
            $table->integer('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unique_codes');
    }
}
