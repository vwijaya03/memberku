<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('uid');
            $table->string('fullname');
            $table->string('name_on_card')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone')->nullable()->unique();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('type');
            $table->string('role');
            $table->string('approve');
            $table->rememberToken();
            $table->timestamps();
            $table->integer('delete');
        });

        DB::statement("
            CREATE INDEX id ON users (id)
        ");
        DB::statement("
            CREATE INDEX email ON users (email)
        ");
        DB::statement("
            CREATE INDEX role ON users (role)
        ");
        DB::statement("
            CREATE INDEX approve ON users (approve)
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
