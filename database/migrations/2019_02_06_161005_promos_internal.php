<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PromosInternal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promos_internal', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('uid');
            $table->longText('user_uid');
            $table->string('title');
            $table->longText('description');
            $table->string('img');
            $table->string('harga_awal');
            $table->string('harga_akhir');
            $table->string('pin')->nullable();
            $table->string('use_pin');
            $table->dateTime('expired_date');
            $table->string('top_promo')->nullable();
            $table->integer('sequence');
            $table->timestamps();
            $table->integer('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promos_internal');
    }
}
