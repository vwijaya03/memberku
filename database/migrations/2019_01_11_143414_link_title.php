<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LinkTitle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_titles', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('uid');
            $table->longText('user_uid');
            $table->longText('description')->nullable();
            $table->string('approve')->nullable();
            $table->timestamps();
            $table->integer('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('link_titles');
    }
}
