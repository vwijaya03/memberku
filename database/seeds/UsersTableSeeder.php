<?php

use Illuminate\Database\Seeder;
use App\Http\Controllers\HelperController;
use App\UserModel;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        UserModel::create
		([
			'uid' => Hash::make(HelperController::uid('users')),
			'fullname' => "Viko Wijaya",
			'name_on_card' => "Viko Wijaya",
			'email' => "viko@mail.com",
			'password' => Hash::make("viko"),
			'phone' => $faker->e164PhoneNumber,
			'address' => $faker->address,
			'role' => "superadmin",
			'approve' => "1",
			'delete' => 0
		]);
    }
}
