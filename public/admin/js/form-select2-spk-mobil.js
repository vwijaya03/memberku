! function(window, document, $) {
    "use strict";

    function formatCustomerResult(result) {
        if (result.loading) return result.fullname;

        return ("<option value='"+result.id+"'>"+result.fullname+"</option>");
    }

    function formatCustomerSelection(result) {
        return result.fullname || result.text;
    }

    function formatBAResult(result) {
        if (result.loading) return result.fullname;

        return ("<option value='"+result.id+"'>"+result.fullname+"</option>");
    }

    function formatBASelection(result) {
        return result.fullname || result.text;
    }

    function formatCarResult(result) {
        if (result.loading) return result.fullname;

        return ("<option value='"+result.id+"'>"+result.name+"</option>");
    }

    function formatCarSelection(result) {
        return result.name || result.text;
    }

    var customer = $(".select2-customer-spk-mobil");
    
    customer.select2({
        placeholder: "Cari customer...",
        ajax: {
            url: search_customer_url,
            dataType: "json",
            delay: 250,
            data: function(params) {
                return {
                    q: params.term,
                    page: params.page
                }
            },
            processResults: function(data, params) {
                return params.page = params.page || 1, {
                    results: data.result.data,
                    pagination: {
                        more: 30 * params.page < data.result.total
                    }
                }
            },
            cache: !0
        },
        escapeMarkup: function(markup) {
            return markup
        },
        minimumInputLength: 1,
        templateResult: formatCustomerResult,
        templateSelection: formatCustomerSelection
    });

    var ba = $(".select2-ba-spk-mobil");
    
    ba.select2({
        placeholder: "Cari ba...",
        ajax: {
            url: search_ba_url,
            dataType: "json",
            delay: 250,
            data: function(params) {
                return {
                    q: params.term,
                    page: params.page
                }
            },
            processResults: function(data, params) {
                return params.page = params.page || 1, {
                    results: data.result.data,
                    pagination: {
                        more: 30 * params.page < data.result.total
                    }
                }
            },
            cache: !0
        },
        escapeMarkup: function(markup) {
            return markup
        },
        minimumInputLength: 1,
        templateResult: formatBAResult,
        templateSelection: formatBASelection
    });

    var mobil = $(".select2-car-spk-mobil");
    
    mobil.select2({
        placeholder: "Cari mobil...",
        ajax: {
            url: search_car_url,
            dataType: "json",
            delay: 250,
            data: function(params) {
                return {
                    q: params.term,
                    page: params.page
                }
            },
            processResults: function(data, params) {
                return params.page = params.page || 1, {
                    results: data.result.data,
                    pagination: {
                        more: 30 * params.page < data.result.total
                    }
                }
            },
            cache: !0
        },
        escapeMarkup: function(markup) {
            return markup
        },
        minimumInputLength: 1,
        templateResult: formatCarResult,
        templateSelection: formatCarSelection
    });
}(window, document, jQuery);