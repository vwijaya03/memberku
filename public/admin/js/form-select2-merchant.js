! function(window, document, $) {
    "use strict";

    function formatMerchantResult(result) {
        if (result.loading) return result.name;

        return ("<option selected>"+decodeURIComponent(result.name.replace(/\+/g, ' '))+"</option>");
    }

    function formatMerchantSelection(result) {
        if(result.name !== undefined || result.name != null) {
            result.name = decodeURIComponent(result.name.replace(/\+/g, ' '));
            result.text = decodeURIComponent(result.text.replace(/\+/g, ' '));
        }

        return result.name || result.text;
    }

    let merchant = $(".select2-merchant");
    let edit_merchant = $(".edit-select2-merchant");

    merchant.select2({
        // tags: true,
        dropdownParent: $("#add-promo"),
        placeholder: "Cari merchant...",
        ajax: {
            url: search_merchant_url,
            dataType: "json",
            delay: 250,
            data: function(params) {
                return {
                    q: params.term,
                    page: params.page
                }
            },
            processResults: function(data, params) {
                return params.page = params.page || 1, {
                    results: data.result.data,
                    pagination: {
                        more: 30 * params.page < data.result.total
                    }
                }
            },
            cache: !0
        },
        escapeMarkup: function(markup) {
            return markup
        },
        minimumInputLength: 1,
        templateResult: formatMerchantResult,
        templateSelection: formatMerchantSelection
    });

    edit_merchant.select2({
        // tags: true,
        dropdownParent: $("#edit-promo"),
        placeholder: "Cari merchant...",
        ajax: {
            url: search_merchant_url,
            dataType: "json",
            delay: 250,
            data: function(params) {
                return {
                    q: params.term,
                    page: params.page
                }
            },
            processResults: function(data, params) {
                return params.page = params.page || 1, {
                    results: data.result.data,
                    pagination: {
                        more: 30 * params.page < data.result.total
                    }
                }
            },
            cache: !0
        },
        escapeMarkup: function(markup) {
            return markup
        },
        minimumInputLength: 1,
        templateResult: formatMerchantResult,
        templateSelection: formatMerchantSelection
    });
}(window, document, jQuery);