$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(".clear-notifications").on("click", function() {
    toastr.clear();
});

function slugify(string)
{
    return string
    .toString()
    .trim()
    .toLowerCase()
    .replace(/\s+/g, "-")
    .replace(/[^\w\-]+/g, "")
    .replace(/\-\-+/g, "-")
    .replace(/^-+/, "")
    .replace(/-+$/, "");
}

function setting(base_url, id) {
    let args = {};
    args.voucher_price = $('.voucher_price').val();
    args.voucher_click_limit = $('.voucher_click_limit').val();
    args.voucher_generate_limit = 0;
    args.server_maintenance = $('.server_maintenance').val();
    args.visibility_remaining_click = $('.visibility_remaining_click').val();
    args.user_id = id;

    $('.save-setting-btn').prop('disabled', true);
    toastr.info("Harap menunggu, data sedang di proses", "Loading...");

    $.ajax({
        type: "POST",
        url: base_url+'setting',
        dataType: "json",
        data: args,
        cache : false,
        success: function(data){
            toastr.clear();
            
            if(data.code == 400) {
                if(Array.isArray(data.message)) {
                    toastr.warning(data.message[0], "Peringatan");
                } else {
                    toastr.warning(data.message, "Peringatan");
                }
            } else if(data.code == 200) {
                toastr.success(data.message, "Sukses");

                $('#setting-modal').modal('hide');
            }

            $('.save-setting-btn').prop('disabled', false);
        } ,error: function(xhr, status, error) {
            console.log(error);
            toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
            $('.save-setting-btn').prop('disabled', false);
        },

    });
}

function change_profile(base_url, id) {
    let args = {};
    args.id = id;
    args.fullname = $('.cp_fullname').val();
    args.name_on_card = $('.cp_name_on_card').val();
    args.email = $('.cp_email').val();
    args.phone = $('.cp_phone').val();
    args.address = $('.cp_address').val();
    args.city = $('.cp_city').val();

    $('.change-profile-btn').prop('disabled', true);
    toastr.info("Harap menunggu, data sedang di proses", "Loading...");

    $.ajax({
        type: "POST",
        url: base_url+'change-profile',
        dataType: "json",
        data: args,
        cache : false,
        success: function(data){
            toastr.clear();
            
            if(data.code == 400) {
                if(Array.isArray(data.message)) {
                    toastr.warning(data.message[0], "Peringatan");
                } else {
                    toastr.warning(data.message, "Peringatan");
                }
            } else if(data.code == 200) {
                toastr.success(data.message, "Sukses");

                $('#change-profile').modal('hide');
            }

            $('.change-profile-btn').prop('disabled', false);
        } ,error: function(xhr, status, error) {
            console.log(error);
            toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
            $('.change-profile-btn').prop('disabled', false);
        },

    });
}

function edit_user(base_url, id) {
    let args = {};
    args.id = id;
    args.fullname = $('.cp_fullname').val();
    args.name_on_card = $('.cp_name_on_card').val();
    args.email = $('.cp_email').val();
    args.phone = $('.cp_phone').val();
    args.address = $('.cp_address').val();

    $('.change-profile-btn').prop('disabled', true);
    toastr.info("Harap menunggu, data sedang di proses", "Loading...");

    $.ajax({
        type: "POST",
        url: base_url+'change-profile',
        dataType: "json",
        data: args,
        cache : false,
        success: function(data){
            toastr.clear();
            
            if(data.code == 400) {
                if(Array.isArray(data.message)) {
                    toastr.warning(data.message[0], "Peringatan");
                } else {
                    toastr.warning(data.message, "Peringatan");
                }
            } else if(data.code == 200) {
                toastr.success(data.message, "Sukses");

                $('#change-profile').modal('hide');
            }

            $('.change-profile-btn').prop('disabled', false);
        } ,error: function(xhr, status, error) {
            console.log(error);
            toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
            $('.change-profile-btn').prop('disabled', false);
        },

    });
}

function master_edit(url)
{
    var execute_url = url;
    window.location.href = execute_url;
}

function master_delete(url, master_data)
{
    swal({
        title: "Apakah anda yakin ?",
        text: "Data tidak bisa di kembalikan !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yakin !',
        cancelButtonText: "Batal !",
        closeOnConfirm: false,
        closeOnCancel: false,
        showLoaderOnConfirm: !0
    },
    function(isConfirm) {
        if (isConfirm) {

            var execute_url = url;

            $.ajax({
                type: "POST",
                url: execute_url,
                dataType: "json",
                cache : false,
                success: function(data){
                    swal({
                        title: 'Berhasil Di Hapus',
                        text: 'Data berhasil di hapus!',
                        type: 'success'
                    }, function() {
                        window.location.href = master_data;
                    });
                } ,error: function(xhr, status, error) {
                  console.log(error);
                },

            });
        } else {
            swal("Cancelled", "", "error");
        }
    });
}

function editStatus(url, master_data)
{
    document.getElementById('changeStatusAirportServiceForm').action = url;
}

function promo_unique_code(url, id)
{
    swal({
        title: "Apakah anda yakin ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yakin !',
        cancelButtonText: "Batal !",
        closeOnConfirm: false,
        closeOnCancel: false,
        showLoaderOnConfirm: !0
    },
    function(isConfirm) {
        if (isConfirm) {

            var execute_url = url;

            $.ajax({
                type: "POST",
                url: execute_url,
                dataType: "json",
                cache : false,
                success: function(data){
                    if(data.status == 1)
                    {
                        swal({
                            title: 'Kode Unik Promo',
                            text: 'Kode Unik : '+data.unique_code,
                            type: 'success'
                        });
                    }
                    else
                    {
                        swal("Data tidak di temukan", "", "error");
                    }
                } ,error: function(xhr, status, error) {
                  console.log(error);
                },

            });
        } else {
            swal("Cancelled", "", "error");
        }
    });
}

function master_push_notification_spk_mobil(url, user_id, car_name, no_polisi)
{
    var args = {};

    args.user_id = user_id;
    args.car_name = car_name;
    args.no_polisi = no_polisi;

    swal({
        title: "Apakah anda yakin ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yakin !',
        cancelButtonText: "Batal !",
        closeOnConfirm: false,
        closeOnCancel: false,
        showLoaderOnConfirm: !0
    },
    function(isConfirm) {
        if (isConfirm) {

            var execute_url = url;

            $.ajax({
                type: "POST",
                url: execute_url,
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                    if(data.status == '200') {
                        swal({
                            title: 'Berhasil Mengirim Notifikasi Ke Customer',
                            text: 'Notifikasi berhasil di kirim !',
                            type: 'success'
                        }, function() {
                            //window.location.href = master_data;
                        });
                    }
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    swal({
                        title: 'Terjadi kesalahan',
                        text: error,
                        type: 'error'
                    }, function() {
                        //window.location.href = master_data;
                    });
                },

            });
        } else {
            swal("Cancelled", "", "error");
        }
    });
}