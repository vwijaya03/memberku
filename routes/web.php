<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicController@getMainPage')->name('getMainPage')->middleware('public_roles');

Route::post('/verify-uc', 'PublicController@postVerifyUniqueCode')->name('postVerifyUniqueCode')->middleware('public_roles');

Route::get('/promo/{promo_uid}/v/{unique_code}/u/{user_uid}/noc/{name_on_card?}', 'PublicController@getPromo')->where('name_on_card', '(.*)')->name('getPromo')->middleware('public_roles');

Route::post('/use-promo', 'PublicController@postUsePromo')->name('postUsePromo')->middleware('public_roles');
Route::post('/direct-use-promo', 'PublicController@postDirectUsePromo')->name('postDirectUsePromo')->middleware('public_roles');

Route::post('/use-promo-internal', 'PublicController@postUsePromoInternal')->name('postUsePromoInternal')->middleware('public_roles');
Route::post('/direct-use-promo-internal', 'PublicController@postDirectUsePromoInternal')->name('postDirectUsePromoInternal')->middleware('public_roles');

Route::get('/promo/{uc?}', 'PublicController@getAllPromo')->where('uc', '(.*)')->name('getAllPromo')->middleware('public_roles');
Route::get('/promo-wa/{uc?}/noc/{noc?}', 'PublicController@getAllPromoByWa')->where('uc', '(.*)')->where('noc', '(.*)')->name('getAllPromoByWa')->middleware('public_roles');

Route::get('/u/{user_code?}', 'PublicController@getAgentProfile')->where('user_code', '(.*)')->name('getAgentProfile')->middleware('public_roles');

// Member Card
// Route::get('/m/{membership_uid?}/u/{user_uid}', 'PublicController@getOneMembership')->where('membership_uid', '(.*)')->name('getOneMembership')->middleware('public_roles');
Route::get('/noc/{noc?}/adr/{address?}/kt/{kota?}/sm/{phone?}/u/{user_uid?}/m/{membership_uid?}', 'PublicController@getOneMembershipPathB')->where('noc', '(.*)')->where('address', '(.*)')->where('kota', '(.*)')->where('phone', '(.*)')->where('user_uid', '(.*)')->where('membership_uid', '(.*)')->name('getOneMembershipPathB')->middleware('public_roles');

Route::get('/booking', 'PartnerBookingController@getPartnerBooking')->name('getPartnerBooking')->middleware('public_roles');
Route::post('/booking', 'PartnerBookingController@postPartnerBooking')->name('postPartnerBooking')->middleware('public_roles');

Route::get('/info-claim-gift/{user_code?}', 'PublicController@getInfoClaimGift')->where('user_code', '(.*)')->name('getInfoClaimGift')->middleware('public_roles');

Route::get('/claim-gift-via-bank/u/{user_code?}', 'PublicController@getClaimGiftViaBank')->where('user_code', '(.*)')->name('getClaimGiftViaBank')->middleware('public_roles');
Route::get('/claim-gift/u/{user_code?}', 'PublicController@getClaimGift')->where('user_code', '(.*)')->name('getClaimGift')->middleware('public_roles');
Route::post('/claim-gift', 'PublicController@postClaimGift')->name('postClaimGift')->middleware('public_roles');
Route::post('/redeem-claim-gift', 'PublicController@postRedeemClaimGift')->name('postRedeemClaimGift')->middleware('public_roles');

Route::post('/merchant-join-bonus-ajax', 'MerchantController@postAjaxMerchantJoinBonus')->name('postAjaxMerchantJoinBonus')->middleware('public_roles');

// Terms Conditions & Privacy Policy
Route::get('/terms-conditions', 'TermsAndConditionsController@getTermsAndConditions')->name('getTermsAndConditions')->middleware('public_roles');
Route::get('/privacy-and-policy', 'PrivacyPolicyController@getPrivacyPolicy')->name('getPrivacyPolicy')->middleware('public_roles');

// Public Agent Registration
Route::get('/registration/{user_code?}', 'PublicAgentRegistrationController@getRegistration')->where('user_code', '(.*)')->name('getRegistration')->middleware('public_roles');
Route::post('/registration', 'PublicAgentRegistrationController@postRegistration')->name('postRegistration')->middleware('public_roles');

// Verify User From Bank
Route::get('/verify', 'VerifyUserFromBankController@getVerifyUserFromBank')->name('getVerifyUserFromBank')->middleware('public_roles');
Route::post('/verify', 'VerifyUserFromBankController@postVerifyUserFromBank')->name('postVerifyUserFromBank')->middleware('public_roles');

Route::group(['prefix' => '/client/'], function () {
	Route::post('/register', 'UserController@postAddAgent')->name('postAddAgent')->middleware('public_roles');

	Route::get('/', 'AgentLoginController@getAgentLogin')->name('getAgentLogin')->middleware('public_roles'); 
	Route::get('/login', 'AgentLoginController@getAgentLogin')->name('getAgentLogin')->middleware('public_roles'); 
	Route::post('/login', 'AgentLoginController@postAgentLogin')->name('postAgentLogin')->middleware('public_roles');
	Route::get('/logout', 'AgentLoginController@getAgentLogout')->name('getAgentLogout')->middleware('agent_roles:superadmin,agent');

	Route::post('/change-profile', 'AgentController@postEditAgent')->name('postEditAgent')->middleware('agent_roles:superadmin,agent');

	Route::get('/change-password', 'AgentLoginController@getAgentChangePassword')->name('getAgentChangePassword')->middleware('agent_roles:superadmin,agent');
	Route::post('/change-password', 'AgentLoginController@postAgentChangePassword')->name('postAgentChangePassword')->middleware('agent_roles:superadmin,agent');

	Route::post('/reset-password', 'AgentLoginController@postResetPassword')->name('postResetPassword');

	Route::get('/dashboard', 'AgentDashboardController@getAgentDashboard')->name('getAgentDashboard')->middleware('agent_roles:superadmin,agent');

	Route::post('/merchant-join-bonus-ajax', 'MerchantController@postAjaxMerchantJoinBonus')->name('postAjaxMerchantJoinBonus')->middleware('agent_roles:superadmin,agent');

	Route::get('/claim-gift', 'ClaimGiftController@getClaimGift')->name('getClaimGift')->middleware('agent_roles:superadmin,agent');
	Route::post('/claim-gift', 'ClaimGiftController@postClaimGift')->name('postClaimGift')->middleware('agent_roles:superadmin,agent');

	// Data Teman
	Route::get('/teman', 'TemanController@getTeman')->name('getTeman')->middleware('agent_roles:superadmin,agent');
	Route::post('/teman-ajax', 'TemanController@postAjaxTeman')->name('postAjaxTeman')->middleware('agent_roles:superadmin,agent');
	Route::post('/add-teman', 'TemanController@postAddTeman')->name('postAddTeman')->middleware('agent_roles:superadmin,agent');
	Route::post('/delete-teman', 'TemanController@postDeleteTeman')->name('postDeleteTeman')->middleware('agent_roles:superadmin,agent');

	// Data Link Title
	Route::get('/link-title', 'GreetingTextController@getLinkTitle')->name('getLinkTitle')->middleware('agent_roles:superadmin,agent');
	Route::post('/link-title', 'GreetingTextController@postAddLinkTitle')->name('postAddLinkTitle')->middleware('agent_roles:superadmin,agent');
	
	// Data Daftar Cashback
	Route::get('/daftar-cashback', 'DaftarCashbackController@getDaftarCashback')->name('getDaftarCashback')->middleware('agent_roles:superadmin,agent');

	// Data Logo
	Route::post('/add-logo', 'PartnerLogoController@postAddPartnerLogo')->name('postAddPartnerLogo')->middleware('agent_roles:superadmin,agent');
	Route::post('/edit-logo', 'PartnerLogoController@postEditPartnerLogo')->name('postEditPartnerLogo')->middleware('agent_roles:superadmin,agent');
	Route::post('/delete-logo', 'PartnerLogoController@postDeletePartnerLogo')->name('postDeletePartnerLogo')->middleware('agent_roles:superadmin,agent');

	Route::get('/search-bank', 'BankController@getSearchBank')->name('getSearchBank')->middleware('agent_roles:superadmin,agent');

	// Data Bank Account
	Route::get('/bank-account', 'UserBankAccountController@getBankAccount')->name('getBankAccount')->middleware('agent_roles:superadmin,agent');
	Route::post('/bank-account-ajax', 'UserBankAccountController@postAjaxBankAccount')->name('postAjaxBankAccount')->middleware('agent_roles:superadmin,agent');
	Route::post('/add-bank-account', 'UserBankAccountController@postAddBankAccount')->name('postAddBankAccount')->middleware('agent_roles:superadmin,agent');
});

Route::group(['prefix' => '/partner/'], function () {
	Route::post('/register', 'UserController@postAddUser')->name('postAddUser')->middleware('public_roles');

	Route::get('/', 'PartnerLoginController@getPartnerLogin')->name('getPartnerLogin')->middleware('public_roles'); 
	Route::get('/login', 'PartnerLoginController@getPartnerLogin')->name('getPartnerLogin')->middleware('public_roles'); 
	Route::post('/login', 'PartnerLoginController@postPartnerLogin')->name('postPartnerLogin')->middleware('public_roles'); 
	Route::get('/logout', 'PartnerLoginController@getPartnerLogout')->name('getPartnerLogout')->middleware('partner_roles:superadmin,partner'); 
	Route::get('/dashboard', 'PartnerDashboardController@getPartnerDashboard')->name('getPartnerDashboard')->middleware('partner_roles:superadmin,partner');

	Route::get('/change-password', 'PartnerLoginController@getPartnerChangePassword')->name('getPartnerChangePassword')->middleware('partner_roles:superadmin,partner');
	Route::post('/change-password', 'PartnerLoginController@postPartnerChangePassword')->name('postPartnerChangePassword')->middleware('partner_roles:superadmin,partner');

	Route::get('/reset-password', 'PartnerLoginController@getPartnerResetPassword')->name('getPartnerResetPassword')->middleware('public_roles');
	Route::post('/reset-password', 'PartnerLoginController@postPartnerResetPassword')->name('postPartnerResetPassword')->middleware('public_roles');

	Route::post('/profile', 'UserController@getAuthUser')->name('getAuthUser')->middleware('partner_roles:superadmin,partner');
	Route::post('/change-profile', 'UserController@postEditUser')->name('postEditUser')->middleware('partner_roles:superadmin,partner');

	// Data Promo Internal Partner
	Route::post('/promo-internal-partner-ajax/{user_uid}', 'PromoInternalPartnerController@postAjaxPromoInternal')->name('postAjaxPromoInternal')->middleware('custom_roles:superadmin,partner');

	// Print Voucher
	Route::get('/print-voucher-v1', 'UniqueCodeController@getPrintVoucherV1')->name('getPrintVoucherV1')->middleware('partner_roles:superadmin,partner');
	Route::post('/print-voucher-v1', 'UniqueCodeController@postPrintVoucherV1')->name('postPrintVoucherV1')->middleware('partner_roles:superadmin,partner');

	Route::get('/print-voucher-v2', 'UniqueCodeController@getPrintVoucherV2')->name('getPrintVoucherV2')->middleware('partner_roles:superadmin,partner');
	Route::post('/print-voucher-v2', 'UniqueCodeController@postPrintVoucherV2')->name('postPrintVoucherV2')->middleware('partner_roles:superadmin,partner');

	Route::get('/print-voucher-v3', 'UniqueCodeController@getPrintVoucherV3')->name('getPrintVoucherV3')->middleware('partner_roles:superadmin,partner');
	Route::post('/print-voucher-v3', 'UniqueCodeController@postPrintVoucherV3')->name('postPrintVoucherV3')->middleware('partner_roles:superadmin,partner');

	// Data Benefit
	Route::get('/benefit', 'PromoController@getPartnerPromo')->name('getPartnerPromo')->middleware('partner_roles:superadmin,partner');
	Route::post('/benefit-ajax', 'PromoController@postAjaxPartnerPromo')->name('postAjaxPartnerPromo')->middleware('partner_roles:superadmin,partner');

	// Data Logo
	Route::post('/add-logo', 'PartnerLogoController@postAddPartnerLogo')->name('postAddPartnerLogo')->middleware('partner_roles:superadmin,partner');
	Route::post('/edit-logo', 'PartnerLogoController@postEditPartnerLogo')->name('postEditPartnerLogo')->middleware('partner_roles:superadmin,partner');
	Route::post('/delete-logo', 'PartnerLogoController@postDeletePartnerLogo')->name('postDeletePartnerLogo')->middleware('partner_roles:superadmin,partner');

	// Data Membership
	Route::get('/membership', 'MembershipController@getPartnerMembership')->name('getPartnerMembership')->middleware('partner_roles:superadmin,partner');
	Route::post('/membership-ajax', 'MembershipController@postAjaxPartnerMembership')->name('postAjaxPartnerMembership')->middleware('partner_roles:superadmin,partner');
	Route::post('/add-membership', 'MembershipController@postAddPartnerMembership')->name('postAddPartnerMembership')->middleware('partner_roles:superadmin,partner');
	Route::post('/edit-membership', 'MembershipController@postEditPartnerMembership')->name('postEditPartnerMembership')->middleware('partner_roles:superadmin,partner');
	Route::post('/delete-membership', 'MembershipController@postDeletePartnerMembership')->name('postDeletePartnerMembership')->middleware('partner_roles:superadmin,partner');

	// Data Intro Text
	Route::get('/greeting-text', 'GreetingTextController@getGreetingText')->name('getGreetingText')->middleware('partner_roles:superadmin,partner');
	Route::post('/greeting-text', 'GreetingTextController@postAddGreetingText')->name('postAddGreetingText')->middleware('partner_roles:superadmin,partner');

	// Data Link Title
	Route::get('/link-title', 'GreetingTextController@getLinkTitle')->name('getLinkTitle')->middleware('partner_roles:superadmin,partner');
	Route::post('/link-title', 'GreetingTextController@postAddLinkTitle')->name('postAddLinkTitle')->middleware('partner_roles:superadmin,partner');

	// Data Unique Code
	Route::get('/unique-code', 'UniqueCodeController@getUniqueCode')->name('getUniqueCode')->middleware('partner_roles:superadmin,partner');
	Route::post('/unique-code-ajax/{user_uid}', 'UniqueCodeController@postAjaxUniqueCode')->name('postAjaxUniqueCode')->middleware('partner_roles:superadmin,partner');

	Route::get('/history-unique-code', 'UniqueCodeController@getHistoryUniqueCode')->name('getHistoryUniqueCode')->middleware('partner_roles:superadmin,partner');
	Route::post('/history-unique-code-ajax/{user_uid}', 'UniqueCodeController@postAjaxHistoryUniqueCode')->name('postAjaxHistoryUniqueCode')->middleware('partner_roles:superadmin,partner');

	Route::post('/add-unique-code', 'UniqueCodeController@postAddUniqueCode')->name('postAddUniqueCode')->middleware('partner_roles:superadmin,partner');
	Route::post('/delete-unique-code', 'UniqueCodeController@postDeleteUniqueCode')->name('postDeleteUniqueCode')->middleware('partner_roles:superadmin,partner');

	// Data Promo
	Route::post('/promo', 'PromoController@postTopPromo')->name('postTopPromo')->middleware('partner_roles:superadmin,partner');

	// Add Data Customer Via WA
	Route::post('/add-data-customer-wa', 'DataCustomerController@postAddDataCustomerWA')->name('postAddDataCustomerWA')->middleware('partner_roles:superadmin,partner');

	// Update Status Berdasarkan Via Kirim Partner, WA atau email
	Route::post('/update-send', 'UniqueCodeController@postUpdateSend')->name('postUpdateSend')->middleware('partner_roles:superadmin,partner');
});

Route::group(['prefix' => '/admin-access/'], function () {

	Route::get('/', 'AdminLoginController@getAdminLogin')->name('getAdminLogin')->middleware('public_roles'); 
	Route::get('/login', 'AdminLoginController@getAdminLogin')->name('getAdminLogin')->middleware('public_roles'); 
	Route::post('/login', 'AdminLoginController@postAdminLogin')->name('postAdminLogin')->middleware('public_roles'); 
	Route::get('/logout', 'AdminLoginController@getAdminLogout')->name('getAdminLogout')->middleware('custom_roles:superadmin'); 
	Route::get('/dashboard', 'AdminDashboardController@getAdminDashboard')->name('getAdminDashboard')->middleware('custom_roles:superadmin');

	Route::get('/change-password', 'AdminLoginController@getChangePassword')->name('getChangePassword')->middleware('custom_roles:superadmin');
	Route::post('/change-password', 'AdminLoginController@postChangePassword')->name('postChangePassword')->middleware('custom_roles:superadmin');

	Route::get('/reset-password', 'AdminLoginController@getResetPassword')->name('getResetPassword');
	Route::post('/reset-password', 'AdminLoginController@postResetPassword')->name('postResetPassword');

	Route::post('/setting', 'SettingController@postAjaxSetting')->name('postAjaxSetting')->middleware('custom_roles:superadmin');

	Route::post('/change-profile', 'UserController@postEditUser')->name('postEditUser')->middleware('custom_roles:superadmin');

	// Data Report Tukar Struk
	Route::get('/report-tukar-struk', 'ReportTukarStrukController@getReportTukarStruk')->name('getReportTukarStruk')->middleware('custom_roles:superadmin');
	Route::post('/report-tukar-struk-ajax', 'ReportTukarStrukController@postAjaxReportTukarStruk')->name('postAjaxReportTukarStruk')->middleware('custom_roles:superadmin');

	// Data Approve Or Reject Tukar Struk
	Route::post('/approve-tukar-struk', 'ClaimGiftController@postApproveTukarStruk')->name('postApproveTukarStruk')->middleware('custom_roles:superadmin');
	Route::post('/reject-tukar-struk', 'ClaimGiftController@postRejectTukarStruk')->name('postRejectTukarStruk')->middleware('custom_roles:superadmin');

	// Data Review Agent Logo
	Route::get('/review-agent-logo', 'ReviewAgentController@getReviewAgentLogo')->name('getReviewAgentLogo')->middleware('custom_roles:superadmin');
	Route::post('/review-agent-logo-ajax', 'ReviewAgentController@postAjaxReviewAgentLogo')->name('postAjaxReviewAgentLogo')->middleware('custom_roles:superadmin');
	Route::post('/approve-agent-logo', 'ReviewAgentController@postApproveAgentLogo')->name('postApproveAgentLogo')->middleware('custom_roles:superadmin');

	// Data Agent Logo
	Route::get('/one-logo', 'PartnerLogoController@getOneLogo')->name('getOneLogo')->middleware('agent_roles:superadmin');
	Route::post('/add-logo', 'PartnerLogoController@postAddPartnerLogoBySuperadmin')->name('postAddPartnerLogoBySuperadmin')->middleware('agent_roles:superadmin,agent');
	Route::post('/delete-logo', 'PartnerLogoController@postDeletePartnerLogo')->name('postDeletePartnerLogo')->middleware('agent_roles:superadmin,agent');

	// Data Review Agent Description
	Route::get('/review-agent-description', 'ReviewAgentController@getReviewAgentDescription')->name('getReviewAgentDescription')->middleware('custom_roles:superadmin');
	Route::post('/review-agent-description-ajax', 'ReviewAgentController@postAjaxReviewAgentDescription')->name('postAjaxReviewAgentDescription')->middleware('custom_roles:superadmin');
	Route::post('/approve-agent-description', 'ReviewAgentController@postApproveAgentDescription')->name('postApproveAgentDescription')->middleware('custom_roles:superadmin');

	// Data Bank
	Route::get('/bank', 'BankController@getBank')->name('getBank')->middleware('custom_roles:superadmin');
	Route::post('/bank-ajax', 'BankController@postAjaxBank')->name('postAjaxBank')->middleware('custom_roles:superadmin');
	Route::post('/add-bank', 'BankController@postAddBank')->name('postAddBank')->middleware('custom_roles:superadmin');
	Route::post('/edit-bank', 'BankController@postEditBank')->name('postEditBank')->middleware('custom_roles:superadmin');
	Route::post('/delete-bank', 'BankController@postDeleteBank')->name('postDeleteBank')->middleware('custom_roles:superadmin');

	// Data Agent Type
	Route::get('/agent-type', 'AgentTypeController@getAgentType')->name('getAgentType')->middleware('custom_roles:superadmin');
	Route::post('/agent-type-ajax', 'AgentTypeController@postAjaxAgentType')->name('postAjaxAgentType')->middleware('custom_roles:superadmin');
	Route::post('/add-agent-type', 'AgentTypeController@postAddAgentType')->name('postAddAgentType')->middleware('custom_roles:superadmin');
	Route::post('/edit-agent-type', 'AgentTypeController@postEditAgentType')->name('postEditAgentType')->middleware('custom_roles:superadmin');
	Route::post('/delete-agent-type', 'AgentTypeController@postDeleteAgentType')->name('postDeleteAgentType')->middleware('custom_roles:superadmin');

	// Data Hadiah
	Route::get('/hadiah', 'HadiahController@getHadiah')->name('getHadiah')->middleware('custom_roles:superadmin');
	Route::post('/hadiah-ajax', 'HadiahController@postAjaxHadiah')->name('postAjaxHadiah')->middleware('custom_roles:superadmin');
	Route::post('/add-hadiah', 'HadiahController@postAddHadiah')->name('postAddHadiah')->middleware('custom_roles:superadmin');
	Route::post('/edit-hadiah', 'HadiahController@postEditHadiah')->name('postEditHadiah')->middleware('custom_roles:superadmin');
	Route::post('/delete-hadiah', 'HadiahController@postDeleteHadiah')->name('postDeleteHadiah')->middleware('custom_roles:superadmin');

	// Data Company
	Route::get('/company', 'CompanyController@getCompany')->name('getCompany')->middleware('custom_roles:superadmin');
	Route::post('/company-ajax', 'CompanyController@postAjaxCompany')->name('postAjaxCompany')->middleware('custom_roles:superadmin');
	Route::post('/add-company', 'CompanyController@postAddCompany')->name('postAddCompany')->middleware('custom_roles:superadmin');
	Route::post('/edit-company', 'CompanyController@postEditCompany')->name('postEditCompany')->middleware('custom_roles:superadmin');
	Route::post('/delete-company', 'CompanyController@postDeleteCompany')->name('postDeleteCompany')->middleware('custom_roles:superadmin');

	// Report Pin
	Route::get('/report-pin/{pin}', 'ReportPinController@getReportPin')->name('getReportPin')->middleware('custom_roles:superadmin');
	Route::post('/report-pin', 'ReportPinController@postReportPin')->name('postReportPin')->middleware('custom_roles:superadmin');

	// Data Setting Pin Promo Internal
	Route::get('/pin-promo-internal', 'SettingController@getSettingPinPromoInternal')->name('getSettingPinPromoInternal')->middleware('custom_roles:superadmin');
	Route::post('/ajax-pin-promo-internal', 'SettingController@getPinSelectedPromoInternal')->name('getPinSelectedPromoInternal')->middleware('custom_roles:superadmin');
	Route::post('/add-pin-promo-internal', 'SettingController@postAddPinPromoInternal')->name('postAddPinPromoInternal')->middleware('custom_roles:superadmin');
	Route::post('/delete-pin-promo-internal', 'SettingController@postDeletePinPromoInternal')->name('postDeletePinPromoInternal')->middleware('custom_roles:superadmin');

	// Data Setting Pin Promo Global
	Route::get('/pin-promo', 'SettingController@getSettingPinPromo')->name('getSettingPinPromo')->middleware('custom_roles:superadmin');
	Route::post('/ajax-pin-promo', 'SettingController@getPinSelectedPromo')->name('getPinSelectedPromo')->middleware('custom_roles:superadmin');
	Route::post('/add-pin-promo', 'SettingController@postAddPinPromo')->name('postAddPinPromo')->middleware('custom_roles:superadmin');
	Route::post('/delete-pin-promo', 'SettingController@postDeletePinPromo')->name('postDeletePinPromo')->middleware('custom_roles:superadmin');

	// Data Setting Promo Partner
	Route::get('/setting-promo-partner', 'SettingController@getSettingPromoPartner')->name('getSettingPromoPartner')->middleware('custom_roles:superadmin');
	Route::get('/partner-selected-promo', 'SettingController@getPartnerSelectedPromo')->name('getPartnerSelectedPromo')->middleware('custom_roles:superadmin');
	Route::get('/partner-unselected-promo', 'SettingController@getPartnerUnselectedPromo')->name('getPartnerUnselectedPromo')->middleware('custom_roles:superadmin');
	Route::post('/add-partner-promo', 'SettingController@postAddPartnerPromo')->name('postAddPartnerPromo')->middleware('custom_roles:superadmin');
	Route::post('/delete-partner-promo', 'SettingController@postDeletePartnerPromo')->name('postDeletePartnerPromo')->middleware('custom_roles:superadmin');

	// Data Setting Unique Code Partner
	Route::get('/setting-unique-code-partner', 'SettingController@getSettingKirimBenefitPartner')->name('getSettingKirimBenefitPartner')->middleware('custom_roles:superadmin,partner');
	Route::post('/setting-unique-code-partner-ajax/{status}', 'SettingController@postSettingUniqueCodePartner')->name('postSettingUniqueCodePartner')->middleware('custom_roles:superadmin');
	Route::post('/data-setting-unique-code-partner', 'SettingController@postGetDataSettingUniqueCodePartner')->name('postGetDataSettingUniqueCodePartner')->middleware('custom_roles:superadmin');
	Route::post('/add-data-setting-unique-code-partner', 'SettingController@postAddDataSettingUniqueCodePartner')->name('postAddDataSettingUniqueCodePartner')->middleware('custom_roles:superadmin');
	Route::post('/reset-total-uc', 'SettingController@postResetTotalUc')->name('postResetTotalUc')->middleware('custom_roles:superadmin');

	// Data Unique Code
	Route::get('/unique-code', 'UniqueCodeController@getAllUniqueCode')->name('getAllUniqueCode')->middleware('custom_roles:superadmin,partner');
	Route::post('/unique-code-ajax', 'UniqueCodeController@postAjaxAllUniqueCode')->name('postAjaxAllUniqueCode')->middleware('custom_roles:superadmin,partner');
	Route::post('/unique-code-total-ajax', 'UniqueCodeController@postAjaxAllUniqueCodeTotal')->name('postAjaxAllUniqueCodeTotal')->middleware('custom_roles:superadmin,partner');

	// Data User (Superadmin)
	Route::get('/user', 'UserController@getUser')->name('getUser')->middleware('custom_roles:superadmin');
	Route::post('/user-ajax', 'UserController@postAjaxUser')->name('postAjaxUser')->middleware('custom_roles:superadmin');
	Route::post('/add-user', 'UserController@postAddUser')->name('postAddUser')->middleware('custom_roles:superadmin');
	Route::post('/edit-user', 'UserController@postEditUser')->name('postEditUser')->middleware('custom_roles:superadmin');
	Route::post('/delete-user', 'UserController@postDeleteUser')->name('postDeleteUser')->middleware('custom_roles:superadmin');

	// Data Merchant
	Route::get('/merchant', 'MerchantController@getMerchant')->name('getMerchant')->middleware('custom_roles:superadmin');
	Route::post('/merchant-ajax', 'MerchantController@postAjaxMerchant')->name('postAjaxMerchant')->middleware('custom_roles:superadmin');
	Route::get('/search-merchant', 'MerchantController@getSearchMerchant')->name('getSearchMerchant')->middleware('custom_roles:superadmin');
	Route::get('/search-merchant-join-promo', 'MerchantController@getSearchMerchantJoinPromo')->name('getSearchMerchantJoinPromo')->middleware('custom_roles:superadmin');
	Route::get('/search-merchant-join-bonus', 'MerchantController@getSearchMerchantJoinBonus')->name('getSearchMerchantJoinBonus')->middleware('custom_roles:superadmin');
	Route::post('/add-merchant', 'MerchantController@postAddMerchant')->name('postAddMerchant')->middleware('custom_roles:superadmin');
	Route::post('/edit-merchant', 'MerchantController@postEditMerchant')->name('postEditMerchant')->middleware('custom_roles:superadmin');
	Route::post('/delete-merchant', 'MerchantController@postDeleteMerchant')->name('postDeleteMerchant')->middleware('custom_roles:superadmin');

	// Data Voucher Expired Date
	Route::get('/voucher-expired-date', 'VoucherExpiredController@getVoucherExpiredDate')->name('getVoucherExpiredDate')->middleware('custom_roles:superadmin');
	Route::post('/voucher-expired-date-ajax', 'VoucherExpiredController@postAjaxVoucherExpiredDate')->name('postAjaxVoucherExpiredDate')->middleware('custom_roles:superadmin');
	Route::post('/add-voucher-expired-date', 'VoucherExpiredController@postAddVoucherExpiredDate')->name('postAddVoucherExpiredDate')->middleware('custom_roles:superadmin');
	Route::post('/edit-voucher-expired-date', 'VoucherExpiredController@postEditVoucherExpiredDate')->name('postEditVoucherExpiredDate')->middleware('custom_roles:superadmin');
	Route::post('/delete-voucher-expired-date', 'VoucherExpiredController@postDeleteVoucherExpiredDate')->name('postDeleteVoucherExpiredDate')->middleware('custom_roles:superadmin');

	// Data Partner
	Route::get('/search-partner', 'PartnerController@getSearchPartner')->name('getSearchPartner')->middleware('custom_roles:superadmin');
	Route::get('/partner/{status}', 'PartnerController@getPartner')->name('getPartner')->middleware('custom_roles:superadmin');
	Route::post('/partner-ajax/{status}', 'PartnerController@postAjaxPartner')->name('postAjaxPartner')->middleware('custom_roles:superadmin');
	Route::post('/add-partner', 'PartnerController@postAddPartner')->name('postAddPartner')->middleware('custom_roles:superadmin');
	Route::post('/edit-partner', 'PartnerController@postEditPartner')->name('postEditPartner')->middleware('custom_roles:superadmin');
	Route::post('/delete-partner', 'PartnerController@postDeletePartner')->name('postDeletePartner')->middleware('custom_roles:superadmin');
	Route::post('/approve-partner', 'PartnerController@postApprovePartner')->name('postApprovePartner')->middleware('custom_roles:superadmin');
	Route::post('/reject-partner', 'PartnerController@postRejectPartner')->name('postRejectPartner')->middleware('custom_roles:superadmin');

	// Data Agent
	Route::get('/search-agent', 'AgentController@getSearchAgent')->name('getSearchAgent')->middleware('custom_roles:superadmin');
	Route::get('/agent', 'AgentController@getAgent')->name('getAgent')->middleware('custom_roles:superadmin');
	Route::post('/agent-ajax', 'AgentController@postAjaxAgent')->name('postAjaxAgent')->middleware('custom_roles:superadmin');
	Route::post('/add-agent', 'AgentController@postAddAgent')->name('postAddAgent')->middleware('custom_roles:superadmin');
	Route::post('/edit-agent', 'AgentController@postEditAgent')->name('postEditAgent')->middleware('custom_roles:superadmin');
	Route::post('/delete-agent', 'AgentController@postDeleteAgent')->name('postDeleteAgent')->middleware('custom_roles:superadmin');
	Route::post('/generate-code', 'AgentController@postGenerateCode')->name('postGenerateCode')->middleware('custom_roles:superadmin');

	// Data Booking
	Route::get('/booking', 'BookingController@getBooking')->name('getBooking')->middleware('custom_roles:superadmin');
	Route::post('/booking-ajax', 'BookingController@postAjaxBooking')->name('postAjaxBooking')->middleware('custom_roles:superadmin');
	Route::post('/add-booking', 'BookingController@postAddBooking')->name('postAddBooking')->middleware('custom_roles:superadmin');
	Route::post('/edit-booking', 'BookingController@postEditBooking')->name('postEditBooking')->middleware('custom_roles:superadmin');
	Route::post('/delete-booking', 'BookingController@postDeleteBooking')->name('postDeleteBooking')->middleware('custom_roles:superadmin');

	// Data Promo
	Route::get('/promo', 'PromoController@getPromo')->name('getPromo')->middleware('custom_roles:superadmin');
	Route::post('/promo-ajax', 'PromoController@postAjaxPromo')->name('postAjaxPromo')->middleware('custom_roles:superadmin');
	Route::post('/add-promo', 'PromoController@postAddPromo')->name('postAddPromo')->middleware('custom_roles:superadmin');
	Route::post('/edit-promo', 'PromoController@postEditPromo')->name('postEditPromo')->middleware('custom_roles:superadmin');
	Route::post('/update-sequence-promo', 'PromoController@postUpdateSequencePromo')->name('postUpdateSequencePromo')->middleware('custom_roles:superadmin');
	Route::post('/delete-promo', 'PromoController@postDeletePromo')->name('postDeletePromo')->middleware('custom_roles:superadmin');

	// Data Promo Internal
	Route::get('/promo-internal', 'PromoInternalController@getPromoInternal')->name('getPromoInternal')->middleware('custom_roles:superadmin');
	Route::post('/promo-internal-ajax', 'PromoInternalController@postAjaxPromoInternal')->name('postAjaxPromoInternal')->middleware('custom_roles:superadmin');
	Route::post('/add-promo-internal', 'PromoInternalController@postAddPromoInternal')->name('postAddPromoInternal')->middleware('custom_roles:superadmin');
	Route::post('/edit-promo-internal', 'PromoInternalController@postEditPromoInternal')->name('postEditPromoInternal')->middleware('custom_roles:superadmin');
	Route::post('/update-sequence-promo-internal', 'PromoInternalController@postUpdateSequencePromoInternal')->name('postUpdateSequencePromoInternal')->middleware('custom_roles:superadmin');
	Route::post('/delete-promo-internal', 'PromoInternalController@postDeletePromoInternal')->name('postDeletePromoInternal')->middleware('custom_roles:superadmin');

	// Data Category Partner
	Route::get('/category-partner', 'CategoryPartnerController@getCategoryPartner')->name('getCategoryPartner')->middleware('custom_roles:superadmin');
	Route::post('/category-partner-ajax', 'CategoryPartnerController@postAjaxCategoryPartner')->name('postAjaxCategoryPartner')->middleware('custom_roles:superadmin');
	Route::post('/add-category-partner', 'CategoryPartnerController@postAddCategoryPartner')->name('postAddCategoryPartner')->middleware('custom_roles:superadmin');
	Route::post('/edit-category-partner', 'CategoryPartnerController@postEditCategoryPartner')->name('postEditCategoryPartner')->middleware('custom_roles:superadmin');
	Route::post('/delete-category-partner', 'CategoryPartnerController@postDeleteCategoryPartner')->name('postDeleteCategoryPartner')->middleware('custom_roles:superadmin');
});