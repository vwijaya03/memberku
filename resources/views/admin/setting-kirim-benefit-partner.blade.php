@extends('admin/header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title">Data {{ $page_title }}</h4>
				            </div>
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
									<table width="1280px" class="table table-striped table-bordered dataex-html5-export server-side-setting-unique-code-partner">
										<thead>
											<tr>
												<th>No.</th>
												<th>Nama Lengkap</th>
												<th>E-mail</th>
												<th>Phone</th>
												<th>Kota</th>
												<th></th>
											</tr>
										</thead>
									</table>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

<!-- Setting Unique Code Partner Modal -->
<div class="modal fade text-xs-left" id="setting-unique-code-partner-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Setting</label>
            </div>

            <form action="#">
                <div class="modal-body">
                    <label>Maximum Kirim Benefit</label>
                    <div class="form-group">
                        <input type="text" placeholder="Maximum Kirim Benefit" class="form-control max-generate-unique-code">
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn save-btn" value="Simpan">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Reset Total UC Modal -->
<div class="modal fade text-xs-left" id="reset-total-uc-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Apakah anda ingin mereset total kirim benefit ini menjadi 0 ?</label>
            </div>

            <form>
                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tidak">
                    <input type="submit" class="btn btn-outline-primary btn reset-btn" value="Ya">
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">
    let table, user_uid = '';

	$(document).ready(function() {
        $('.save-btn').on('click', addSettingUniqueCodePartner);
        $('.reset-btn').on('click', resetTotalUc);

	    table = $('.server-side-setting-unique-code-partner').DataTable({
	    	"scrollX": !0,
	    	"lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
	        "processing": true,
	        "serverSide": true,
	        "ajax":{
	        	"type": "POST",
            	"url": "{{ url($url_admin.'/setting-unique-code-partner-ajax/approved') }}",
            	"dataType": "json",
           	},
	        "columns": [
	            { "data": "no" },
	            { "data": "res.fullname" },
	            { "data": "res.email" },
	            { "data": "res.phone" },
	            { "data": "res.city" },
	            { "data": "action_btn" }
	        ],
	        order: [[1, 'asc']],
            "columnDefs": [
                { "orderable": false, "targets": [ 0, 5 ] },
                { "width": "190px", "targets": [ 5 ] },
                { "width": "120px", "targets": [ 5, 4 ] },
            ]
	    });

        function addSettingUniqueCodePartner() {
            let args = {};
            args.user_uid = user_uid;
            args.max_total_uc = $('.max-generate-unique-code').val();

            $('.save-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                url: "{{ url($url_admin.'/add-data-setting-unique-code-partner') }}",
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");
                        table.ajax.reload(null, false);
                    }

                    $('.save-btn').prop('disabled', false);
                    $('#setting-unique-code-partner-modal').modal('hide');
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
                    $('.save-btn').prop('disabled', false);
                },

            });
        }

        function resetTotalUc() {
            let args = {};
            args.user_uid = user_uid;

            $('.reset-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                url: "{{ url($url_admin.'/reset-total-uc') }}",
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");
                        table.ajax.reload(null, false);
                    }

                    $('#reset-total-uc-modal').modal('hide');
                    $('.reset-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
                    $('.reset-btn').prop('disabled', false);
                    $('#reset-total-uc-modal').modal('hide');
                },

            });
        }
	});

    function getSettingUniqueCodePartner(obj) {
        let args = {};
        args.user_uid = obj.uid;
        user_uid = obj.uid;

        toastr.info("Harap menunggu, data sedang di proses", "Loading...");
        $('.setting-unique-code-partner-btn').prop('disabled', true);

        $.ajax({
            type: "POST",
            url: "{{ url($url_admin.'/data-setting-unique-code-partner') }}",
            dataType: "json",
            data: args,
            cache : false,
            success: function(data){
                toastr.clear();
              
                if(data.code == 400) {
                    if(Array.isArray(data.message)) {
                      toastr.warning(data.message[0], "Peringatan");
                    } else {
                      toastr.warning(data.message, "Peringatan");
                    }
                } else if(data.code == 200) {
                    if(data.result != null) {
                        if(data.result.max_total_uc != null) {
                            $('.max-generate-unique-code').val(data.result.max_total_uc);
                        }
                    } else {
                        $('.max-generate-unique-code').val("0");
                    }
                    
                    $('#setting-unique-code-partner-modal').modal('show');
                }

                $('.setting-unique-code-partner-btn').prop('disabled', false);
            } ,error: function(xhr, status, error) {
                console.log(error);
                toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
                $('.setting-unique-code-partner-btn').prop('disabled', false);
            },

        });
    }

    function confirmResetTotalUc(obj)
    {
        user_uid = obj.uid;
    }
</script>

@endsection