@extends('admin/header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title">Data {{ $page_title }}</h4>
				                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
			        			<div class="heading-elements">
				                    <ul class="list-inline mb-0">
				                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
				                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				                    </ul>
				                </div>
				            </div>
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
									<a href="#" class="btn btn-success mr-1 mb-1" data-toggle="modal" data-target="#add-bank">Tambah Data {{ $page_title }} Baru</a>
									<br><br>
									<table width="1580px" class="table table-striped table-bordered dataex-html5-export server-side-bank">
										<thead>
											<tr>
												<th>No.</th>
                                                <th>Nama Bank</th>
                                                <th>Tipe</th>
												<th></th>
											</tr>
										</thead>
									</table>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

<!-- Add Bank Modal -->
<div class="modal fade text-xs-left" id="add-bank" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Bank</label>
            </div>

            <form action="#">
                <div class="modal-body">

                    <label>Nama Bank *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Bank" class="form-control name">
                    </div>

                    <label>Tipe *</label>
                    <div class="form-group">
                        <select class="form-control type">
                            <option value="bank" selected>Bank</option>
                            <option value="wallet">Wallet</option>
                        </select>
                    </div>
                    
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn save-btn" value="Simpan">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit Bank Modal -->
<div class="modal fade text-xs-left" id="edit-bank" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Bank</label>
            </div>

            <form action="#">
                <div class="modal-body">
                    <label>Nama Bank *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Bank" class="form-control edit_name">
                    </div>

                    <label>Tipe *</label>
                    <div class="form-group">
                        <select class="form-control edit_type">
                        </select>
                    </div>

                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn update-btn" value="Ubah">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Delete Bank Modal -->
<div class="modal fade text-xs-left" id="delete-bank" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Apakah anda ingin menghapus data ini ?</label>
            </div>

            <form>
                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tidak">
                    <input type="submit" class="btn btn-outline-primary btn delete-btn" value="Ya">
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">
    let table, bank_uid = '';

	$(document).ready(function() {
        $('.save-btn').on('click', addBank);
        $('.update-btn').on('click', updateBank);
		$('.delete-btn').on('click', destroyBank);

	    table = $('.server-side-bank').DataTable({
	    	"scrollX": !0,
	    	"lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
	        "processing": true,
	        "serverSide": true,
	        "ajax":{
	        	"type": "POST",
            	"url": "{{ url($url_admin.'/bank-ajax') }}",
            	"dataType": "json",
           	},
	        "columns": [
	            { "data": "no" },
                { "data": "name" },
                { "data": "res.type" },
	            { "data": "action_btn" }
	        ],
	        order: [[1, 'asc']],
            "columnDefs": [
                { "orderable": false, "targets": [ 0, 2 ] },
                // { "width": "190px", "targets": [ 9 ] },
                // { "width": "120px", "targets": [ 5 ] },
            ]
	    });

	    function addBank() {
	    	let args = {};
            args.name = $('.name').val();
            args.type = $('.type').val();
            
	    	$('.save-btn').prop('disabled', true);
	    	toastr.info("Harap menunggu, data sedang di proses", "Loading...");

	    	$.ajax({
                type: "POST",
                url: '{{ $base_url }}'+'add-bank',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                	toastr.clear();
                	
                    if(data.code == 400) {
                    	if(Array.isArray(data.message)) {
                    		toastr.warning(data.message[0], "Peringatan");
                    	} else {
                    		toastr.warning(data.message, "Peringatan");
                    	}
                    } else if(data.code == 200) {
                    	toastr.success(data.message, "Sukses");

                    	$('#add-bank').modal('hide');
                    	
                        $('.name').val("");

						table.ajax.reload(null, false);
                    }

                    $('.save-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                	console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.save-btn').prop('disabled', false);
                },

            });
	    }

        function updateBank() {
            let args = {};
            args.uid = bank_uid;
            args.name = $('.edit_name').val();
            args.type = $('.edit_type').val();

            $('.update-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                url: '{{ $base_url }}'+'edit-bank',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");

                        $('#edit-bank').modal('hide');
                        
                        table.ajax.reload(null, false);
                    }

                    $('.update-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.update-btn').prop('disabled', false);
                },

            });
        }

        function destroyBank() {
            let args = {};
            args.uid = bank_uid;
            
            $('.delete-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                url: '{{ $base_url }}'+'delete-bank',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");

                        $('#delete-bank').modal('hide');
                        
                        table.ajax.reload(null, false);
                    }

                    $('.delete-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.delete-btn').prop('disabled', false);
                },

            });
        }
	});

    function editBank(obj) {
        bank_uid = obj.uid;
        $('.edit_type').empty();
        $('.edit_name').val(decodeURIComponent(obj.name.replace(/\+/g, ' ')));

        if(obj.type == "bank") {
            $('.edit_type').append('<option value="bank" selected>Bank</option>')
            $('.edit_type').append('<option value="wallet">Wallet</option>')
        } else {
            $('.edit_type').append('<option value="wallet" selected>Wallet</option>')
            $('.edit_type').append('<option value="bank">Bank</option>')
        }
    }

    function deleteBank(obj) {
        bank_uid = obj.uid;
    }
</script>

@endsection