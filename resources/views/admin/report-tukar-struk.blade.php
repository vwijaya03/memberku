@extends('admin/header')

@section('content')

<style>
	body.modal-open, .modal-open .modal {
		overflow: hidden !important;
	}
</style>

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title">Data {{ $page_title }}</h4>
				                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
			        			<div class="heading-elements">
				                    <ul class="list-inline mb-0">
				                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
				                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				                    </ul>
				                </div>
				            </div>
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
									
									<table width="1580px" class="table table-striped table-bordered dataex-html5-export server-side-report-tukar-struk">
										<thead>
											<tr>
												<th>No.</th>
												<th>Merchant</th>
                                                <th>Nama Lengkap</th>
												<th>Phone</th>
												<th>Nominal</th>
												<th>No. Struk</th>
												<th>Foto Struk</th>
												<th>Status</th>
												<th>Tanggal Transaksi</th>
												<th></th>
											</tr>
										</thead>
									</table>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

<!-- Approve Description Modal -->
<div class="modal fade text-xs-left" id="description-approve-tukar-struk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>

				<label class="modal-title text-text-bold-600" id="myModalLabel33">Catatan Tambahan</label>
			</div>

			<form>
				<div class="modal-body">
					<label>Catatan Tambahan *</label>
					<div class="form-group">
						<textarea placeholder="Catatan Tambahan..." class="form-control additional-note-approve judul-link-maxlength" id="maxlength-textarea" maxlength="50" rows="8"></textarea>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-outline-primary btn save-additional-note-btn" onclick="approveTukarStruk()">Kirim</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Reject Description Modal -->
<div class="modal fade text-xs-left" id="description-reject-tukar-struk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>

				<label class="modal-title text-text-bold-600" id="myModalLabel33">Catatan Tambahan</label>
			</div>

			<form>
				<div class="modal-body">
					<label>Catatan Tambahan *</label>
					<div class="form-group">
						<textarea placeholder="Catatan Tambahan..." class="form-control additional-note-reject judul-link-maxlength" id="maxlength-textarea" maxlength="80" rows="8"></textarea>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-outline-primary btn save-additional-note-btn" onclick="rejectTukarStruk()">Kirim</button>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">
    let table, glCgUid = '';

	$(".judul-link-maxlength").maxlength({
		alwaysShow: !0,
		warningClass: "tag tag-success judul-link-class",
		limitReachedClass: "tag tag-danger judul-link-class"
	});

	$('#description-reject-tukar-struk').on('hidden.bs.modal', function () {
		$('.bootstrap-maxlength').hide();
	});

	$('#description-approve-tukar-struk').on('hidden.bs.modal', function () {
		$('.bootstrap-maxlength').hide();
	});

	$(document).ready(function() {
        table = $('.server-side-report-tukar-struk').DataTable({
	    	"scrollX": !0,
			"scrollY": '65vh',
            "scrollCollapse": true,
	    	"lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
	        "processing": true,
	        "serverSide": true,
	        "ajax":{
	        	"type": "POST",
            	"url": "{{ url($url_admin.'/report-tukar-struk-ajax') }}",
            	"dataType": "json",
           	},
	        "columns": [
	            { "data": "no" },
                { "data": "merchant" },
                { "data": "fullname" },
                { "data": "res.phone" },
	            { "data": "res.nominal" },
	            { "data": "res.no_struk" },
	            { "data": "fotoStruk" },
	            { "data": "status" },
                { "data": "tukarStrukCreatedAt" },
                { "data": "action" },
	        ],
	        // order: [[1, 'asc']],
            "columnDefs": [
                { "orderable": false, "targets": [ 0, 9] },
                // { "width": "190px", "targets": [ 9 ] },
                // { "width": "120px", "targets": [ 5 ] },
            ]
	    });
	});

	function approveTukarStrukTemp(cgUid) {
		glCgUid = cgUid;
		
		$('#description-approve-tukar-struk').modal();
	}

	function approveTukarStruk() {
		if(glCgUid == null || glCgUid == "") {
			toastr.warning("Data tukar struk tidak ditemukan", "Peringatan");
			return
		}

		let args = {};
		args.cgUid = glCgUid;
		args.additional_note = $('.additional-note-approve').val();

		toastr.info("Harap menunggu, data sedang di proses", "Loading...");

		$.ajax({
			type: "POST",
			url: '{{ $base_url }}'+'approve-tukar-struk',
			dataType: "json",
			data: args,
			cache : false,
			success: function(data){
				toastr.clear();
				
				if(data.code == 400) {
					if(Array.isArray(data.message)) {
						toastr.warning(data.message[0], "Peringatan");
					} else {
						toastr.warning(data.message, "Peringatan");
					}
				} else if(data.code == 200) {
					toastr.success(data.message, "Sukses");

					table.ajax.reload(null, false);
				}

				$('.additional-note-approve').val("");
				$('#description-approve-tukar-struk').modal('hide')
			} ,error: function(xhr, status, error) {
				console.log(error);
				toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
				$('.update-btn').prop('disabled', false);
			},

		});
	}
	
	function rejectTukarStrukTemp(cgUid) {
		glCgUid = cgUid;

		$('#description-reject-tukar-struk').modal();
	}

	function rejectTukarStruk() {
		if(glCgUid == null || glCgUid == "") {
			toastr.warning("Data tukar struk tidak ditemukan", "Peringatan");
			return
		}

		let args = {};
		args.cgUid = glCgUid;
		args.additional_note = $('.additional-note-reject').val();

		toastr.info("Harap menunggu, data sedang di proses", "Loading...");

		$.ajax({
			type: "POST",
			url: '{{ $base_url }}'+'reject-tukar-struk',
			dataType: "json",
			data: args,
			cache : false,
			success: function(data){
				toastr.clear();
				
				if(data.code == 400) {
					if(Array.isArray(data.message)) {
						toastr.warning(data.message[0], "Peringatan");
					} else {
						toastr.warning(data.message, "Peringatan");
					}
				} else if(data.code == 200) {
					toastr.success(data.message, "Sukses");

					table.ajax.reload(null, false);
				}

				$('.additional-note-reject').val("");
				$('#description-reject-tukar-struk').modal('hide')
			} ,error: function(xhr, status, error) {
				console.log(error);
				toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
				$('.update-btn').prop('disabled', false);
			},

		});
	}
</script>

@endsection