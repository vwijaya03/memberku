@extends('admin/header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title">Data {{ $page_title }}</h4>
				                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
			        			<div class="heading-elements">
				                    <ul class="list-inline mb-0">
				                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
				                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				                    </ul>
				                </div>
				            </div>
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
                                    @if ($errors->has('total_code'))
                                        <div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <strong>Jumlah Kode</strong> tidak boleh kosong !
                                        </div>
                                    @endif

                                    <a href="#" class="btn btn-success mr-1 mb-1" data-toggle="modal" data-target="#add-agent">Tambah Data {{ $page_title }} Baru</a>
                                    
                                    <a href="#" class="btn btn-success mr-1 mb-1" data-toggle="modal" data-target="#generate-code">Generate Code</a>
									<br><br>
									<table width="1280px" class="table table-striped table-bordered dataex-html5-export server-side-agent">
										<thead>
											<tr>
												<th>No.</th>
												<th>Nama Lengkap</th>
												<th>Email</th>
												<th>No. HP</th>
												<th>Tipe Agent</th>
												<th></th>
											</tr>
										</thead>
									</table>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

<!-- Generate Code Modal -->
<div class="modal fade text-xs-left" id="generate-code" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Generate Code</label>
            </div>

            <form action="{{ url($url_admin.'/generate-code') }}" method="POST">
                <div class="modal-body">
                    {{ csrf_field() }}

                    <label>Masukkan jumlah code yang ingin digenerate *</label>
                    <div class="form-group">
                        <input type="text" class="form-control total-code" name="total_code">
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <button type="submit" class="btn btn-outline-primary btn">Generate</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Add Agent Modal -->
<div class="modal fade text-xs-left" id="add-agent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data {{ $page_title }}</label>
            </div>

            <form action="#">
                <div class="modal-body">
                    <label>Nama Lengkap *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Lengkap" class="form-control fullname">
                    </div>

                    <label>No HP *</label>
                    <div class="form-group">
                        <input type="text" placeholder="No HP" class="form-control phone">
                    </div>

                    <label>Tipe User *</label>
                    <div class="form-group">
                        <select class="form-control type">
                            <option value="user_only">Customer Only</option>
                            <option value="standart" selected>Customer Plus</option>
                            <option value="toko">Toko</option>
                            <option value="perusahaan">Perusahaan</option>
                            <!-- 
                                <option value="perusahaan">Perusahaan</option> 
                                <option value="toko">Toko</option>
                            -->
                        </select>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn save-btn" value="Simpan">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit Agent Modal -->
<div class="modal fade text-xs-left" id="edit-agent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data {{ $page_title }}</label>
            </div>

            <form action="#">
                <div class="modal-body">
                    <label>Email *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Email" class="form-control edit_email">
                    </div>

                    <label>Nama Lengkap *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Lengkap" class="form-control edit_fullname">
                    </div>

                    <label>No HP *</label>
                    <div class="form-group">
                        <input type="text" placeholder="No HP" class="form-control edit_phone">
                    </div>

                    <label>Tipe User *</label>
                    <div class="form-group">
                        <select class="form-control edit_type">
                            <option value="user_only">Customer Only</option>
                            <option value="standart" selected>Customer Plus</option>
                            <!-- 
                                <option value="perusahaan">Perusahaan</option> 
                                <option value="toko">Toko</option>
                            -->
                        </select>
                    </div>

                    <label>Password (Isi password jika anda ingin mengubahnya)</label>
                    <div class="form-group">
                        <input type="password" placeholder="Password" class="form-control edit_password" value="">
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn update-btn" value="Ubah">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Delete Agent Modal -->
<div class="modal fade text-xs-left" id="delete-agent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Apakah anda ingin menghapus data ini ?</label>
            </div>

            <form>
                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tidak">
                    <input type="submit" class="btn btn-outline-primary btn delete-btn" value="Ya">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Agent Logo Modal -->
<div class="modal fade text-xs-left" id="agent-logo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">My Logo</label>
            </div>

            <form enctype="multipart/form-data" id="upload_logo">
                <div class="modal-body">
                    <label for="file">Logo</label>      
                    <div class="form-group">
                        <input type="file" name="img" class="form-control-file logo">

                        <br>
                        
                        <span class="edit-area-logo">
                            <a href="#" class="btn btn-outline-danger btn delete-logo-btn" onclick="deleteLogo()">Hapus Logo</a>
                            Klik <a href="#" target="_blank" class="current-logo">di sini</a> untuk melihat logo
                        <span>
                    </div>    
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn my-logo-btn" value="Simpan" onclick="addLogo()">
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">
    let table, agent_id, agent_uid = '';

	$(document).ready(function() {
        $('.save-btn').on('click', addAgent);
        $('.update-btn').on('click', updateAgent);
		$('.delete-btn').on('click', destroyAgent);

	    table = $('.server-side-agent').DataTable({
	    	"scrollX": !0,
	    	"lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
	        "processing": true,
	        "serverSide": true,
	        "ajax":{
	        	"type": "POST",
            	"url": "{{ url($url_admin.'/agent-ajax') }}",
            	"dataType": "json",
           	},
	        "columns": [
	            { "data": "no" },
	            { "data": "fullname" },
	            { "data": "email" },
	            { "data": "res.phone" },
	            { "data": "res.type" },
	            { "data": "action_btn" }
	        ],
	        order: [[1, 'asc']],
            "columnDefs": [
                { "orderable": false, "targets": [ 0, 4 ] },
                { "width": "320px", "targets": [ 5 ] },
                //{ "width": "120px", "targets": [ 5, 6 ] },
            ]
	    });

	    function addAgent() {
	    	let args = {};
	    	args.fullname = $('.fullname').val();
	    	args.phone = $('.phone').val();
	    	args.type = $('.type').val();

	    	$('.save-btn').prop('disabled', true);
	    	toastr.info("Harap menunggu, data sedang di proses", "Loading...");

	    	$.ajax({
                type: "POST",
                url: '{{ $base_url }}'+'add-agent',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                	toastr.clear();
                	
                    if(data.code == 400) {
                    	if(Array.isArray(data.message)) {
                    		toastr.warning(data.message[0], "Peringatan");
                    	} else {
                    		toastr.warning(data.message, "Peringatan");
                    	}
                    } else if(data.code == 200) {
                    	toastr.success(data.message, "Sukses");

                    	$('#add-agent').modal('hide');
                    	
                    	$('.fullname').val("");
						$('.email').val("");
						$('.password').val("");

						table.ajax.reload(null, false);
                    }

                    $('.save-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                	console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.save-btn').prop('disabled', false);
                },

            });
	    }

        function updateAgent() {
            let args = {};
            args.id = agent_id;
            args.fullname = $('.edit_fullname').val();
            args.email = $('.edit_email').val();
            args.phone = $('.edit_phone').val();
            args.type = $('.edit_type').val();
            args.password = $('.edit_password').val();

            $('.update-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                url: '{{ $base_url }}'+'edit-agent',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");

                        $('#edit-agent').modal('hide');
                        
                        $('.edit_password').val("");

                        table.ajax.reload(null, false);
                    }

                    $('.update-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.update-btn').prop('disabled', false);
                },

            });
        }

        function destroyAgent() {
            let args = {};
            args.id = agent_id;
            
            $('.delete-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                url: '{{ $base_url }}'+'delete-agent',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");

                        $('#delete-agent').modal('hide');
                        
                        table.ajax.reload(null, false);
                    }

                    $('.delete-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.delete-btn').prop('disabled', false);
                },

            });
        }
	});

    function editAgent(obj) {
        $('.edit_type').empty();

        agent_id = obj.id;
        $('.edit_fullname').val(decodeURIComponent(obj.fullname.replace(/\+/g, ' ')));
        $('.edit_email').val(decodeURIComponent(obj.email.replace(/\+/g, ' ')));
        $('.edit_phone').val(obj.phone);
        
        if(obj.type == "toko") {
            $('.edit_type').append('<option value="toko" selected>Toko</option>')
            $('.edit_type').append('<option value="standart">Standart</option>')
            $('.edit_type').append('<option value="perusahaan">Perusahaan</option>')
            $('.edit_type').append('<option value="user_only">User Only</option>')
        } else if(obj.type == "perusahaan") {
            $('.edit_type').append('<option value="perusahaan" selected>Perusahaan</option>')
            $('.edit_type').append('<option value="standart">Standart</option>')
            $('.edit_type').append('<option value="toko">Toko</option>')
            $('.edit_type').append('<option value="user_only">User Only</option>')
        } else if(obj.type == "user_only") {
            $('.edit_type').append('<option value="user_only" selected>User Only</option>')
            $('.edit_type').append('<option value="perusahaan">Perusahaan</option>')
            $('.edit_type').append('<option value="standart">Standart</option>')
            $('.edit_type').append('<option value="toko">Toko</option>')
        } else {
            $('.edit_type').append('<option value="standart" selected>Standart</option>')
            $('.edit_type').append('<option value="toko">Toko</option>')
            $('.edit_type').append('<option value="perusahaan">Perusahaan</option>')
            $('.edit_type').append('<option value="user_only">User Only</option>')
        }
    }

    function deleteAgent(obj) {
        agent_id = obj.id;
    }

    function addLogo() {
        let data = new FormData($("#upload_logo")[0]);
        data.append('agent_uid', agent_uid);

        $('.my-logo-btn').prop('disabled', true);
        toastr.info("Harap menunggu, data sedang di proses", "Loading...");

        $.ajax({
            type: "POST",
            processData: false,
            contentType: false,
            url: '{{ $base_url }}'+'add-logo',
            dataType: "json",
            data: data,
            cache : false,
            success: function(data){
                toastr.clear();
                
                if(data.code == 400) {
                    if(Array.isArray(data.message)) {
                        toastr.warning(data.message[0], "Peringatan");
                    } else {
                        toastr.warning(data.message, "Peringatan");
                    }
                } else if(data.code == 200) {
                    toastr.success(data.message, "Sukses");

                    $('#agent-logo').modal('hide');                        
                }
                
                $('.logo').val('');  

                $('.my-logo-btn').prop('disabled', false);
            } ,error: function(xhr, status, error) {
                console.log(error);
                toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
                $('.my-logo-btn').prop('disabled', false);
            },

        });
    }

    function deleteLogo() {
        let args = {};
        args.user_uid = agent_uid;

        $('.delete-logo-btn').prop('disabled', true);
        toastr.info("Harap menunggu, data sedang di proses", "Loading...");

        $.ajax({
            type: "POST",
            url: '{{ $base_url }}'+'delete-logo',
            dataType: "json",
            data: args,
            cache : false,
            success: function(data){
                toastr.clear();
                
                if(data.code == 400) {
                    if(Array.isArray(data.message)) {
                        toastr.warning(data.message[0], "Peringatan");
                    } else {
                        toastr.warning(data.message, "Peringatan");
                    }
                } else if(data.code == 200) {
                    toastr.success(data.message, "Sukses");

                    $('#agent-logo').modal('hide');
                }

                $('.delete-logo-btn').prop('disabled', false);
            } ,error: function(xhr, status, error) {
                console.log(error);
                toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
                $('.delete-logo-btn').prop('disabled', false);
            },

        });
    }

    function editLogoAgent(obj) {
        let args = {};
        args.agent_uid = obj.uid;

        agent_uid = obj.uid;

        toastr.info("Harap menunggu, data sedang di proses", "Loading...");

        $.ajax({
            type: "GET",
            url: '{{ $base_url }}'+'one-logo',
            dataType: "json",
            data: args,
            cache : false,
            success: function(data){
                toastr.clear();
                
                if(data.code == 400) {
                    if(Array.isArray(data.message)) {
                        toastr.warning(data.message[0], "Peringatan");
                    } else {
                        toastr.warning(data.message, "Peringatan");
                    }
                } else if(data.code == 200) {
                    //toastr.success(data.message, "Sukses");
                    
                    if(data.result == null) {
                        $('.edit-area-logo').hide();
                    } else {
                        $('.current-logo').attr('href', "{{ url('/') }}"+data.result.img);
                        $('.edit-area-logo').show();
                    }

                    $('#agent-logo').modal();
                }
                
                $('.logo-btn').prop('disabled', false);
            } ,error: function(xhr, status, error) {
                console.log(error);
                toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
                $('.logo-btn').prop('disabled', false);
            },

        });
    }
</script>

@endsection