@extends('admin/header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title">Data {{ $page_title }}</h4>
				                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
			        			<div class="heading-elements">
				                    <ul class="list-inline mb-0">
				                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
				                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				                    </ul>
				                </div>
				            </div>
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
									<a href="#" class="btn btn-success mr-1 mb-1" data-toggle="modal" data-target="#add-merchant">Tambah Data {{ $page_title }} Baru</a>
									<br><br>
									<table width="1580px" class="table table-striped table-bordered dataex-html5-export server-side-merchant">
										<thead>
											<tr>
												<th>No.</th>
                                                <th>Nama Merchant</th>
												<th>E-mail</th>
												<th>Nama Contact Person</th>
												<th>Phone</th>
												<th>Address</th>
												<th>Join Promo</th>
                                                <th>Join Bonus</th>
                                                <th>Available</th>
                                                <th>Foto Struk</th>
                                                <th>Logo Merchant</th>
												<th>Rate</th>
												<th></th>
											</tr>
										</thead>
									</table>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

<!-- Add Merchant Modal -->
<div class="modal fade text-xs-left" id="add-merchant" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Merchant</label>
            </div>

            <form action="#" enctype="multipart/form-data" id="upload_merchant">
                <div class="modal-body">
                    <label>Ready ?</label>
                    <div class="form-group">
                        <input type="checkbox" class="available" name="available">
                    </div>

                    <label>Join Promo</label>
                    <div class="form-group">
                        <input type="checkbox" class="join_promo" name="join_promo">
                    </div>

                    <label>Join Bonus</label>
                    <div class="form-group">
                        <input type="checkbox" class="join_bonus" name="join_bonus">
                    </div>

                    <label>Contoh Struk *</label>
                    <div class="form-group">
                        <input type="file" placeholder="Contoh Struk" class="form-control img" name="img">
                    </div>

                    <label>Logo Merchant *</label>
                    <div class="form-group">
                        <input type="file" placeholder="Logo Merchant" class="form-control logo_merchant" name="logo_merchant">
                    </div>

                    <label>Nama Merchant *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Merchant" class="form-control name" name="name">
                    </div>

                    <label>E-mail *</label>
                    <div class="form-group">
                        <input type="text" placeholder="E-mail" class="form-control email" name="email">
                    </div>

                    <label>Nama Contact Person *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Contact Person" class="form-control contact_person" name="contact_person">
                    </div>

                    <label>Rate *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Rate" class="form-control rate" name="rate">
                    </div>

                    <label>No. HP *</label>
                    <div class="form-group">
                        <input type="text" placeholder="No. HP" class="form-control phone" name="phone">
                    </div>

                    <label>Address *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Address" class="form-control address" name="address">
                    </div>
                    
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn save-btn" value="Simpan">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit Merchant Modal -->
<div class="modal fade text-xs-left" id="edit-merchant" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Merchant</label>
            </div>

            <form action="#" enctype="multipart/form-data" id="edit_upload_merchant">
                <div class="modal-body">
                    <label>Ready ?</label>
                    <div class="form-group">
                        <input type="checkbox" class="edit_available" name="available" checked=''>
                    </div>

                    <label>Join Promo</label>
                    <div class="form-group">
                        <input type="checkbox" class="edit_join_promo" name="join_promo" checked=''>
                    </div>

                    <label>Join Bonus</label>
                    <div class="form-group">
                        <input type="checkbox" class="edit_join_bonus" name="join_bonus" checked=''>
                    </div>

                    <label>Contoh Struk (Upload contoh struk, untuk mengganti contoh struk sebelum nya)</label>
                    <div class="form-group">
                        <input type="file" placeholder="Contoh Struk" class="form-control edit-img" name="img">
                    </div>

                    <label>Logo Merchant</label>
                    <div class="form-group">
                        <input type="file" placeholder="Logo Merchant" class="form-control edit_logo_merchant" name="logo_merchant">
                    </div>

                    <label>Nama Merchant *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Merchant" class="form-control edit_name" name="name">
                    </div>

                    <label>E-mail *</label>
                    <div class="form-group">
                        <input type="text" placeholder="E-mail" class="form-control edit_email" name="email">
                    </div>

                    <label>Nama Contact Person *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Contact Person" class="form-control edit_contact_person" name="contact_person">
                    </div>

                    <label>Rate *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Rate" class="form-control edit_rate" name="rate">
                    </div>

                    <label>No. HP *</label>
                    <div class="form-group">
                        <input type="text" placeholder="No. HP" class="form-control edit_phone" name="phone">
                    </div>

                    <label>Address *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Address" class="form-control edit_address" name="address">
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn update-btn" value="Ubah">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Delete Merchant Modal -->
<div class="modal fade text-xs-left" id="delete-merchant" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Apakah anda ingin menghapus data ini ?</label>
            </div>

            <form>
                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tidak">
                    <input type="submit" class="btn btn-outline-primary btn delete-btn" value="Ya">
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">
    let table, merchant_id = '';

	$(document).ready(function() {
        $('.save-btn').on('click', addMerchant);
        $('.update-btn').on('click', updateMerchant);
		$('.delete-btn').on('click', destroyMerchant);

	    table = $('.server-side-merchant').DataTable({
	    	"scrollX": !0,
	    	"lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
	        "processing": true,
	        "serverSide": true,
	        "ajax":{
	        	"type": "POST",
            	"url": "{{ url($url_admin.'/merchant-ajax') }}",
            	"dataType": "json",
           	},
	        "columns": [
	            { "data": "no" },
                { "data": "name" },
                { "data": "res.email" },
	            { "data": "res.contact_person" },
	            { "data": "res.phone" },
	            { "data": "res.address" },
	            { 
                    "data": "res.join_promo",
                    "render": function ( data, type, row, meta ) {
                        if(data == "yes") {
                            return "yes";
                        } else {
                            return "no";
                        }
                    }
                },
	            { "data": "res.join_bonus",
                    "render": function ( data, type, row, meta ) {
                        if(data == "yes") {
                            return "yes";
                        } else {
                            return "no";
                        }
                    } 
                },
                { "data": "res.available",
                    "render": function ( data, type, row, meta ) {
                        if(data == "yes") {
                            return "yes";
                        } else {
                            return "no";
                        }
                    } 
                },
                { "data": "img" },
                { "data": "logo" },
                { "data": "res.rate",
                    "render": function ( data, type, row, meta ) {
                        if(data) {
                            return data+" %";
                        } else {
                            return data;
                        }
                    } 
                },
	            { "data": "action_btn" }
	        ],
	        order: [[1, 'asc']],
            "columnDefs": [
                // { "orderable": false, "targets": [ 0, 6, 7, 8, 9] },
                // { "width": "190px", "targets": [ 9 ] },
                // { "width": "120px", "targets": [ 5 ] },
            ]
	    });

	    function addMerchant() {
            let data = new FormData($("#upload_merchant")[0]);
            
	    	$('.save-btn').prop('disabled', true);
	    	toastr.info("Harap menunggu, data sedang di proses", "Loading...");

	    	$.ajax({
                type: "POST",
                processData: false,
                contentType: false,
                url: '{{ $base_url }}'+'add-merchant',
                dataType: "json",
                data: data,
                cache : false,
                success: function(data){
                	toastr.clear();
                	
                    if(data.code == 400) {
                    	if(Array.isArray(data.message)) {
                    		toastr.warning(data.message[0], "Peringatan");
                    	} else {
                    		toastr.warning(data.message, "Peringatan");
                    	}
                    } else if(data.code == 200) {
                    	toastr.success(data.message, "Sukses");

                    	$('#add-merchant').modal('hide');
                    	
                        $('.name').val("");
                    	$('.contact_person').val("");
						$('.email').val("");
						$('.phone').val("");
                        $('.address').val("");

                        $('.join_promo').prop('checked', false);
                        $('.join_bonus').prop('checked', false);

						$('.rate').val("");

						table.ajax.reload(null, false);
                    }

                    $('.save-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                	console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.save-btn').prop('disabled', false);
                },

            });
	    }

        function updateMerchant() {
            let data = new FormData($("#edit_upload_merchant")[0]);
            data.append('id', merchant_id);

            $('.update-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                processData: false,
                contentType: false,
                url: '{{ $base_url }}'+'edit-merchant',
                dataType: "json",
                data: data,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");

                        $('#edit-merchant').modal('hide');
                        
                        table.ajax.reload(null, false);
                    }

                    $('.update-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.update-btn').prop('disabled', false);
                },

            });
        }

        function destroyMerchant() {
            let args = {};
            args.id = merchant_id;
            
            $('.delete-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                url: '{{ $base_url }}'+'delete-merchant',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");

                        $('#delete-merchant').modal('hide');
                        
                        table.ajax.reload(null, false);
                    }

                    $('.delete-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.delete-btn').prop('disabled', false);
                },

            });
        }
	});

    function editMerchant(obj) {
        merchant_id = obj.id;
        $('.edit_name').val(decodeURIComponent(obj.name.replace(/\+/g, ' ')));
        $('.edit_contact_person').val(obj.contact_person);
        $('.edit_email').val(obj.email);
        $('.edit_phone').val(obj.phone);
        $('.edit_address').val(obj.address);
        $('.edit_rate').val(obj.rate);

        if(obj.join_promo == 'yes') {
            $('.edit_join_promo').prop('checked', true);
        } else {
            $('.edit_join_promo').prop('checked', false);
        }

        if(obj.join_bonus == 'yes') {
            $('.edit_join_bonus').prop('checked', true);
        } else {
            $('.edit_join_bonus').prop('checked', false);
        }

        if(obj.available == 'yes') {
            $('.edit_available').prop('checked', true);
        } else {
            $('.edit_available').prop('checked', false);
        }
    }

    function deleteMerchant(obj) {
        merchant_id = obj.id;
    }
</script>

@endsection