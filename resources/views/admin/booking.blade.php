@extends('admin/header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title">Data {{ $page_title }}</h4>
				                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
			        			<div class="heading-elements">
				                    <ul class="list-inline mb-0">
				                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
				                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				                    </ul>
				                </div>
				            </div>
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
									<a href="#" class="btn btn-success mr-1 mb-1" data-toggle="modal" data-target="#add-booking">Tambah Data {{ $page_title }} Baru</a>
									<br><br>
									<table width="1580px" class="table table-striped table-bordered dataex-html5-export server-side-booking">
										<thead>
											<tr>
												<th>No.</th>
                                                <th>E-mail</th>
                                                <th>Nama Usaha / Profesi</th>
                                                <th>Contact Person</th>
												<th>Alamat</th>
                                                <th>Social Media</th>
                                                <th>Phone / WA</th>
												<th></th>
											</tr>
										</thead>
									</table>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

<!-- Add Booking Modal -->
<div class="modal fade text-xs-left" id="add-booking" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Booking</label>
            </div>

            <form action="#">
                <div class="modal-body">
                    <label>E-mail *</label>
                    <div class="form-group">
                        <input type="text" placeholder="E-mail" class="form-control email">
                    </div>

                    <label>Nama Usaha / Profesi *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Usaha / Profesi" class="form-control nama_usaha">
                    </div>

                    <label>Nama Contact Person *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Contact Person" class="form-control contact_person">
                    </div>

                    <label>Alamat *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Alamat" class="form-control alamat_usaha">
                    </div>

                    <label>Link IG / FB / Website *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Link IG / FB / Website" class="form-control social_media">
                    </div>

                    <label>No. HP / WA *</label>
                    <div class="form-group">
                        <input type="text" placeholder="No. HP / WA" class="form-control phone">
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn save-btn" value="Simpan">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit Booking Modal -->
<div class="modal fade text-xs-left" id="edit-booking" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Booking</label>
            </div>

            <form>
                <div class="modal-body">
                    <label>E-mail *</label>
                    <div class="form-group">
                        <input type="text" placeholder="E-mail" class="form-control edit_email">
                    </div>

                    <label>Nama Usaha / Profesi *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Usaha / Profesi" class="form-control edit_nama_usaha">
                    </div>

                    <label>Nama Contact Person *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Contact Person" class="form-control edit_contact_person">
                    </div>

                    <label>Alamat *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Alamat" class="form-control edit_alamat_usaha">
                    </div>

                    <label>Link IG / FB / Website *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Link IG / FB / Website" class="form-control edit_social_media">
                    </div>

                    <label>No. HP / WA *</label>
                    <div class="form-group">
                        <input type="text" placeholder="No. HP / WA" class="form-control edit_phone">
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn edit-btn" value="Ubah">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Delete Booking Modal -->
<div class="modal fade text-xs-left" id="delete-booking" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Apakah anda ingin menghapus data ini ?</label>
            </div>

            <form>
                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tidak">
                    <input type="submit" class="btn btn-outline-primary btn delete-btn" value="Ya">
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">
    let table, booking_id = '';

	$(document).ready(function() {
        $('.save-btn').on('click', addBooking);
        $('.edit-btn').on('click', updateBooking);
		$('.delete-btn').on('click', destroyBooking);

	    table = $('.server-side-booking').DataTable({
	    	"scrollX": !0,
	    	"lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
	        "processing": true,
	        "serverSide": true,
	        "ajax":{
	        	"type": "POST",
            	"url": "{{ url($url_admin.'/booking-ajax') }}",
            	"dataType": "json",
           	},
	        "columns": [
	            { "data": "no" },
	            { "data": "res.email" },
	            { "data": "res.nama_usaha" },
	            { "data": "res.contact_person" },
	            { "data": "res.alamat_usaha" },
                { "data": "res.social_media" },
                { "data": "res.phone" },
	            { "data": "action_btn" }
	        ],
	        order: [[1, 'asc']],
            "columnDefs": [
                { "orderable": false, "targets": [ 0, 7 ] },
                { "width": "290px", "targets": [ 7 ] },
                // { "width": "120px", "targets": [ 5 ] },
            ]
	    });

	    function addBooking() {
	    	let args = {};
	    	args.email = $('.email').val();
	    	args.nama_usaha = $('.nama_usaha').val();
            args.contact_person = $('.contact_person').val();
            args.alamat_usaha = $('.alamat_usaha').val();
	    	args.social_media = $('.social_media').val();
            args.phone = $('.phone').val();

	    	$('.save-btn').prop('disabled', true);
	    	toastr.info("Harap menunggu, data sedang di proses", "Loading...");

	    	$.ajax({
                type: "POST",
                url: '{{ $base_url }}'+'add-booking',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                	toastr.clear();
                	
                    if(data.code == 400) {
                    	if(Array.isArray(data.message)) {
                    		toastr.warning(data.message[0], "Peringatan");
                    	} else {
                    		toastr.warning(data.message, "Peringatan");
                    	}
                    } else if(data.code == 200) {
                    	toastr.success(data.message, "Sukses");

                    	$('#add-booking').modal('hide');
                    	
                        $('.email').val("");
                        $('.nama_usaha').val("");
                        $('.contact_person').val("");
                        $('.alamat_usaha').val("");
                        $('.social_media').val("");
                        $('.phone').val("")

						table.ajax.reload(null, false);
                    }

                    $('.save-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                	console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.save-btn').prop('disabled', false);
                },

            });
	    }

        function updateBooking() {
            let args = {};
            args.id = booking_id;
            args.email = $('.edit_email').val();
            args.nama_usaha = $('.edit_nama_usaha').val();
            args.contact_person = $('.edit_contact_person').val();
            args.alamat_usaha = $('.edit_alamat_usaha').val();
            args.social_media = $('.edit_social_media').val();
            args.phone = $('.edit_phone').val();

            $('.edit-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                url: '{{ $base_url }}'+'edit-booking',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");

                        $('#edit-booking').modal('hide');
                        
                        table.ajax.reload(null, false);
                    }

                    $('.edit-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.edit-btn').prop('disabled', false);
                },

            });
        }

        function destroyBooking() {
            let args = {};
            args.id = booking_id;
            
            $('.delete-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                url: '{{ $base_url }}'+'delete-booking',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");

                        $('#delete-booking').modal('hide');
                        
                        table.ajax.reload(null, false);
                    }

                    $('.delete-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.delete-btn').prop('disabled', false);
                },

            });
        }
	});

    function editBooking(obj) {
        booking_id = obj.id;

        $('.edit_email').val(obj.email);
        $('.edit_nama_usaha').val(obj.nama_usaha);
        $('.edit_contact_person').val(obj.contact_person);
        $('.edit_alamat_usaha').val(obj.alamat_usaha);
        $('.edit_social_media').val(obj.social_media);
        $('.edit_phone').val(obj.phone)
    }

    function deleteBooking(obj) {
        booking_id = obj.id;
    }
</script>

@endsection