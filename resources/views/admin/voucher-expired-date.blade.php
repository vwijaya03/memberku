@extends('admin/header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title">Data {{ $page_title }}</h4>

                                <br><br>

                                <div class="form-group">
                                    <label><b>Pilih Partner</b></label> <br>
                                    <select class="select2-partner form-control selected-partner" style="width: 50%;">
                                    </select>
                                </div>
				            </div>
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
									<a href="#" class="btn btn-success mr-1 mb-1" data-toggle="modal" data-target="#add-voucher-expired-date">Tambah Data {{ $page_title }} Baru</a>
									<br><br>
									<table width="1280px" class="table table-striped table-bordered dataex-html5-export server-side-voucher-expired-date">
										<thead>
											<tr>
												<th>No.</th>
												<th>Description</th>
                                                <th>Hari</th>
												<th>Default</th>
												<th></th>
											</tr>
										</thead>
									</table>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

<!-- Add Promo Expired Modal -->
<div class="modal fade text-xs-left" id="add-voucher-expired-date" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Promo Expired</label>
            </div>

            <form action="#">
                <div class="modal-body">
                    <label>Set As Default</label>
                    <div class="form-group">
                        <input type="checkbox" class="top_promo default">
                    </div>

                    <label>Description (Contoh: 1 Minggu) *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Description" class="form-control description">
                    </div>

                    <label>Hari *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Hari" class="form-control days">
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn save-btn" value="Simpan">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit Promo Expired Modal -->
<div class="modal fade text-xs-left" id="edit-voucher-expired-date" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Promo Expired</label>
            </div>

            <form action="#">
                <div class="modal-body">
                    <label>Set As Default</label>
                    <div class="form-group">
                        <input type="checkbox" class="top_promo edit_default">
                    </div>

                    <label>Description (Contoh: 1 Minggu) *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Description" class="form-control edit_description">
                    </div>

                    <label>Hari *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Hari" class="form-control edit_days">
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn update-btn" value="Ubah">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Delete Promo Expired Modal -->
<div class="modal fade text-xs-left" id="delete-voucher-expired-date" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Apakah anda ingin menghapus data ini ?</label>
            </div>

            <form>
                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tidak">
                    <input type="submit" class="btn btn-outline-primary btn delete-btn" value="Ya">
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">
    let table, voucher_expired_id = '';

	$(document).ready(function() {
        $('.save-btn').on('click', addVoucherExpiredDate);
        $('.update-btn').on('click', updateVoucherExpiredDate);
		$('.delete-btn').on('click', destroyVoucherExpiredDate);
        $('.selected-partner').on('change', showSelectedVoucherExpired);

	    table = $('.server-side-voucher-expired-date').DataTable({
	    	"scrollX": !0,
	    	"lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
	        "processing": true,
	        "serverSide": true,
	        "ajax":{
	        	"type": "POST",
            	"url": "{{ url($url_admin.'/voucher-expired-date-ajax') }}",
            	"dataType": "json",
                "data": function(param) {
                    param.user_uid = ($('.selected-partner').val() != null) ? $('.selected-partner').val() : 'abc';
                },
           	},
	        "columns": [
	            { "data": "no" },
                { "data": "res.description" },
                { "data": "res.days" },
	            { "data": "res.default" },
	            { "data": "action_btn" }
	        ],
	        order: [[1, 'asc']],
            "columnDefs": [
                { "orderable": false, "targets": [ 0, 1, 2, 4 ] },
                { "width": "50px", "targets": [ 0 ] },
                { "width": "100px", "targets": [ 1, 2 ] }
            ]
	    });

	    function addVoucherExpiredDate() {
	    	let args = {};
            args.user_uid = $('.selected-partner').val();
	    	args.description = $('.description').val();
            args.days = $('.days').val();
	    	args.default = $('.default').val();

	    	$('.save-btn').prop('disabled', true);
	    	toastr.info("Harap menunggu, data sedang di proses", "Loading...");

	    	$.ajax({
                type: "POST",
                url: '{{ $base_url }}'+'add-voucher-expired-date',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                	toastr.clear();
                	
                    if(data.code == 400) {
                    	if(Array.isArray(data.message)) {
                    		toastr.warning(data.message[0], "Peringatan");
                    	} else {
                    		toastr.warning(data.message, "Peringatan");
                    	}
                    } else if(data.code == 200) {
                    	toastr.success(data.message, "Sukses");

                    	$('#add-voucher-expired-date').modal('hide');
                    	
                    	$('.description').val("");
						$('.days').val("");

						table.ajax.reload(null, false);
                    }

                    $('.save-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                	console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.save-btn').prop('disabled', false);
                },

            });
	    }

        function updateVoucherExpiredDate() {
            let args = {};
            args.id = voucher_expired_id;
            args.user_uid = $('.selected-partner').val();
            args.description = $('.edit_description').val();
            args.days = $('.edit_days').val();
            args.default = $('.edit_default').val();
            
            $('.update-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                url: '{{ $base_url }}'+'edit-voucher-expired-date',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");

                        $('#edit-voucher-expired-date').modal('hide');
                        
                        table.ajax.reload(null, false);
                    }

                    $('.update-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.update-btn').prop('disabled', false);
                },

            });
        }

        function destroyVoucherExpiredDate() {
            let args = {};
            args.id = voucher_expired_id;
            
            $('.delete-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                url: '{{ $base_url }}'+'delete-voucher-expired-date',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");

                        $('#delete-voucher-expired-date').modal('hide');
                        
                        table.ajax.reload(null, false);
                    }

                    $('.delete-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.delete-btn').prop('disabled', false);
                },

            });
        }

        function showSelectedVoucherExpired() {
            table.ajax.reload();   
        }
	});

    function editVoucherExpiredDate(obj) {
        voucher_expired_id = obj.id;
        $('.edit_description').val(obj.description);
        $('.edit_days').val(obj.days);
        
        if(obj.default == 'yes') {
            $('.edit_default').prop('checked', true);
        } else {
            $('.edit_default').prop('checked', false);
        }
    }

    function deleteVoucherExpiredDate(obj) {
        voucher_expired_id = obj.id;
    }

    function formatPartnerResult(result) {
        if (result.loading) return result.fullname;

        return decodeURIComponent(result.fullname.replace(/\+/g, ' '));
    }

    function formatPartnerSelection(result) {
        if(result.fullname !== undefined || result.fullname != null) {
            result.fullname = decodeURIComponent(result.fullname.replace(/\+/g, ' '));
            result.text = decodeURIComponent(result.text.replace(/\+/g, ' '));
        }

        return result.fullname || result.text;
    }

    let partner = $(".select2-partner");
    let search_partner_url = "{{ $search_partner_url }}";

    partner.select2({
        // tags: true,
        // dropdownParent: $("#add-promo"),
        placeholder: "Cari partner...",
        ajax: {
        url: search_partner_url,
            dataType: "json",
            delay: 250,
            data: function(params) {
                return {
                    q: params.term,
                    page: params.page
                }
            },
            processResults: function(data, params) {
                return params.page = params.page || 1, {
                    results: data.result.data.map(function(item) {
                        return {
                            id : item.uid,
                            fullname : item.fullname
                        };
                    }),
                    pagination: {
                        more: 30 * params.page < data.result.total
                    }
                }
            },
            cache: !0
        },
        // escapeMarkup: function(markup) {
        //     return markup
        // },
        minimumInputLength: 1,
        templateResult: formatPartnerResult,
        templateSelection: formatPartnerSelection
    });
</script>

@endsection