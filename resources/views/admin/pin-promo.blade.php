@extends('admin/header')

@section('content')
<style type="text/css">
    .picker {
        top: -400% !important;
    }    
</style>

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title">Data {{ $page_title }}</h4>

                                <br><br>

				                <div class="form-group">
                                    <label><b>Pilih Promo</b></label> <br>
                                    <select class="form-control selected-promo" style="width: 50%;">
                                        @foreach($global_promos as $global_promo)
                                            <option value="{{ $global_promo->uid }}">{{ urldecode($global_promo->title) }}</option>
                                        @endforeach
                                    </select>
                                </div>
				            </div>
                            
                            <div class="card-body collapse in">
                                <div class="card-block card-dashboard">
                                    <table width="1000px" class="table table-striped table-bordered dataex-html5-export server-side-selected-promo">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Pin</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

<!-- Add Pin Modal -->
<div class="modal fade text-xs-left" id="add-pin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Pin</label>
            </div>

            <form action="#">
                <div class="modal-body">
                    <label>Pin *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Pin" class="form-control pin">
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn save-btn" value="Simpan">
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">
    let tableSelectedPromo, promo_id = '';
    let currentSelectedPromoUID = [];

	$(document).ready(function() {
        $('.selected-promo').on('change', showSelectedPromo);
        $('.save-btn').on('click', addPin);

	    tableSelectedPromo = $('.server-side-selected-promo').DataTable({
	    	"scrollX": !0,
            "scrollY": "200px",
	    	"lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
            "processing": true,
	        "serverSide": true,
            "ajax":{
                type: "POST",
                "url": "{{ url($url_admin.'/ajax-pin-promo') }}",
                "dataType": "json",
                "data": function(param) {
                    param.promo_uid = ($('.selected-promo').val() != null) ? $('.selected-promo').val() : 'abc';
                },
            },
            "drawCallback": function (result) { 
                // Here the response
                let data = result.json.data; 
                currentSelectedPromoUID = [];
                $.map(data, function (item, index) {
                    if(item != null || item !== undefined) {
                        currentSelectedPromoUID.push(item.uid);
                    }
                });
            },
	        "columns": [
                { "data": "no" },
	            { 
                    "data": "pin",
                    "render": function ( data, type, row, meta ) {
                        return decodeURIComponent(data.replace(/\+/g, ' '));
                    }
                },
                { "data": "action_btn" },
	        ],
	        order: [[0, 'asc']],
            "columnDefs": [
                { "orderable": false, "targets": [ 0, 1 ] },
                { "width": "30px", "targets": [ 0 ] },
                { "width": "500px", "targets": [ 1 ] },
                { "width": "150px", "targets": [ 2 ] },
                // { "width": "250px", "targets": [ 3 ] }
            ],
            dom: "lBfrtip",
            select: !0,
            buttons: [ 
                {
                    text: 'Hapus Pin',
                    action: function ( e, dt, node, config ) {
                        if($('.selected-promo').val() == '' || $('.selected-promo').val() == null || $('.selected-promo').val() === undefined || $('.selected-promo').val().length == 0) {
                            toastr.warning("Promo tidak boleh kosong", "Peringatan");
                            return;
                        }

                        let pin_promo_uid = $.map(dt.rows( { selected: true } ).data(), function (item, index) {
                            return item.uid;
                        });
                        
                        let args = {};
                        args.promo_uid = $('.selected-promo').val();
                        args.pin_promo_uid = pin_promo_uid;

                        $.ajax({
                            type: "POST",
                            "url": "{{ url($url_admin.'/delete-pin-promo') }}",
                            dataType: "json",
                            data: args,
                            cache : false,
                            success: function(data){
                                toastr.clear();
                                
                                if(data.code == 400) {
                                    if(Array.isArray(data.message)) {
                                        toastr.warning(data.message[0], "Peringatan");
                                    } else {
                                        toastr.warning(data.message, "Peringatan");
                                    }
                                } else if(data.code == 200) {
                                    tableSelectedPromo.ajax.reload();
                                    tableSelectedPromo.buttons(0).disable();

                                    toastr.success(data.message, "Sukses");
                                }
                            } ,error: function(xhr, status, error) {
                                console.log(error);
                                toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                            },

                        });
                    },
                    enabled: false
                },
                {
                    text: 'Tambah Pin',
                    action: function ( e, dt, node, config ) {
                        $('#add-pin').modal();
                        console.log('asd');
                    },
                    enabled: true
                }
            ],
	    });

        tableSelectedPromo.off("select").on( "select", function( e, dt, type, indexes ) {
            let selectedRows = tableSelectedPromo.rows( { selected: true } ).count();
            tableSelectedPromo.button( 0 ).enable( selectedRows === 1 || selectedRows > 0 );
        } );

        tableSelectedPromo.off("deselect").on( "deselect", function( e, dt, type, indexes ) {
            let deselectedRows = tableSelectedPromo.rows( { selected: true } ).count();
            if(deselectedRows === 0)
            {
                tableSelectedPromo.button( 0 ).disable( deselectedRows === 0 );
            }
        } );

        function showSelectedPromo() {
            tableSelectedPromo.ajax.reload();
            tableSelectedPromo.buttons(0).disable();
        }

        function addPin() {
            let args = {};
            args.pin = $('.pin').val();
            args.promo_uid = $('.selected-promo').val();

            $('.save-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                url: "{{ url($url_admin.'/add-pin-promo') }}",
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");

                        $('#add-pin').modal('hide');
                        
                        $('.pin').val("");

                        tableSelectedPromo.ajax.reload(null, false);
                    }

                    $('.save-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.save-btn').prop('disabled', false);
                },

            });
        }
	});

</script>

@endsection