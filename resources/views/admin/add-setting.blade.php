@extends('admin/header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-2">
                <h3 class="content-header-title mb-0">Data {{ $page_title }}</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url($url_admin.'/dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{ url($url_admin.'/setting') }}">{{ $page_title }}</a>
                            </li>
                            <li class="breadcrumb-item active">Add {{ $page_title }}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
        	<section id="basic-form-layouts">
				<div class="row match-height">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title" id="basic-layout-form">Add {{ $page_title }}</h4>
								<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
										<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="card-body collapse in">
								<div class="card-block">
									@if ($errors->has('emergency_call_phone'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Nomor Emergency Yang Dapat Di Hubungi</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('phone'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Nomor Yang Dapat Di Hubungi</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('description'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Deskripsi Tentang Asri Motor</strong> tidak boleh kosong !
										</div>
									@endif

									@if(Session::has('done'))
						                <div class="alert bg-success alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											{{ Session::get('done') }}
										</div>
						            @endif
									
									@if(Session::has('err'))
						                <div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											{{ Session::get('err') }}
										</div>
						            @endif
						            
									<form action="{{ url($url_admin.'/add-setting') }}" method="POST" class="form">

										{!! csrf_field() !!}

										<h4 class="form-section"><i class="ft-file-text"></i> Add {{ $page_title }}</h4>
										<div class="row">

											<div class="col-md-6">
												<div class="form-group">
													<label for="file">Nomor Emergency Yang Dapat Di Hubungi</label>
													<input type="text" class="form-control" name="emergency_call_phone" value="{{ old('emergency_call_phone') }}">
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group">
													<label for="file">Nomor Yang Dapat Di Hubungi</label>
													<input type="text" class="form-control" name="phone" value="{{ old('phone') }}">
												</div>
											</div>

											<div class="col-md-12">
												<div class="form-group">
													<label for="file">Deskripsi Tentang Asri Motor</label>
				                                    <textarea class="form-control" rows="10" name="description">{{ old('description') }}</textarea>
												</div>
											</div>
										</div>

										<div class="form-actions">
											<button type="submit" class="btn btn-primary mr-1">
												<i class="fa fa-check-square-o"></i> Simpan
											</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
        </div>
    </div>
</div>

@endsection