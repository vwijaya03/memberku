@extends('admin/header')

@section('content')
<style type="text/css">
    .picker {
        top: -400% !important;
    }
</style>

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title">Data {{ $page_title }}</h4>
				                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
			        			<div class="heading-elements">
				                    <ul class="list-inline mb-0">
				                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
				                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				                    </ul>
				                </div>
				            </div>
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
									<a href="#" class="btn btn-success mr-1 mb-1" data-toggle="modal" data-target="#add-hadiah">Tambah Data {{ $page_title }} Baru</a>
									<br><br>
									<table width="1280px" class="table table-striped table-bordered dataex-html5-export server-side-hadiah">
										<thead>
											<tr>
												<th>No.</th>
                                                <th>Merchant</th>
                                                <th>Title</th>
                                                <th>Description</th>
												<th>Gambar</th>
                                                <th>Expired Hadiah</th>
												<th></th>
											</tr>
										</thead>
									</table>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

<!-- Add Hadiah Modal -->
<div class="modal fade text-xs-left" id="add-hadiah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Hadiah</label>
            </div>

            <form action="#" enctype="multipart/form-data" id="upload_hadiah">
                <div class="modal-body">
                    <div class="row">

                        <div class="col-md-4">
                            <label>Merchant *</label>
                            <div class="form-group">
                                <select class="select2-merchant form-control merchant" name="merchant" style="width: 100%">
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-md-8">
                            <label>Title *</label>
                            <div class="form-group">
                                <input type="text" placeholder="Title" class="form-control title" name="title">
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <label>Description *</label>
                            <div class="form-group">
                                <textarea class="form-control" id="description_editor" rows="6" name="description"></textarea>
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <label>Gambar *</label>
                            <div class="form-group">
                                <input type="file" placeholder="Gambar" class="form-control img" name="img">
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <label>Expired Promo *</label>
                            <div class="form-group">
                                <input style="background-color: white !important;" type='text' class="form-control pickadate-selectors-event" placeholder="Expired Promo" name="expired_date"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn save-btn" value="Simpan">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit Hadiah Modal -->
<div class="modal fade text-xs-left" id="edit-hadiah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Hadiah</label>
            </div>

            <form enctype="multipart/form-data" id="edit_upload_hadiah">
                <div class="modal-body">
                    <div class="row">
                        
                        <div class="col-md-4">
                            <label>Merchant *</label>
                            <div class="form-group">
                                <select class="edit-select2-merchant form-control merchant edit-merchant" name="merchant" style="width: 100%">
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-md-8">
                            <label>Title *</label>
                            <div class="form-group">
                                <input type="text" placeholder="Title" class="form-control edit-title" name="title">
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <label>Description *</label>
                            <div class="form-group">
                                <textarea class="form-control edit-description" id="edit_description_editor" rows="6" name="description"></textarea>
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <label>Gambar (Upload gambar, untuk mengganti gambar sebelum nya)</label>
                            <div class="form-group">
                                <input type="file" placeholder="Gambar" class="form-control edit-img" name="img">
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <label>Expired Hadiah *</label>
                            <div class="form-group">
                                <input style="background-color: white !important;" type='text' class="form-control pickadate-selectors-event edit-expired-hadiah" placeholder="Expired Hadiah" name="expired_date"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <a class="btn btn-outline-primary btn edit-btn" href="#">Ubah</a>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Delete Hadiah Modal -->
<div class="modal fade text-xs-left" id="delete-hadiah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Apakah anda ingin menghapus data ini ?</label>
            </div>

            <form>
                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tidak">
                    <input type="submit" class="btn btn-outline-primary btn delete-btn" value="Ya">
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')
<script charset="utf-8" src=" {{ URL::asset('/ckeditor/ckeditor.js') }} "></script>
<script type="text/javascript">
    CKEDITOR.replace( 'description_editor', {
        extraPlugins: 'colorbutton,colordialog'
    } );

    CKEDITOR.replace( 'edit_description_editor', {
        extraPlugins: 'colorbutton,colordialog'
    } );
</script>

<script type="text/javascript">
    let table, hadiah_uid = '';

	$(document).ready(function() {
        $('.save-btn').on('click', addHadiah);
        $('.edit-btn').on('click', updateHadiah);
		$('.delete-btn').on('click', destroyHadiah);

	    table = $('.server-side-hadiah').DataTable({
	    	"scrollX": !0,
            "scrollY": "370px",
	    	"lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
	        "processing": true,
	        "serverSide": true,
            "rowReorder": {
                update: false
            },
	        "ajax":{
	        	"type": "POST",
            	"url": "{{ url($url_admin.'/hadiah-ajax') }}",
            	"dataType": "json",
           	},
	        "columns": [
	            { "data": "no" },
	            { "data": "name" },
	            { "data": "title" },
	            { "data": "description" },
	            { "data": "img" },
                { "data": "expired_date" },
	            { "data": "action_btn" }
	        ],
	        order: [[0, 'asc']],
            "columnDefs": [
                { "orderable": false, "targets": [ 0, 4, 6 ] },
                { "width": "180px", "targets": [ 6 ] },
                // { "width": "250px", "targets": [ 2 ] }
            ]
	    });
        
	    function addHadiah() {
            let data = new FormData($("#upload_hadiah")[0]);
            data.append('description', CKEDITOR.instances['description_editor'].getData());

	    	$('.save-btn').prop('disabled', true);
	    	toastr.info("Harap menunggu, data sedang di proses", "Loading...");

	    	$.ajax({
                type: "POST",
                processData: false,
                contentType: false,
                url: '{{ $base_url }}'+'add-hadiah',
                dataType: "json",
                data: data,
                cache : false,
                success: function(data){
                	toastr.clear();
                	
                    if(data.code == 400) {
                    	if(Array.isArray(data.message)) {
                    		toastr.warning(data.message[0], "Peringatan");
                    	} else {
                    		toastr.warning(data.message, "Peringatan");
                    	}
                    } else if(data.code == 200) {
                    	toastr.success(data.message, "Sukses");

                    	$('#add-hadiah').modal('hide');
                    	
						table.ajax.reload(null, false);
                    }

                    $('.save-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                	console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.save-btn').prop('disabled', false);
                },

            });
	    }

        function updateHadiah() {
            let data = new FormData($("#edit_upload_hadiah")[0]);
            data.append('description', CKEDITOR.instances['edit_description_editor'].getData());
            data.append('uid', hadiah_uid);

            $('.update-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                processData: false,
                contentType: false,
                url: '{{ $base_url }}'+'edit-hadiah',
                dataType: "json",
                data: data,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");
                        $('.edit-img').val('');
                        $('#edit-hadiah').modal('hide');
                        
                        table.ajax.reload(null, false);
                    }

                    $('.update-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.update-btn').prop('disabled', false);
                },

            });
        }

        function destroyHadiah() {
            let args = {};
            args.uid = hadiah_uid;
            
            $('.delete-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                url: '{{ $base_url }}'+'delete-hadiah',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");

                        $('#delete-hadiah').modal('hide');
                        
                        table.ajax.reload(null, false);
                    }

                    $('.delete-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.delete-btn').prop('disabled', false);
                },

            });
        }
	});

    function editHadiah(obj) {
        $('.edit-merchant').empty();

        let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

        let expired_date_position = obj.expired_date.indexOf(' ');
        let formatted_expired_date = obj.expired_date.substr(0, expired_date_position);
        let date = new Date(formatted_expired_date);
        let expired_date = ("0" + date.getDate()).slice(-2)+" "+months[date.getMonth()]+" "+date.getFullYear();

        hadiah_uid = obj.uid;
        
        $('.edit-merchant').append('<option value="'+obj.merchant_uid+'">'+decodeURIComponent(obj.name.replace(/\+/g, ' '))+'</option>');
        $('.edit-title').val(decodeURIComponent(obj.title.replace(/\+/g, ' ')));
        CKEDITOR.instances['edit_description_editor'].setData(decodeURIComponent(obj.description.replace(/\+/g, ' ')));
        
        $('.edit-expired-hadiah').val(expired_date);
    }

    function deleteHadiah(obj) {
        hadiah_uid = obj.uid;
    }

    let merchant = $(".select2-merchant");
    let edit_merchant = $(".edit-select2-merchant");
    let search_merchant_url = "{{ $search_merchant_url }}";

    function formatMerchantResult(result) {
        if (result.loading) return result.name;

        return decodeURIComponent(result.name.replace(/\+/g, ' '));
    }

    function formatMerchantSelection(result) {
        if(result.name !== undefined || result.name != null) {
            result.name = decodeURIComponent(result.name.replace(/\+/g, ' '));
            result.text = decodeURIComponent(result.text.replace(/\+/g, ' '));
        }

        return result.name || result.text;
    }

    merchant.select2({
        // tags: true,
        dropdownParent: $("#add-hadiah"),
        placeholder: "Cari merchant...",
        ajax: {
            url: search_merchant_url,
            dataType: "json",
            delay: 250,
            data: function(params) {
                return {
                    q: params.term,
                    page: params.page
                }
            },
            processResults: function(data, params) {
                return params.page = params.page || 1, {
                    results: data.result.data.map(function(item) {
                        return {
                            id : item.uid,
                            name : item.name
                        };
                    }),
                    pagination: {
                        more: 30 * params.page < data.result.total
                    }
                }
            },
            cache: !0
        },
        // escapeMarkup: function(markup) {
        //     return markup
        // },
        minimumInputLength: 1,
        templateResult: formatMerchantResult,
        templateSelection: formatMerchantSelection
    });

    edit_merchant.select2({
        // tags: true,
        width: 'style',
        dropdownParent: $("#edit-hadiah"),
        placeholder: "Cari merchant...",
        ajax: {
            url: search_merchant_url,
            dataType: "json",
            delay: 250,
            data: function(params) {
                return {
                    q: params.term,
                    page: params.page
                }
            },
            processResults: function(data, params) {
                return params.page = params.page || 1, {
                    results: data.result.data.map(function(item) {
                        return {
                            id : item.uid,
                            name : item.name
                        };
                    }),
                    pagination: {
                        more: 30 * params.page < data.result.total
                    }
                }
            },
            cache: !0
        },
        // escapeMarkup: function(markup) {
        //     return markup
        // },
        minimumInputLength: 1,
        templateResult: formatMerchantResult,
        templateSelection: formatMerchantSelection
    });
</script>

@endsection