@extends('admin/header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title">Data {{ $page_title }}</h4>
				                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
			        			<div class="heading-elements">
				                    <ul class="list-inline mb-0">
				                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
				                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				                    </ul>
				                </div>
				            </div>
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
                                    @if($status == 'pending')
                                        <a href="#" class="btn btn-success mr-1 mb-1" data-toggle="modal" data-target="#add-partner">Tambah Data Partner Baru</a>
                                    @endif
									
									<br><br>
									<table width="1280px" class="table table-striped table-bordered dataex-html5-export server-side-partner">
										<thead>
											<tr>
												<th>No.</th>
												<th>Nama Lengkap</th>
												<th>Nama Pada Kartu</th>
												<th>E-mail</th>
												<th>Phone</th>
                                                <th>Address</th>
												<th>City</th>
												<th></th>
											</tr>
										</thead>
									</table>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

<!-- Add Partner Modal -->
<div class="modal fade text-xs-left" id="add-partner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Partner</label>
            </div>

            <form action="#">
                <div class="modal-body">
                    <label>Nama Lengkap *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Lengkap" class="form-control fullname">
                    </div>

                    <label>Nama Pada Kartu</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Pada Kartu" class="form-control name_on_card">
                    </div>

                    <label>E-mail *</label>
                    <div class="form-group">
                        <input type="text" placeholder="E-mail" class="form-control email">
                    </div>

                    <label>Telp / IG / Web</label>
                    <div class="form-group">
                        <input type="text" placeholder="Telp / IG / Web" class="form-control phone">
                    </div>

                    <label>Address</label>
                    <div class="form-group">
                        <input type="text" placeholder="Address" class="form-control address">
                    </div>

                    <label>City</label>
                    <div class="form-group">
                        <input type="text" placeholder="City" class="form-control city">
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn save-btn" value="Simpan">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit Partner Modal -->
<div class="modal fade text-xs-left" id="edit-partner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Partner</label>
            </div>

            <form action="#">
                <div class="modal-body">
                    <label>Nama Lengkap *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Lengkap" class="form-control edit_fullname">
                    </div>

                    <label>Nama Pada Kartu</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Pada Kartu" class="form-control edit_name_on_card">
                    </div>

                    <label>E-mail *</label>
                    <div class="form-group">
                        <input type="text" placeholder="E-mail" class="form-control edit_email">
                    </div>

                    <label>Password (Kosongi jika tidak ingin mengubah password)</label>
                    <div class="form-group">
                        <input type="password" placeholder="Password" class="form-control edit_password">
                    </div>

                    <label>Telp / IG / Web</label>
                    <div class="form-group">
                        <input type="text" placeholder="Telp / IG / Web" class="form-control edit_phone">
                    </div>

                    <label>Address</label>
                    <div class="form-group">
                        <input type="text" placeholder="Address" class="form-control edit_address">
                    </div>

                    <label>City</label>
                    <div class="form-group">
                        <input type="text" placeholder="City" class="form-control edit_city">
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn update-btn" value="Ubah">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Delete Partner Modal -->
<div class="modal fade text-xs-left" id="delete-partner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Apakah anda ingin menghapus data ini ?</label>
            </div>

            <form>
                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tidak">
                    <input type="submit" class="btn btn-outline-primary btn delete-btn" value="Ya">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Approve Partner Modal -->
<div class="modal fade text-xs-left" id="approve-partner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Apakah anda ingin approve user ini ?</label>
            </div>

            <form>
                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tidak">
                    <input type="submit" class="btn btn-outline-primary btn approve-btn" value="Ya">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Reject Partner Modal -->
<div class="modal fade text-xs-left" id="reject-partner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Apakah anda reject user ini ?</label>
            </div>

            <form>
                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tidak">
                    <input type="submit" class="btn btn-outline-primary btn reject-btn" value="Ya">
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">
    let table, user_id = '';

	$(document).ready(function() {
        $('.save-btn').on('click', addPartner);
        $('.update-btn').on('click', updatePartner);
        $('.delete-btn').on('click', destroyPartner);
        $('.approve-btn').on('click', approvePartner);
		$('.reject-btn').on('click', rejectPartner);

	    table = $('.server-side-partner').DataTable({
	    	"scrollX": !0,
	    	"lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
	        "processing": true,
	        "serverSide": true,
	        "ajax":{
	        	"type": "POST",
            	"url": "{{ url($url_admin.'/partner-ajax/'.$status) }}",
            	"dataType": "json",
           	},
	        "columns": [
	            { "data": "no" },
	            { "data": "fullname" },
	            { "data": "res.name_on_card" },
	            { "data": "res.email" },
	            { "data": "res.phone" },
                { "data": "res.address" },
	            { "data": "res.city" },
	            { "data": "action_btn" }
	        ],
	        order: [[1, 'asc']],
            "columnDefs": [
                { "orderable": false, "targets": [ 0, 7 ] },
                { "width": "190px", "targets": [ 7 ] },
                { "width": "120px", "targets": [ 5 ] },
            ]
	    });

	    function addPartner() {
	    	let args = {};
	    	args.fullname = $('.fullname').val();
	    	args.name_on_card = $('.name_on_card').val();
	    	args.email = $('.email').val();
	    	args.phone = $('.phone').val();
	    	args.address = $('.address').val();
            args.city = $('.city').val();
	    	args.role = 'partner';

	    	$('.save-btn').prop('disabled', true);
	    	toastr.info("Harap menunggu, data sedang di proses", "Loading...");

	    	$.ajax({
                type: "POST",
                url: '{{ $base_url }}'+'add-partner',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                	toastr.clear();
                	
                    if(data.code == 400) {
                    	if(Array.isArray(data.message)) {
                    		toastr.warning(data.message[0], "Peringatan");
                    	} else {
                    		toastr.warning(data.message, "Peringatan");
                    	}
                    } else if(data.code == 200) {
                    	toastr.success(data.message, "Sukses");

                    	$('#add-partner').modal('hide');
                    	
                    	$('.fullname').val("");
						$('.name_on_card').val("");
						$('.email').val("");
						$('.phone').val("");
                        $('.address').val("");
						$('.city').val("");

						table.ajax.reload(null, false);
                    }

                    $('.save-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                	console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.save-btn').prop('disabled', false);
                },

            });
	    }

        function updatePartner() {
            let args = {};
            args.id = user_id;
            args.fullname = $('.edit_fullname').val();
            args.name_on_card = $('.edit_name_on_card').val();
            args.email = $('.edit_email').val();
            args.password = $('.edit_password').val();
            args.phone = $('.edit_phone').val();
            args.address = $('.edit_address').val();
            args.city = $('.edit_city').val();

            $('.update-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                url: '{{ $base_url }}'+'edit-partner',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");

                        $('#edit-partner').modal('hide');
                        
                        $('.edit_password').val("");

                        table.ajax.reload(null, false);
                    }

                    $('.update-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.update-btn').prop('disabled', false);
                },

            });
        }

        function destroyPartner() {
            let args = {};
            args.id = user_id;
            
            $('.delete-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                url: '{{ $base_url }}'+'delete-partner',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");

                        $('#delete-partner').modal('hide');
                        
                        table.ajax.reload(null, false);
                    }

                    $('.delete-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.delete-btn').prop('disabled', false);
                },

            });
        }

        function approvePartner() {
            let args = {};
            args.id = user_id;
            
            $('.approve-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                url: '{{ $base_url }}'+'approve-partner',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");

                        $('#approve-partner').modal('hide');
                        
                        table.ajax.reload(null, false);
                    }

                    $('.approve-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.approve-btn').prop('disabled', false);
                },

            });
        }

        function rejectPartner() {
            let args = {};
            args.id = user_id;
            
            $('.reject-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                url: '{{ $base_url }}'+'reject-partner',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");

                        $('#reject-partner').modal('hide');
                        
                        table.ajax.reload(null, false);
                    }

                    $('.reject-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.reject-btn').prop('disabled', false);
                },

            });
        }
	});

    function editPartner(obj) {
        user_id = obj.id;
        $('.edit_fullname').val(decodeURIComponent(obj.fullname.replace(/\+/g, ' ')));
        $('.edit_name_on_card').val(obj.name_on_card);
        $('.edit_email').val(obj.email);
        $('.edit_phone').val(obj.phone);
        $('.edit_address').val(obj.address);
        $('.edit_city').val(obj.city);
    }

    function deletePartner(obj) {
        user_id = obj.id;
    }

    function approvePartner(id) {
        user_id = id;
    }

    function rejectPartner(id) {
        user_id = id;
    }
</script>

@endsection