@extends('admin/header')

@section('content')
<style type="text/css">
    .picker {
        top: -400% !important;
    }    
</style>

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title">Data {{ $page_title }}</h4>
				                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
			        			<div class="heading-elements">
				                    <ul class="list-inline mb-0">
				                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
				                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				                    </ul>
				                </div>
				            </div>
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
									<a href="#" class="btn btn-success mr-1 mb-1" data-toggle="modal" data-target="#add-promo-internal">Tambah Data {{ $page_title }} Baru</a>
									<br><br>
									<table width="1880px" class="table table-striped table-bordered dataex-html5-export server-side-promo-internal">
										<thead>
											<tr>
												<th>No.</th>
                                                <th>Partner</th>
                                                <th>Title</th>
                                                <th>Description</th>
												<th>Gambar</th>
                                                <th>Harga Awal</th>
                                                <th>Harga Akhir</th>
                                                <th>Expired Promo</th>
                                                <th>Top Promo</th>
												<th></th>
											</tr>
										</thead>
									</table>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

<!-- Add Promo Internal Modal -->
<div class="modal fade text-xs-left" id="add-promo-internal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Promo</label>
            </div>

            <form action="#" enctype="multipart/form-data" id="upload_promo">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Top Promo</label>
                            <div class="form-group">
                                <input type="checkbox" class="top_promo" name="top_promo">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label>Partner *</label>
                            <div class="form-group">
                                <select class="select2-partner form-control partner" name="partner" style="width: 100%">
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-md-8">
                            <label>Title *</label>
                            <div class="form-group">
                                <input type="text" placeholder="Title" class="form-control title" name="title">
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <label>Description *</label>
                            <div class="form-group">
                                <textarea class="form-control" id="description_editor" rows="6" name="description"></textarea>
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <label>Gambar *</label>
                            <div class="form-group">
                                <input type="file" placeholder="Gambar" class="form-control img" name="img">
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <label>Harga Awal *</label>
                            <div class="form-group">
                                <input type="text" placeholder="Harga Awal" class="form-control harga_awal" name="harga_awal">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <label>Harga Akhir *</label>
                            <div class="form-group">
                                <input type="text" placeholder="Harga Akhir" class="form-control harga_akhir" name="harga_akhir">
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <label>Expired Promo *</label>
                            <div class="form-group">
                                <input style="background-color: white !important;" type='text' class="form-control pickadate-selectors-event" placeholder="Expired Promo" name="expired_date"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn save-btn" value="Simpan">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit Promo Internal Modal -->
<div class="modal fade text-xs-left" id="edit-promo-internal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Promo</label>
            </div>

            <form enctype="multipart/form-data" id="edit_upload_promo">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-2">
                            <label>Top Promo</label>
                            <div class="form-group">
                                <input type="checkbox" class="edit-top-promo-internal" name="top_promo">
                            </div>
                        </div>

                        <div class="col-md-10">
                            <label>Menggunakan Pin ?</label>
                            <div class="form-group">
                                <input type="checkbox" class="edit-use-pin" name="use_pin">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label>Partner *</label>
                            <div class="form-group">
                                <select class="edit-select2-partner form-control edit-partner" name="partner" style="width: 100%">
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-md-8">
                            <label>Title *</label>
                            <div class="form-group">
                                <input type="text" placeholder="Title" class="form-control edit-title" name="title">
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <label>Description *</label>
                            <div class="form-group">
                                <textarea class="form-control edit-description" id="edit_description_editor" rows="6" name="description"></textarea>
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <label>Gambar (Upload gambar, untuk mengganti gambar sebelum nya)</label>
                            <div class="form-group">
                                <input type="file" placeholder="Gambar" class="form-control edit-img" name="img">
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <label>Harga Awal *</label>
                            <div class="form-group">
                                <input type="text" placeholder="Harga Awal" class="form-control edit-harga-awal" name="harga_awal">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <label>Harga Akhir *</label>
                            <div class="form-group">
                                <input type="text" placeholder="Harga Akhir" class="form-control edit-harga-akhir" name="harga_akhir">
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <label>Expired Promo *</label>
                            <div class="form-group">
                                <input style="background-color: white !important;" type='text' class="form-control pickadate-selectors-event edit-expired-promo-internal" placeholder="Expired Promo" name="expired_date"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <a class="btn btn-outline-primary btn edit-btn" href="#">Ubah</a>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Delete Promo Internal Modal -->
<div class="modal fade text-xs-left" id="delete-promo-internal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Apakah anda ingin menghapus data ini ?</label>
            </div>

            <form>
                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tidak">
                    <input type="submit" class="btn btn-outline-primary btn delete-btn" value="Ya">
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')
<script charset="utf-8" src=" {{ URL::asset('/ckeditor/ckeditor.js') }} "></script>
<script type="text/javascript">
    CKEDITOR.replace( 'description_editor', {
        extraPlugins: 'colorbutton,colordialog'
    } );

    CKEDITOR.replace( 'edit_description_editor', {
        extraPlugins: 'colorbutton,colordialog'
    } );
</script>

<script type="text/javascript">
    let table, promo_id = '';

	$(document).ready(function() {
        $('.save-btn').on('click', addPromoInternal);
        $('.edit-btn').on('click', updatePromoInternal);
		$('.delete-btn').on('click', destroyPromoInternal);

	    table = $('.server-side-promo-internal').DataTable({
	    	"scrollX": !0,
            "scrollY": "370px",
	    	"lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
	        "processing": true,
	        "serverSide": true,
            "rowReorder": {
                update: false
            },
	        "ajax":{
	        	"type": "POST",
            	"url": "{{ url($url_admin.'/promo-internal-ajax') }}",
            	"dataType": "json",
           	},
	        "columns": [
	            { "data": "no" },
	            { "data": "fullname" },
	            { "data": "title" },
	            { "data": "description" },
	            { "data": "img" },
                { "data": "res.harga_awal" },
                { "data": "res.harga_akhir" },
                { "data": "expired_date" },
                { "data": "res.top_promo" },
	            { "data": "action_btn" }
	        ],
	        order: [[0, 'asc']],
            "columnDefs": [
                { "orderable": false, "targets": [ 0, 4, 9 ] },
                { "width": "180px", "targets": [ 9 ] },
                { "width": "250px", "targets": [ 2 ] }
            ]
	    });

        // table.on( 'row-reorder', function ( e, diff, edit ) {
        //     let start = table.page.info().start;
        //     let args = {};
        //     args.promo_uid = [];
        //     args.new_position = [];
        //     args.title = [];

        //     // let result = 'Reorder started on row: '+edit.triggerRow.data().title+'<br>';
            
        //     for (let i = 0, ien = diff.length; i < ien; i++) {
        //         let rowData = table.row( diff[i].node ).data();
                
        //         args.promo_uid.push(rowData.res.uid);
        //         args.new_position.push(Number(diff[i].newPosition+1+start));
        //         args.title.push(rowData.title);

        //         // result += rowData.title+' updated to be in position '+Number(diff[i].newPosition+1)+' (was '+Number(diff[i].oldPosition+1)+')<br>';
        //     }
            
        //     $.ajax({
        //         type: "POST",
        //         url: '{{ $base_url }}'+'update-sequence-promo-internal',
        //         dataType: "json",
        //         data: args,
        //         cache : false,
        //         success: function(data){
        //             toastr.clear();
                    
        //             if(data.code == 400) {
        //                 if(Array.isArray(data.message)) {
        //                     toastr.warning(data.message[0], "Peringatan");
        //                 } else {
        //                     toastr.warning(data.message, "Peringatan");
        //                 }
        //             } else if(data.code == 200) {
        //                 toastr.success(data.message, "Sukses");
                        
        //                 table.ajax.reload(null, false);
        //             }
        //         } ,error: function(xhr, status, error) {
        //             console.log(error);
        //             toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
        //         },

        //     });     
        // } );
        
	    function addPromoInternal() {
            let data = new FormData($("#upload_promo")[0]);
            data.append('description', CKEDITOR.instances['description_editor'].getData());

	    	$('.save-btn').prop('disabled', true);
	    	toastr.info("Harap menunggu, data sedang di proses", "Loading...");

	    	$.ajax({
                type: "POST",
                processData: false,
                contentType: false,
                url: '{{ $base_url }}'+'add-promo-internal',
                dataType: "json",
                data: data,
                cache : false,
                success: function(data){
                	toastr.clear();
                	
                    if(data.code == 400) {
                    	if(Array.isArray(data.message)) {
                    		toastr.warning(data.message[0], "Peringatan");
                    	} else {
                    		toastr.warning(data.message, "Peringatan");
                    	}
                    } else if(data.code == 200) {
                    	toastr.success(data.message, "Sukses");

                    	$('#add-promo-internal').modal('hide');
                    	
						table.ajax.reload(null, false);
                    }

                    $('.save-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                	console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.save-btn').prop('disabled', false);
                },

            });
	    }

        function updatePromoInternal() {
            let data = new FormData($("#edit_upload_promo")[0]);
            data.append('description', CKEDITOR.instances['edit_description_editor'].getData());
            data.append('id', promo_id);

            $('.update-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                processData: false,
                contentType: false,
                url: '{{ $base_url }}'+'edit-promo-internal',
                dataType: "json",
                data: data,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");
                        $('.edit-img').val('');
                        $('#edit-promo-internal').modal('hide');
                        
                        table.ajax.reload(null, false);
                    }

                    $('.update-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.update-btn').prop('disabled', false);
                },

            });
        }

        function destroyPromoInternal() {
            let args = {};
            args.id = promo_id;
            
            $('.delete-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                url: '{{ $base_url }}'+'delete-promo-internal',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");

                        $('#delete-promo-internal').modal('hide');
                        
                        table.ajax.reload(null, false);
                    }

                    $('.delete-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.delete-btn').prop('disabled', false);
                },

            });
        }
	});

    function editPromoInternal(obj) {
        console.log(obj);
        $('.edit-partner').empty();

        let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        let date = new Date(obj.expired_date);
        let formatDate = ("0" + date.getDate()).slice(-2)+" "+months[date.getMonth()]+" "+date.getFullYear();

        promo_id = obj.id;

        if(obj.top_promo == 'yes') {
            $('.edit-top-promo-internal').prop('checked', true);
        } else {
            $('.edit-top-promo-internal').prop('checked', false);
        }

        if(obj.use_pin == 'yes') {
            $('.edit-use-pin').prop('checked', true);
        } else {
            $('.edit-use-pin').prop('checked', false);
        }
        
        $('.edit-partner').append('<option value="'+obj.user_uid+'">'+decodeURIComponent(obj.fullname.replace(/\+/g, ' '))+'</option>');
        $('.edit-title').val(decodeURIComponent(obj.title.replace(/\+/g, ' ')));
        CKEDITOR.instances['edit_description_editor'].setData(decodeURIComponent(obj.description.replace(/\+/g, ' ')));
        $('.edit-harga-awal').val(obj.harga_awal);
        $('.edit-harga-akhir').val(obj.harga_akhir);
        $('.edit-expired-promo-internal').val(formatDate);
    }

    function deletePromoInternal(id) {
        promo_id = id;
    }

    function formatPartnerResult(result) {
        if (result.loading) return result.fullname;

        return ("<option selected>"+decodeURIComponent(result.fullname.replace(/\+/g, ' '))+"</option>");
    }

    function formatPartnerSelection(result) {
        if(result.fullname !== undefined || result.fullname != null) {
            result.fullname = decodeURIComponent(result.fullname.replace(/\+/g, ' '));
            result.text = decodeURIComponent(result.text.replace(/\+/g, ' '));
        }

        return result.fullname || result.text;
    }

    let partner = $(".select2-partner");
    let edit_partner = $(".edit-select2-partner");
    let search_partner_url = "{{ $search_partner_url }}";

    partner.select2({
        // tags: true,
        dropdownParent: $("#add-promo-internal"),
        placeholder: "Cari partner...",
        ajax: {
        url: search_partner_url,
            dataType: "json",
            delay: 250,
            data: function(params) {
                return {
                    q: params.term,
                    page: params.page
                }
            },
            processResults: function(data, params) {
                return params.page = params.page || 1, {
                    results: data.result.data.map(function(item) {
                        return {
                            id : item.uid,
                            fullname : item.fullname
                        };
                    }),
                    pagination: {
                        more: 30 * params.page < data.result.total
                    }
                }
            },
            cache: !0
        },
        escapeMarkup: function(markup) {
            return markup
        },
        minimumInputLength: 1,
        templateResult: formatPartnerResult,
        templateSelection: formatPartnerSelection
    });

    edit_partner.select2({
        // tags: true,
        dropdownParent: $("#edit-promo-internal"),
        placeholder: "Cari partner...",
        ajax: {
        url: search_partner_url,
            dataType: "json",
            delay: 250,
            data: function(params) {
                return {
                    q: params.term,
                    page: params.page
                }
            },
            processResults: function(data, params) {
                return params.page = params.page || 1, {
                    results: data.result.data.map(function(item) {
                        return {
                            id : item.uid,
                            fullname : item.fullname
                        };
                    }),
                    pagination: {
                        more: 30 * params.page < data.result.total
                    }
                }
            },
            cache: !0
        },
        escapeMarkup: function(markup) {
            return markup
        },
        minimumInputLength: 1,
        templateResult: formatPartnerResult,
        templateSelection: formatPartnerSelection
    });
</script>

@endsection