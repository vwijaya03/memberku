<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="AAI">
    <meta name="keywords" content="AAI">
    <meta name="author" content="AAI">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>AAI</title>
    <link rel="apple-touch-icon" href="{{ URL::asset('img/icon_kd.jpeg') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('img/icon_kd.jpeg') }}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/fonts/feather/style.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/fonts/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/fonts/flag-icon-css/css/flag-icon.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/extensions/pace.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/ui/prism.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/extensions/sweetalert.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/tables/datatable/select.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/tables/extensions/rowReorder.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/forms/selects/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/pickers/daterange/daterangepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/pickers/datetime/bootstrap-datetimepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/pickers/pickadate/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/extensions/toastr.css') }}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN TERA CSS-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/app.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/colors.min.css') }}">
    <!-- END TERA CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/core/menu/menu-types/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/core/colors/palette-gradient.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/plugins/extensions/toastr.min.css') }}">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/assets/css/style.css') }}">
    <!-- END Custom CSS-->
  </head>
  <body data-open="hover" data-menu="horizontal-menu" data-col="2-columns" class="horizontal-layout horizontal-menu 2-columns menu-expanded">

    <!-- navbar-fixed-top-->
    <nav class="header-navbar navbar navbar-with-menu navbar-static-top navbar-dark bg-gradient-x-grey-blue navbar-border navbar-brand-center">
      <div class="navbar-wrapper">
        <div class="navbar-header">
          <ul class="nav navbar-nav">

            <!-- <li class="nav-item mobile-menu hidden-md-up float-xs-left hamburger"><a href="#" class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="ft-menu font-large-1"></i></a></li> -->
            <li class="nav-item mobile-menu hidden-md-up float-xs-left hamburger"><a href="#" class="nav-link nav-menu-main hidden-xs"><i class="ft-menu font-large-1"></i></a></li>

            <li class="nav-item"><a href="{{ url($url_admin.'/dashboard') }}" class="navbar-brand"><img alt="stack admin logo" src="{{ URL::asset('admin/app-assets/images/logo/stack-logo-light.png') }}" class="brand-logo">
                <h2 class="brand-text">AAI</h2></a></li>

            <!-- <li class="nav-item hidden-md-up float-xs-right"><a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="fa fa-ellipsis-v"></i></a>
            </li> -->

            <li class="dropdown dropdown-user nav-item hidden-md-up float-xs-right">
                <a data-toggle="dropdown" class="nav-link open-navbar-container dropdown-user-link"><i class="fa fa-ellipsis-v"></i></a>

                <div class="dropdown-menu dropdown-menu-right">
                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#setting-modal"><i class="ft-settings"></i>Setting</a>
                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#change-profile"><i class="ft-user"></i>My Profile</a>
                    <a href="{{ url($url_partner.'/change-password') }}" class="dropdown-item"><i class="ft-lock"></i> Ganti Password</a>
                    <a href="{{ url($url_partner.'/logout') }}" class="dropdown-item"><i class="ft-power"></i> Logout</a>
                </div>
            </li>

          </ul>
        </div>
        <div class="navbar-container content container-fluid">
          <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
            <ul class="nav navbar-nav">
                <!-- <li class="nav-item hidden-sm-down"><a href="#" class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="ft-menu"></i></a></li> -->
                
                <!-- <li class="nav-item hidden-sm-down"><a href="#" class="nav-link nav-link-expand"><i class="ficon ft-maximize"></i></a></li> -->
            </ul>
            <ul class="nav navbar-nav float-xs-right">
            
              <li class="dropdown dropdown-user nav-item"><a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link"><span class="avatar avatar-online"><img src="{{ URL::asset('admin/app-assets/images/user-icon.png') }}" alt="avatar"><i></i></span><span class="user-name">{{ urldecode($current_user->fullname) }}</span></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#setting-modal"><i class="ft-settings"></i>Setting</a>
                    <a href="{{ url($url_admin.'/change-profile') }}" class="dropdown-item" data-toggle="modal" data-target="#change-profile"><i class="ft-user"></i>My Profile</a>
                    <a href="{{ url($url_admin.'/change-password') }}" class="dropdown-item"><i class="ft-lock"></i> Ganti Password</a>
                    <a href="{{ url($url_admin.'/logout') }}" class="dropdown-item"><i class="ft-power"></i> Logout</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>

    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <!-- Horizontal navigation-->
    <div role="navigation" data-menu="menu-wrapper" class="header-navbar navbar navbar-horizontal navbar-fixed navbar-light navbar-without-dd-arrow navbar-shadow menu-border">
        <!-- Horizontal menu content-->
        <div data-menu="menu-container" class="navbar-container main-menu-content">
            <!-- include includes/mixins-->
            <ul id="main-menu-navigation" data-menu="menu-navigation" class="nav navbar-nav">
                
                <li data-menu="dropdown" class="dropdown nav-item"><a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link"><i class="ft-book"></i><span>Master Data</span></a>
                    <ul class="dropdown-menu">
                        
                        <li data-menu="">
                            <a href="{{ url($url_admin.'/bank') }}" data-toggle="dropdown" class="dropdown-item">Data Bank</a>
                        </li>

                        <li data-menu="">
                            <a href="{{ url($url_admin.'/hadiah') }}" data-toggle="dropdown" class="dropdown-item">Data Hadiah</a>
                        </li>

                        <li data-menu="">
                            <a href="{{ url($url_admin.'/merchant') }}" data-toggle="dropdown" class="dropdown-item">Data Merchant</a>
                        </li>

                        <li data-menu="">
                            <a href="{{ url($url_admin.'/company') }}" data-toggle="dropdown" class="dropdown-item">Data Perusahaan</a>
                        </li>
                        
                        {{-- <li data-menu="">
                            <a href="{{ url($url_admin.'/promo') }}" data-toggle="dropdown" class="dropdown-item">Data Promo Global</a>
                        </li>

                        <li data-menu="">
                            <a href="{{ url($url_admin.'/promo-internal') }}" data-toggle="dropdown" class="dropdown-item">Data Promo Internal</a>
                        </li> --}}

                    </ul>
                </li>

                <!-- <li class="nav-item"><a href="{{ url($url_admin.'/booking') }}" class="nav-link"><i class="ft-clipboard"></i><span>Data Booking</span></a>
                </li>

                <li class="nav-item"><a href="{{ url($url_admin.'/category-partner') }}" class="nav-link"><i class="ft-list"></i><span>Data Kategori Partner</span></a>
                </li>

                <li class="nav-item"><a href="{{ url($url_admin.'/unique-code') }}" class="nav-link"><i class="ft-list"></i><span>Data Histori Kode Unik Partner</span></a>
                </li> -->
                
                <li data-menu="dropdown" class="dropdown nav-item"><a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link"><i class="ft-user"></i><span>Master Data User</span>&nbsp;<i class="ft-chevron-down"></i></a>
                    <ul class="dropdown-menu">
                        <li class="nav-item">
                            <a href="{{ url($url_admin.'/agent') }}" class="dropdown-item">Data Agent</a>
                        </li>

                        {{-- <li data-menu="">
                            <a href="{{ url($url_admin.'/partner/pending') }}" data-toggle="dropdown" class="dropdown-item">Data Pending Partner</a>
                        </li>

                        <li data-menu="">
                            <a href="{{ url($url_admin.'/partner/approved') }}" data-toggle="dropdown" class="dropdown-item">Data Approved Partner</a>
                        </li>

                        <li data-menu="">
                            <a href="{{ url($url_admin.'/partner/rejected') }}" data-toggle="dropdown" class="dropdown-item">Data Rejected Partner</a>
                        </li> --}}

                        <li class="nav-item">
                            <a href="{{ url($url_admin.'/user') }}" class="dropdown-item">Data Superadmin</a>
                        </li>
                    </ul>
                </li>

                <li data-menu="dropdown" class="dropdown nav-item"><a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link"><i class="ft-settings"></i><span>Data Setting</span>&nbsp;<i class="ft-chevron-down"></i></a>
                    <ul class="dropdown-menu">
                        <li data-menu="">
                            <a href="{{ url($url_admin.'/setting-unique-code-partner') }}" data-toggle="dropdown" class="dropdown-item">Data Setting Kirim Voucher Partner</a>
                        </li>
                        
                        <li data-menu="">
                            <a href="{{ url($url_admin.'/pin-promo') }}" data-toggle="dropdown" class="dropdown-item">Data Setting Pin Promo Global</a>
                        </li>

                        <li data-menu="">
                            <a href="{{ url($url_admin.'/pin-promo-internal') }}" data-toggle="dropdown" class="dropdown-item">Data Setting Pin Promo Internal</a>
                        </li>

                        <li data-menu="">
                            <a href="{{ url($url_admin.'/setting-promo-partner') }}" data-toggle="dropdown" class="dropdown-item">Data Setting Promo Partner</a>
                        </li>
                        
                        <li data-menu="">
                            <a href="{{ url($url_admin.'/voucher-expired-date') }}" data-toggle="dropdown" class="dropdown-item">Data Setting Voucher Expired</a>
                        </li>
                    </ul>
                </li>

                <li data-menu="dropdown" class="dropdown nav-item"><a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link"><i class="fa fa-list"></i><span>Review Agent</span>&nbsp;<i class="ft-chevron-down"></i></a>
                    <ul class="dropdown-menu">
                        <li data-menu="">
                            <a href="{{ url($url_admin.'/review-agent-description') }}" data-toggle="dropdown" class="dropdown-item">Data Review Agent Description</a>
                        </li>

                        <li data-menu="">
                            <a href="{{ url($url_admin.'/review-agent-logo') }}" data-toggle="dropdown" class="dropdown-item">Data Review Agent Logo</a>
                        </li>
                    </ul>
                </li>

                <li data-menu="dropdown" class="dropdown nav-item"><a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link"><i class="ft-bar-chart"></i><span>Report</span></a>
                    <ul class="dropdown-menu">
                        <li data-menu="">
                            <a href="{{ url($url_admin.'/report-tukar-struk') }}" data-toggle="dropdown" class="dropdown-item">Data Report Tukar Struk</a>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
        <!-- /horizontal menu content-->
    </div>
    <!-- Horizontal navigation-->
    
    @yield('content')
    
    <!-- Change Profile Modal -->
    <div class="modal fade text-xs-left" id="change-profile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    <label class="modal-title text-text-bold-600" id="myModalLabel33">My Profile</label>
                </div>

                <form>
                    <div class="modal-body">
                        <label>Nama Lengkap: </label>
                        <div class="form-group">
                            <input type="text" placeholder="Nama Lengkap" class="form-control cp_fullname" value="{{ $current_user->fullname }}">
                        </div>

                        <label>Nama Pada Kartu: </label>
                        <div class="form-group">
                            <input type="text" placeholder="Nama Pada Kartu" class="form-control cp_name_on_card" value="{{ $current_user->name_on_card }}">
                        </div>

                        <label>E-mail: </label>
                        <div class="form-group">
                            <input type="text" placeholder="E-mail" class="form-control cp_email" value="{{ $current_user->email }}">
                        </div>

                        <label>No. HP: </label>
                        <div class="form-group">
                            <input type="text" placeholder="No. HP" class="form-control cp_phone" value="{{ $current_user->phone }}">
                        </div>

                        <label>Address: (Max 35 karakter)</label>
                        <div class="form-group">
                            <input type="text" placeholder="Address" class="form-control cp_address" value="{{ $current_user->address }}">
                        </div>

                        <label>Kota: (Max 35 karakter)</label>
                        <div class="form-group">
                            <input type="text" placeholder="Kota" class="form-control cp_city" value="{{ $current_user->city }}">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                        <input type="submit" class="btn btn-outline-primary btn change-profile-btn" value="Ubah" onclick="change_profile('{{ $base_url }}', '{{ $current_user->id }}')">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Setting Modal -->
    <div class="modal fade text-xs-left" id="setting-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    <label class="modal-title text-text-bold-600" id="myModalLabel33">Setting</label>
                </div>

                <form>
                    <div class="modal-body">
                        <label>Harga Per Voucher: </label>
                        <div class="form-group">
                            <input type="text" placeholder="Harga Per Voucher" class="form-control voucher_price" value="@if($setting != null) {{ $setting->voucher_price }} @endif">
                        </div>

                        <label>Jumlah Maximum Voucher Di Klik: </label>
                        <div class="form-group">
                            <input type="text" placeholder="Jumlah Maximum Voucher Di Klik" class="form-control voucher_click_limit" value="@if($setting != null) {{ $setting->voucher_click_limit }} @endif">
                        </div>
                        
                        <label>Tampilkan Remaining Click: </label>
                        <div class="form-group">
                            <select class="form-control visibility_remaining_click">
                                @if($setting != null)
                                    @if($setting->visibility_remaining_click == 'yes')
                                        <option selected value="yes">Ya</option>
                                        <option value="no">Tidak</option>
                                    @else
                                        <option selected value="no">Tidak</option>
                                        <option value="yes">Ya</option>
                                    @endif
                                @else
                                    <option value="no">Tidak</option>
                                    <option value="yes">Ya</option>
                                @endif
                            </select>
                        </div>

                        <label>Server Maintenace: </label>
                        <div class="form-group">
                            <select class="form-control server_maintenance">
                                @if($setting != null)
                                    @if($setting->server_maintenance == 'yes')
                                        <option selected value="yes">Ya</option>
                                        <option value="no">Tidak</option>
                                    @else
                                        <option selected value="no">Tidak</option>
                                        <option value="yes">Ya</option>
                                    @endif
                                @else
                                    <option value="no">Tidak</option>
                                    <option value="yes">Ya</option>
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                        <input type="submit" class="btn btn-outline-primary btn save-setting-btn" value="Simpan" onclick="setting('{{ $base_url }}', '{{ $current_user->id }}')">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <footer class="footer footer-static footer-light navbar-shadow">
      <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Copyright  &copy; <?php echo date('Y'); ?> PT. Arius Angkasa Indonesia, All rights reserved. </span></p>
    </footer>

    <!-- BEGIN VENDOR JS-->
    <script src="{{ URL::asset('admin/app-assets/vendors/js/vendors.min.js') }}" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script type="text/javascript" src="{{ URL::asset('admin/app-assets/vendors/js/ui/jquery.sticky.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('admin/app-assets/vendors/js/charts/jquery.sparkline.min.js') }}"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/ui/prism.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/extensions/sweetalert.min.js') }}" type="text/javascript"></script>

    <script src="{{ URL::asset('admin/app-assets/vendors/js/extensions/toastr.min.js') }}" type="text/javascript"></script>

    <script src="{{ URL::asset('admin/app-assets/vendors/js/forms/extended/inputmask/jquery.inputmask.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/forms/extended/maxlength/bootstrap-maxlength.js') }}" type="text/javascript"></script>

    <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/datatable/dataTables.rowReorder.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/jszip.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/pdfmake.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/vfs_fonts.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/buttons.html5.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/buttons.print.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/buttons.colVis.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/datatable/dataTables.colVis.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/forms/select/select2.full.min.js') }}" type="text/javascript"></script>
    
    <script src="{{ URL::asset('admin/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/pickers/dateTime/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/pickers/pickadate/picker.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/pickers/pickadate/picker.time.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/pickers/pickadate/legacy.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/pickers/daterange/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/js/scripts/pickers/dateTime/picker-date-time.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN TERA JS-->
    <script src="{{ URL::asset('admin/app-assets/js/core/app-menu.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/js/core/app.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/js/scripts/customizer.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/js/scripts/forms/extended/form-maxlength.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/js/scripts/popover/popover.min.js') }}" type="text/javascript"></script>
    <!-- END TERA JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script type="text/javascript" src="{{ URL::asset('admin/app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js') }}"></script>

    <!-- END PAGE LEVEL JS-->

    @yield('google_map_script')
    
    @yield('server_side_datatable')

    @yield('custom_js_script')

    <script type="text/javascript" src="{{ URL::asset('admin/app-assets/js/scripts/custom.js') }}"></script>

    <script type="text/javascript">
        $(".app-content").click(function(e) {
            $("body").removeClass("menu-open");
            $("body").addClass("menu-hide");
        });

        $('.hamburger').click(function(e) {
            $("body").removeClass("menu-hide");
            $("body").toggleClass("menu-open");
        });
    </script>
  </body>
</html>