@extends('admin/header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
                                <h4 class="card-title">Data {{ $page_title }}</h4>
                            
				            </div>
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
									<table width="100%" class="table table-striped table-bordered dataex-html5-export server-side-review-agent-logo">
										<thead>
											<tr>
												<th>No.</th>
                                                <th>Nama Agent</th>
												<th>No. HP</th>
												<th>Logo</th>
												<th></th>
											</tr>
										</thead>
									</table>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

<!-- Approve Agent Logo Modal -->
<div class="modal fade text-xs-left" id="approve-agent-logo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Apakah anda yakin ?</label>
            </div>

            <form>
                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tidak">
                    <input type="button" class="btn btn-outline-primary btn approve-btn" value="Ya">
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">
    let table, user_uid = '';

	$(document).ready(function() {
        $('.approve-btn').on('click', updateAgentLogo);

	    table = $('.server-side-review-agent-logo').DataTable({
	    	"scrollX": !0,
	    	"lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
	        "processing": true,
	        "serverSide": true,
	        "ajax":{
	        	"type": "POST",
            	"url": "{{ url($url_admin.'/review-agent-logo-ajax') }}",
            	"dataType": "json",
           	},
	        "columns": [
	            { "data": "no" },
                { "data": "fullname" },
                { "data": "res.phone" },
	            { "data": "img" },
	            { "data": "action_btn" }
	        ],
	        order: [[1, 'asc']],
            "columnDefs": [
                { "orderable": false, "targets": [ 0, 3, 4] },
                // { "width": "190px", "targets": [ 9 ] },
                { "width": "120px", "targets": [ 0, 1, 2, 3, 4 ] },
            ]
	    });

        function updateAgentLogo() {
            let args = {};
            args.uid = user_uid;
            
            $('.approve-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                url: '{{ $base_url }}'+'approve-agent-logo',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");

                        $('#approve-agent-logo').modal('hide');
                        
                        table.ajax.reload(null, false);
                    }

                    $('.approve-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.approve-btn').prop('disabled', false);
                },

            });
        }
	});

    function approveAgentLogo(obj) {
        user_uid = obj.uid;
    }
</script>

@endsection