@extends('admin/header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-2">
                <h3 class="content-header-title mb-0">Data {{ $page_title }}</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url($url_admin.'/dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{ url($url_admin.'/promo') }}">{{ $page_title }}</a>
                            </li>
                            <li class="breadcrumb-item active">Edit {{ $page_title }}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
        	<section id="basic-form-layouts">
				<div class="row match-height">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title" id="basic-layout-form">Edit {{ $page_title }}</h4>
								<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
										<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="card-body collapse in">
								<div class="card-block">
									
									@if ($errors->has('vendor_id'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Vendor</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('title'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Judul</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('description'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Description</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('img'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Gambar</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('expired_date'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Tanggal Expired Promo</strong> tidak boleh kosong !
										</div>
									@endif

									@if(Session::has('done'))
						                <div class="alert bg-success alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											{{ Session::get('done') }}
										</div>
						            @endif

									@if($user->role == 'superadmin' || $user->role == 'admin')
						            <a href="#" class="btn btn-secondary mr-1" onclick="promo_unique_code('{{ $promo_unique_code_url }}', {{ $promo->id }})">Lihat Kode Unik Promo</a>
						            <br><br>
									@endif

									<form action="{{ url($url_admin.'/edit-promo/'.$promo->id) }}" method="POST" class="form" enctype="multipart/form-data">

										{!! csrf_field() !!}
				
										<h4 class="form-section"><i class="ft-file-text"></i> Edit {{ $page_title }}</h4>
										<div class="row">

											<div class="col-md-12">
												<div class="form-group">
													<label>Judul</label>
													<input type="text" class="form-control" placeholder="Judul" name="title" value="{{ $promo->title }}">
												</div>
											</div>

											<div class="col-md-4">
												<div class="form-group">
													<label>Vendor</label><br>
													<select class="select2 form-control block" id="responsive_single" style="width: 100%" name="vendor_id">
														<option value="{{ $promo->vendor_id }}">{{ $promo->vendor_name }}</option>
														@foreach($unselected_vendors as $unselected_vendor)
														<option value="{{ $unselected_vendor->id }}">{{ $unselected_vendor->nama_vendor }}</option>
														@endforeach
													</select>
												</div>
											</div>

											<div class="col-md-4">
												<div class="form-group">
													<label>Tanggal Expired Promo</label>
													<div class="input-group">
														<input style="background-color: white !important;" type='text' class="form-control pickadate-selectors-event" placeholder="Tanggal Expired Promo" name="expired_date" value="{{ $promo->expired_date }}" />
													</div>
												</div>
											</div>

											<div class="col-md-4">
												<div class="form-group">
													<label for="file">
														Upload Gambar &nbsp;&nbsp;

														<button id="popover-info" type="button" class="btn mr-1 mb-1 btn-secondary btn-sm" data-toggle="popover" data-content="
														@if($promo->img != null)
															Untuk melihat gambar saat ini klik <a href='{{ url($promo->img) }}' target='_blank'>disini</a>, untuk mengganti gambar yang baru, tekan choose file, pilih gambar nya lalu tekan simpan.
														@else
															Gambar tidak di temukan.
														@endif
														" data-original-title="Info" data-html="true" data-placement="top" data-trigger="focus">
														<i class="ft-info"></i>
															Info
														</button>
													</label>
				                                    <input type="file" name="img" class="form-control-file" id="basicInputFile">
												</div>
											</div>

											<div class="col-md-12">
												<div class="form-group">
													<label for="file">Description</label>
				                                    <textarea class="form-control" id="description_editor" rows="6" name="description">{{ $promo->description }}</textarea>
												</div>
											</div>
										</div>

										<div class="form-actions">
											<a href="{{ url($url_admin.'/promo') }}" class="btn btn-warning mr-1"><i class="ft-x"></i> Batal</a>

											<button type="submit" class="btn btn-primary mr-1">
												<i class="fa fa-check-square-o"></i> Simpan
											</button>

											<a href="{{ url($url_admin.'/add-promo') }}" class="btn btn-success mr-1">Tambahkan {{ $page_title }} Lagi</a>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
        </div>
    </div>
</div>

@endsection

@section('custom_js_script')
    <script charset="utf-8" src=" {{ URL::asset('/ckeditor/ckeditor.js') }} "></script>
    <script type="text/javascript">
        CKEDITOR.replace( 'description_editor', {
            extraPlugins: 'colorbutton,colordialog'
        } );
    </script>
@endsection