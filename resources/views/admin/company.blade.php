@extends('admin/header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title">Data {{ $page_title }}</h4>
				                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
			        			<div class="heading-elements">
				                    <ul class="list-inline mb-0">
				                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
				                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				                    </ul>
				                </div>
				            </div>
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
									<a href="#" class="btn btn-success mr-1 mb-1" data-toggle="modal" data-target="#add-company">Tambah Data {{ $page_title }} Baru</a>
									<br><br>
									<table width="100%" class="table table-striped table-bordered dataex-html5-export server-side-company">
										<thead>
											<tr>
												<th>No.</th>
                                                <th>Nama Perusahaan</th>
												<th>Kode</th>
												<th>Judul</th>
												<th>Logo</th>
												<th></th>
											</tr>
										</thead>
									</table>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

<!-- Add Company Modal -->
<div class="modal fade text-xs-left" id="add-company" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Company</label>
            </div>

            <form enctype="multipart/form-data" action="#" id="upload_company">
                <div class="modal-body">

                    <label>Nama Perusahaan *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Perusahaan" class="form-control name" name="name">
                    </div>

                    <label>Kode Perusahaan</label>
                    <div class="form-group">
                        <input type="text" placeholder="Kode Perusahaan" class="form-control code" name="code">
                    </div>

                    <label>Judul</label>
                    <div class="form-group">
                        <input type="text" placeholder="Judul" class="form-control" id="title_editor" name="title">
                    </div>

                    <label>Logo</label>
                    <div class="form-group">
                        <input type="file" placeholder="Logo" class="form-control logo" name="img">
                    </div>
                    
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn save-btn" value="Simpan">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit Company Modal -->
<div class="modal fade text-xs-left" id="edit-company" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Company</label>
            </div>

            <form enctype="multipart/form-data" id="edit_upload_company" action="#">
                <div class="modal-body">
                    <label>Nama Perusahaan *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Perusahaan" class="form-control edit_name" name="name">
                    </div>

                    <label>Kode Perusahaan *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Kode Perusahaan" class="form-control edit_code" name="code">
                    </div>

                    <label>Judul</label>
                    <div class="form-group">
                        <input type="text" placeholder="Judul" class="form-control" id="edit_title_editor" name="title">
                    </div>

                    <label>Logo</label>
                    <div class="form-group">
                        <input type="file" placeholder="Logo" class="form-control edit_logo" name="img">
                    </div>

                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn update-btn" value="Ubah">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Delete Merchant Modal -->
<div class="modal fade text-xs-left" id="delete-company" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Apakah anda ingin menghapus data ini ?</label>
            </div>

            <form>
                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tidak">
                    <input type="submit" class="btn btn-outline-primary btn delete-btn" value="Ya">
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')
<script charset="utf-8" src=" {{ URL::asset('/ckeditor/ckeditor.js') }} "></script>
<script type="text/javascript">
    CKEDITOR.replace( 'title_editor', {
        extraPlugins: 'colorbutton,colordialog'
    } );

    CKEDITOR.replace( 'edit_title_editor', {
        extraPlugins: 'colorbutton,colordialog'
    } );
</script>

<script type="text/javascript">
    let table, company_uid = '';

	$(document).ready(function() {
        $('.save-btn').on('click', addCompany);
        $('.update-btn').on('click', updateCompany);
		$('.delete-btn').on('click', destroyCompany);

	    table = $('.server-side-company').DataTable({
	    	"scrollX": !0,
	    	"lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
	        "processing": true,
	        "serverSide": true,
	        "ajax":{
	        	"type": "POST",
            	"url": "{{ url($url_admin.'/company-ajax') }}",
            	"dataType": "json",
           	},
	        "columns": [
	            { "data": "no" },
                { "data": "name" },
                { "data": "res.code" },
                { "data": "title" },
                { "data": "img" },
	            { "data": "action_btn" }
	        ],
	        order: [[1, 'asc']],
            "columnDefs": [
                { "orderable": false, "targets": [ 0, 3] },
                { "width": "190px", "targets": [ 3 ] },
                { "width": "120px", "targets": [ 0 ] },
            ]
	    });

	    function addCompany() {
            let data = new FormData($("#upload_company")[0]);
            data.append('title', CKEDITOR.instances['title_editor'].getData());

	    	// let args = {};
            // args.name = $('.name').val();
	    	// args.code = $('.code').val();
            
	    	$('.save-btn').prop('disabled', true);
	    	toastr.info("Harap menunggu, data sedang di proses", "Loading...");

	    	$.ajax({
                type: "POST",
                processData: false,
                contentType: false,
                url: '{{ $base_url }}'+'add-company',
                dataType: "json",
                data: data,
                cache : false,
                success: function(data){
                	toastr.clear();
                	
                    if(data.code == 400) {
                    	if(Array.isArray(data.message)) {
                    		toastr.warning(data.message[0], "Peringatan");
                    	} else {
                    		toastr.warning(data.message, "Peringatan");
                    	}
                    } else if(data.code == 200) {
                    	toastr.success(data.message, "Sukses");

                    	$('#add-company').modal('hide');
                    	
                        $('.name').val("");
                    	$('.code').val("");
                        $('.logo').val("");
                        CKEDITOR.instances['title_editor'].setData('');

						table.ajax.reload(null, false);
                    }

                    $('.save-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                	console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.save-btn').prop('disabled', false);
                },

            });
	    }

        function updateCompany() {
            let data = new FormData($("#edit_upload_company")[0]);
            data.append('title', CKEDITOR.instances['edit_title_editor'].getData());
            data.append('uid', company_uid);

            // let args = {};
            // args.uid = company_uid;
            // args.name = $('.edit_name').val();
            // args.code = $('.edit_code').val();
            
            $('.update-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                processData: false,
                contentType: false,
                url: '{{ $base_url }}'+'edit-company',
                dataType: "json",
                data: data,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");

                        $('.edit_logo').val("");
                        
                        $('#edit-company').modal('hide');
                        
                        table.ajax.reload(null, false);
                    }

                    $('.update-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.update-btn').prop('disabled', false);
                },

            });
        }

        function destroyCompany() {
            let args = {};
            args.uid = company_uid;
            
            $('.delete-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                url: '{{ $base_url }}'+'delete-company',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");

                        $('#delete-company').modal('hide');
                        
                        table.ajax.reload(null, false);
                    }

                    $('.delete-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.delete-btn').prop('disabled', false);
                },

            });
        }
	});

    function editCompany(obj) {
        company_uid = obj.uid;

        $('.edit_name').val(decodeURIComponent(obj.name.replace(/\+/g, ' ')));
        $('.edit_code').val(obj.code);
        CKEDITOR.instances['edit_title_editor'].setData(decodeURIComponent(obj.title.replace(/\+/g, ' ')));
    }

    function deleteCompany(obj) {
        company_uid = obj.uid;
    }
</script>

@endsection