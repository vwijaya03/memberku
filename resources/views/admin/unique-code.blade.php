@extends('admin/header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">
				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title">History Jumlah Kode Unik Partner</h4>
				                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
			        			<div class="heading-elements">
				                    <ul class="list-inline mb-0">
				                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
				                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				                    </ul>
				                </div>
				            </div>
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
                                    
									<table width="1000px" class="table table-striped table-bordered dataex-html5-export server-side-unique-code-total">
										<thead>
											<tr>
												<th>No.</th>
                                                <th>Partner</th>
												<th>Jumlah Kode Unik</th>
											</tr>
										</thead>
									</table>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
            <section id="server-processing">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">History Kode Unik</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-body collapse in">
                                <div class="card-block card-dashboard">
                                    
                                    <table width="1280px" class="table table-striped table-bordered dataex-html5-export server-side-unique-code">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Partner</th>
                                                <th>Kode Unik</th>
                                                <th>Di Buat Tanggal</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<!-- Add User Modal -->
<div class="modal fade text-xs-left" id="share-wa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Customer</label>
            </div>

            <form action="#">
                <div class="modal-body">

                    <input type="hidden" class="uc" />
                    <label>Nama Lengkap</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Lengkap" class="form-control fullname">
                    </div>

                    <label>E-mail</label>
                    <div class="form-group">
                        <input type="text" placeholder="E-mail" class="form-control email">
                    </div>

                    <label>No. HP *</label>
                    <div class="form-group">
                        <input type="text" placeholder="No. HP" class="form-control phone">
                    </div>

                    <label>Alamat</label>
                    <div class="form-group">
                        <input type="text" placeholder="Alamat" class="form-control address">
                    </div>

                    <label>Text *</label>
                    <div class="form-group">
                        <textarea class="form-control wa" rows="8">
                        </textarea>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <a href="#" class="btn btn-outline-primary btn send-wa-btn">Kirim</a>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">
    let table, table_jumlah_unique_code_partner, selected_unique_code = '';

	$(document).ready(function() {
	    table = $('.server-side-unique-code').DataTable({
	    	"scrollX": !0,
	    	"lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
	        "processing": true,
	        "serverSide": true,
	        "ajax":{
	        	"type": "POST",
            	"url": "{{ url($base_url.'unique-code-ajax') }}",
            	"dataType": "json",
           	},
	        "columns": [
	            { "data": "no" },
                { "data": "res.fullname" },
	            { "data": "res.unique_code" },
	            { "data": "created_at" }
	        ],
	        order: [[3, 'desc']],
            "columnDefs": [
                { "orderable": false, "targets": [ 0 ] },
                { "width": "80px", "targets": [ 0 ] },
                { "width": "220px", "targets": [ 2 ] },
                { "width": "200px", "targets": [ 1 ] },
            ]
	    });

        table_jumlah_unique_code_partner = $('.server-side-unique-code-total').DataTable({
            "scrollX": !0,
            "lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
            "processing": true,
            "serverSide": true,
            "ajax":{
                "type": "POST",
                "url": "{{ url($base_url.'unique-code-total-ajax') }}",
                "dataType": "json",
            },
            "columns": [
                { "data": "no" },
                { "data": "res.fullname" },
                { "data": "res.total_unique_code" },
            ],
            order: [[2, 'desc']],
            "columnDefs": [
                { "orderable": false, "targets": [ 0 ] },
                { "width": "30px", "targets": [ 0 ] },
                { "width": "120px", "targets": [ 2 ] },
                { "width": "100px", "targets": [ 1 ] },
            ]
        });
	});
</script>

@endsection