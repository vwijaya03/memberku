@extends('admin/header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-2">
                <h3 class="content-header-title mb-0">Data {{ $page_title }}</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url($url_admin.'/dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{ url($url_admin.'/promo') }}">{{ $page_title }}</a>
                            </li>
                            <li class="breadcrumb-item active">Add {{ $page_title }}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
        	<section id="basic-form-layouts">
				<div class="row match-height">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title" id="basic-layout-form">Add {{ $page_title }}</h4>
								<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
										<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="card-body collapse in">
								<div class="card-block">
									
									@if ($errors->has('vendor_id'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Vendor</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('title'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Judul</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('description'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Description</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('img'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Gambar</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('expired_date'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Tanggal Expired Promo</strong> tidak boleh kosong !
										</div>
									@endif

									@if(Session::has('done'))
						                <div class="alert bg-success alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											{{ Session::get('done') }}
										</div>
						            @endif

									<form action="{{ url($url_admin.'/add-promo') }}" method="POST" class="form" enctype="multipart/form-data">

										{!! csrf_field() !!}
				
										<h4 class="form-section"><i class="ft-file-text"></i> Add {{ $page_title }}</h4>
										<div class="row">
											
											<div class="col-md-12">
												<div class="form-group">
													<div class="row skin skin-square">
														<div class="col-md-12 col-sm-12">
															<fieldset>
																<input type="checkbox" name="push_notification">
																<label for="input-11">Kirim push notification ke customer</label>
															</fieldset>
														</div>
													</div>
												</div>
											</div>
											
											<div class="col-md-12">
												<div class="form-group">
													<label>Judul</label>
													<input type="text" class="form-control" placeholder="Judul" name="title" value="{{ old('title') }}">
												</div>
											</div>

											<div class="col-md-4">
												<div class="form-group">
													<label>Vendor</label><br>
													<select class="select2 form-control block" id="responsive_single" style="width: 100%" name="vendor_id">
														@foreach($vendors as $vendor)
														<option value="{{ $vendor->id }}">{{ $vendor->nama_vendor }}</option>
														@endforeach
													</select>
												</div>
											</div>

											<div class="col-md-4">
												<div class="form-group">
													<label>Tanggal Expired Promo</label>
													<div class="input-group">
														<input style="background-color: white !important;" type='text' class="form-control pickadate-selectors-event" placeholder="Tanggal Expired Promo" name="expired_date" value="{{ old('expired_date') }}" />
													</div>
												</div>
											</div>

											<div class="col-md-4">
												<div class="form-group">
													<label for="file">Upload Gambar</label>
				                                    <input type="file" name="img" class="form-control-file">
												</div>
											</div>

											<div class="col-md-12">
												<div class="form-group">
													<label for="file">Description</label>
				                                    <textarea class="form-control" id="description_editor" rows="6" name="description">{{ old('description') }}</textarea>
												</div>
											</div>
										</div>

										<div class="form-actions">
											<a href="{{ url($url_admin.'/promo') }}" class="btn btn-warning mr-1"><i class="ft-x"></i> Batal</a>

											<button type="submit" class="btn btn-primary mr-1">
												<i class="fa fa-check-square-o"></i> Simpan
											</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
        </div>
    </div>
</div>

@endsection

@section('custom_js_script')
    <script charset="utf-8" src=" {{ URL::asset('/ckeditor/ckeditor.js') }} "></script>
    <script type="text/javascript">
        CKEDITOR.replace( 'description_editor', {
            extraPlugins: 'colorbutton,colordialog'
        } );
    </script>
@endsection