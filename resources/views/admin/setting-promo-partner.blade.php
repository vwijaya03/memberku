@extends('admin/header')

@section('content')
<style type="text/css">
    .picker {
        top: -400% !important;
    }    
</style>

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title">Data {{ $page_title }}</h4>

                                <br><br>

				                <div class="form-group">
                                    <label><b>Pilih Partner</b></label> <br>
                                    <select class="select2-partner form-control selected-partner" style="width: 50%;">
                                    </select>
                                </div>
				            </div>

                            <ul class="nav nav-tabs nav-underline no-hover-bg nav-justified">
                                <li class="nav-item">
                                    <a class="nav-link active" id="active-tab32" data-toggle="tab" href="#active32" aria-controls="active32" aria-expanded="true">Promo Yang Sudah Di Pilih</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="link-tab32" data-toggle="tab" href="#link32" aria-controls="link32" aria-expanded="false">Promo Yang Belum Di Pilih</a>
                                </li>
                            </ul>

                            <div class="tab-content px-1 pt-1">
                                <div role="tabpanel" class="tab-pane fade active in" id="active32" aria-labelledby="active-tab32" aria-expanded="true">
                                    <div class="card-body collapse in">
                                        <div class="card-block card-dashboard">
                                            <table width="1400px" class="table table-striped table-bordered dataex-html5-export server-side-selected-promo">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Merchant</th>
                                                        <th>Title</th>
                                                        <th>Description</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="link32" role="tabpanel" aria-labelledby="link-tab32" aria-expanded="false">
                                    <div class="card">

                                        <div class="card-body collapse in">
                                            <div class="card-block card-dashboard">
                                                <table width="1400px" class="table table-striped table-bordered dataex-html5-export server-side-unselected-promo">
                                                    <thead>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>Merchant</th>
                                                            <th>Title</th>
                                                            <th>Description</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

				            
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">
    let tableSelectedPromo, tableUnselectedPromo, promo_id = '';
    let currentSelectedPromoUID = [];

	$(document).ready(function() {
        $('.selected-partner').on('change', showSelectedPromo);

	    tableSelectedPromo = $('.server-side-selected-promo').DataTable({
	    	"scrollX": !0,
            "scrollY": "200px",
	    	"lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
            "processing": true,
	        "serverSide": true,
            "ajax":{
                type: "GET",
                "url": "{{ url($url_admin.'/partner-selected-promo') }}",
                "dataType": "json",
                "data": function(param) {
                    param.user_uid = ($('.selected-partner').val() != null) ? $('.selected-partner').val() : 'abc';
                },
            },
            "drawCallback": function (result) { 
                // Here the response
                let data = result.json.data; 
                currentSelectedPromoUID = [];
                $.map(data, function (item, index) {
                    if(item != null || item !== undefined) {
                        currentSelectedPromoUID.push(item.uid);
                    }
                });

                tableUnselectedPromo.ajax.reload();
                tableUnselectedPromo.buttons().disable();
            },
	        "columns": [
                { "data": "no" },
	            { 
                    "data": "name",
                    "render": function ( data, type, row, meta ) {
                        return decodeURIComponent(data.replace(/\+/g, ' '));
                    }
                },
	            { 
                    "data": "title",
                    "render": function ( data, type, row, meta ) {
                        return decodeURIComponent(data.replace(/\+/g, ' '));
                    }
                },
                { 
                    "data": "description",
                    "render": function ( data, type, row, meta ) {
                        return decodeURIComponent(data.replace(/\+/g, ' '));
                    }
                }
	        ],
	        order: [[0, 'asc']],
            "columnDefs": [
                { "orderable": false, "targets": [ 0, 3 ] },
                { "width": "30px", "targets": [ 0 ] },
                { "width": "120px", "targets": [ 1, 2 ] },
                // { "width": "250px", "targets": [ 3 ] }
            ],
            dom: "lBfrtip",
            select: !0,
            buttons: [ 
                {
                    text: 'Hapus Promo',
                    action: function ( e, dt, node, config ) {
                        if($('.selected-partner').val() == '' || $('.selected-partner').val() == null || $('.selected-partner').val() === undefined || $('.selected-partner').val().length == 0) {
                            toastr.warning("Partner tidak boleh kosong", "Peringatan");
                            return;
                        }

                        let promo_uid = $.map(dt.rows( { selected: true } ).data(), function (item, index) {
                            return item.uid;
                        });
                        
                        let args = {};
                        args.user_uid = $('.selected-partner').val();
                        args.promo_uid = promo_uid;

                        $.ajax({
                            type: "POST",
                            "url": "{{ url($url_admin.'/delete-partner-promo') }}",
                            dataType: "json",
                            data: args,
                            cache : false,
                            success: function(data){
                                toastr.clear();
                                
                                if(data.code == 400) {
                                    if(Array.isArray(data.message)) {
                                        toastr.warning(data.message[0], "Peringatan");
                                    } else {
                                        toastr.warning(data.message, "Peringatan");
                                    }
                                } else if(data.code == 200) {
                                    tableSelectedPromo.ajax.reload();
                                    tableSelectedPromo.buttons().disable();

                                    toastr.success(data.message, "Sukses");
                                }
                            } ,error: function(xhr, status, error) {
                                console.log(error);
                                toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                            },

                        });
                        // table.buttons().disable();
                    },
                    enabled: false
                }
            ],
	    });

        tableUnselectedPromo = $('.server-side-unselected-promo').DataTable({
            "scrollX": !0,
            "scrollY": "200px",
            "lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
            "processing": true,
            "serverSide": true,
            "ajax":{
                type: "GET",
                "url": "{{ url($url_admin.'/partner-unselected-promo') }}",
                "dataType": "json",
                "data": function(param) {
                    param.user_uid = ($('.selected-partner').val() != null) ? $('.selected-partner').val() : 'abc';
                    param.selected_promo_uid = currentSelectedPromoUID;
                },
            },
            "columns": [
                { "data": "no" },
                { 
                    "data": "name",
                    "render": function ( data, type, row, meta ) {
                        return decodeURIComponent(data.replace(/\+/g, ' '));
                    }
                },
                { 
                    "data": "title",
                    "render": function ( data, type, row, meta ) {
                        return decodeURIComponent(data.replace(/\+/g, ' '));
                    }
                },
                { 
                    "data": "description",
                    "render": function ( data, type, row, meta ) {
                        return decodeURIComponent(data.replace(/\+/g, ' '));
                    }
                }
            ],
            order: [[0, 'asc']],
            "columnDefs": [
                { "orderable": false, "targets": [ 0, 3 ] },
                { "width": "30px", "targets": [ 0 ] },
                { "width": "120px", "targets": [ 1, 2 ] },
                // { "width": "250px", "targets": [ 3 ] }
            ],
            dom: "lBfrtip",
            select: !0,
            buttons: [ 
                {
                    text: 'Pilih Promo',
                    action: function ( e, dt, node, config ) {
                        if($('.selected-partner').val() == '' || $('.selected-partner').val() == null || $('.selected-partner').val() === undefined || $('.selected-partner').val().length == 0) {
                            toastr.warning("Partner tidak boleh kosong", "Peringatan");
                            return;
                        }

                        toastr.info("Harap menunggu, data sedang di proses", "Loading...");
                        
                        let promo_uid = $.map(dt.rows( { selected: true } ).data(), function (item, index) {
                            return item.uid;
                        });
                        
                        let args = {};
                        args.user_uid = $('.selected-partner').val();
                        args.promo_uid = promo_uid;

                        $.ajax({
                            type: "POST",
                            "url": "{{ url($url_admin.'/add-partner-promo') }}",
                            dataType: "json",
                            data: args,
                            cache : false,
                            success: function(data){
                                toastr.clear();
                                
                                if(data.code == 400) {
                                    if(Array.isArray(data.message)) {
                                        toastr.warning(data.message[0], "Peringatan");
                                    } else {
                                        toastr.warning(data.message, "Peringatan");
                                    }
                                } else if(data.code == 200) {
                                    tableSelectedPromo.ajax.reload();
                                    tableSelectedPromo.buttons().disable();

                                    toastr.success(data.message, "Sukses");
                                }
                            } ,error: function(xhr, status, error) {
                                console.log(error);
                                toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                            },

                        });
                        // table.buttons().disable();
                    },
                    enabled: false
                }
            ],
        });

        tableSelectedPromo.off("select").on( "select", function( e, dt, type, indexes ) {
            let selectedRows = tableSelectedPromo.rows( { selected: true } ).count();
            tableSelectedPromo.button( 0 ).enable( selectedRows === 1 || selectedRows > 0 );
        } );

        tableSelectedPromo.off("deselect").on( "deselect", function( e, dt, type, indexes ) {
            let deselectedRows = tableSelectedPromo.rows( { selected: true } ).count();
            if(deselectedRows === 0)
            {
                tableSelectedPromo.button( 0 ).disable( deselectedRows === 0 );
            }
        } );

        tableUnselectedPromo.off("select").on( "select", function( e, dt, type, indexes ) {
            let selectedRows = tableUnselectedPromo.rows( { selected: true } ).count();
            tableUnselectedPromo.button( 0 ).enable( selectedRows === 1 || selectedRows > 0 );
        } );

        tableUnselectedPromo.off("deselect").on( "deselect", function( e, dt, type, indexes ) {
            let deselectedRows = tableUnselectedPromo.rows( { selected: true } ).count();
            if(deselectedRows === 0)
            {
                tableUnselectedPromo.button( 0 ).disable( deselectedRows === 0 );
            }
        } );

        function showSelectedPromo() {
            tableSelectedPromo.ajax.reload();
            tableSelectedPromo.buttons().disable();
            
        }
	});

    function editPromo(obj) {
        $('.edit-merchant').empty();

        let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        let date = new Date(obj.expired_date);
        let formatDate = ("0" + date.getDate()).slice(-2)+" "+months[date.getMonth()]+" "+date.getFullYear();

        promo_id = obj.id;

        if(obj.top_promo == 'yes') {
            $('.edit-top-promo').prop('checked', true);
        } else {
            $('.edit-top-promo').prop('checked', false);
        }

        if(obj.use_pin == 'yes') {
            $('.edit-use-pin').prop('checked', true);
        } else {
            $('.edit-use-pin').prop('checked', false);
        }
        
        $('.edit-merchant').append('<option value="'+obj.merchant_id+'">'+decodeURIComponent(obj.name.replace(/\+/g, ' '))+'</option>');
        $('.edit-title').val(decodeURIComponent(obj.title.replace(/\+/g, ' ')));
        CKEDITOR.instances['edit_description_editor'].setData(decodeURIComponent(obj.description.replace(/\+/g, ' ')));
        $('.edit-harga-awal').val(obj.harga_awal);
        $('.edit-harga-akhir').val(obj.harga_akhir);
        $('.edit-expired-promo').val(formatDate);
        $('.edit-pin').val(obj.pin);
    }

    function formatPartnerResult(result) {
        if (result.loading) return result.fullname;

        return ("<option selected>"+decodeURIComponent(result.fullname.replace(/\+/g, ' '))+"</option>");
    }

    function formatPartnerSelection(result) {
        if(result.fullname !== undefined || result.fullname != null) {
            result.fullname = decodeURIComponent(result.fullname.replace(/\+/g, ' '));
            result.text = decodeURIComponent(result.text.replace(/\+/g, ' '));
        }

        return result.fullname || result.text;
    }

    let partner = $(".select2-partner");
    let search_partner_url = "{{ $search_partner_url }}";

    partner.select2({
        // tags: true,
        // dropdownParent: $("#add-promo"),
        placeholder: "Cari partner...",
        ajax: {
        url: search_partner_url,
            dataType: "json",
            delay: 250,
            data: function(params) {
                return {
                    q: params.term,
                    page: params.page
                }
            },
            processResults: function(data, params) {
                return params.page = params.page || 1, {
                    results: data.result.data.map(function(item) {
                        return {
                            id : item.uid,
                            fullname : item.fullname
                        };
                    }),
                    pagination: {
                        more: 30 * params.page < data.result.total
                    }
                }
            },
            cache: !0
        },
        escapeMarkup: function(markup) {
            return markup
        },
        minimumInputLength: 1,
        templateResult: formatPartnerResult,
        templateSelection: formatPartnerSelection
    });
</script>

@endsection