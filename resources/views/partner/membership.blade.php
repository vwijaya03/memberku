@extends('partner/header')

@section('content')
<style type="text/css">
    .fa {
        font-size: 20px !important;
    }  

    .ft-mail {
        font-size: 17px !important;
    }  
</style>

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title">Cetak {{ $page_title }}</h4>
				            </div>
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
									<a href="#" class="btn btn-success mr-1 mb-1" data-toggle="modal" data-target="#add-membership">Tambah Data {{ $page_title }}</a>

                                    <a href="#" class="btn btn-success mr-1 mb-1" data-toggle="modal" data-target="#my-logo">Edit Logo</a>

                                    <a href="#" class="btn btn-success mr-1 mb-1" data-toggle="modal" data-target="#change-profile">Edit Profile</a>

									<br><br>

									<table width="980px" class="table table-striped table-bordered dataex-html5-export server-side-membership">
										<thead>
											<tr>
												<th>Share Via</th>
                                                <th>Nama Lengkap</th>
                                                <th>Nomor Kartu</th>
												<th></th>
											</tr>
										</thead>
									</table>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

<!-- Add Membership Modal -->
<div class="modal fade text-xs-left" id="add-membership" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Membership</label>
            </div>

            <form action="#">
                <div class="modal-body">
                    <label>Nama Lengkap Customer *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Lengkap Customer" class="form-control fullname">
                    </div>

                    <!-- <label>Nama Usaha *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Usaha" class="form-control nama_usaha">
                    </div>

                    <label>Alamat *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Alamat" class="form-control address">
                    </div>
                    
                    <label>Kota *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Kota" class="form-control city">
                    </div>

                    <label>Telp *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Telp" class="form-control phone">
                    </div> -->
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn save-btn" value="Simpan">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit Membership Modal -->
<div class="modal fade text-xs-left" id="edit-membership" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Membership</label>
            </div>

            <form action="#">
                <div class="modal-body">
                    <label>Nama Lengkap Customer *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Lengkap Customer" class="form-control edit_fullname">
                    </div>

                    <!-- <label>Nama Usaha *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Usaha" class="form-control edit_nama_usaha">
                    </div>

                    <label>Alamat *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Alamat" class="form-control edit_address">
                    </div>
                    
                    <label>Kota *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Kota" class="form-control edit_city">
                    </div>

                    <label>Telp *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Telp" class="form-control edit_phone">
                    </div> -->
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn update-btn" value="Ubah">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Delete Membership Modal -->
<div class="modal fade text-xs-left" id="delete-membership" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Apakah anda ingin menghapus data ini ?</label>
            </div>

            <form>
                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tidak">
                    <input type="submit" class="btn btn-outline-primary btn delete-btn" value="Ya">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Whats App Modal -->
<div class="modal fade text-xs-left" id="share-wa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33"><b>Share Via Whats App</b></label>
            </div>

            <form action="#">
                <div class="modal-body">
                    <label>No. Whats App *</label>
                    <div class="form-group">
                        <input type="text" placeholder="No. Whats App" class="form-control phone">
                    </div>
                    
                    <input type="hidden" class="wa">
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <a href="#" class="btn btn-outline-primary btn send-wa-btn">Kirim</a>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- My Logo Modal -->
<div class="modal fade text-xs-left" id="my-logo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">My Logo</label>
            </div>

            <form enctype="multipart/form-data" id="upload_logo">
                <div class="modal-body">
                    <label for="file">Logo</label>      
                    <div class="form-group">
                        <input type="file" name="img" class="form-control-file">

                        <br>
                        
                        @if($logo != null)
                        <a href="#" class="btn btn-outline-danger btn delete-logo-btn" onclick="deleteLogo()">Hapus Logo</a>
                        
                        Klik <a href="{{ $public_base_url.$logo->img }}" target="_blank">di sini</a> untuk melihat logo 
                        @endif    
                    </div>   
                    
                            
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn my-logo-btn" value="Simpan" onclick="myLogo('{{ $auth_partner_base_url }}', '{{ $current_user->id }}')">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('server_side_datatable')

<script type="text/javascript">
    let table, membership_id, membership_obj = '';

	$(document).ready(function() {
        $('.save-btn').on('click', addMembership);
        $('.update-btn').on('click', updateMembership);
		$('.delete-btn').on('click', destroyMembership);
        $('.send-wa-btn').on('click', sendMembership);

	    table = $('.server-side-membership').DataTable({
	    	"scrollX": !0,
            "scrollY": "370px",
	    	"lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
	        "processing": true,
	        "serverSide": true,
	        "ajax":{
	        	"type": "POST",
            	"url": "{{ url($auth_partner_base_url.'membership-ajax') }}",
            	"dataType": "json",
           	},
	        "columns": [
	            { "data": "share_btn" },
	            { "data": "res.fullname" },
	            // { "data": "res.nama_usaha" },
	            // { "data": "res.address" },
	            // { "data": "res.city" },
             //    { "data": "res.phone" },
                { "data": "res.card_number" },
	            { "data": "action_btn" }
	        ],
	        order: [[0, 'desc']],
            "columnDefs": [
                { "orderable": false, "targets": [ 0, 2, 3 ] },
                { "width": "180px", "targets": [ 3 ] },
                { "width": "160px", "targets": [ 0 ] }
                // { "width": "250px", "targets": [ 2 ] }
            ]
	    });

	    function addMembership() {
            let args = {};
            // args.nama_usaha = $('.nama_usaha').val();
            // args.address = $('.address').val();
            // args.city = $('.city').val();
            // args.phone = $('.phone').val();
            args.fullname = $('.fullname').val();
                        
	    	$('.save-btn').prop('disabled', true);
	    	toastr.info("Harap menunggu, data sedang di proses", "Loading...");

	    	$.ajax({
                type: "POST",
                url: '{{ $auth_partner_base_url }}'+'add-membership',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                	toastr.clear();
                	
                    if(data.code == 400) {
                    	if(Array.isArray(data.message)) {
                    		toastr.warning(data.message[0], "Peringatan");
                    	} else {
                    		toastr.warning(data.message, "Peringatan");
                    	}
                    } else if(data.code == 200) {
                    	toastr.success(data.message, "Sukses");

                    	$('#add-membership').modal('hide');

                    	// $('.nama_usaha').val("");
                     //    $('.address').val("");
                     //    $('.city').val("");
                     //    $('.phone').val("");
                        $('.fullname').val("");

						table.ajax.reload();
                    }

                    $('.save-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                	console.log(error);
                    toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
                    $('.save-btn').prop('disabled', false);
                },

            });
	    }

        function updateMembership() {
            let args = {};
            args.id = membership_id;
            // args.nama_usaha = $('.edit_nama_usaha').val();
            // args.address = $('.edit_address').val();
            // args.city = $('.edit_city').val();
            // args.phone = $('.edit_phone').val();
            args.fullname = $('.edit_fullname').val();

            $('.update-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                url: '{{ $auth_partner_base_url }}'+'edit-membership',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");

                        $('#edit-membership').modal('hide');
                        
                        table.ajax.reload();
                    }

                    $('.update-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
                    $('.update-btn').prop('disabled', false);
                },

            });
        }

        function destroyMembership() {
            let args = {};
            args.id = membership_id;
            
            $('.delete-btn').prop('disabled', true);
            toastr.info("Harap menunggu, data sedang di proses", "Loading...");

            $.ajax({
                type: "POST",
                url: '{{ $auth_partner_base_url }}'+'delete-membership',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                    toastr.clear();
                    
                    if(data.code == 400) {
                        if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                        } else {
                            toastr.warning(data.message, "Peringatan");
                        }
                    } else if(data.code == 200) {
                        toastr.success(data.message, "Sukses");

                        $('#delete-membership').modal('hide');
                        
                        table.ajax.reload();
                    }

                    $('.delete-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                    console.log(error);
                    toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
                    $('.delete-btn').prop('disabled', false);
                },

            });
        }
	});

    function editMembership(obj) {
        membership_id = obj.id;
        // $('.edit_nama_usaha').val(obj.nama_usaha);
        // $('.edit_address').val(obj.address);
        // $('.edit_city').val(obj.city);
        // $('.edit_phone').val(obj.phone);
        $('.edit_fullname').val(obj.fullname);
    }

    function deleteMembership(obj) {
        membership_id = obj.id;
    }

    function shareWA(obj) {
        membership_obj = obj;
        toastr.info("Harap menunggu", "Loading...");

        $.ajax({
            type: "POST",
            url: '{{ $auth_partner_base_url }}'+'profile',
            dataType: "json",
            cache : false,
            success: function(data){
                let noc = encodeURIComponent(data.result.name_on_card);
                let address = encodeURIComponent(data.result.address);
                let city = encodeURIComponent(data.result.city);
                let phone = encodeURIComponent(data.result.phone);
                
                toastr.clear();

                let text = `Klik Link Kartu Member Di Bawah \n\nhttps://promo-member.com/noc/${noc}/adr/${address}/kt/${city}/sm/${phone}/u/{{ $current_user->uid }}/m/${obj.uid}\n\n`;
        
                $('.wa').val(text);
                $('#share-wa').modal('show');
            } ,error: function(xhr, status, error) {
                console.log(error);
                toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
            },

        });
    }

    function sendMembership() {
        let text = encodeURIComponent($('.wa').val());
        let phone = '62'+$('.phone').val().substring(1);

        if(phone.length == 0) {
            toastr.warning("No. Whats App tidak boleh kosong", "Peringatan");
            return;
        }

        if(phone.length < 10 || phone.length > 13) {
            toastr.warning("Format No. Whats App salah", "Peringatan");
            return;
        }

        // toastr.info("Harap menunggu, data sedang di proses", "Loading...");
        $('.send-wa-btn').prop('disabled', true);

        $('#share-wa').modal('hide');

        window.open(`https://api.whatsapp.com/send?phone=${phone}&text=${text}`);

        $('.send-wa-btn').prop('disabled', false);
    }
</script>

@endsection