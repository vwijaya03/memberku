<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
		<meta name="description" content="AAI">
		<meta name="keywords" content="AAI">
		<meta name="author" content="AAI">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>AAI</title>
		<link rel="apple-touch-icon" href="{{ URL::asset('admin/app-assets/images/ico/apple-icon-120.png') }}">
		<link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('admin/app-assets/images/ico/favicon.ico') }}">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
		<!-- BEGIN VENDOR CSS-->
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/bootstrap.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/fonts/feather/style.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/fonts/font-awesome/css/font-awesome.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/fonts/flag-icon-css/css/flag-icon.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/extensions/pace.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/forms/icheck/icheck.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/forms/icheck/custom.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/extensions/toastr.css') }}">
		<!-- END VENDOR CSS-->
		<!-- BEGIN TERA CSS-->
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/bootstrap-extended.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/app.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/colors.min.css') }}">
		<!-- END TERA CSS-->
		<!-- BEGIN Page Level CSS-->
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/core/menu/menu-types/horizontal-menu.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/core/colors/palette-gradient.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/pages/login-register.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/plugins/extensions/toastr.min.css') }}">
		<!-- END Page Level CSS-->
		<!-- BEGIN Custom CSS-->
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/assets/css/style.css') }}">
		<!-- END Custom CSS-->
	</head>

	<body data-open="hover" data-menu="horizontal-menu" data-col="1-column" class="horizontal-layout horizontal-menu 1-column menu-expanded blank-page blank-page">

    <!-- ////////////////////////////////////////////////////////////////////////////-->
	<div class="app-content content container-fluid">
		<div class="content-wrapper">
		<div class="content-header row">
		</div>
		<div class="content-body">
			<section class="flexbox-container" style="height: 100% !important;">
				<div class="col-md-6 offset-md-3 col-xs-10 offset-xs-1  box-shadow-2 p-0">
					<div class="card border-grey border-lighten-3 m-0">
						<div class="card-header no-border">
						    <div class="card-title text-xs-center">
						        <div class="p-1">AAI System</div>
						    </div>

						    <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2"><span>Form Booking</span></h6>
						</div>

						<div class="card-body collapse in">
						    <div class="card-block">
						    	
								@if ($errors->has('email'))
									<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<strong>E-mail</strong> tidak boleh kosong !
									</div>
								@endif

								@if ($errors->has('password'))
									<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<strong>Password</strong> tidak boleh kosong !
									</div>
								@endif

								@if(Session::has('err'))
					                <div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										{{ Session::get('err') }}
									</div>
					            @endif

						        <form class="form">
									<div class="form-body">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label>E-mail</label>
													<input type="text" class="form-control email" placeholder="E-mail">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Nama Usaha / Profesi</label>
													<input type="text" class="form-control nama_usaha" placeholder="Nama Usaha / Profesi">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label>Nama Contact Person</label>
													<input type="text" class="form-control contact_person" placeholder="Nama Contact Person">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Alamat</label>
													<input type="text" class="form-control alamat_usaha" placeholder="Alamat" name="phone">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label>Link IG / FB / Website</label>
													<input type="text" class="form-control social_media" placeholder="Link IG / FB / Website">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>No. HP / WA</label>
													<input type="text" class="form-control phone" placeholder="No. HP / WA">
												</div>
											</div>
										</div>
									</div>

									<div class="form-actions right">
										<button type="submit" class="btn btn-primary save-btn">
											<i class="fa fa-check-square-o"></i> Book Now
										</button>
									</div>
								</form>
						    </div>
						</div>
					</div>
				</div>
			</section>

		</div>
		</div>
	</div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->

    <!-- BEGIN VENDOR JS-->
    <script src="{{ URL::asset('admin/app-assets/vendors/js/vendors.min.js') }}" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script type="text/javascript" src="{{ URL::asset('admin/app-assets/vendors/js/ui/jquery.sticky.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('admin/app-assets/vendors/js/charts/jquery.sparkline.min.js') }}"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/forms/icheck/icheck.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/extensions/toastr.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN TERA JS-->
    <script src="{{ URL::asset('admin/app-assets/js/core/app-menu.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/js/core/app.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/js/scripts/customizer.min.js') }}" type="text/javascript"></script>
    <!-- END TERA JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script type="text/javascript" src="{{ URL::asset('admin/app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js') }}"></script>
    <script src="{{ URL::asset('admin/app-assets/js/scripts/forms/form-login-register.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->

    <script type="text/javascript">
    	$(document).ready(function() {
    		$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});

	        $('.save-btn').on('click', addBooking);

		    function addBooking() {
		    	let args = {};
		    	args.email = $('.email').val();
		    	args.nama_usaha = $('.nama_usaha').val();
	            args.contact_person = $('.contact_person').val();
	            args.alamat_usaha = $('.alamat_usaha').val();
		    	args.social_media = $('.social_media').val();
	            args.phone = $('.phone').val();

		    	$('.save-btn').prop('disabled', true);
		    	toastr.info("Harap menunggu, data sedang di proses", "Loading...");

		    	$.ajax({
	                type: "POST",
	                url: '{{ $partner_base_url }}'+'booking',
	                dataType: "json",
	                data: args,
	                cache : false,
	                success: function(data){
	                	toastr.clear();
	                	
	                    if(data.code == 400) {
	                    	if(Array.isArray(data.message)) {
	                    		toastr.warning(data.message[0], "Peringatan");
	                    	} else {
	                    		toastr.warning(data.message, "Peringatan");
	                    	}
	                    } else if(data.code == 200) {
	                    	toastr.success(data.message, "Sukses");

	                    	$('#add-booking').modal('hide');
	                    	
	                        $('.email').val("");
	                        $('.nama_usaha').val("");
	                        $('.contact_person').val("");
	                        $('.alamat_usaha').val("");
	                        $('.social_media').val("");
	                        $('.phone').val("")
	                    }

	                    $('.save-btn').prop('disabled', false);
	                } ,error: function(xhr, status, error) {
	                	console.log(error);
	                    toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
	                    $('.save-btn').prop('disabled', false);
	                },

	            });
		    }
		});
    </script>
  </body>
</html>