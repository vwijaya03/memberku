@extends('partner/header')

@section('content')
<style type="text/css">
    .ft-info {
        font-size: 22px !important;
    }  
</style>

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title">Data Benefit</h4>
				            </div>
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
									<div class="col-md-6">
                                        <h4><b>Promo Global</b></h4> <br><br>
                                        <table width="350px" class="table table-striped table-bordered dataex-html5-export server-side-benefit">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Title</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>

                                    <div class="col-md-6">
                                        <h4><b>Promo Internal</b></h4> <br><br>
                                        <table width="350px" class="table table-striped table-bordered dataex-html5-export server-side-promo-internal">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Title</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

<!-- Add Promo Modal -->
<div class="modal fade text-xs-left" id="data-benefit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Benefit</label>
            </div>

            <form action="#" enctype="multipart/form-data" id="upload_promo">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-8">
                            <label class="merchant"></label>
                            <div class="form-group">
                                <p class="title"></p>
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <label>Deskripsi</label>
                            <div class="form-group">
                                <p class="description"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">
    let table, table_pi = '';

	$(document).ready(function() {

	    table = $('.server-side-benefit').DataTable({
	    	"scrollX": !0,
            "scrollY": "370px",
	    	"lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
	        "processing": true,
	        "serverSide": true,
	        "ajax":{
	        	"type": "POST",
            	"url": "{{ url($auth_partner_base_url.'benefit-ajax') }}",
            	"dataType": "json",
           	},
	        "columns": [
	            { "data": "no" },
	            { "data": "title" },
	            { "data": "action_btn" }
	        ],
	        order: [[0, 'desc']],
            "columnDefs": [
                { "orderable": false, "targets": [ 1, 2 ] },
                { "width": "25px", "targets": [ 0 ] }
            ]
	    });

        table_pi = $('.server-side-promo-internal').DataTable({
            "scrollX": !0,
            "scrollY": "370px",
            "lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
            "processing": true,
            "serverSide": true,
            "ajax":{
                "type": "POST",
                "url": "{{ url($auth_partner_base_url.'promo-internal-partner-ajax/'.$current_user->uid) }}",
                "dataType": "json",
            },
            "columns": [
                { "data": "no" },
                { "data": "title" },
                { "data": "action_btn" }
            ],
            order: [[0, 'desc']],
            "columnDefs": [
                { "orderable": false, "targets": [ 1, 2 ] },
                { "width": "25px", "targets": [ 0 ] }
            ]
        });
	});

    function detailPromo(obj) {
        $('.merchant').html(decodeURIComponent(obj.name.replace(/\+/g, ' ')));
        $('.title').html(decodeURIComponent(obj.title.replace(/\+/g, ' ')));
        $('.description').html(decodeURIComponent(obj.description.replace(/\+/g, ' ')));
    }

    function detailPromoInternalPartner(obj) {
        console.log(obj);
        $('.merchant').html(decodeURIComponent(obj.fullname.replace(/\+/g, ' ')));
        $('.title').html(decodeURIComponent(obj.title.replace(/\+/g, ' ')));
        $('.description').html(decodeURIComponent(obj.description.replace(/\+/g, ' ')));
    }
</script>

@endsection