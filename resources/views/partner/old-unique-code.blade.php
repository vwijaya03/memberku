@extends('partner/header')

@section('content')
<style type="text/css">
    .fa {
        font-size: 20px !important;
    }  

    .ft-mail {
        font-size: 17px !important;
    }  
</style>

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title">Kirim Voucher</h4>
                                <p><h6>Credit: <span class="total-uc">{{ $total_uc }}</span> / <span class="max-total-uc">{{ $max_total_uc }}</span></h6></p>
				            </div>
				            <div class="card-body collapse in">

								<div class="card-block card-dashboard">

                                    <!-- <div class="alert alert-info no-border alert-dismissible fade in mb-2" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h4 class="alert-heading mb-2">Informasi</h4>
                                        <p>Free Kirim 5 Promo</p>
                                    </div> -->

                                    <button type="submit" class="btn btn-success mr-1 mb-1 generate-btn">Kirim Voucher</button>

                                    <a href="#" class="btn btn-success mr-1 mb-1" onclick="getGreetingText()"><span>Edit Greeting Text</span></a>

                                    <a href="#" class="btn btn-success mr-1 mb-1" onclick="getLinkTitle()"><span>Edit Judul Link</span></a>

									<br><br>
                                    
									<table width="500px" class="table table-striped table-bordered dataex-html5-export server-side-unique-code">
										<thead>
											<tr>
												<th>Kode Promo</th>
                                                <th>Share</th>
											</tr>
										</thead>
									</table>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

<!-- Whats App Modal -->
<div class="modal fade text-xs-left" id="share-wa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33"><b>Share Via Whats App</b></label>
            </div>

            <form action="#">
                <div class="modal-body">

                    <input type="hidden" class="uc" />
                    <label>Nama Lengkap</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Lengkap" class="form-control fullname">
                    </div>

                    <label>E-mail</label>
                    <div class="form-group">
                        <input type="text" placeholder="E-mail" class="form-control email">
                    </div>

                    <label>No. HP *</label>
                    <div class="form-group">
                        <input type="text" placeholder="No. HP" class="form-control phone">
                    </div>

                    <label>Alamat</label>
                    <div class="form-group">
                        <input type="text" placeholder="Alamat" class="form-control address">
                    </div>

                    <label>Text *</label>
                    <div class="form-group">
                        <textarea class="form-control wa" rows="8">
                        </textarea>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <a href="#" class="btn btn-outline-primary btn send-wa-btn">Kirim</a>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">
    let table, obj_uc = '';

	$(document).ready(function() {
        $('.generate-btn').on('click', addUniqueCode);
        $('.send-wa-btn').on('click', sendWAUniqueCode);

	    table = $('.server-side-unique-code').DataTable({
            "searching": false,
            "lengthChange": false,
	    	"scrollX": !0,
	        "processing": true,
	        "serverSide": true,
	        "ajax":{
	        	"type": "POST",
            	"url": "{{ url($auth_partner_base_url.'unique-code-ajax/'.$current_user->uid) }}",
            	"dataType": "json",
           	},
	        "columns": [
	            { "data": "res.unique_code" },
                { "data": "share_btn" }
	        ],
            "columnDefs": [
                { "orderable": false, "targets": [ 1 ] },
                { "width": "5px", "targets": [ 0 ] },
                { "width": "120px", "targets": [ 1 ] },
            ]
	    });

	    function addUniqueCode() {
            let args = {};
            args.user_uid = '{{ $current_user->uid }}';

	    	$('.generate-btn').prop('disabled', true);
	    	toastr.info("Harap menunggu, data sedang di proses", "Loading...");

	    	$.ajax({
                type: "POST",
                url: '{{ $auth_partner_base_url }}'+'add-unique-code',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                	toastr.clear();
                	
                    if(data.code == 400) {
                    	if(Array.isArray(data.message)) {
                    		toastr.warning(data.message[0], "Peringatan");
                    	} else {
                    		toastr.warning(data.message, "Peringatan");
                    	}
                    } else if(data.code == 200) {
                        if(data.total_uc == null || data.total_uc == '' || data.total_uc === undefined) {
                            $('.total-uc').text("0");
                        } else {
                            $('.total-uc').text(data.total_uc);
                        }

                        if(data.max_total_uc == null || data.max_total_uc == '' || data.max_total_uc === undefined) {
                            $('.max-total-uc').text("0");
                        } else {
                            $('.max-total-uc').text(data.max_total_uc);
                        }
                        
                    	toastr.success(data.message, "Sukses");
						table.ajax.reload();
                    }

                    $('.generate-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                	console.log(error);
                    toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
                    $('.generate-btn').prop('disabled', false);
                },

            });
	    }
	});

    function shareWA(obj) {
        $('.uc').val(obj.unique_code);

        let expired_date_position = obj.expired_date.indexOf(' ');
        let formatted_expired_date = obj.expired_date.substr(0, expired_date_position);

        let d = new Date(formatted_expired_date);
        let expired_date = ("0" + d.getDate()).slice(-2) + '/' +("0" + (d.getMonth() + 1)).slice(-2) + '/' + d.getFullYear();
        
        toastr.info("Harap menunggu", "Loading...");

        $.ajax({
            type: "POST",
            url: '{{ $auth_partner_base_url }}'+'promo',
            dataType: "json",
            cache : false,
            success: function(data){
                toastr.clear();

                let greeting_text = (data.greeting_text != null) ? data.greeting_text : '';
                let link_title = (data.link_title != null) ? encodeURIComponent(data.link_title) : '';
                let text = `${greeting_text}\n`;
                let text_link_title = `${link_title}`;
                let merchant, title;

                for (let i = 0; i < data.result.length; i++) {
                    merchant = decodeURIComponent(data.result[i].name.replace(/\+/g, ' '));
                    title = decodeURIComponent(data.result[i].title.replace(/\+/g, ' '));

                    text += `${merchant} - ${title} \n\nhttps://promo-member.com/promo/${data.result[i].uid}/v/${obj.unique_code}/u/${obj.user_uid}/noc/${text_link_title} \n\n`;
                }
                                    
                $('.wa').val(text);
                $('#share-wa').modal('show'); 
            } ,error: function(xhr, status, error) {
                console.log(error);
                toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
                $('.save-btn').prop('disabled', false);
            },

        });
    }

    function sendWAUniqueCode() {
        let phone = '62'+$('.phone').val().substring(1);
        let text = encodeURIComponent($('.wa').val());
        let unique_code = $('.uc').val();
        let args = {};
        
        args.fullname = $('.fullname').val();
        args.email = $('.email').val();
        args.phone = $('.phone').val();
        args.address = $('.address').val();
        args.user_uid = '{{ $current_user->uid }}';

        toastr.info("Harap menunggu, data sedang di proses", "Loading...");
        $('.send-wa-btn').prop('disabled', true);

        updateSend(unique_code, '{{ $current_user->uid }}', 'wa');

        $.ajax({
            type: "POST",
            url: '{{ $auth_partner_base_url }}'+'add-data-customer-wa',
            dataType: "json",
            data: args,
            cache : false,
            success: function(data){
                toastr.clear();
                
                if(data.code == 400) {
                    if(Array.isArray(data.message)) {
                        toastr.warning(data.message[0], "Peringatan");
                    } else {
                        toastr.warning(data.message, "Peringatan");
                    }
                } else if(data.code == 200) {
                    $('.fullname').val("");
                    $('.email').val("");
                    $('.phone').val("");
                    $('.address').val("");
                    $('.wa').val("");
                    $('#share-wa').modal('hide');

                    window.location.href = `https://api.whatsapp.com/send?phone=${phone}&text=${text}`;
                }

                $('.send-wa-btn').prop('disabled', false);
            } ,error: function(xhr, status, error) {
                console.log(error);
                toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
                $('.send-wa-btn').prop('disabled', false);
            },

        });
    }

    function updateSend(unique_code, user_uid, category) {
        let args = {};
        args.unique_code = unique_code;
        args.user_uid = user_uid;
        args.category = category;

        $.ajax({
            type: "POST",
            url: '{{ $auth_partner_base_url }}'+'update-send',
            dataType: "json",
            data: args,
            cache : false,
            success: function(data){
                toastr.clear();
                
                if(data.code == 400) {
                    if(Array.isArray(data.message)) {
                        toastr.warning(data.message[0], "Peringatan");
                    } else {
                        toastr.warning(data.message, "Peringatan");
                    }
                } else if(data.code == 200) {
                    table.ajax.reload();
                }
            } ,error: function(xhr, status, error) {
                console.log(error);
                toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
            },

        });
    }
</script>

@endsection