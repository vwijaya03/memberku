<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
		<meta name="description" content="AAI">
		<meta name="keywords" content="AAI">
		<meta name="author" content="AAI">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>AAI</title>
		<link rel="apple-touch-icon" href="{{ URL::asset('admin/app-assets/images/ico/apple-icon-120.png') }}">
		<link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('admin/app-assets/images/ico/favicon.ico') }}">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
		<!-- BEGIN VENDOR CSS-->
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/bootstrap.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/fonts/feather/style.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/fonts/font-awesome/css/font-awesome.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/fonts/flag-icon-css/css/flag-icon.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/extensions/pace.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/forms/icheck/icheck.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/forms/icheck/custom.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/extensions/toastr.css') }}">
		<!-- END VENDOR CSS-->
		<!-- BEGIN TERA CSS-->
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/bootstrap-extended.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/app.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/colors.min.css') }}">
		<!-- END TERA CSS-->
		<!-- BEGIN Page Level CSS-->
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/core/menu/menu-types/horizontal-menu.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/core/colors/palette-gradient.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/pages/login-register.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/plugins/extensions/toastr.min.css') }}">
		<!-- END Page Level CSS-->
		<!-- BEGIN Custom CSS-->
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/assets/css/style.css') }}">
		<!-- END Custom CSS-->
	</head>

	<body data-open="hover" data-menu="horizontal-menu" data-col="1-column" class="horizontal-layout horizontal-menu 1-column   menu-expanded blank-page blank-page">

    <!-- ////////////////////////////////////////////////////////////////////////////-->
	<div class="app-content content container-fluid">
		<div class="content-wrapper">
		<div class="content-header row">
		</div>
		<div class="content-body">
			<section class="flexbox-container">
				<div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1  box-shadow-2 p-0">
					<div class="card border-grey border-lighten-3 m-0">
						<div class="card-header no-border">
						    <div class="card-title text-xs-center">
						        <div class="p-1">AAI System</div>
						    </div>

						    <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2"><span>Login Area</span></h6>
						</div>

						<div class="card-body collapse in">
						    <div class="card-block">
						    	
								@if ($errors->has('email'))
									<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<strong>E-mail</strong> tidak boleh kosong !
									</div>
								@endif

								@if ($errors->has('password'))
									<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<strong>Password</strong> tidak boleh kosong !
									</div>
								@endif

								@if(Session::has('err'))
					                <div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										{{ Session::get('err') }}
									</div>
					            @endif

						        <form class="form-horizontal form-simple" action="{{ url($url_partner.'/login') }}" method="POST">
						        	{{ csrf_field() }}
						            <fieldset class="form-group position-relative has-icon-left mb-0">
						                <input type="text" class="form-control form-control-lg input-lg" id="user-name" placeholder="Masukkan E-mail" name="email">
						                <div class="form-control-position">
						                    <i class="ft-user"></i>
						                </div>
						            </fieldset>
						            <br>
						            <fieldset class="form-group position-relative has-icon-left">
						                <input type="password" class="form-control form-control-lg input-lg" id="user-password" placeholder="Masukkan Password" name="password">
						                <div class="form-control-position">
						                    <i class="fa fa-key"></i>
						                </div>
						            </fieldset>
						            
						            <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="ft-unlock"></i> Login</button>
						        </form>
								<br>
						        <div class="card card-inverse box-shadow-0 card-info">
									<div class="card-header">
										<h4 class="card-title" style="text-align: center;">Customer Care</h4>
									</div>
									<div class="card-body collapse in">
										<div class="card-block" style="padding-top: 0;">
											<p class="card-text" style="text-align: justify-all;"><i class='fa fa-whatsapp' style="font-size: 20px;"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WA 0813-5659-8383</p>
											<p class="card-text" style="text-align: justify-all;"><i class='ft-mail' style="font-size: 18px;"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;admin@ariusangkasa.com</p>
										</div>
									</div>
								</div>
						    </div>
						</div>
						<div class="card-footer">
						    <div class="">
						        <p class="float-sm-left text-xs-center m-0"><a href="{{ url($url_partner.'/reset-password') }}" class="card-link">Forgot password ?</a></p>
						        <p class="float-sm-right text-xs-center m-0"><a href="#" class="card-link" data-toggle="modal" data-target="#register-modal">Register</a></p>
						    </div>
						</div>
					</div>
				</div>
			</section>

		</div>
		</div>
	</div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->
	
	<!-- Register Modal -->
    <div class="modal fade text-xs-left" id="register-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    <label class="modal-title text-text-bold-600" id="myModalLabel33">Form Register</label>
                </div>

                <form>
                    <div class="modal-body">
                        <label>Nama Lengkap *</label>
	                    <div class="form-group">
	                        <input type="text" placeholder="Nama Lengkap" class="form-control fullname">
	                    </div>

	                    <label>Nama Pada Kartu</label>
	                    <div class="form-group">
	                        <input type="text" placeholder="Nama Pada Kartu" class="form-control name_on_card">
	                    </div>

	                    <label>E-mail *</label>
	                    <div class="form-group">
	                        <input type="text" placeholder="E-mail" class="form-control email">
	                    </div>

	                    <label>Telp / IG / Web</label>
	                    <div class="form-group">
	                        <input type="text" placeholder="Telp / IG / Web" class="form-control phone">
	                    </div>

	                    <label>Address(Max 35 karakter)</label>
	                    <div class="form-group">
	                        <input type="text" placeholder="Address" class="form-control address">
	                    </div>

	                    <label>City(Max 35 karakter)</label>
	                    <div class="form-group">
	                        <input type="text" placeholder="City" class="form-control city">
	                    </div>
                    </div>

                    <div class="modal-footer">
                        <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                        <input type="submit" class="btn btn-outline-primary btn register-btn" value="Register">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- BEGIN VENDOR JS-->
    <script src="{{ URL::asset('admin/app-assets/vendors/js/vendors.min.js') }}" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script type="text/javascript" src="{{ URL::asset('admin/app-assets/vendors/js/ui/jquery.sticky.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('admin/app-assets/vendors/js/charts/jquery.sparkline.min.js') }}"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/forms/icheck/icheck.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/extensions/toastr.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN TERA JS-->
    <script src="{{ URL::asset('admin/app-assets/js/core/app-menu.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/js/core/app.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/js/scripts/customizer.min.js') }}" type="text/javascript"></script>
    <!-- END TERA JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script type="text/javascript" src="{{ URL::asset('admin/app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js') }}"></script>
    <script src="{{ URL::asset('admin/app-assets/js/scripts/forms/form-login-register.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->

    <script type="text/javascript">
    	$(document).ready(function() {
    		$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});

	        $('.register-btn').on('click', register);

		    function register() {
		    	let args = {};
		    	args.fullname = $('.fullname').val();
		    	args.name_on_card = $('.name_on_card').val();
		    	args.email = $('.email').val();
		    	args.phone = $('.phone').val();
		    	args.address = $('.address').val();
		    	args.city = $('.city').val();
		    	args.role = 'partner';

		    	$('.register-btn').prop('disabled', true);
		    	toastr.info("Harap menunggu, data sedang di proses", "Loading...");

		    	$.ajax({
	                type: "POST",
	                url: '{{ $auth_partner_base_url }}'+'register',
	                dataType: "json",
	                data: args,
	                cache : false,
	                success: function(data){
	                	toastr.clear();
	                	
	                    if(data.code == 400) {
	                    	if(Array.isArray(data.message)) {
	                    		toastr.warning(data.message[0], "Peringatan");
	                    	} else {
	                    		toastr.warning(data.message, "Peringatan");
	                    	}
	                    } else if(data.code == 200) {
	                    	toastr.success("Data anda sedang di review", "Sukses");

	                    	$('#register-modal').modal('hide');
	                    	
	                        $('.fullname').val("");
							$('.name_on_card').val("");
							$('.email').val("");
							$('.phone').val("");
							$('.address').val("");
							$('.city').val("");
	                    }

	                    $('.register-btn').prop('disabled', false);
	                } ,error: function(xhr, status, error) {
	                	console.log(error);
	                    toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
	                    $('.register-btn').prop('disabled', false);
	                },

	            });
		    }
		});
    </script>
  </body>
</html>