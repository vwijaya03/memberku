@extends('partner/header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title">History Kode Unik</h4>
				            </div>
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
                                    
									<table width="1280px" class="table table-striped table-bordered dataex-html5-export server-side-unique-code">
										<thead>
											<tr>
												<th>No.</th>
												<th>Kode Unik</th>
												<th>Di Buat Tanggal</th>
											</tr>
										</thead>
									</table>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

<!-- Add User Modal -->
<div class="modal fade text-xs-left" id="share-wa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Customer</label>
            </div>

            <form action="#">
                <div class="modal-body">

                    <input type="hidden" class="uc" />
                    <label>Nama Lengkap</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Lengkap" class="form-control fullname">
                    </div>

                    <label>E-mail</label>
                    <div class="form-group">
                        <input type="text" placeholder="E-mail" class="form-control email">
                    </div>

                    <label>No. HP *</label>
                    <div class="form-group">
                        <input type="text" placeholder="No. HP" class="form-control phone">
                    </div>

                    <label>Alamat</label>
                    <div class="form-group">
                        <input type="text" placeholder="Alamat" class="form-control address">
                    </div>

                    <label>Text *</label>
                    <div class="form-group">
                        <textarea class="form-control wa" rows="8">
                        </textarea>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <a href="#" class="btn btn-outline-primary btn send-wa-btn">Kirim</a>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">
    let table, selected_unique_code = '';

	$(document).ready(function() {
        $('.generate-btn').on('click', addUniqueCode);
        $('.send-wa-btn').on('click', sendWAUniqueCode);

	    table = $('.server-side-unique-code').DataTable({
	    	"scrollX": !0,
	    	"lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
	        "processing": true,
	        "serverSide": true,
	        "ajax":{
	        	"type": "POST",
            	"url": "{{ url($auth_partner_base_url.'history-unique-code-ajax/'.$current_user->uid) }}",
            	"dataType": "json",
           	},
	        "columns": [
	            { "data": "no" },
	            { "data": "res.unique_code" },
	            { "data": "created_at" }
	        ],
	        order: [[2, 'desc']],
            "columnDefs": [
                { "orderable": false, "targets": [ 0, 2 ] },
                { "width": "80px", "targets": [ 0 ] },
                { "width": "220px", "targets": [ 1 ] },
                { "width": "200px", "targets": [ 0 ] },
            ]
	    });

	    function addUniqueCode() {
            let args = {};
            args.user_uid = '{{ $current_user->uid }}';

	    	$('.generate-btn').prop('disabled', true);
	    	toastr.info("Harap menunggu, data sedang di proses", "Loading...");

	    	$.ajax({
                type: "POST",
                url: '{{ $auth_partner_base_url }}'+'add-unique-code',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                	toastr.clear();
                	
                    if(data.code == 400) {
                    	if(Array.isArray(data.message)) {
                    		toastr.warning(data.message[0], "Peringatan");
                    	} else {
                    		toastr.warning(data.message, "Peringatan");
                    	}
                    } else if(data.code == 200) {
                    	toastr.success(data.message, "Sukses");
						table.ajax.reload();
                    }

                    $('.generate-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                	console.log(error);
                    toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
                    $('.generate-btn').prop('disabled', false);
                },

            });
	    }
	});

    function shareWA(obj) {
        $('.uc').val(obj.unique_code);

        toastr.info("Harap menunggu", "Loading...");

        $.ajax({
            type: "POST",
            url: '{{ $auth_partner_base_url }}'+'promo',
            dataType: "json",
            cache : false,
            success: function(data){
                toastr.clear();

                let text = `Terima kasih telah membeli barang di toko kami. \nBerikut promo yang kami sediakan, \n\n`;

                for (let i = 0; i < data.result.length; i++) {
                    text += `Promo ${i+1} \n\nhttps://ariusangkasa.com/promo/${data.result[i].uid}/kode-unik/${obj.unique_code} \n\n`;
                }
                
                $('.wa').val(text);
                $('#share-wa').modal('show'); 
            } ,error: function(xhr, status, error) {
                console.log(error);
                toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
                $('.save-btn').prop('disabled', false);
            },

        });
    }

    function sendWAUniqueCode() {
        let phone = '62'+$('.phone').val().substring(1);
        let text = encodeURI($('.wa').val());
        let unique_code = $('.uc').val();
        let args = {};
        
        args.fullname = $('.fullname').val();
        args.email = $('.email').val();
        args.phone = $('.phone').val();
        args.address = $('.address').val();
        args.user_uid = '{{ $current_user->uid }}';

        toastr.info("Harap menunggu, data sedang di proses", "Loading...");
        $('.send-wa-btn').prop('disabled', true);

        updateSend(unique_code, '{{ $current_user->uid }}', 'email');

        $.ajax({
            type: "POST",
            url: '{{ $auth_partner_base_url }}'+'add-data-customer-wa',
            dataType: "json",
            data: args,
            cache : false,
            success: function(data){
                toastr.clear();
                
                if(data.code == 400) {
                    if(Array.isArray(data.message)) {
                        toastr.warning(data.message[0], "Peringatan");
                    } else {
                        toastr.warning(data.message, "Peringatan");
                    }
                } else if(data.code == 200) {
                    $('.fullname').val("");
                    $('.email').val("");
                    $('.phone').val("");
                    $('.address').val("");
                    $('.wa').val("");
                    $('#share-wa').modal('hide');
                    
                    window.open(`https://api.whatsapp.com/send?phone=${phone}&text=${text}`);
                }

                $('.send-wa-btn').prop('disabled', false);
            } ,error: function(xhr, status, error) {
                console.log(error);
                toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
                $('.send-wa-btn').prop('disabled', false);
            },

        });
    }

    function updateSend(unique_code, user_uid, category) {
        let args = {};
        args.unique_code = unique_code;
        args.user_uid = user_uid;
        args.category = category;

        $.ajax({
            type: "POST",
            url: '{{ $auth_partner_base_url }}'+'update-send',
            dataType: "json",
            data: args,
            cache : false,
            success: function(data){
                toastr.clear();
                
                if(data.code == 400) {
                    if(Array.isArray(data.message)) {
                        toastr.warning(data.message[0], "Peringatan");
                    } else {
                        toastr.warning(data.message, "Peringatan");
                    }
                } else if(data.code == 200) {
                    table.ajax.reload();
                }
            } ,error: function(xhr, status, error) {
                console.log(error);
                toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
            },

        });
    }
</script>

@endsection