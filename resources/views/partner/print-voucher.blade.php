@extends('partner/header')

@section('content')
<style type="text/css">
    .fa {
        font-size: 20px !important;
    }  

    .ft-mail {
        font-size: 17px !important;
    }  
</style>

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title">Print Voucher</h4>
				            </div>
				            <div class="card-body collapse in">

								<div class="card-block card-dashboard">

                                    @if(Session::has('err'))
                                        <div class="alert alert-danger alert-dismissible">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            {{ Session::get('err') }}
                                        </div>
                                    @endif
                                    
                                    <a href="#" class="btn btn-success mr-1 mb-1 generate-btn" data-toggle="modal" data-target="#print-voucher">Print Voucher</a>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

<!-- Whats App Modal -->
<div class="modal fade text-xs-left" id="print-voucher" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33"><b>Print Voucher</b></label>
            </div>

            <form action="{{ $url_post_print_voucher }}" method="POST">
                <div class="modal-body">
                
                    {!! csrf_field() !!}

                    <label>Jumlah Halaman * ({{ $title_jumlah_per_halaman }})</label>
                    <div class="form-group">
                        <input type="text" placeholder="Jumlah Halaman" class="form-control" name="page" value="1">
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <button class="btn btn-outline-primary btn" type="submit" onclick="notify()">Print</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('server_side_datatable')

<script type="text/javascript">
    function notify() {
        toastr.info("Harap menunggu, data sedang di proses", "Loading...");
    }
</script>

@endsection