<?php 
    use App\TaskModel;
    $this->taskModel = new TaskModel();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 

        <style type="text/css">
            table, th, td {
                border: 1px solid black;
            }

            a {
                color: black;
                text-decoration: none;
            }

            .header {
                font-size: 11px;
                text-align: center;
            }

            .txt-voucher {
                font-size: 10px;
                text-align: center;
            }

            .txt-promo-member {
                font-size: 10px;
                text-align: center;
                word-wrap: break-word;
            }

            .txt-expired-date {
                font-size: 10px;
                text-align: center;
            }

            * {
				box-sizing: border-box;
			}

            .txt-voucher-header {
                font-size: 8px;
                text-align: center;
            }

            .txt-expired-date-header {
                font-size: 8px;
                text-align: center;
            }

			/* Create two equal columns that floats next to each other */
			.column {
				float: left;
				width: 50%;
				word-wrap: break-word;
			}

			/* Clear floats after the columns */
			.row:after {
				display: table;
				clear: both;
			}
        </style>
    </head>
    
    <body>
        <table width="100%">
            <?php 
                $count_j = 0; 
                $count_k = 0; 
                $total_data = $total_data - 1;
            ?>
            
            @for($i = 0; $i < $total_row; $i++)
            <tr>
                @for($j = 0; $j < 5; $j++)
                    @if($count_j <= $total_data)
                        <td height="130px">
                            <p class="txt-voucher">Kode Voucher: <b>{{ $unique_codes[$count_j]['unique_code'] }}</b></p>
                            <p class="txt-promo-member"><b><a href="https://myvoucher.id/promo-wa/{{ $unique_codes[$count_j]['unique_code'] }}/noc/{{ urlencode($judul_link) }}" style="font-size: 12px !important;">https://myvoucher.id/promo-wa/{{ $unique_codes[$count_j]['unique_code'] }}/noc/{{ urlencode($judul_link) }}</a></b></p>
                        </td>                        
                    @endif
                    
                    {{ $count_j++ }}
                @endfor
            </tr>
            @endfor

            <?php 
                $this->taskModel->postUpdateStatusTask($task_id);
            ?>
        </table>
    </body>
</html>