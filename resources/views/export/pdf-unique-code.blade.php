<?php 
    use App\TaskModel;
    $this->taskModel = new TaskModel();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 

        <style type="text/css">
            table, th, td {
                border: 1px solid black;
            }

            a {
                color: black;
                text-decoration: none;
            }

            .header {
                font-size: 11px;
                text-align: center;
            }

            .txt-voucher {
                font-size: 10px;
                text-align: center;
            }

            .txt-promo-member {
                font-size: 10px;
                text-align: center;
            }

            .txt-expired-date {
                font-size: 10px;
                text-align: center;
            }
        </style>
    </head>
    
    <body>
        <table width="100%">
            <?php 
                $count_j = 0; 
                $total_data = $total_data - 1;
            ?>
            
            @for($i = 0; $i < $total_row; $i++)
            <tr>
                @for($j = 0; $j < 4; $j++)
                    @if($count_j <= $total_data)
                        <td height="130px">
                            <p class="header"><b>{{ $judul_link }}</b></p>
                            <p class="txt-voucher">Kode Voucher: <b>{{ $unique_codes[$count_j]['unique_code'] }}</b></p>
                            <p class="txt-promo-member">Masukkan kode voucher diatas di : <br> <b><a href="https://myvoucher.id" style="font-size: 12px !important;">myvoucher.id</a></b></p>
                            <p class="txt-expired-date">Berlaku hingga : <b>{{ date('d/m/Y', strtotime($unique_codes[$count_j]['expired_date']) ) }}</b></p>
                        </td>                        
                    @endif
                    
                    {{ $count_j++ }}
                @endfor
            </tr>
            @endfor

            <?php 
                $this->taskModel->postUpdateStatusTask($task_id);
            ?>
        </table>
    </body>
</html>