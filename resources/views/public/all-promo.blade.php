<?php 
    use App\Http\Controllers\HelperController;
?>

@extends('public/header')

@section('content')
<br>
<section id="description" class="card">
    <div class="card-header">
        <h4 class="card-title" style="text-align: center;">
            @if($link_title != null) {{ $link_title }} @else @endif <br><br>
        </h4>
    </div>
</section>

<div class="content-body">
    <div class="row match-height">

        @foreach($promos_internal as $promo_internal)
        <div class="col-xl-4 col-md-6 col-sm-12">
            <div class="card">
                <div class="card-body">
                    <h5 style="position: absolute;"></h5>
                    <img class="card-img-top img-fluid" src="{{ $public_base_url.$promo_internal->img }}" alt="Card image cap" width="100%" style="height: 300px">
                    <div class="card-block">
                        <h4 class="card-title">{{ urldecode($promo_internal->title) }}</h4>
                        @if($setting != null)
                            @if($setting->visibility_remaining_click == 'yes')
                                <p class="card-text remaining-click" style="word-wrap: break-word;"><b>Remaining Use: {{ $voucher->click_limit }}</b></p>
                            @endif
                        @endif
                        
                        @if($voucher != null) 
                            
                            @if(HelperController::getDayDifferenceFromDate(date('Y-m-d'), $voucher->expired_date) > HelperController::getDayDifferenceFromDate(date('Y-m-d'), $promo_internal->expired_date))
                                Berlaku hingga : {{ date('d/m/Y', strtotime($promo_internal->expired_date) ) }} 
                            @else
                                Berlaku hingga : {{ date('d/m/Y', strtotime($voucher->expired_date) ) }}
                            @endif
                        
                        @endif

                        <br><br> 
                        
                        <p class="card-text" style="word-wrap: break-word;">Kode Promo {{ $voucher->unique_code }}</p>
                        <p class="card-text" style="word-wrap: break-word;"><?php echo(urldecode($promo_internal->description)); ?></p>

                        @if($voucher->click_limit > 0)
                            @if($promo_internal->use_pin == 'yes')

                                @if($promo_internal->is_record_exist == 'yes')
                                    <button class="btn btn-outline-danger" style="border: 3px solid;" disabled>HABIS</button>
                                @else
                                    <button class="btn btn-outline-primary use-btn btn{{ $promo_internal->uid }}" style="border: 3px solid;" onclick="inputPinPI('{{ $promo_internal->uid }}')">USE</button>
                                @endif
                                
                            @else
                                @if($promo_internal->is_record_exist == 'yes')
                                    <button class="btn btn-outline-danger" style="border: 3px solid;" disabled>HABIS</button>
                                @else
                                    <button class="btn btn-outline-primary use-btn btn{{ $promo_internal->uid }}" style="border: 3px solid;" onclick="directUsePI('{{ $promo_internal->uid }}', '{{ $voucher->unique_code }}', '{{ $voucher->user_uid }}')">USE</button>
                                @endif
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>

<div class="content-body">
    <div class="row match-height">

        @foreach($promos as $promo)
        <div class="col-xl-4 col-md-6 col-sm-12">
            <div class="card">
                <div class="card-body">
                    <h5 style="position: absolute;"></h5>
                    <img class="card-img-top img-fluid" src="{{ $public_base_url.$promo->img }}" alt="Card image cap" width="100%" style="height: 300px">
                    <div class="card-block">
                        <h4 class="card-title">{{ urldecode($promo->title) }}</h4>
                        @if($setting != null)
                            @if($setting->visibility_remaining_click == 'yes')
                                <p class="card-text remaining-click" style="word-wrap: break-word;"><b>Remaining Use: {{ $voucher->click_limit }}</b></p>
                            @endif
                        @endif
                        
                        @if($voucher != null) 
                            
                            @if(HelperController::getDayDifferenceFromDate(date('Y-m-d'), $voucher->expired_date) > HelperController::getDayDifferenceFromDate(date('Y-m-d'), $promo->expired_date))
                                Berlaku hingga : {{ date('d/m/Y', strtotime($promo->expired_date) ) }} 
                            @else
                                Berlaku hingga : {{ date('d/m/Y', strtotime($voucher->expired_date) ) }}
                            @endif
                        
                        @endif

                        <br><br>
                        
                        <p class="card-text" style="word-wrap: break-word;">Kode Promo {{ $voucher->unique_code }}</p>
                        <p class="card-text" style="word-wrap: break-word;"><?php echo(urldecode($promo->description)); ?></p>

                        @if($voucher->click_limit > 0)
                            @if($promo->use_pin == 'yes')

                                @if($promo->is_record_exist == 'yes')
                                    <button class="btn btn-outline-danger" style="border: 3px solid;" disabled>HABIS</button>
                                @else
                                    <button class="btn btn-outline-primary use-btn btn{{ $promo->uid }}" style="border: 3px solid;" onclick="inputPin('{{ $promo->uid }}')">USE</button>
                                @endif
                                
                            @else
                                @if($promo->is_record_exist == 'yes')
                                    <button class="btn btn-outline-danger" style="border: 3px solid;" disabled>HABIS</button>
                                @else
                                    <button class="btn btn-outline-primary use-btn btn{{ $promo->uid }}" style="border: 3px solid;" onclick="directUse('{{ $promo->uid }}', '{{ $voucher->unique_code }}', '{{ $voucher->user_uid }}')">USE</button>
                                @endif
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>

<!-- Pin Modal -->
<div class="modal fade text-xs-left" id="pin-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Masukkan Pin</label>
            </div>

            <div class="modal-body">
                <div class="row">

                    <div class="col-md-12">
                        <label>Pin</label>
                        <div class="form-group">
                            <input type="text" class="form-control pin" name="pin">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                <button class="btn btn-outline-primary btn send-btn" onclick="use('{{ $voucher->unique_code }}', '{{ $voucher->user_uid }}')">Kirim</button>
            </div>
        </div>
    </div>
</div>

<!-- Pin PI Modal -->
<div class="modal fade text-xs-left" id="pin-pi-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Masukkan Pin</label>
            </div>

            <div class="modal-body">
                <div class="row">

                    <div class="col-md-12">
                        <label>Pin</label>
                        <div class="form-group">
                            <input type="text" class="form-control pin-pi" name="pin-pi">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                <button class="btn btn-outline-primary btn send-btn" onclick="usePI('{{ $voucher->unique_code }}', '{{ $voucher->user_uid }}')">Kirim</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js_script')

<script type="text/javascript">
    let global_promo_uid = '';

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function use(uc, user_uid) {
        let args = {};
        args.promo_uid = global_promo_uid;  
        args.unique_code = uc;  
        args.user_uid = user_uid;  
        args.pin = $('.pin').val();  

        toastr.info("Harap menunggu, data sedang di proses", "Loading...");
        // $('.btn'+escapeSelector(global_promo_uid)).prop('disabled', true);

        $.ajax({
            type: "POST",
            url: '{{ $public_base_url }}'+'/use-promo',
            dataType: "json",
            data: args,
            cache : false,
            success: function(data){
                toastr.clear();
              
                if(data.code == 400) {
                    if(Array.isArray(data.message)) {
                      toastr.warning(data.message[0], "Peringatan");
                    } else {
                      toastr.warning(data.message, "Peringatan");
                    }
                } else if(data.code == 200) {
                    if(data.remaining_click < 1) 
                    {
                        $('.use-btn').hide();
                        // let element_class = 'use-btn-'+global_promo_uid;
                        // document.getElementById(element_class).style.visibility = 'hidden';
                    }

                    $('.remaining-click').text(`Remaining Use: `+data.remaining_click).css({ 'font-weight': 'bold' });
                    $('.btn'+escapeSelector(global_promo_uid)).removeClass('btn-outline-primary');
                    $('.btn'+escapeSelector(global_promo_uid)).addClass('btn-outline-danger');
                    $('.btn'+escapeSelector(global_promo_uid)).text('HABIS');

                    toastr.success(data.message, "Sukses");
                    $('.btn'+escapeSelector(global_promo_uid)).prop('disabled', true);
                }

                $('.pin').val("");
                $('#pin-modal').modal('hide');
                // $('.send-btn').prop('disabled', false);
                
            } ,error: function(xhr, status, error) {
              console.log(error);
                toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                $('.send-btn').prop('disabled', false);
            },

        });
    }

    function usePI(uc, user_uid) {
        let args = {};
        args.promo_uid = global_promo_uid;  
        args.unique_code = uc;  
        args.user_uid = user_uid;  
        args.pin = $('.pin-pi').val();  
        
        toastr.info("Harap menunggu, data sedang di proses", "Loading...");
        // $('.send-btn').prop('disabled', true);

        $.ajax({
            type: "POST",
            url: '{{ $public_base_url }}'+'/use-promo-internal',
            dataType: "json",
            data: args,
            cache : false,
            success: function(data){
                toastr.clear();
              
                if(data.code == 400) {
                    if(Array.isArray(data.message)) {
                      toastr.warning(data.message[0], "Peringatan");
                    } else {
                      toastr.warning(data.message, "Peringatan");
                    }
                } else if(data.code == 200) {
                    if(data.remaining_click < 1) 
                    {
                        $('.use-btn').hide();
                        // let element_class = 'use-btn-'+global_promo_uid;
                        // document.getElementById(element_class).style.visibility = 'hidden';
                    }

                    $('.remaining-click').text(`Remaining Use: `+data.remaining_click).css({ 'font-weight': 'bold' });
                    $('.btn'+escapeSelector(global_promo_uid)).removeClass('btn-outline-primary');
                    $('.btn'+escapeSelector(global_promo_uid)).addClass('btn-outline-danger');
                    $('.btn'+escapeSelector(global_promo_uid)).text('HABIS');

                    toastr.success(data.message, "Sukses");
                    $('.btn'+escapeSelector(global_promo_uid)).prop('disabled', true);
                }

                $('.pin-pi').val("");
                $('#pin-pi-modal').modal('hide');
                // $('.send-btn').prop('disabled', false);
                
            } ,error: function(xhr, status, error) {
                console.log(error);
                toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                $('.send-btn').prop('disabled', false);
            },

        });
    }

    function directUse(promo_uid, uc, user_uid) {
        let args = {};
        args.promo_uid = promo_uid;  
        args.unique_code = uc;  
        args.user_uid = user_uid;  

        toastr.info("Harap menunggu, data sedang di proses", "Loading...");
        // $('.use-btn').prop('disabled', true);

        $.ajax({
            type: "POST",
            url: '{{ $public_base_url }}'+'/direct-use-promo',
            dataType: "json",
            data: args,
            cache : false,
            success: function(data){
                toastr.clear();
              
                if(data.code == 400) {
                    if(Array.isArray(data.message)) {
                      toastr.warning(data.message[0], "Peringatan");
                    } else {
                      toastr.warning(data.message, "Peringatan");
                    }
                } else if(data.code == 200) {
                    if(data.remaining_click < 1) 
                    {
                        $('.use-btn').hide();
                        // let element_class = 'use-btn-'+global_promo_uid;
                        // document.getElementById(element_class).style.visibility = 'hidden';
                    }

                    $('.remaining-click').text(`Remaining Use: `+data.remaining_click).css({ 'font-weight': 'bold' });
                    $('.btn'+escapeSelector(promo_uid)).removeClass('btn-outline-primary');
                    $('.btn'+escapeSelector(promo_uid)).addClass('btn-outline-danger');
                    $('.btn'+escapeSelector(promo_uid)).text('HABIS');

                    toastr.success(data.message, "Sukses");
                    $('.btn'+escapeSelector(promo_uid)).prop('disabled', true);
                }

                // $('.use-btn').prop('disabled', false);
                
            } ,error: function(xhr, status, error) {
              console.log(error);
                toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                $('.use-btn').prop('disabled', false);
            },

        });
    }

    function directUsePI(promo_uid, uc, user_uid) {
        let args = {};
        args.promo_uid = promo_uid;  
        args.unique_code = uc;  
        args.user_uid = user_uid;  

        toastr.info("Harap menunggu, data sedang di proses", "Loading...");
        // $('.use-btn').prop('disabled', true);

        $.ajax({
            type: "POST",
            url: '{{ $public_base_url }}'+'/direct-use-promo-internal',
            dataType: "json",
            data: args,
            cache : false,
            success: function(data){
                toastr.clear();
              
                if(data.code == 400) {
                    if(Array.isArray(data.message)) {
                      toastr.warning(data.message[0], "Peringatan");
                    } else {
                      toastr.warning(data.message, "Peringatan");
                    }
                } else if(data.code == 200) {
                    if(data.remaining_click < 1) 
                    {
                        $('.use-btn').hide();
                        // let element_class = 'use-btn-'+global_promo_uid;
                        // document.getElementById(element_class).style.visibility = 'hidden';
                    }

                    $('.remaining-click').text(`Remaining Use: `+data.remaining_click).css({ 'font-weight': 'bold' });
                    $('.btn'+escapeSelector(promo_uid)).removeClass('btn-outline-primary');
                    $('.btn'+escapeSelector(promo_uid)).addClass('btn-outline-danger');
                    $('.btn'+escapeSelector(promo_uid)).text('HABIS');

                    toastr.success(data.message, "Sukses");
                }

                // $('.use-btn').prop('disabled', false);
                $('.btn'+escapeSelector(promo_uid)).prop('disabled', true);
            } ,error: function(xhr, status, error) {
              console.log(error);
                toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                $('.use-btn').prop('disabled', false);
            },

        });
    }

    function inputPin(promo_uid) {
        global_promo_uid = promo_uid;

        $('#pin-modal').modal('show');
    }

    function inputPinPI(promo_uid) {
        global_promo_uid = promo_uid;

        $('#pin-pi-modal').modal('show');
    }

    function escapeSelector(s){
        return s.replace( /(:|\.|\[|\])/g, "\\$1" );
    }
</script>

@endsection