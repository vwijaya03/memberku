@extends('public/header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
                                    
									<div class="col-md-12">
                                            <form action="#">
                                                <div class="modal-body">
                                                    @if($link_title != null || $link_title != "")
                                                        <div class="card-block" align="center">
                                                            {{ urldecode($link_title) }}
                                                        </div>  
                                                    @endif

                                                    @if($company_logo != null || $company_logo != "")
                                                        <div class="card-block" align="center">
                                                            <img src="{{ URL::asset($company_logo) }}" style="height: 60px;" alt="Card image">
                                                        </div>  
                                                    @endif

                                                    <p align="center"><label class="modal-title text-text-bold-600" id="myModalLabel33">PENDAFTARAN</label></p>
                                                     
                                                    <br><br>
                                                    
                                                    <label>Nama Lengkap *</label>
                                                    <div class="form-group">
                                                        <input type="text" placeholder="Nama Lengkap" class="form-control fullname">
                                                    </div>
                                
                                                    <label>No HP *</label>
                                                    <div class="form-group">
                                                        <input type="text" placeholder="No HP" class="form-control phone">
                                                    </div>

                                                    <label>Password *</label>
                                                    <div class="form-group">
                                                        <input type="password" placeholder="Password" class="form-control password">
                                                    </div>
                                                    
                                                </div>
                                
                                                <div class="modal-footer">
                                                    <input type="submit" class="btn btn-outline-primary btn save-btn" value="Simpan">
                                                </div>
                                            </form>
                                    </div>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

	$(document).ready(function() {
        $('.save-btn').on('click', addAgent);

	    function addAgent() {
	    	let args = {};
	    	args.fullname = $('.fullname').val();
	    	args.phone = $('.phone').val();
	    	args.password = $('.password').val();
            args.code = "{{ $user_code }}";

	    	$('.save-btn').prop('disabled', true);
	    	toastr.info("Harap menunggu, data sedang di proses", "Loading...");

	    	$.ajax({
                type: "POST",
                url: '{{ $public_base_url }}'+'/registration',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                	toastr.clear();
                	
                    if(data.code == 400) {
                    	if(Array.isArray(data.message)) {
                    		toastr.warning(data.message[0], "Peringatan");
                    	} else {
                    		toastr.warning(data.message, "Peringatan");
                    	}
                    } else if(data.code == 200) {
                    	toastr.success(data.message, "Sukses");

                    	$('.fullname').val("");
                    	$('.phone').val("");
                    	$('.password').val("");

                        toastr.success("Tunggu sebentar, sedang mengarahkan ke halaman login", "Sukses");

                        setTimeout(function() { 
                            window.location.href = "{{ url('/client') }}"; 
                        }, 3000);
                    }

                    $('.save-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                	console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.save-btn').prop('disabled', false);
                },

            });
	    }
	});

</script>

@endsection