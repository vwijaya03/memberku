@extends('public/header')

@section('content')
<style type="text/css">
    .ft-info {
        font-size: 22px !important;
    }  

    .voucher-bonus-img {
        display: block !important;
        margin-left: auto !important;
        margin-right: auto !important;
        width: 60% !important;
    }

    .modal {
        overflow-y: auto !important;
    }

    .modal-body {
        overflow-y: hidden !important;
    }

    .center {
        width: 80%;
        /* border: 3px solid green; */
        padding: 10px;
        position: absolute; 
        left: 0; 
        right: 0; 
        margin-left: auto; 
        margin-right: auto;
        top: 50px;
        color: white;
        align: center;
    }

    #data-benefit {
        width: 350px;
        margin: auto;
    }

</style>

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">

									<div class="col-md-12">   
                                        <h5 align="center">
                                            <b>Struk Ditukar: <span class="total-claim">{{ $total_claim }}</span> </b> 
                                            @if($current_user->phone == "081330231182" || $current_user->phone == "081330231183")
                                            <br><br> 
                                            <span>
                                                <a href="#" class="btn btn-success mr-1 mb-1" data-toggle="modal" data-target="#jumlah-redeem-modal">Redeem</a>
                                            </span> 
                                            @endif
                                        </h5>
                                        
                                        <br>                                    
                                        {{-- <h6 align="center" style="font-size: 12px;"><b>MERCHANT AKAN BERTAMBAH SETIAP BULAN</b></h6> <br><br> --}}
                                        {{-- <h6 align="center" style="font-size: 12px;"><b>** Uang akan bertambah disaldo akun, bukan dipotong distruk</b></h6> --}}

                                        <table width="100%" class="table table-striped table-bordered dataex-html5-export server-side-merchant">
                                            <thead>
                                                <tr>
                                                    <th>Merchant</th>
                                                    <th></th>
                                                    <th>Contoh Struk</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

<!-- Redeem Modal -->
<div class="modal fade text-xs-left" id="jumlah-redeem-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>                
            </div>

            <form action="#" enctype="multipart/form-data" id="upload_promo">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="form-group">
                                <label>Masukkan Jumlah Redeem *</label>
                                <input type="text" class="form-control jumlah-redeem" placeholder="Masukkan Jumlah Redeem...">
                            </div>

                            <div class="modal-footer" style="border-top: 0px !important;">
                                <button type="button" class='btn btn-info mr-1 mb-1 btn-jumlah-redeem' onclick="redeem()">Kirim</button>
                            </div>
                                
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Claim Gift Example Struk Modal -->
<div class="modal fade text-xs-left" id="claim-gift-ex-struk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>    
                
                <label class="modal-title text-text-bold-600" id="myModalLabel33" style="text-align: center !important; display: contents !important;"><h5><b>Tunjukkan Copy Struk Pada Kasir</b></h5></label>
            </div>

            <form action="#" enctype="multipart/form-data" id="upload_promo">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <img src="" width="100%" class="ex-struk">
                        </div>
                    </div>
                </div>

                <div class="modal-footer" style="border-top: 0px !important;">
                    <img src="{{ URL::asset('img/logo_aai_with_txt.jpeg') }}" height="60px" style="float: left;">
                    <button type="button" class='btn btn-info mr-1 mb-1' onclick="nextToProfileCard()">Lanjut</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Add Voucher Bonus Modal -->
<div class="modal fade text-xs-left" id="data-benefit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <form action="#" enctype="multipart/form-data" id="upload_promo">
                <div class="modal-body">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="center">                            
                                <p align="center"><b>Tunjukkan Halaman Ini Pada Kasir </b></p>
                                
                                <!-- <p align="center">Pastikan kasir menginput / mencatat nomor ID anda</p> <br> -->
    
                                <h4 class="card-title" align="center">
                                    @if($mod_user_code != null) ID: {{ $user_code }} @endif
                                </h4>
    
                                <br><br>
                                
                                <div align="center">
                                    <img src="{{ URL::asset('img/logo_aai_with_txt.jpeg') }}" height="60px" width="120px"><br>
                                    {!! QrCode::size(120)->generate($user_code) !!}
                                </div>
    
                            </div>
    
                            <img src="{{ URL::asset('img/bg_id_card.png') }}" width="100%">
                        </div>
                       
                    </div>
                </div>

                <div class="modal-footer">
                    <a href="#" class='btn btn-info mr-1 mb-1' onclick="nextToInsertNominal()">Lanjut</a>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Claim Gift Insert Nominal Modal -->
<div class="modal fade text-xs-left" id="claim-gift-insert-nominal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>                
            </div>

            <form action="#" enctype="multipart/form-data" id="upload_promo">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="form-group">
                                <label>Masukkan Nominal Struk *</label>
                                <input type="text" class="form-control jumlah-nominal" placeholder="Masukkan Nominal Struk...">
                            </div>

                            <div class="form-group">
                                <label>Masukkan Nomor Struk **</label>
                                <input type="text" class="form-control no-struk" placeholder="Masukkan Nomor Struk...">
                            </div>

                            <div class="form-group">
                                <label>Masukkan Nomor Register ***</label>
                                <input type="text" class="form-control no-reg" placeholder="Masukkan Nomor Register...">
                            </div>

                            <div class="modal-footer" style="border-top: 0px !important;">
                                <button type="button" class='btn btn-info mr-1 mb-1 jumlah-nominal' onclick="nextToAgreement()">Lanjut</button>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <img src="" width="100%" class="ex-struk">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Claim Gift Agreement Modal -->
<div class="modal fade text-xs-left" id="claim-gift-agreement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33"></label>
            </div>

            <form action="#" enctype="multipart/form-data" id="upload_promo">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12 col-lg-6 col-xs-12">
                            <div class="card border-grey border-lighten-2">
                                <div class="text-xs-center">
                                    <div class="card-block">
                                        <p align="center"><b><u>SYARAT DAN KETENTUAN</u></b></p>
                                        <p align="left">1. Struk hanya dapat ditukar dengan hadiah jika No ID anda tercatat di kasir merchant</p>
                                        <p align="left">2. Verifikasi selesai setiap tanggal 15 bulan berikutnya</p>
                                        <p align="left">3. Hadiah dapat dibatalkan jika terdapat kejanggalan atau kecurangan</p>
                                        <p align="left">4. Syarat dan ketentuan ini dapat berubah sewaktu-waktu</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer" style="border-top: 0px !important;">
                    <button class='btn btn-info mr-1 mb-1 btn-nominal' onclick="agree()">Setuju</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Add Voucher Bonus Result Modal -->
<div class="modal fade text-xs-left" id="voucher-bonus-result" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <form action="#" enctype="multipart/form-data" id="upload_promo">
                <div class="modal-body">
                    <div class="row">
                        
                    <div class="col-xl-12 col-lg-6 col-xs-12">

                        <div class="card border-grey border-lighten-2">
                            <div class="text-xs-center">
                                <div class="card-block">
                                    <img src="{{ URL::asset('img/success_icon.png') }}" class="rounded-circle" style="height: 60px;" alt="Card image">
                                </div>
                                <div class="card-block">
                                    <h4 class="card-title">TUKAR STRUK SEDANG DIPROSES</h4> <br><br>
                                    {{-- <h4 class="card-title">SILAHKAN LOGIN UNTUK MELIHAT SALDO</h4> --}}
                                    <!-- <h6 class="card-subtitle text-muted">Anda akan mendapatkan bonus cash <span class="p-nominal"></span></h6> <br><br>
                                    <h6 class="card-subtitle text-muted">STATUS</h6> <br>
                                    <h6 class="card-subtitle text-muted"><input class="btn btn-secondary mr-1 mb-1" value="Menunggu Approval" /></h6> <br><br>
                                    <h6 class="card-subtitle text-muted">Jika sudah terverifikasi bonus cash bisa di lihat di menu saldo cash</h6> -->
                                </div>
                            </div>
                        </div>

                    </div>

                    </div>
                </div>

                <div class="modal-footer" style="border-top: 0px !important;">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="SELESAI">
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">
    let table, table_pi, merchant_uid = '';
    let nominal, no_struk, no_reg = null;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.jumlah-nominal').keyup(function() {
        $('.jumlah-nominal').val(formatRupiah($('.jumlah-nominal').val()));
    });

	$(document).ready(function() {
        table = $('.server-side-merchant').DataTable({
            "lengthChange": false,
	    	"lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
	        "processing": true,
	        "serverSide": true,
	        "ajax":{
	        	"type": "POST",
            	"url": "{{ url('/merchant-join-bonus-ajax') }}",
            	"dataType": "json",
           	},
	        "columns": [
                { "data": "name" },
	            { "data": "action_btn" },
	            { "data": "img" },
	        ],
	        order: [[1, 'asc']],
            "columnDefs": [
                { "orderable": false, "targets": [ 1, 2 ] },
                { "width": "190px", "targets": [ 1 ] },
                // { "width": "120px", "targets": [ 5 ] },
            ]
	    });
	});

    function detailBonus(merchant) {
        // $('.merchant').html(decodeURIComponent(merchant));
        // $('.title').html(decodeURIComponent(title));
        // $('.description').html(decodeURIComponent(description));
        // $('.voucher-bonus-img').attr("src", img_path);
        merchant_uid = merchant.uid;
        
        var img = "{{ url('/') }}"+merchant.img;

        $('.merchant-name').text(decodeURIComponent(merchant.name.toUpperCase().replace(/\+/g, ' ')));
        $('.ex-struk').attr("src", img)
        $('#claim-gift-ex-struk').modal({
            backdrop: 'static', 
            keyboard: false
        });
    }

    function nextToProfileCard() {
        $('#claim-gift-ex-struk').modal('hide');
        $('#data-benefit').modal({backdrop: 'static', keyboard: false});
    }

    function nextToInsertNominal() {
        $('#data-benefit').modal('hide');
        $('#claim-gift-insert-nominal').modal({backdrop: 'static', keyboard: false});
    }

    function nextToAgreement() {
        nominal = $('.jumlah-nominal').val();
        no_struk = $('.no-struk').val();
        no_reg = $('.no-reg').val();

        if(nominal == null || nominal == 0 || nominal == "") {
            toastr.warning("Jumlah nominal tidak 0", "Peringatan");
            $('.jumlah-nominal').val(0);

            return
        }

        if(no_struk == null || no_struk == "") {
            toastr.warning("Nomor struk tidak boleh kosong", "Peringatan");
            $('.jumlah-nominal').val("");

            return
        }

        if(no_reg == null || no_reg == "") {
            toastr.warning("Nomor register tidak boleh kosong", "Peringatan");
            return
        }

        $('#claim-gift-insert-nominal').modal('hide');
        $('#claim-gift-agreement').modal({backdrop: 'static', keyboard: false});
    }

    // function verifyPin() {
    //     toastr.info("Harap menunggu, data sedang di proses", "Loading...");
    //     $('.btn-pin').prop('disabled', true);

    //     setTimeout(function() { 
    //         toastr.clear();
    //         toastr.success("Pin berhasil di verifikasi", "Sukses");
    //         toastr.clear();
    //         $('#claim-gift-insert-nominal').modal('hide');
    //         $('#claim-gift-agreement').modal({backdrop: 'static', keyboard: false});

    //         $('.btn-pin').prop('disabled', false);
    //     }, 3000);
    // }

    function agree() {
        toastr.info("Harap menunggu, data sedang di proses", "Loading...");
        $('.btn-nominal').prop('disabled', true);

        let args = {};
        args.merchant_uid = merchant_uid;
        args.user_uid = "{{ $user_uid }}";
        args.nominal = nominal.replace(/\./g, '');
        args.no_struk = no_struk;
        args.no_reg = no_reg;

        $.ajax({
            type: "POST",
            url: '{{ url("/") }}'+'/claim-gift',
            dataType: "json",
            data: args,
            cache : false,
            success: function(data){
                toastr.clear();
              
                if(data.code == 400) {
                    if(Array.isArray(data.message)) {
                      toastr.warning(data.message[0], "Peringatan");
                    } else {
                      toastr.warning(data.message, "Peringatan");
                    }
                } else if(data.code == 200) {
                    $('#claim-gift-agreement').modal('hide');
                    $('#voucher-bonus-result').modal({backdrop: 'static', keyboard: false});
                }

                $('.pin-pi').val("");
                $('#pin-pi-modal').modal('hide');
                // $('.send-btn').prop('disabled', false);
                $('.btn-nominal').prop('disabled', false);
            } ,error: function(xhr, status, error) {
                console.log(error);
                toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                $('.btn-nominal').prop('disabled', false);
            },

        });

        // setTimeout(function() { 
        //     toastr.clear();
        //     toastr.success("Data berhasil di proses", "Sukses");
        //     toastr.clear();

        //     let nominal = formatRupiah($('.nominal').val(), 'Rp. ');
        //     $('.p-nominal').html(nominal);

            

        //     $('.nominal').val("");
        //     $('.img_struk').val("");
        //     $('.pin').val("");

        //     $('.btn-nominal').prop('disabled', false);
        // }, 2000);
    }

    function redeem() {
        toastr.info("Harap menunggu, data sedang di proses", "Loading...");
        $('.btn-jumlah-redeem').prop('disabled', true);

        let args = {};
        args.user_uid = "{{ $user_uid }}";
        args.jumlah_redeem = $('.jumlah-redeem').val();

        $.ajax({
            type: "POST",
            url: '{{ url("/") }}'+'/redeem-claim-gift',
            dataType: "json",
            data: args,
            cache : false,
            success: function(data){
                toastr.clear();
              
                if(data.code == 400) {
                    if(Array.isArray(data.message)) {
                      toastr.warning(data.message[0], "Peringatan");
                    } else {
                      toastr.warning(data.message, "Peringatan");
                    }
                } else if(data.code == 200) {
                    toastr.success(data.message, "Sukses");
                    $('.total-claim').text(data.total_claim);
                }
                
                $('#jumlah-redeem-modal').modal('hide');
                $('.btn-jumlah-redeem').prop('disabled', false);
            } ,error: function(xhr, status, error) {
                console.log(error);
                toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                $('.btn-jumlah-redeem').prop('disabled', false);
            },
        });
    }

    function formatRupiah(angka, prefix) {

        let number_string   = angka.replace(/[^,\d]/g, '').toString(),
        split   		    = number_string.split(','),
        sisa     		    = split[0].length % 3,
        rupiah     		    = split[0].substr(0, sisa),
        ribuan     		    = split[0].substr(sisa).match(/\d{3}/gi);
    
        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
    
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
</script>

@endsection