@extends('public/header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
                                    

									<div class="col-md-12">
                                        <a href="{{ url('/u/'.$user_code) }}" class="btn btn-secondary btn-min-width mr-1 mb-1"><i class="ft-arrow-left"></i>&nbsp;Kembali</a> <br><br><br>

                                        <!-- <h6><p>Dear Clients,</p></h6>
                                        <h6><p>Jangan buang struk anda, karena mulai 1 Mei 2019 anda dapat menukarkan struk anda dengan hadiah uang atau pulsa</p></h6>
                                        <h6><p>Kini secarik kertas pun dapat di tukar dengan uang</p></h6>

                                        <br>

										<h4><p><b>Cara menukar:</b></p></h4>
										<h6><p>1. Pilih merchant, lalu klik tukar struk</p></h6>
										<h6><p>2. Mintalah kasir mencatat nomor ID anda</p></h6>
										<h6><p>3. Copy struk anda akan otomatis di catat dan di kumpulkan</p></h6>
										<h6><p>4. Penukaran dilakukan setiap tanggal 15 bulan berikutnya</p></h6>

                                        <br> -->

                                        <a href="{{ url('claim-gift/u/'.$user_code) }}" class="btn btn-secondary btn-min-width mr-1 mb-1" style="float: right;">&nbsp;PILIH MERCHANT</a>
                                    </div>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

@endsection