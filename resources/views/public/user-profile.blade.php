<?php 
    use App\Http\Controllers\HelperController;
?>

@extends('public/header')

@section('content')

<style>
    .info-wallet, .info-wallet-process {
        font-size: 20px !important;
    }
</style>

<div class="content-body">
    <br> 

    {{-- <div class="col-xl-12 col-lg-6 col-xs-12">
        <div class="card">
            <div class="card-body">
                <div class="media">
                    <div class="p-2 text-xs-center bg-primary bg-darken-2 media-left media-middle">
                        <i class="icon-wallet font-large-2 white"></i>
                    </div>
                    
                    <div class="p-2 bg-gradient-x-primary white media-body">
                        <span><b>Struk Bisa Ditukarkan</b></span> &nbsp; <i class="fa fa-info-circle info-wallet-process"></i> <br>
                        <h5 class="text-bold-400">Rp. {{ floor($user_balance_standart) }}</h5>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-12 col-lg-6 col-xs-12">
        <div class="card">
            <div class="card-body">
                <div class="media">
                    <div class="p-2 text-xs-center bg-success bg-darken-2 media-left media-middle">
                        <i class="icon-wallet font-large-2 white"></i>
                    </div>
                    
                    <div class="p-2 bg-gradient-x-success white media-body">
                        <span><b>Uang Bisa Dicairkan</b></span> &nbsp; <i class="fa fa-info-circle info-wallet"></i> <br>
                        <h5 class="text-bold-400">Rp. 0</h5>
                    </div>

                </div>
            </div>
        </div>
    </div> --}}

    <div class="col-xl-12 col-lg-6 col-xs-12">

        <div class="card border-grey border-lighten-2">
            <div class="text-xs-center">

                <div class="card-block">
                    <h2><?php echo(urldecode($company_title)); ?></h2>
                </div>
                
                @if($company_link_title == null || $company_link_title == "")
                    @if($link_title != null || $link_title != "")
                        <div class="card-block">
                            <p>
                                @if($link_title->description != "")
                                    <b>{{ urldecode($link_title->description) }}</b>
                                @else
                                <b>MEMBERKU</b>
                                @endif
                            </p>
                            
                        </div>
                    @else
                        <div class="card-block">
                            <p>
                                <b>MEMBERKU</b>
                            </p>
                            
                        </div>
                    @endif
                @endif

                @if($logo_untuk_bawahan != null || $logo_untuk_bawahan != "")
                    <div class="card-block">
                        @if($logo_untuk_bawahan->img != "")
                            <img src="{{ URL::asset($logo_untuk_bawahan->img) }}" style="height: 60px;" alt="Card image">
                        @endif
                        
                    </div>          
                @endif
                
                @if($company_link_title != null || $company_link_title != "")
                    <div class="card-block">
                        <p>
                            <b>{{ urldecode($company_link_title) }}</b>
                        </p>
                    </div>
                @endif
                
                @if($company_logo != null || $company_logo != "")
                    <div class="card-block" align="center">
                        <img src="{{ URL::asset($company_logo) }}" style="height: 60px;" alt="Card image">
                    </div>  
                @endif

                <div class="card-block">
                    @if($user != null) 
                        {!! QrCode::size(160)->generate($user_code_data->code) !!}
                    @endif
                    
                </div>
                <div class="card-block">
                    <h4 class="card-title">@if($user_code_data != null) ID: {{ $user_code_data->code }} @endif</h4>
                    <h6 class="card-subtitle text-muted">@if($user != null) {{ urldecode($user->fullname) }} @endif</h6> <br> <br>

                    <span>
                        <a href="{{ url($url_agent.'/login') }}" class="btn btn-primary btn-min-width mr-1 mb-1">Login</a> 
                        <a href="{{ url('/claim-gift/u/'.$user_code) }}" class="btn btn-success btn-min-width mr-1 mb-1">Tukar Struk</a>
                    </span>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection