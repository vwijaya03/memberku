@extends('public/header')

@section('content')
<br>
<section id="description" class="card">
    <div class="card-header">
        <h4 class="card-title" style="text-align: center;">Member Card</h4>
    </div>
</section>

<div class="row mt-2">
    <div class="col-xl-12 col-md-12 col-xs-12">
        <div class="card profile-card-with-cover">
            <div class="card-body">
                <img src="{{ URL::asset('img/aai-card.png') }}" class="membercard">
                <div class="top-left">
                    @if($noc != 'null') {{ $noc }} @endif <br>
                    @if($address != 'null') {{ $address }} @endif<br> 
                    @if($kota != 'null') {{ $kota }} @endif<br> 
                    @if($phone != 'null') {{ $phone }} @endif
                </div>

                <div class="bottom-left">
                    @if($member != null) {{ $member->card_number }} @endif<br>
                    @if($member != null) {{ $member->fullname }} @endif
                </div>
            </div>
        </div>
    </div>
</div>

@if($logo != null)
<img src="{{ $public_base_url.$logo->img }}" class="right-bottom">
@endif

<style type="text/css">
    @media only screen and (width: 320px) {
        .membercard {
            width: 320px; 
            display: block; 
            margin-left: auto; 
            margin-right: auto; 
        }

        .card-body > .top-left {
            position: absolute;
            left: 5vw;
            bottom: 100px;
            color: white;
            font-size: 12px;
        }

        .card-body > .bottom-left {
            position: absolute;
            left: 5vw;
            bottom: 35px;
            color: white;
            font-size: 12px;
        }

        .right-bottom {
            position: absolute;
            width: 25% !important;
            left: 71vw;
            bottom: 280px;
        }
    }

    @media only screen and (width: 360px) {
        .membercard {
            width: 360px; 
            display: block; 
            margin-left: auto; 
            margin-right: auto; 
        }

        .card-body > .top-left {
            position: absolute;
            left: 5vw;
            bottom: 130px;
            color: white;
        }

        .card-body > .bottom-left {
            position: absolute;
            left: 5vw;
            bottom: 40px;
            color: white;
        }

        .right-bottom {
            position: absolute;
            width: 25% !important;
            left: 69vw;
            bottom: 53%;
        }
    }
    
    @media only screen and (width: 375px) {
        .membercard {
            width: 375px; 
            display: block; 
            margin-left: auto; 
            margin-right: auto; 
        }

        .card-body > .top-left {
            position: absolute;
            left: 5vw;
            bottom: 130px;
            color: white;
        }

        .card-body > .bottom-left {
            position: absolute;
            left: 5vw;
            bottom: 40px;
            color: white;
        }

        .right-bottom {
            position: absolute;
            width: 25% !important;
            left: 69vw;
            bottom: 60%;
        }
    }

    @media only screen and (width: 400px) {
        .membercard {
            width: 320px; 
            display: block; 
            margin-left: auto; 
            margin-right: auto; 
        }

        .card-body > .top-left {
            position: absolute;
            left: 15vw;
            bottom: 100px;
            color: white;
        }

        .card-body > .bottom-left {
            position: absolute;
            left: 15vw;
            bottom: 40px;
            color: white;
        }

        .right-bottom {
            position: absolute;
            width: 22% !important;
            left: 63vw;
            bottom: 350px;
        }
    }
    
    @media only screen and (width: 411px) {
        .membercard {
            width: 375px; 
            display: block; 
            margin-left: auto; 
            margin-right: auto; 
        }

        .card-body > .top-left {
            position: absolute;
            left: 11vw;
            bottom: 130px;
            color: white;
        }

        .card-body > .bottom-left {
            position: absolute;
            left: 11vw;
            bottom: 40px;
            color: white;
        }

        .right-bottom {
            position: absolute;
            width: 20% !important;
            left: 71vw;
            bottom: 57%;
        }
    }

    @media only screen and (width: 414px) {
        .membercard {
            width: 375px; 
            display: block; 
            margin-left: auto; 
            margin-right: auto; 
        }

        .card-body > .top-left {
            position: absolute;
            left: 11vw;
            bottom: 130px;
            color: white;
        }

        .card-body > .bottom-left {
            position: absolute;
            left: 11vw;
            bottom: 40px;
            color: white;
        }

        .right-bottom {
            position: absolute;
            width: 20% !important;
            left: 69vw;
            bottom: 57%;
        }
    }

    @media only screen and (width: 480px) {
        .membercard {
            width: 320px; 
            display: block; 
            margin-left: auto; 
            margin-right: auto; 
        }

        .card-body > .top-left {
            position: absolute;
            left: 22vw;
            bottom: 100px;
            color: white;
        }

        .card-body > .bottom-left {
            position: absolute;
            left: 22vw;
            bottom: 40px;
            color: white;
        }

        .right-bottom {
            position: absolute;
            width: 22% !important;
            left: 58vw;
            bottom: 350px;
        }
    }

    @media only screen and (width: 768px) {
        .membercard {
            width: 380px; 
            display: block; 
            margin-left: auto; 
            margin-right: auto; 
        }

        .top-left {
            position: absolute;
            left: 28vw;
            bottom: 140px;
            color: white;
        }

        .card-body > .bottom-left {
            position: absolute;
            left: 28vw;
            bottom: 40px;
            color: white;
        }

        .right-bottom {
            position: absolute;
            width: 10% !important;
            left: 62vw;
            bottom: 310px;
        }
    }

    @media only screen and (width: 720px) {
        .membercard {
            width: 380px; 
            display: block; 
            margin-left: auto; 
            margin-right: auto; 
        }

        .top-left {
            position: absolute;
            left: 28vw;
            bottom: 140px;
            color: white;
        }

        .card-body > .bottom-left {
            position: absolute;
            left: 28vw;
            bottom: 40px;
            color: white;
        }

        .right-bottom {
            position: absolute;
            width: 10% !important;
            left: 62vw;
            bottom: 310px;
        }
    }

    @media only screen and (width: 1080px) {
        .membercard {
            width: 380px; 
            display: block; 
            margin-left: auto; 
            margin-right: auto; 
        }

        .top-left {
            position: absolute;
            left: 35vw;
            bottom: 140px;
            color: white;
        }

        .card-body > .bottom-left {
            position: absolute;
            left: 35vw;
            bottom: 40px;
            color: white;
        }

        .right-bottom {
            position: absolute;
            width: 10% !important;
            left: 55vw;
            bottom: 310px;
        }
    }

    @media only screen and (width: 1024px) {
        .membercard {
            width: 380px; 
            display: block; 
            margin-left: auto; 
            margin-right: auto; 
        }

        .top-left {
            position: absolute;
            left: 34vw;
            bottom: 140px;
            color: white;
        }

        .card-body > .bottom-left {
            position: absolute;
            left: 34vw;
            bottom: 40px;
            color: white;
        }

        .right-bottom {
            position: absolute;
            width: 8% !important;
            left: 58vw;
            bottom: 310px;
        }
    }

    @media only screen and (min-width: 1200px) {
        .membercard {
            width: 380px; 
            display: block; 
            margin-left: auto; 
            margin-right: auto; 
        }

        .top-left {
            position: absolute;
            left: 37vw;
            bottom: 140px;
            color: white;
        }

        .card-body > .bottom-left {
            position: absolute;
            left: 37vw;
            bottom: 40px;
            color: white;
        }

        .right-bottom {
            position: absolute;
            width: 8% !important;
            left: 55vw;
            bottom: 310px;
        }
    }
</style>

@endsection