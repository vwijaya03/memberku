@extends('public/header')

@section('content')
<br>
<section id="description" class="card">
    <div class="card-header">
        <h4 class="card-title" style="text-align: center;">{{ $name_on_card }}</h4>
    </div>
</section>

<div class="content-body">
    <div class="row match-height">
        <div class="col-xl-4 col-md-6 col-sm-12">
            <div class="card">
                <div class="card-body">
                    <h5 style="position: absolute;"></h5>
                    <img class="card-img-top img-fluid" src="{{ $public_base_url.$promo->img }}" alt="Card image cap" width="100%">
                    <div class="card-block">
                        <h4 class="card-title">{{ urldecode($promo->title) }}</h4>
                        @if($setting != null)
                            @if($setting->visibility_remaining_click == 'yes')
                                <p class="card-text remaining-click" style="word-wrap: break-word;"><b>Remaining Use: {{ $voucher->click_limit }}</b></p>
                            @endif
                        @endif
                        
                        <p class="card-text" style="word-wrap: break-word;">Kode Promo {{ $voucher->unique_code }}</p>
                        <p class="card-text" style="word-wrap: break-word;"><?php echo(urldecode($promo->description)); ?></p>

                        @if($voucher->click_limit > 0)
                        <button class="btn btn-outline-primary use-btn" onclick="use()" style="border: 3px solid;"><b>USE</b></button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js_script')

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function use() {
        let args = {};
        args.promo_uid = '{{ $promo->uid }}';  
        args.unique_code = '{{ $voucher->unique_code }}';  
        args.user_uid = '{{ $voucher->user_uid }}';  

        toastr.info("Harap menunggu, data sedang di proses", "Loading...");
        $('.use-btn').prop('disabled', true);

        $.ajax({
            type: "POST",
            url: '{{ $public_base_url }}'+'/use-promo',
            dataType: "json",
            data: args,
            cache : false,
            success: function(data){
                toastr.clear();
              
                if(data.code == 400) {
                    if(Array.isArray(data.message)) {
                      toastr.warning(data.message[0], "Peringatan");
                    } else {
                      toastr.warning(data.message, "Peringatan");
                    }
                } else if(data.code == 200) {
                    if(data.remaining_click < 1) 
                    {
                        $('.use-btn').hide();
                    }

                    $('.remaining-click').text(`Remaining Use: `+data.remaining_click).css({ 'font-weight': 'bold' });
                    toastr.success(data.message, "Sukses");
                }

                $('.use-btn').prop('disabled', false);
            } ,error: function(xhr, status, error) {
              console.log(error);
                toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                $('.use-btn').prop('disabled', false);
            },

        });
    }
</script>

@endsection