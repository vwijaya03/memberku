@extends('public/header')

@section('content')
<br>

<section id="description" class="card">
    <div class="card-header">
        @if(Session::has('err'))
            <div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ Session::get('err') }}
            </div>
        @endif

        <h4 class="card-title" style="text-align: center;">Masukkan Kode Voucher</h4>
    </div>
</section>

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
            <section id="server-processing">
                <div class="row">

                    <div class="col-xs-12">
                        <div class="card">
                            
                            <div class="card-body collapse in">

                                <div class="card-block card-dashboard">
                                    <label>Kode Voucher</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control uc" style="width: 300px !important;">
                                    </div>
                                    <button class="btn btn-outline-primary btn uc-btn" onclick="verify()">Kirim</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection

@section('custom_js_script')

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function verify() {
        let args = {}; 
        args.unique_code = $('.uc').val();

        toastr.info("Harap menunggu, data sedang di proses", "Loading...");
        $('.uc-btn').prop('disabled', true);

        $.ajax({
            type: "POST",
            url: '{{ $public_base_url }}'+'/verify-uc',
            dataType: "json",
            data: args,
            cache : false,
            success: function(data){
                toastr.clear();
              
                if(data.code == 400) {
                    if(Array.isArray(data.message)) {
                      toastr.warning(data.message[0], "Peringatan");
                    } else {
                      toastr.warning(data.message, "Peringatan");
                    }
                } else if(data.code == 200) {
                    toastr.success(data.message, "Sukses");
                    setTimeout(function(){  
                        window.location.href = `{{ $public_base_url }}/promo/${args.unique_code}`;
                    }, 2000);
                }

                $('.uc-btn').prop('disabled', false);
            } ,error: function(xhr, status, error) {
              console.log(error);
                toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                $('.uc-btn').prop('disabled', false);
            },

        });
    }
</script>

@endsection