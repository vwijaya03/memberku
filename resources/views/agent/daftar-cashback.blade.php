@extends('agent/header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
                            <h4 class="card-title">{{ $page_title }}</h4>
				            </div>
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
									
									<table width="100%" class="table table-striped table-bordered dataex-html5-export server-side-user">
										<thead>
											<tr>
												<th>No.</th>
												<th>Merchant</th>
												<th>Uang</th>
											</tr>
                                        </thead>
                                        
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Yoshinoya</td>
                                                <td>5%</td>
                                            </tr>

                                            <tr>
                                                <td>2</td>
                                                <td>Breadlife</td>
                                                <td>5%</td>
                                            </tr>

                                            <tr>
                                                <td>3</td>
                                                <td>Charles & Keith</td>
                                                <td>5%</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                    *) Dihitung dari harga sebelum pajak
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

@endsection