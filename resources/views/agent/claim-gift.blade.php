@extends('agent/header')

@section('content')
<style type="text/css">
    .ft-info {
        font-size: 22px !important;
    }  

    .voucher-bonus-img {
        display: block !important;
        margin-left: auto !important;
        margin-right: auto !important;
        width: 60% !important;
    }
</style>

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
									<div class="col-md-12">
                                        <h5><b>MERCHANT AKAN BERTAMBAH SETIAP BULAN, IKUTI TERUS DAN KUMPULKAN HADIAHNYA</b></h5> <br><br>

                                        <table width="100%" class="table table-striped table-bordered dataex-html5-export server-side-merchant">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

<!-- Add Voucher Bonus Modal -->
<div class="modal fade text-xs-left" id="data-benefit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33"></label>
            </div>

            <form action="#" enctype="multipart/form-data" id="upload_promo">
                <div class="modal-body">
                    <div class="row">
                        <!-- <div class="col-md-12">
                            <img class="voucher-bonus-img">
                        </div>
                        
                        <div class="col-md-8">
                            <br><br>
                            <label class="merchant"></label>
                            <div class="form-group">
                                <p class="title"></p>
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <label>Deskripsi</label>
                            <div class="form-group">
                                <p class="description"></p>
                            </div>
                        </div> -->

                        <div class="col-xl-12 col-lg-6 col-xs-12">

                            <div class="card border-grey border-lighten-2">
                                <div class="text-xs-center">
                                    <div class="card-block">
                                        <b>TUNJUKKAN HALAMAN INI PADA KASIR <span class="merchant-name"></span></b> <br><br>
                                        Pastikan kasir menginput nomor ID anda <br><br>
                                        Copy struk (data transaksi) anda akan otomatis tercatat dan di kumpulkan untuk di tukar setiap bulan
                                    </div>

                                    <div class="card-block">
                                        {!! QrCode::size(160)->generate($user_code) !!} <br><br>
                                        <img src="{{ URL::asset('img/aai.png') }}" alt="AAI" style="height:30px;">
                                    </div>

                                    <div class="card-block">
                                        <h4 class="card-title">ID: {{ $mod_user_code }}</h4>
                                        <h6 class="card-subtitle text-muted">{{ urldecode($current_user->fullname) }}</h6> <br>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <a href="#" class='btn btn-info mr-1 mb-1' onclick="popupPin()">Next</a>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Add Voucher Bonus Pin Modal -->
<div class="modal fade text-xs-left" id="voucher-bonus-pin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Masukkan Pin</label>
            </div>

            <form action="#" enctype="multipart/form-data" id="upload_promo">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" class="form-control pin" placeholder="Masukkan pin...">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer" style="border-top: 0px !important;">
                    <button class='btn btn-info mr-1 mb-1 btn-pin' onclick="verifyPin()">Kirim</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Add Voucher Bonus Value Modal -->
<div class="modal fade text-xs-left" id="voucher-bonus-nominal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33"></label>
            </div>

            <form action="#" enctype="multipart/form-data" id="upload_promo">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <p align="center"><b><u>SYARAT DAN KETENTUAN</u></b></p>
                                <p>1. Copy struk hanya dapat di tukar dengan hadiah <b>JIKA</b> data transaksi anda tercatat di sistem merchant</p>
                                <p>2. Copy struk (data transaksi) yang tercatat akan di kumpulkan dahulu dan di proses sebulan sekali</p>
                                <p>3. Saldo anda akan otomatis bertambah jika hadiah uang sudah di cairkan. Silahkan login untuk melihat jumlah saldo anda</p>
                                <p>4. Status hadiah merupakan pemberian cuma-cuma dari data transaksi anda yang tercatat, tanpa di pungut biaya sehingga tidak dapat di tuntut jika hadiah tidak dapat cair di karenakan data transaksi tidak tercatat di dalam sistem merchant dengan alasan apapun</p>
                                <p>5. Syarat dan ketentuan ini dapat berubah sewaktu-waktu</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer" style="border-top: 0px !important;">
                    <button class='btn btn-info mr-1 mb-1 btn-nominal' onclick="insertNominal()">Setuju</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Add Voucher Bonus Result Modal -->
<div class="modal fade text-xs-left" id="voucher-bonus-result" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <form action="#" enctype="multipart/form-data" id="upload_promo">
                <div class="modal-body">
                    <div class="row">
                        
                    <div class="col-xl-12 col-lg-6 col-xs-12">

                        <div class="card border-grey border-lighten-2">
                            <div class="text-xs-center">
                                <div class="card-block">
                                    <img src="{{ URL::asset('img/success_icon.png') }}" class="rounded-circle" style="height: 60px;" alt="Card image">
                                </div>
                                <div class="card-block">
                                    <h4 class="card-title">PENUKARAN STRUK DENGAN HADIAH SEDANG DIPROSES</h4> <br><br>
                                    <h4 class="card-title">SILAHKAN LOGIN UNTUK MELIHAT SALDO</h4>
                                    <!-- <h6 class="card-subtitle text-muted">Anda akan mendapatkan bonus cash <span class="p-nominal"></span></h6> <br><br>
                                    <h6 class="card-subtitle text-muted">STATUS</h6> <br>
                                    <h6 class="card-subtitle text-muted"><input class="btn btn-secondary mr-1 mb-1" value="Menunggu Approval" /></h6> <br><br>
                                    <h6 class="card-subtitle text-muted">Jika sudah terverifikasi bonus cash bisa di lihat di menu saldo cash</h6> -->
                                </div>
                            </div>
                        </div>

                    </div>

                    </div>
                </div>

                <div class="modal-footer" style="border-top: 0px !important;">
                    <a href="{{ url($url_agent.'/login') }}" class="btn btn-secondary mr-1 mb-1 btn-nominal">HOME</a>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">
    let table, table_pi, merchant_uid = '';

    $('.nominal').keyup(function() {
        $('.nominal').val(formatRupiah($('.nominal').val()));
    });

	$(document).ready(function() {
        table = $('.server-side-merchant').DataTable({
            "lengthChange": false,
	    	"lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
	        "processing": true,
	        "serverSide": true,
	        "ajax":{
	        	"type": "POST",
            	"url": "{{ url($url_agent.'/merchant-join-bonus-ajax') }}",
            	"dataType": "json",
           	},
	        "columns": [
                { "data": "name" },
	            { "data": "action_btn" }
	        ],
	        order: [[1, 'asc']],
            "columnDefs": [
                { "orderable": false, "targets": [ 1] },
                { "width": "190px", "targets": [ 1 ] },
                // { "width": "120px", "targets": [ 5 ] },
            ]
	    });
	});

    function detailBonus(merchant) {
        // $('.merchant').html(decodeURIComponent(merchant));
        // $('.title').html(decodeURIComponent(title));
        // $('.description').html(decodeURIComponent(description));
        // $('.voucher-bonus-img').attr("src", img_path);
        merchant_uid = merchant.uid;

        $('.merchant-name').text(decodeURIComponent(merchant.name.toUpperCase().replace(/\+/g, ' ')));

        $('#data-benefit').modal({
            backdrop: 'static', 
            keyboard: false
        });
    }

    function popupPin() {
        $('#data-benefit').modal('hide');
        $('#voucher-bonus-nominal').modal({backdrop: 'static', keyboard: false});
    }

    // function verifyPin() {
    //     toastr.info("Harap menunggu, data sedang di proses", "Loading...");
    //     $('.btn-pin').prop('disabled', true);

    //     setTimeout(function() { 
    //         toastr.clear();
    //         toastr.success("Pin berhasil di verifikasi", "Sukses");
    //         toastr.clear();
    //         $('#voucher-bonus-pin').modal('hide');
    //         $('#voucher-bonus-nominal').modal({backdrop: 'static', keyboard: false});

    //         $('.btn-pin').prop('disabled', false);
    //     }, 3000);
    // }

    function insertNominal() {
        toastr.info("Harap menunggu, data sedang di proses", "Loading...");
        $('.btn-nominal').prop('disabled', true);

        let args = {};
        args.merchant_uid = merchant_uid;
        args.user_uid = "{{ $current_user->uid }}";

        $.ajax({
            type: "POST",
            url: '{{ url($url_agent) }}'+'/claim-gift',
            dataType: "json",
            data: args,
            cache : false,
            success: function(data){
                toastr.clear();
              
                if(data.code == 400) {
                    if(Array.isArray(data.message)) {
                      toastr.warning(data.message[0], "Peringatan");
                    } else {
                      toastr.warning(data.message, "Peringatan");
                    }
                } else if(data.code == 200) {
                    $('#voucher-bonus-nominal').modal('hide');
                    $('#voucher-bonus-result').modal({backdrop: 'static', keyboard: false});

                    $('.btn-nominal').prop('disabled', false);
                }

                $('.pin-pi').val("");
                $('#pin-pi-modal').modal('hide');
                // $('.send-btn').prop('disabled', false);
                
            } ,error: function(xhr, status, error) {
                console.log(error);
                toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                $('.btn-nominal').prop('disabled', false);
            },

        });

        // setTimeout(function() { 
        //     toastr.clear();
        //     toastr.success("Data berhasil di proses", "Sukses");
        //     toastr.clear();

        //     let nominal = formatRupiah($('.nominal').val(), 'Rp. ');
        //     $('.p-nominal').html(nominal);

            

        //     $('.nominal').val("");
        //     $('.img_struk').val("");
        //     $('.pin').val("");

        //     $('.btn-nominal').prop('disabled', false);
        // }, 2000);
    }

    function formatRupiah(angka, prefix) {

        let number_string   = angka.replace(/[^,\d]/g, '').toString(),
        split   		    = number_string.split(','),
        sisa     		    = split[0].length % 3,
        rupiah     		    = split[0].substr(0, sisa),
        ribuan     		    = split[0].substr(sisa).match(/\d{3}/gi);
    
        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
    
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
</script>

@endsection