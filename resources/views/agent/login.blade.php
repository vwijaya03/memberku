<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
		<meta name="description" content="MEMBERKU">
		<meta name="keywords" content="MEMBERKU">
		<meta name="author" content="MEMBERKU">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>MEMBERKU</title>
		<link rel="apple-touch-icon" href="{{ URL::asset('img/icon_kd.jpeg') }}">
		<link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('img/icon_kd.jpeg') }}">
			
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
		<!-- BEGIN VENDOR CSS-->
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/bootstrap.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/fonts/feather/style.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/fonts/font-awesome/css/font-awesome.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/fonts/flag-icon-css/css/flag-icon.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/extensions/pace.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/forms/icheck/icheck.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/forms/icheck/custom.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/extensions/toastr.css') }}">
		<!-- END VENDOR CSS-->
		<!-- BEGIN TERA CSS-->
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/bootstrap-extended.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/app.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/colors.min.css') }}">
		<!-- END TERA CSS-->
		<!-- BEGIN Page Level CSS-->
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/core/menu/menu-types/horizontal-menu.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/core/colors/palette-gradient.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/pages/login-register.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/plugins/extensions/toastr.min.css') }}">
		<!-- END Page Level CSS-->
		<!-- BEGIN Custom CSS-->
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/assets/css/style.css') }}">
		<!-- END Custom CSS-->
	</head>

	<body data-open="hover" data-menu="horizontal-menu" data-col="1-column" class="horizontal-layout horizontal-menu 1-column   menu-expanded blank-page blank-page">

    <!-- ////////////////////////////////////////////////////////////////////////////-->
	<div class="app-content content container-fluid">
		<div class="content-wrapper">
		<div class="content-header row">
		</div>
		<div class="content-body">
			<section class="flexbox-container">
				<div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1  box-shadow-2 p-0">
					<div class="card border-grey border-lighten-3 m-0">
						<div class="card-header no-border">
						    <div class="card-title text-xs-center">
								<div class="p-1">MEMBERKU</div>
								
								{{-- <div class="card-block">
									<img src="{{ URL::asset('img/icon_kd.jpeg') }}" style="height: 60px;" alt="Card image">
								</div> --}}
						    </div>
							
						    <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2"><span>Login Area</span></h6>
						</div>

						<div class="card-body collapse in">
						    <div class="card-block">
						    	
								@if ($errors->has('email'))
									<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<strong>E-mail</strong> tidak boleh kosong !
									</div>
								@endif

								@if ($errors->has('password'))
									<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<strong>Password</strong> tidak boleh kosong !
									</div>
								@endif

								@if(Session::has('err'))
					                <div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										{{ Session::get('err') }}
									</div>
					            @endif

						        <form class="form-horizontal form-simple" action="{{ url($url_agent.'/login') }}" method="POST">
						        	{{ csrf_field() }}
						            <fieldset class="form-group position-relative has-icon-left mb-0">
						                <input type="text" class="form-control form-control-lg input-lg" id="user-name" placeholder="Masukkan Nomor HP" name="email">
						                <div class="form-control-position">
						                    <i class="ft-phone"></i>
						                </div>
						            </fieldset>
						            <br>
						            <fieldset class="form-group position-relative has-icon-left">
						                <input type="password" class="form-control form-control-lg input-lg" id="user-password" placeholder="Masukkan Password" name="password">
						                <div class="form-control-position">
						                    <i class="fa fa-key"></i>
						                </div>
						            </fieldset>
						            
						            <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="ft-unlock"></i> Login</button>
						        </form>
						    </div>
						</div>
						<div class="card-footer">
						    <div class="">
						        {{-- <p class="float-sm-left text-xs-center m-0"><a href="{{ url($url_partner.'/reset-password') }}" class="card-link">Lupa password ?</a></p> --}}
						    </div>
						</div>
					</div>
					
					<footer class="footer footer-static footer-light navbar-shadow" >
							<p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block"></span>Dengan menggunakan layanan ini, maka anda setuju dan terikat dengan <a href="{{ url('/terms-conditions') }}">Syarat Penggunaan</a> dan <a href="{{ url('/privacy-and-policy') }}">Kebijakan Privasi</a></p>
					
							<br>
					
							<p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block"></span>Contact Centre: <a href="#">admin@ariusangkasa.com</a></p>
					
							<br>
							
							<!-- <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block"></span>Copyright  &copy; <?php echo date('Y'); ?> PT. Arius Angkasa Indonesia, All rights reserved. </p> -->
						</footer>
				</div>

			</section>
		</div>
		</div>
	</div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->

    <!-- BEGIN VENDOR JS-->
    <script src="{{ URL::asset('admin/app-assets/vendors/js/vendors.min.js') }}" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script type="text/javascript" src="{{ URL::asset('admin/app-assets/vendors/js/ui/jquery.sticky.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('admin/app-assets/vendors/js/charts/jquery.sparkline.min.js') }}"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/forms/icheck/icheck.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/vendors/js/extensions/toastr.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN TERA JS-->
    <script src="{{ URL::asset('admin/app-assets/js/core/app-menu.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/js/core/app.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('admin/app-assets/js/scripts/customizer.min.js') }}" type="text/javascript"></script>
    <!-- END TERA JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script type="text/javascript" src="{{ URL::asset('admin/app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js') }}"></script>
    <script src="{{ URL::asset('admin/app-assets/js/scripts/forms/form-login-register.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->

    <script type="text/javascript">
    	$(document).ready(function() {
    		$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});
		});
    </script>
  </body>
</html>