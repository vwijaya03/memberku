@extends('agent/header')

@section('content')
<style type="text/css">
    .ft-info {
        font-size: 22px !important;
    }  

    .voucher-bonus-img {
        display: block !important;
        margin-left: auto !important;
        margin-right: auto !important;
        width: 60% !important;
    }
</style>

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title"></h4>
				            </div>
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
									<div class="col-md-12">
                                        <h4><b></b></h4> <br><br>
                                        <table width="100%" class="table table-striped table-bordered dataex-html5-export server-side-benefit">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Title</th>
                                                    <th></th>
                                                </tr>

                                                <tr>
                                                    <td>1</td>
                                                    <td>Nav Karaoke</td>
                                                    <td>
                                                        <button onclick='detailBonus("pr", "Nav Karaoke", "", "Tunjukkan Promo ini kepada Staff Nav Karaoke", "{{ URL::asset('img/nav.jpeg') }}")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-backdrop="static" data-target='#data-benefit'><i class='ft-info'></i></button>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>2</td>
                                                    <td>Bakmi GM</td>
                                                    <td>
                                                        <button onclick='detailBonus("pr", "Bakmi GM", "", "Tunjukkan Promo ini kepada Staff Bakmi GM", "{{ URL::asset('img/bgm.jpeg') }}")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-backdrop="static" data-target='#data-benefit'><i class='ft-info'></i></button>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>3</td>
                                                    <td>Yoshinoya</td>
                                                    <td>
                                                        <button onclick='detailBonus("pr", "Yoshinoya", "", "Tunjukkan Promo ini kepada Staff Yoshinoya", "{{ URL::asset('img/yn.jpeg') }}")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-backdrop="static" data-target='#data-benefit'><i class='ft-info'></i></button>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>4</td>
                                                    <td>Pizza Hut</td>
                                                    <td>
                                                        <button onclick='detailBonus("ph", "Pizza Hut", "", "Tunjukkan Promo ini kepada Staff Pizza Hut", "{{ URL::asset('img/ph.jpeg') }}")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-backdrop="static" data-target='#data-benefit'><i class='ft-info'></i></button>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>5</td>
                                                    <td>J.Co</td>
                                                    <td>
                                                        <button onclick='detailBonus("jco", "J.Co", "", "Tunjukkan Promo ini kepada Staff J.Co", "{{ URL::asset('img/jco.jpeg') }}")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-backdrop="static" data-target='#data-benefit'><i class='ft-info'></i></button>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>6</td>
                                                    <td>Cincau Station</td>
                                                    <td>
                                                        <button onclick='detailBonus("cs", "Cincau Station", "FREE UPSIZE ALL VARIANT", "Tunjukkan Promo ini kepada Staff Cincau Station, Berlaku di seluruh Outlet Cincau Station", "{{ URL::asset('img/css.jpeg') }}")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-backdrop="static" data-target='#data-benefit'><i class='ft-info'></i></button>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>7</td>
                                                    <td>Barbys</td>
                                                    <td>
                                                        <button onclick='detailBonus("cs", "Barbys", "FREE UPSIZE ALL VARIANT", "Tunjukkan Promo ini kepada Staff Barbys", "{{ URL::asset('img/bb.jpeg') }}")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-backdrop="static" data-target='#data-benefit'><i class='ft-info'></i></button>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>8</td>
                                                    <td>Teapresso</td>
                                                    <td>
                                                        <button onclick='detailBonus("tp", "Teapresso", "BUY 1 GET 1 FREE", "Tunjukkan Promo ini kepada Staff Teapresso", "{{ URL::asset('img/tpp.jpeg') }}")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-backdrop="static" data-target='#data-benefit'><i class='ft-info'></i></button>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>9</td>
                                                    <td>Gold's Gym</td>
                                                    <td>
                                                        <button onclick='detailBonus("gg", "Golds Gym", "GRATIS FITNESS 3 HARI", "Tunjukkan Promo ini kepada Staff Golds Gym", "{{ URL::asset('img/ggs.jpeg') }}")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-backdrop="static" data-target='#data-benefit'><i class='ft-info'></i></button>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>10</td>
                                                    <td>Sentosa Seafood</td>
                                                    <td>
                                                        <button onclick='detailBonus("ss", "Sentosa Seafood", "VOUCHER RP 30.000", "Tunjukkan Promo ini kepada Staff Sentosa Seafood", "{{ URL::asset('img/sss.jpeg') }}")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-backdrop="static" data-target='#data-benefit'><i class='ft-info'></i></button>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>11</td>
                                                    <td>Prodia</td>
                                                    <td>
                                                        <button onclick='detailBonus("pr", "Prodia", "KERINGANAN BIAYA PEMERIKSAAN 10%25 DAN 15%25", "Tunjukkan Promo ini kepada Staff Prodia", "{{ URL::asset('img/prr.jpeg') }}")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-backdrop="static" data-target='#data-benefit'><i class='ft-info'></i></button>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>12</td>
                                                    <td>Kedai 27</td>
                                                    <td>
                                                        <button onclick='detailBonus("pr", "Kedai 27", "KERINGANAN BIAYA PEMERIKSAAN 10%25 DAN 15%25", "Tunjukkan Promo ini kepada Staff Kedai 27", "{{ URL::asset('img/kedai.jpeg') }}")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-backdrop="static" data-target='#data-benefit'><i class='ft-info'></i></button>
                                                    </td>
                                                </tr>

                                            </thead>
                                        </table>
                                    </div>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

<!-- Add Voucher Bonus Modal -->
<div class="modal fade text-xs-left" id="data-benefit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33"></label>
            </div>

            <form action="#" enctype="multipart/form-data" id="upload_promo">
                <div class="modal-body">
                    <div class="row">
                        <!-- <div class="col-md-12">
                            <img class="voucher-bonus-img">
                        </div>
                        
                        <div class="col-md-8">
                            <br><br>
                            <label class="merchant"></label>
                            <div class="form-group">
                                <p class="title"></p>
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <label>Deskripsi</label>
                            <div class="form-group">
                                <p class="description"></p>
                            </div>
                        </div> -->

                        <div class="col-xl-12 col-lg-6 col-xs-12">

                            <div class="card border-grey border-lighten-2">
                                <div class="text-xs-center">
                                    
                                    <div class="card-block">
                                        <img src="{{ URL::asset('img/qrcode.png') }}" class="height-150" alt="Card image">
                                    </div>
                                    <div class="card-block">
                                        <h4 class="card-title">ID: 238-990-34</h4>
                                        <h6 class="card-subtitle text-muted">Antoni Gunawan Saputra</h6> <br>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <a href="#" class='btn btn-info mr-1 mb-1' onclick="popupPin()">Next</a>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Add Voucher Bonus Pin Modal -->
<div class="modal fade text-xs-left" id="voucher-bonus-pin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Masukkan Pin</label>
            </div>

            <form action="#" enctype="multipart/form-data" id="upload_promo">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" class="form-control pin" placeholder="Masukkan pin...">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer" style="border-top: 0px !important;">
                    <button class='btn btn-info mr-1 mb-1 btn-pin' onclick="verifyPin()">Kirim</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Add Voucher Bonus Value Modal -->
<div class="modal fade text-xs-left" id="voucher-bonus-nominal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data</label>
            </div>

            <form action="#" enctype="multipart/form-data" id="upload_promo">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nominal Transaksi</label>
                                <input type="text" class="form-control nominal" placeholder="Masukkan nominal transaksi...">

                                <br>

                                <label>Upload Foto Struk</label>
                                <input type="file" class="form-control img_struk">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer" style="border-top: 0px !important;">
                    <button class='btn btn-info mr-1 mb-1 btn-nominal' onclick="insertNominal()">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Add Voucher Bonus Result Modal -->
<div class="modal fade text-xs-left" id="voucher-bonus-result" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form action="#" enctype="multipart/form-data" id="upload_promo">
                <div class="modal-body">
                    <div class="row">
                        
                    <div class="col-xl-12 col-lg-6 col-xs-12">

                        <div class="card border-grey border-lighten-2">
                            <div class="text-xs-center">
                                <div class="card-block">
                                    <img src="{{ URL::asset('img/success_icon.png') }}" class="rounded-circle" style="height: 60px;" alt="Card image">
                                </div>
                                <div class="card-block">
                                    <h4 class="card-title">DATA BERHASIL DI PROSES</h4>
                                    <!-- <h6 class="card-subtitle text-muted">Anda akan mendapatkan bonus cash <span class="p-nominal"></span></h6> <br><br>
                                    <h6 class="card-subtitle text-muted">STATUS</h6> <br>
                                    <h6 class="card-subtitle text-muted"><input class="btn btn-secondary mr-1 mb-1" value="Menunggu Approval" /></h6> <br><br>
                                    <h6 class="card-subtitle text-muted">Jika sudah terverifikasi bonus cash bisa di lihat di menu saldo cash</h6> -->
                                </div>
                            </div>
                        </div>

                    </div>

                    </div>
                </div>

                <div class="modal-footer" style="border-top: 0px !important;">
                    
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">
    let table, table_pi = '';

    $('.nominal').keyup(function() {
        $('.nominal').val(formatRupiah($('.nominal').val()));
    });

	$(document).ready(function() {

	});

    function detailBonus(type, merchant, title, description, img_path) {
        $('.merchant').html(decodeURIComponent(merchant));
        $('.title').html(decodeURIComponent(title));
        $('.description').html(decodeURIComponent(description));
        $('.voucher-bonus-img').attr("src", img_path);
    }

    function popupPin() {
        $('#data-benefit').modal('hide');
        $('#voucher-bonus-nominal').modal({backdrop: 'static', keyboard: false});
    }

    function verifyPin() {
        toastr.info("Harap menunggu, data sedang di proses", "Loading...");
        $('.btn-pin').prop('disabled', true);

        setTimeout(function() { 
            toastr.clear();
            toastr.success("Pin berhasil di verifikasi", "Sukses");
            toastr.clear();
            $('#voucher-bonus-pin').modal('hide');
            $('#voucher-bonus-nominal').modal({backdrop: 'static', keyboard: false});

            $('.btn-pin').prop('disabled', false);
        }, 3000);
    }

    function insertNominal() {
        toastr.info("Harap menunggu, data sedang di proses", "Loading...");
        $('.btn-nominal').prop('disabled', true);

        setTimeout(function() { 
            toastr.clear();
            toastr.success("Data berhasil di proses", "Sukses");
            toastr.clear();

            let nominal = formatRupiah($('.nominal').val(), 'Rp. ');
            $('.p-nominal').html(nominal);

            $('#voucher-bonus-nominal').modal('hide');
            $('#voucher-bonus-result').modal({backdrop: 'static', keyboard: false});

            $('.nominal').val("");
            $('.img_struk').val("");
            $('.pin').val("");

            $('.btn-nominal').prop('disabled', false);
        }, 2000);
    }

    function formatRupiah(angka, prefix) {

        let number_string   = angka.replace(/[^,\d]/g, '').toString(),
        split   		    = number_string.split(','),
        sisa     		    = split[0].length % 3,
        rupiah     		    = split[0].substr(0, sisa),
        ribuan     		    = split[0].substr(sisa).match(/\d{3}/gi);
    
        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
    
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
</script>

@endsection