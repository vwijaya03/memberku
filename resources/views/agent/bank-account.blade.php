@extends('agent/header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title">{{ $page_title }}</h4>
				                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
			        			<div class="heading-elements">
				                    <ul class="list-inline mb-0">
				                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
				                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				                    </ul>
				                </div>
				            </div>
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
                                    @if($res_user_bank_account == null)
									<a href="#" class="btn btn-success mr-1 mb-1" data-toggle="modal" data-target="#add-bank-account">Simpan Data</a>
                                    <br><br>
                                    @endif
									<table width="1580px" class="table table-striped table-bordered dataex-html5-export server-side-bank-account">
										<thead>
											<tr>
												<th>No</th>
                                                <th>Bank</th>
												<th>Nomor Rekening</th>
											</tr>
										</thead>
									</table>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

<!-- Add Merchant Modal -->
<div class="modal fade text-xs-left" id="add-bank-account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Bank Account</label>

                <br><br>

                <p>* Demi keamanan, nomor rekening yang disimpan TIDAK DAPAT DIUBAH</p>
                <p>* Ketentuan Pencairan Uang: </p>
                <p>1. Saldo minimal Rp. 25.000</p>
                <p>2. Pencairan otomatis tanggal 15 setiap bulan</p>
            </div>

            <form action="#">
                <div class="modal-body">

                    <label>Bank / Wallet Account *</label>
                    <div class="form-group">
                        <select class="select2-bank form-control bank" name="bank" style="width: 100%">
                            @foreach($banks as $bank)
                                <option value="{{ $bank->uid }}">{{ urldecode($bank->name) }}</option>
                            @endforeach
                        </select>
                    </div>

                    <label>Nomor Account *</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nomor Account" class="form-control no_rek">
                    </div>
                    
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn save-btn" value="Simpan">
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">
    let table, merchant_id = '';

	$(document).ready(function() {
        $('.save-btn').on('click', addBankAccount);

	    table = $('.server-side-bank-account').DataTable({
	    	"scrollX": !0,
	    	"lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
	        "processing": true,
	        "serverSide": true,
	        "ajax":{
	        	"type": "POST",
            	"url": "{{ url($url_agent.'/bank-account-ajax') }}",
            	"dataType": "json",
                "data": function(param) {
                    param.user_uid = "{{ $current_user->uid }}";
                },
           	},
	        "columns": [
	            { "data": "no" },
                { "data": "name" },
                { "data": "res.no_rek" },
	        ],
	        order: [[1, 'asc']],
            "columnDefs": [
                { "orderable": false, "targets": [ 0 ] },
                // { "width": "190px", "targets": [ 9 ] },
                // { "width": "120px", "targets": [ 5 ] },
            ]
	    });

	    function addBankAccount() {
	    	let args = {};
            args.user_uid = "{{ $current_user->uid }}";
	    	args.bank_uid = $('.bank').val();
	    	args.no_rek = $('.no_rek').val();
            
	    	$('.save-btn').prop('disabled', true);
	    	toastr.info("Harap menunggu, data sedang di proses", "Loading...");

	    	$.ajax({
                type: "POST",
                url: '{{ $auth_agent_base_url }}'+'add-bank-account',
                dataType: "json",
                data: args,
                cache : false,
                success: function(data){
                	toastr.clear();
                	
                    if(data.code == 400) {
                    	if(Array.isArray(data.message)) {
                    		toastr.warning(data.message[0], "Peringatan");
                    	} else {
                    		toastr.warning(data.message, "Peringatan");
                    	}
                    } else if(data.code == 200) {
                    	toastr.success(data.message, "Sukses");

                    	$('#add-bank-account').modal('hide');
                    	
                        $('.no_rek').val("");

						table.ajax.reload(null, false);
                    }

                    $('.save-btn').prop('disabled', false);
                } ,error: function(xhr, status, error) {
                	console.log(error);
                    toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                    $('.save-btn').prop('disabled', false);
                },

            });
	    }
	});

</script>

@endsection