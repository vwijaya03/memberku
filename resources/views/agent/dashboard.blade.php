@extends('agent/header')

@section('content')

<style>
    .info-wallet, .info-wallet-process {
        font-size: 20px !important;
    }
</style>

<div class="app-content content container-fluid">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-xs-12 mb-2">
        <h3 class="content-header-title mb-0">Home</h3>
        <div class="row breadcrumbs-top">
          <div class="breadcrumb-wrapper col-xs-12">
            <ol class="breadcrumb">
              <li class=""><a href="{{ url($url_partner.'/dashboard') }}">Home</a>
              </li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">
        
        @if($current_user->type != "skidipapap")
            <div class="col-xl-3 col-lg-6 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="media">
                            <div class="p-2 text-xs-center bg-primary bg-darken-2 media-left media-middle">
                                <i class="icon-wallet font-large-2 white"></i>
                            </div>
                            
                            <div class="p-2 bg-gradient-x-primary white media-body">
                                <b>Struk Diproses</b> &nbsp; <i class="fa fa-info-circle info-wallet-process"></i> <br>
                                <h5 class="text-bold-400">{{ floor($user_balance_standart) }}</h5>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-3 col-lg-6 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="media">
                            <div class="p-2 text-xs-center bg-success bg-darken-2 media-left media-middle">
                                <i class="icon-wallet font-large-2 white"></i>
                            </div>
                            
                            <div class="p-2 bg-gradient-x-success white media-body">
                                <span><b>Struk Bisa Ditukarkan</b></span> &nbsp; <i class="fa fa-info-circle info-wallet"></i> <br>
                                <h5 class="text-bold-400">0</h5>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if($user_balance_toko != 0)
        <div class="col-xl-3 col-lg-6 col-xs-12">
            <div class="card">
                <div class="card-body">
                    <div class="media">
                        <div class="p-2 text-xs-center bg-warning bg-darken-2 media-left media-middle">
                            <i class="ft-credit-card font-large-2 white"></i>
                        </div>
                        
                        <div class="p-2 bg-gradient-x-warning white media-body">
                            <h5>Saldo Toko</h5>
                            <h5 class="text-bold-400">{{ floor($user_balance_toko) }}</h5>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        @endif

        <div class="col-xl-12 col-lg-6 col-xs-12">

            <div class="card border-grey border-lighten-2">
                <div class="text-xs-center">
                    
                    <div class="card-block">
                        <h2><?php echo($company_title); ?></h2>
                    </div>
                    
                    @if($current_user->type == "toko" || $current_user->type == "perusahaan")
                        @if($link_title_if_toko_or_company != null)
                            <div class="card-block">
                                <p>
                                    @if($link_title_if_toko_or_company->description != "")
                                        <b>{{ urldecode($link_title_if_toko_or_company->description) }}</b>
                                    @else
                                    <b>MEMBERKU</b>
                                    @endif
                                </p>
                                
                            </div>
                        @endif
                    @else
                        @if($link_title != null || $link_title != "")
                            <div class="card-block">
                                <p>
                                    @if($link_title->description != "")
                                        <b>{{ urldecode($link_title->description) }}</b>
                                    @else
                                    <b>MEMBERKU</b>
                                    @endif
                                </p>
                                
                            </div>
                        @else
                            <div class="card-block">
                                <p>
                                    <b>MEMBERKU</b>
                                </p>
                                
                            </div>
                        @endif
                    @endif

                    @if($current_user->type == "toko" || $current_user->type == "perusahaan")
                        @if($logo_toko_or_perusahaan != null)
                            <img src="{{ URL::asset($logo_toko_or_perusahaan->img) }}" style="height: 60px;" alt="Card image">
                        @endif
                    @else
                        @if($logo_untuk_bawahan != null || $logo_untuk_bawahan != "")
                            <div class="card-block">
                                @if($logo_untuk_bawahan->img != "")
                                    <img src="{{ URL::asset($logo_untuk_bawahan->img) }}" style="height: 60px;" alt="Card image">
                                
                                @endif
                            </div>           
                        @endif
                    @endif

                    <div class="card-block">
                        {!! QrCode::size(160)->generate($user_code) !!}
                    </div>
                    <div class="card-block">
                        <h4 class="card-title">ID: {{ $user_code }}</h4>
                        <h6 class="card-subtitle text-muted">{{ urldecode($current_user->fullname) }}</h6> <br>
                        <h6 class="card-subtitle text-muted"><a href="{{ url('/u/'.$user_code) }}">Download Kartu Anda</a></h6>
                    </div>

                    <div class="card-block">
                        <a href="{{ url('/claim-gift/u/'.$user_code) }}" class="btn btn-success btn-min-width mr-1 mb-1">Tukar Struk</a>
                    </div>
                    
                    @if($current_user->type == "toko" || $current_user->type == "perusahaan")
                        <div class="card-block">
                            <a href="#" class="btn btn-success mr-1 mb-1" onclick="getLinkTitle()"><span>Edit Judul Kartu</span></a>
                            <a href="#" class="btn btn-success mr-1 mb-1" data-toggle="modal" data-target="#my-logo">Edit Logo</a>
                        </div>
                    @endif

                    @if($current_user->type == "toko" || $current_user->type == "perusahaan")
                        <div class="card-block">
                            <p>Link Pendaftaran</p>
                            <a href="{{ url('/registration/'.$user_code) }}" target="blank">{{ secure_url('/registration/'.$user_code) }}</a>
                        </div>
                    @endif
                </div>
            </div>
            
        </div>

    </div>
  </div>
</div>

<!-- My Logo Modal -->
<div class="modal fade text-xs-left" id="my-logo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <label class="modal-title text-text-bold-600" id="myModalLabel33">My Logo</label>
            </div>

            <form enctype="multipart/form-data" id="upload_logo">
                <div class="modal-body">
                    <label for="file">Logo</label>      
                    <div class="form-group">
                        <input type="file" name="img" class="form-control-file">

                        <br>
                        
                        @if($logo_toko_or_perusahaan != null)
                        <a href="#" class="btn btn-outline-danger btn delete-logo-btn" onclick="deleteLogo()">Hapus Logo</a>
                        
                        Klik <a href="{{ $public_base_url.$logo_toko_or_perusahaan->img }}" target="_blank">di sini</a> untuk melihat logo 
                        @endif    
                    </div>   
                    
                            
                </div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                    <input type="submit" class="btn btn-outline-primary btn my-logo-btn" value="Simpan" onclick="myLogo('{{ $auth_partner_base_url }}', '{{ $current_user->id }}')">
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">

	function deleteLogo() {
        let args = {};
        args.user_uid = '{{ $current_user->uid }}';

        $('.delete-logo-btn').prop('disabled', true);
        toastr.info("Harap menunggu, data sedang di proses", "Loading...");

        $.ajax({
            type: "POST",
            url: '{{ $auth_agent_base_url }}'+'delete-logo',
            dataType: "json",
            data: args,
            cache : false,
            success: function(data){
                toastr.clear();
                
                if(data.code == 400) {
                    if(Array.isArray(data.message)) {
                        toastr.warning(data.message[0], "Peringatan");
                    } else {
                        toastr.warning(data.message, "Peringatan");
                    }
                } else if(data.code == 200) {
                    toastr.success(data.message, "Sukses");

                    $('#my-logo').modal('hide');
                }

                location.reload();
                $('.delete-logo-btn').prop('disabled', false);
            } ,error: function(xhr, status, error) {
                console.log(error);
                toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
                $('.delete-logo-btn').prop('disabled', false);
            },

        });
    }

    function myLogo() {
        let data = new FormData($("#upload_logo")[0]);

        $('.my-logo-btn').prop('disabled', true);
        toastr.info("Harap menunggu, data sedang di proses", "Loading...");

        $.ajax({
            type: "POST",
            processData: false,
            contentType: false,
            url: '{{ $auth_agent_base_url }}'+'add-logo',
            dataType: "json",
            data: data,
            cache : false,
            success: function(data){
                toastr.clear();
                
                if(data.code == 400) {
                    if(Array.isArray(data.message)) {
                        toastr.warning(data.message[0], "Peringatan");
                    } else {
                        toastr.warning(data.message, "Peringatan");
                    }
                } else if(data.code == 200) {
                    toastr.success(data.message, "Sukses");

                    $('#my-logo').modal('hide');                        
                }
                
                location.reload();
                $('.my-logo-btn').prop('disabled', false);
            } ,error: function(xhr, status, error) {
                console.log(error);
                toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
                $('.my-logo-btn').prop('disabled', false);
            },

        });
    }
</script>

@endsection