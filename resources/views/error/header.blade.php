<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="AAI">
        <meta name="keywords" content="AAI">
        <meta name="author" content="AAI">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>AAI</title>
        <link rel="apple-touch-icon" href="{{ URL::asset('admin/app-assets/images/ico/apple-icon-120.png') }}">
        <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('admin/app-assets/images/ico/favicon.ico') }}">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/fonts/feather/style.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/fonts/font-awesome/css/font-awesome.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/fonts/flag-icon-css/css/flag-icon.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/extensions/pace.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/ui/prism.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/extensions/sweetalert.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/tables/datatable/select.dataTables.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/forms/selects/select2.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/pickers/daterange/daterangepicker.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/pickers/datetime/bootstrap-datetimepicker.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/pickers/pickadate/pickadate.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/extensions/toastr.css') }}">
        <!-- END VENDOR CSS-->
        <!-- BEGIN TERA CSS-->
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/bootstrap-extended.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/app.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/colors.min.css') }}">
        <!-- END TERA CSS-->
        <!-- BEGIN Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/core/menu/menu-types/horizontal-menu.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/core/colors/palette-gradient.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/plugins/extensions/toastr.min.css') }}">
        <!-- END Page Level CSS-->
        <!-- BEGIN Custom CSS-->
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/assets/css/style.css') }}">
        <!-- END Custom CSS-->
    </head>
    <body data-open="hover" data-menu="horizontal-menu" data-col="2-columns" class="horizontal-layout horizontal-menu 2-columns   menu-expanded">

        @yield('content')

        <footer class="footer footer-static footer-light navbar-shadow">
            <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Copyright  &copy; 2017 - <?php echo date('Y'); ?> Karya Teknologi Berjaya Indonesia, All rights reserved. </span></p>
        </footer>

        <!-- BEGIN VENDOR JS-->
        <script src="{{ URL::asset('admin/app-assets/vendors/js/vendors.min.js') }}" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <!-- BEGIN PAGE VENDOR JS-->
        <script type="text/javascript" src="{{ URL::asset('admin/app-assets/vendors/js/ui/jquery.sticky.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('admin/app-assets/vendors/js/charts/jquery.sparkline.min.js') }}"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/ui/prism.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/extensions/sweetalert.min.js') }}" type="text/javascript"></script>

        <script src="{{ URL::asset('admin/app-assets/vendors/js/extensions/toastr.min.js') }}" type="text/javascript"></script>

        <script src="{{ URL::asset('admin/app-assets/vendors/js/forms/extended/inputmask/jquery.inputmask.bundle.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/forms/extended/maxlength/bootstrap-maxlength.js') }}" type="text/javascript"></script>

        <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/jszip.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/pdfmake.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/vfs_fonts.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/buttons.html5.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/buttons.print.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/buttons.colVis.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/tables/datatable/dataTables.colVis.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/forms/select/select2.full.min.js') }}" type="text/javascript"></script>

        <script src="{{ URL::asset('admin/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/pickers/dateTime/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/pickers/pickadate/picker.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/pickers/pickadate/picker.time.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/pickers/pickadate/legacy.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/pickers/daterange/daterangepicker.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/js/scripts/pickers/dateTime/picker-date-time.min.js') }}" type="text/javascript"></script>
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN TERA JS-->
        <script src="{{ URL::asset('admin/app-assets/js/core/app-menu.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/js/core/app.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/js/scripts/customizer.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/js/scripts/forms/extended/form-maxlength.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/app-assets/js/scripts/popover/popover.min.js') }}" type="text/javascript"></script>
        <!-- END TERA JS-->
        <!-- BEGIN PAGE LEVEL JS-->
        <script type="text/javascript" src="{{ URL::asset('admin/app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js') }}"></script>
    </body>
</html>