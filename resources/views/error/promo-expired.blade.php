@extends('error/header')

@section('content')

<div class="app-content content container-fluid">
	<div class="content-wrapper">
		<div class="content-header row">
		</div>

		<div class="content-body">
			<section class="flexbox-container">
				<div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1">
					<div class="card-header bg-transparent no-border pb-0">
						<h2 class="error-code text-xs-center mb-2">Promo Expired</h2>
						<h3 class="text-uppercase text-xs-center">Promo / Link Telah Expired</h3>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>

@endsection