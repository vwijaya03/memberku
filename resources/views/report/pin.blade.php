@extends('admin/header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title">Data {{ $page_title }}</h4>
				            </div>
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
									<table width="1280px" class="table table-striped table-bordered dataex-html5-export server-side-report-pin">
										<thead>
											<tr>
												<th>No.</th>
												<th>Promo</th>
												<th>Pin</th>
												<th>Tanggal</th>
											</tr>
										</thead>
									</table>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">
    let table, user_id = '';

	$(document).ready(function() {
        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth()+1; //January is 0!
        let yyyy = today.getFullYear();

        if(dd<10) {
            dd = '0'+dd
        } 

        if(mm<10) {
            mm = '0'+mm
        }

        today = dd + '-' + mm + '-' + yyyy;

	    table = $('.server-side-report-pin').DataTable({
	    	"scrollX": !0,
	    	"lengthMenu": [[25, 50, 100, 200], [25, 50, 100, 200]],
	        "processing": true,
	        "serverSide": true,
	        "ajax":{
	        	"type": "POST",
            	"url": "{{ url($url_admin.'/report-pin') }}",
                "dataType": "json",
                "data": function(param) {
                    param.pin = '{{ $current_pin }}';
                }
           	},
	        "columns": [
	            { "data": "no" },
	            { "data": "title" },
	            { "data": "pin" },
	            { "data": "created_at" }
	        ],
            order: [[3, 'desc']],
            dom: "lBrtip",
            buttons: [{
                extend: 'pdf',
                title: 'Report Pin' + decodeURIComponent(' {{ $current_user->fullname }} ') + today,
                customize: function (doc) {
                    doc.content[1].table.widths = [ '15%', '25%', '20%', '35%' ];
                    doc.defaultStyle.alignment = 'center';
                }
            }],
            "columnDefs": [
                { "orderable": false, "targets": [ 0 ] },
                // { "width": "190px", "targets": [ 7 ] },
                // { "width": "120px", "targets": [ 5, 6 ] },
            ]
	    });

	});
</script>

@endsection