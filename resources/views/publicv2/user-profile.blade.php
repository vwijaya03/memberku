<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="MEMBERKU">
		<meta name="keywords" content="MEMBERKU">
		<meta name="author" content="MEMBERKU">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>MEMBERKU</title>
        
        <link rel="apple-touch-icon" href="{{ URL::asset('img/icon_kd.jpeg') }}">
		<link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('img/icon_kd.jpeg') }}">
        <link href="https://fonts.googleapis.com/css?family=Cabin:400,400i,500,500i,600,600i,700,700i&display=swap" rel="stylesheet">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ URL::asset('admin/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('admin/css/slick.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('admin/css/slick-theme.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('admin/css/smart_wizard.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('admin/css/smart_wizard_theme_circles.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('admin/css/style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/extensions/toastr.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/plugins/extensions/toastr.min.css') }}">

        <style>
            .center {
                width: 100%;
                /* border: 3px solid green; */
                padding: 10px;
                position: absolute; 
                left: 0; 
                right: 0; 
                margin-left: auto; 
                margin-right: auto;
                top: 40px;
                color: white;
                align: center;
            }

            .img-center {
                display: block;
                margin-left: auto;
                margin-right: auto;
                width: 100%;
            }
        </style>
    </head>

    <body>
        <div class="top-menu">
            <nav class="navbar navbar-dark topmenu">
                <div class="dropdown">
                    <button class="btn menu-btn" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-bars" aria-hidden="true"></i>
                    </button>

                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        <a class="dropdown-item" href="{{ url($auth_agent_base_url.'dashboard') }}">Home</a>
                        <a class="dropdown-item" href="{{ url('/u/'.$user_code) }}">Tukar Struk</a>
                        <a class="dropdown-item" href="{{ url($auth_agent_base_url.'teman') }}">Pendaftaran</a>
                        <a class="dropdown-item" href="{{ url($auth_agent_base_url) }}">Login</a>
                        {{-- <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModalCenter">Tarik Uang</a> --}}
                    </div>
                </div>

                {{-- <button type="button" class="btn app-button" onclick="redirectLogin()">Login</button> --}}
                <img src="{{ URL::asset("/img/AAI_white.png") }}" style="height: 40px;" alt="Card image">
            </nav>
        </div>

        <section class="user-details">
            <p class="user-name">@if($user != null) {{ urldecode($user->fullname) }} @endif</p>
            <p>@if($user != null) {{ $user_code }} @endif</p>
            
            @if($logo_untuk_bawahan != null || $logo_untuk_bawahan != "")
                <div class="card-block">
                    @if($logo_untuk_bawahan->img != "")
                        <img src="{{ URL::asset($logo_untuk_bawahan->img) }}" style="height: 50px;" alt="Card image">
                    @endif
                    
                </div>          
            @endif
            
            @if($company_logo != null || $company_logo != "")
                <div class="card-block" align="center">
                    <img src="{{ URL::asset($company_logo) }}" style="height: 50px;" alt="Card image">
                </div>  
            @endif

            <br><br>
        </section>

        <div id="smartwizard">
            <ul>
                <li><a href="#step-1">1<br /><small>Pilih merchant</small></a></li>
                <li><a href="#step-2">2<br /><small>Tunjukkan member</small></a></li>
                <li><a href="#step-3">3<br /><small>Input data struk</small></a></li>
                <li><a href="#step-4">4<br /><small>Selesai</small></a></li>
            </ul>

            <div>
                <div id="step-1" class="">
                    <section class="merchats">
                        <h3 class="merchats-heading">Merchants</h3>

                        <div class="merchats-logos slider">
                            @foreach ($merchants as $merchant)
                                <div class="slide slider-img-{{ $merchant->uid }}" onclick="chooseMerchant('{{ $merchant->uid }}', '{{ url('/').$merchant->img }}', event)">
                                    <a href="#">
                                        <img src="{{ URL::asset($merchant->logo) }}" style="width: 100px; height: 100px;">
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </section>
                </div>

                <div id="step-2" class="">
                    <section class="second-step">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="center">                            
                                    <p align="center"><b>Tunjukkan Halaman Ini Pada Kasir </b></p>
                                    
                                    <!-- <p align="center">Pastikan kasir menginput / mencatat nomor ID anda</p> <br> -->
        
                                    <h4 class="card-title" align="center">
                                        @if($user_code != null) ID: {{ $user_code }} @endif
                                    </h4>
        
                                    <br><br>
                                    
                                    <div align="center">
                                        <img src="{{ URL::asset('img/logo_aai_with_txt.jpeg') }}" height="60px" width="120px"><br>
                                        {!! QrCode::size(120)->generate($user_code) !!}
                                    </div>
        
                                </div>
        
                                <img src="{{ URL::asset('img/bg_id_card.png') }}" class="img-center">
                            </div>
                        </div>
                    </section>
                </div>

                <div id="step-3" class="">
                    <section class="third-step">
                        <form action="#" enctype="multipart/form-data" id="uploadTukarStruk">
                            <div class="form-group">
                                <label for="NomorStruk">Nomor Struk</label>
                                <input type="text" class="form-control no-struk" id="NomorStruk" name="no_struk">
                            </div>
                            
                            <div class="form-group">
                                <label for="NominalStruk">Nominal Struk</label>
                                <input type="text" class="form-control jumlah-nominal" id="NominalStruk">
                            </div>
    
                            <div class="form-group">
                                <label for="NominalStruk">Foto Struk</label>
                                <input type="file" class="form-control ImgStruk" id="ImgStruk" accept="image/*" capture="camera" name="img_struk">
                            </div>
                        </form>
                        
                        <div class="form-group" align="center">
                            <p><b>Contoh Struk</b></p>
                            <img src="" width="40%" class="ex-struk">
                        </div>
                    </section>
                </div>

                <div id="step-4" class="">
                    <section class="forth-step">
                        <p class="check-icon"><i class="fa fa-check" aria-hidden="true"></i></p>
                        <p class="finishing-text">STRUK SEDANG DIPROSES</p>
                        <p>Syarat Penukaran Struk:</p>
                        <p align="left">1. Tercetak kode AAI / Arius, atau stempel (khusus McD) pada struk</p>
                        {{-- <p align="left">2. Hanya berlaku 1x input untuk 1 nomor struk</p> --}}
                        <p align="left">2. Struk akan ditolak jika terindikasi kecurangan atau bukan transaksi yang sebenarnya</p>
                        <p align="right">
                            <button type="button" class="btn app-button" onclick="resetWizard()">Setuju</button>
                        </p>
                    </section>
                </div>
            </div>
        </div>

        {{-- <section class="promos">
            <h3 class="promos-heading">Promos</h3>

            <div class="promos-offer slider">
                <div class="slide">
                    <a href="#">
                        <img src="https://via.placeholder.com/908x390.png">
                    </a>
                </div>

                <div class="slide">
                    <a href="#">
                        <img src="https://via.placeholder.com/908x390.png">
                    </a>
                </div>

                <div class="slide">
                    <a href="#">
                        <img src="https://via.placeholder.com/908x390.png">
                    </a>
                </div>

                <div class="slide">
                    <a href="#">
                        <img src="https://via.placeholder.com/908x390.png">
                    </a>
                </div>

                <div class="slide">
                    <a href="#">
                        <img src="https://via.placeholder.com/908x390.png">
                    </a>
                </div>

                <div class="slide">
                    <a href="#">
                        <img src="https://via.placeholder.com/908x390.png">
                    </a>
                </div>
            </div>
        </section> --}}

        <!-- Modal -->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Kode AAI</h5>

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p class="model-text">contoh kode struk</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="{{ URL::asset('admin/js/jquery-3.4.1.js') }}"></script>
        <script src="{{ URL::asset('admin/js/jquery-migrate-1.2.1.min.js') }}"></script>
        <script src="{{ URL::asset('admin/js/jquery-ui.js') }}"></script>
        <script src="{{ URL::asset('admin/js/popper.min.js') }}"></script>
        <script src="{{ URL::asset('admin/js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('admin/js/slick.min.js') }}"></script>
        <script src="{{ URL::asset('admin/js/jquery.smartWizard.js') }}"></script>
        <script src="{{ URL::asset('admin/js/custom.js') }}"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/extensions/toastr.min.js') }}" type="text/javascript"></script>
        <script type="text/javascript">
            let merchant = "";

            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $('.jumlah-nominal').keyup(function() {
                    $('.jumlah-nominal').val(formatRupiah($('.jumlah-nominal').val()));
                });

                // Smart Wizard
                $('#smartwizard').smartWizard({
                    selected: 0,  // Initial selected step, 0 = first step 
                    keyNavigation:true, // Enable/Disable keyboard navigation(left and right keys are used if enabled)
                    autoAdjustHeight:true, // Automatically adjust content height
                    cycleSteps: false, // Allows to cycle the navigation of steps
                    backButtonSupport: true, // Enable the back button support
                    useURLhash: true, // Enable selection of the step based on url hash
                    lang: {  // Language variables
                        next: 'Lanjut', 
                        previous: 'Kembali'
                    },
                    toolbarSettings: {
                        toolbarPosition: 'bottom', // none, top, bottom, both
                        toolbarButtonPosition: 'right', // left, right
                        showNextButton: true, // show/hide a Next button
                        showPreviousButton: true, // show/hide a Previous button
                        toolbarExtraButtons: [
                            $('<button></button>').text('Kirim')
                            .addClass('btn btn-info finish-btn')
                            .on('click', function(){ 
                                $('.finish-btn').prop('disabled', true);

                                nominal = $('.jumlah-nominal').val();
                                no_struk = $('.no-struk').val();
                                // no_reg = $('.no-reg').val();

                                if(nominal == null || nominal == 0 || nominal == "") {
                                    toastr.warning("Jumlah nominal tidak boleh 0", "Peringatan");
                                    $('.jumlah-nominal').val(0);
                                    $('.finish-btn').prop('disabled', false);
                                    return
                                }

                                if(no_struk == null || no_struk == "") {
                                    toastr.warning("Nomor struk tidak boleh kosong", "Peringatan");
                                    $('.jumlah-nominal').val("");
                                    $('.finish-btn').prop('disabled', false);
                                    return
                                }

                                // if(no_reg == null || no_reg == "") {
                                //     toastr.warning("Nomor register tidak boleh kosong", "Peringatan");
                                //     return
                                // }
                                
                                let data = new FormData($("#uploadTukarStruk")[0]);
                                data.append('merchant_uid', merchant);
                                data.append('user_uid', "{{ $user->uid }}");
                                data.append('no_reg', "empty");
                                data.append('nominal', nominal.replace(/\./g, ''));

                                // let args = {};
                                // args.merchant_uid = merchant;
                                // args.user_uid = "{{ $user->uid }}";
                                // args.nominal = nominal.replace(/\./g, '');
                                // args.no_struk = no_struk;
                                // args.no_reg = "empty";

                                $.ajax({
                                    type: "POST",
                                    processData: false,
                                    contentType: false,
                                    url: '{{ url("/") }}'+'/claim-gift',
                                    dataType: "json",
                                    data: data,
                                    cache : false,
                                    success: function(data){
                                        toastr.clear();
                                    
                                        if(data.code == 400) {
                                            if(Array.isArray(data.message)) {
                                            toastr.warning(data.message[0], "Peringatan");
                                            } else {
                                            toastr.warning(data.message, "Peringatan");
                                            }
                                        } else if(data.code == 200) {
                                            // $('#claim-gift-agreement').modal('hide');
                                            // $('#voucher-bonus-result').modal({backdrop: 'static', keyboard: false});
                                            toastr.success("Input data struk berhasil", "Success");
                                            $('.finish-btn').hide();
                                            $('#smartwizard').smartWizard("next");
                                        }

                                        $('.finish-btn').prop('disabled', false);
                                    } ,error: function(xhr, status, error) {
                                        console.log(error);
                                        toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                                        $('.finish-btn').prop('disabled', false);
                                    },

                                });                           
                            }),
                        ]
                    }, 
                    anchorSettings: {
                        anchorClickable: false, // Enable/Disable anchor navigation
                        enableAllAnchors: false, // Activates all anchors clickable all times
                        markDoneStep: true, // add done css
                        enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
                    },            
                    contentURL: null, // content url, Enables Ajax content loading. can set as data data-content-url on anchor
                    disabledSteps: [],    // Array Steps disabled
                    errorSteps: [],    // Highlight step with errors
                    theme: 'default',
                    transitionEffect: 'fade', // Effect on navigation, none/slide/fade
                    transitionSpeed: '400',
                }); 
                
                $("#smartwizard").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
                    var activeli = $('ul.step-anchor').find('li.active');
                    
                    // return confirm("Do you want to leave the step "+stepNumber+"?");
                    if(stepDirection == "forward" && stepNumber == 1) {
                        $('.sw-btn-next').hide();
                        $('.finish-btn').show();
                    } else {
                        $('.sw-btn-next').show();
                        $('.finish-btn').hide();
                    }

                    if(stepDirection == "forward" && stepNumber == 0) {
                        if(merchant == "" || merchant == null) {
                            activeli.addClass("danger");
                            toastr.warning("Merchant belum ada yang dipilih", "Peringatan");
                            return false;
                        } else {
                            activeli.removeClass("danger");
                            activeli.addClass("success");
                        }
                    }

                    if(stepDirection == "forward" && stepNumber == 2) {
                        if(merchant == "" || merchant == null) {
                            activeli.addClass("danger");
                            toastr.warning("Merchant belum ada yang dipilih", "Peringatan");
                            return false;
                        } else {
                            activeli.removeClass("danger");
                            activeli.addClass("success");
                            $('.sw-btn-next').hide();
                        }
                    }
                });

                $('.finish-btn').hide();
                $('.sw-btn-prev').hide();
            });

            function chooseMerchant(merchantUid, imgPath, e) {
                e.preventDefault();
                $("[class*=slider-img-]").css('border', "none");
                $(".slider-img-"+merchantUid).css('border', "solid 3px #43a047");
                $('.ex-struk').attr("src", imgPath)

                merchant = merchantUid;
            }

            function formatRupiah(angka, prefix) {
                let number_string   = angka.replace(/[^,\d]/g, '').toString(),
                split   		    = number_string.split(','),
                sisa     		    = split[0].length % 3,
                rupiah     		    = split[0].substr(0, sisa),
                ribuan     		    = split[0].substr(sisa).match(/\d{3}/gi);

                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }

                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;

                return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
            }

            function redirectLogin() {
                window.location.href = "{{ url('/client') }}";
            }

            function resetWizard() {
                $('#smartwizard').smartWizard("reset");
                $('.finish-btn').hide();
                $('.sw-btn-prev').hide();
            }
        </script>
    </body>
</html>