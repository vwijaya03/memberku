<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="MEMBERKU">
		<meta name="keywords" content="MEMBERKU">
		<meta name="author" content="MEMBERKU">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>MEMBERKU</title>

        <link rel="apple-touch-icon" href="{{ URL::asset('img/icon_kd.jpeg') }}">
		<link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('img/icon_kd.jpeg') }}">
        <link href="https://fonts.googleapis.com/css?family=Cabin:400,400i,500,500i,600,600i,700,700i&display=swap" rel="stylesheet">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ URL::asset('admin/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('admin/css/slick.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('admin/css/slick-theme.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('admin/css/smart_wizard.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('admin/css/smart_wizard_theme_circles.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('admin/css/style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/extensions/toastr.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/plugins/extensions/toastr.min.css') }}">

        <style>
            .centered-img {
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                width: 14%;
            }
        </style>
    </head>

    <body>
        <div class="top-menu">
            <nav class="navbar navbar-dark topmenu">
                <div class="dropdown">
                    {{-- <button class="btn menu-btn" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-bars" aria-hidden="true"></i>
                    </button> --}}

                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        <a class="dropdown-item" href="#" >Home</a>
                        <a class="dropdown-item" href="#">Profile</a>
                        <a class="dropdown-item" href="#">Member</a>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModalCenter">Syarat & Ketentuan</a>
                    </div>

                </div>

                {{-- <button type="button" class="btn app-button">Login</button> --}}
                <img class="centered-img" src="{{ URL::asset("/img/AAI_white.png") }}" alt="AAI">
            </nav>
        </div>

        <section class="account-steps">
            <div class="login-details">
                <div class="login-form">
                    <p class="user-login">User Login</p>

                    @if ($errors->has('email'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            No. HP tidak boleh kosong
                        </div>
                    @endif

                    @if ($errors->has('password'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            Password tidak boleh kosong
                        </div>
                    @endif
                        
                    @if(Session::has('err'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            {{ Session::get('err') }}
                        </div>
                    @endif
                        
                    <form action="{{ url($url_agent.'/login') }}" method="POST" id="signin" class="navbar-form navbar-right" role="form">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                            <input id="email" type="text" class="form-control" name="email" value="" placeholder="No. HP">
                        </div>

                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-key" aria-hidden="true"></i></span>
                            <input id="password" type="password" class="form-control" name="password" value="" placeholder="Password">
                        </div>

                        <button type="submit" class="btn app-button">Login</button>
                    </form>

                    <a href="#" data-toggle="modal" data-target="#reset-password" class="forgot-password-link">lupa passsword</a>
                </div>

                <hr>

                <div class="contact-info">
                    <p>Dengan menggunakan layanan ini, maka Anda setuju dengan<br> 
                    <a href="{{ url('/terms-conditions') }}">Syarat Penggunaan</a> dan <a href="{{ url('/privacy-and-policy') }}">Kebijakan Privasi</a></p>
                    <p>Contact Centre:<br>
                    <a href="#">admin@ariusangkasa.com</a></p>
                </div>
            </div>
        </section>

        <!-- Reset Password Modal -->
        <div class="modal fade text-xs-left" id="reset-password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                    </div>

                    <form>
                        <div class="modal-body">
                            <label>Email </label>
                            <div class="form-group">
                                <input type="text" placeholder="Email" class="form-control rp_email">
                            </div>
                        </div>

                        <div class="modal-footer">
                            <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                        <input type="submit" class="btn btn-outline-primary btn reset-password-btn" value="Kirim" onclick="resetPassword('{{ $auth_agent_base_url }}')">
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="{{ URL::asset('admin/js/jquery-3.4.1.js') }}"></script>
        <script type="text/javascript"src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="{{ URL::asset('admin/js/popper.min.js') }}"></script>
        <script src="{{ URL::asset('admin/js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('admin/js/slick.min.js') }}"></script>
        <script src="{{ URL::asset('admin/js/jquery.smartWizard.js') }}"></script>
        <script src="{{ URL::asset('admin/js/custom.js') }}"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/extensions/toastr.min.js') }}" type="text/javascript"></script>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            function resetPassword(base_url) {
                let args = {};
                args.email = $('.rp_email').val();

                $('.reset-password-btn').prop('disabled', true);
                toastr.info("Harap menunggu, sedang mengirim data", "Loading...");
                
                $.ajax({
                    type: "POST",
                    url: base_url+'reset-password',
                    dataType: "json",
                    data: args,
                    cache : false,
                    success: function(data){
                        toastr.clear();
                        
                        if(data.code == 400) {
                            if(Array.isArray(data.message)) {
                                toastr.warning(data.message[0], "Peringatan");
                            } else {
                                toastr.warning(data.message, "Peringatan");
                            }
                        } else if(data.code == 200) {
                            toastr.success(data.message, "Sukses");
                            $('#reset-password').modal('hide');
                        }

                        $('.reset-password-btn').prop('disabled', false);
                    } ,error: function(xhr, status, error) {
                        console.log(error);
                        toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                        $('.reset-password-btn').prop('disabled', false);
                    },
                });
            }
        </script>
    </body>
</html>