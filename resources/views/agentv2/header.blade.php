<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="MEMBERKU">
		<meta name="keywords" content="MEMBERKU">
		<meta name="author" content="MEMBERKU">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>MEMBERKU</title>

        <link rel="apple-touch-icon" href="{{ URL::asset('img/icon_kd.jpeg') }}">
		<link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('img/icon_kd.jpeg') }}">
        <link href="https://fonts.googleapis.com/css?family=Cabin:400,400i,500,500i,600,600i,700,700i&display=swap" rel="stylesheet">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ URL::asset('admin/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
        <link rel="stylesheet" href="{{ URL::asset('admin/css/slick.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('admin/css/slick-theme.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('admin/css/smart_wizard.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('admin/css/smart_wizard_theme_circles.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('admin/css/style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/extensions/toastr.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/plugins/extensions/toastr.min.css') }}">
    </head>

    <body>
        <div class="top-menu">
            <nav class="navbar navbar-dark topmenu">
                <div class="dropdown">
                    <button class="btn menu-btn" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-bars" aria-hidden="true"></i>
                    </button>

                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        <a class="dropdown-item" href="{{ url($auth_agent_base_url.'dashboard') }}">Home</a>
                        <a class="dropdown-item" href="{{ url('/u/'.$user_code) }}">Tukar Struk</a>
                        <a class="dropdown-item" href="{{ url($auth_agent_base_url.'teman') }}">Pendaftaran</a>
                        {{-- <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModalCenter">Tarik Uang</a> --}}
                    </div>
                </div>

                <a href="{{ url($url_agent.'/logout') }}" class="btn app-button"><i class="ft-power"></i> Logout</a>
            </nav>
        </div>

        @yield('content')

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="{{ URL::asset('admin/js/jquery-3.4.1.js') }}"></script>
        <script src="{{ URL::asset('admin/js/jquery-migrate-1.2.1.min.js') }}"></script>
        <script src="{{ URL::asset('admin/js/jquery-ui.js') }}"></script>
        <script src="{{ URL::asset('admin/js/popper.min.js') }}"></script>
        <script src="{{ URL::asset('admin/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
        <script src="{{ URL::asset('admin/js/slick.min.js') }}"></script>
        <script src="{{ URL::asset('admin/js/jquery.smartWizard.js') }}"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/extensions/toastr.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/js/custom.js') }}"></script>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.info-wallet-process').click(function(e) {
                toastr.info("Value akan dimasukkan ke saldo anda setelah proses verifikasi setiap tanggal 20 pada bulan berikutnya", "Info");
            });
        </script>
        
        @yield('custom_js_script')
    </body>
</html>