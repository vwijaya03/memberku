<?php
    use App\Http\Controllers\HelperController;
?>

<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="MEMBERKU">
		<meta name="keywords" content="MEMBERKU">
		<meta name="author" content="MEMBERKU">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>MEMBERKU</title>

        <link rel="apple-touch-icon" href="{{ URL::asset('img/icon_kd.jpeg') }}">
		<link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('img/icon_kd.jpeg') }}">
        <link href="https://fonts.googleapis.com/css?family=Cabin:400,400i,500,500i,600,600i,700,700i&display=swap" rel="stylesheet">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ URL::asset('admin/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('admin/css/slick.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('admin/css/slick-theme.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('admin/css/smart_wizard.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('admin/css/smart_wizard_theme_circles.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('admin/css/style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/vendors/css/extensions/toastr.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/app-assets/css/plugins/extensions/toastr.min.css') }}">

        <!-- Font Awesome JS -->        
        <script src="{{ URL::asset('admin/font-awesome/js/all.js') }}"></script>
        
        <style>
            .row-design {
                padding: 0px 0px !important;
            }
        </style>
    </head>

    <body>
        <div class="top-menu">
            <nav class="navbar navbar-dark topmenu">
                <div class="dropdown">
                    <button class="btn menu-btn" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-bars" aria-hidden="true"></i>
                    </button>

                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        <a class="dropdown-item" href="{{ url($auth_agent_base_url.'dashboard') }}">Home</a>
                        <a class="dropdown-item" href="{{ url('/u/'.$user_code) }}">Tukar Struk</a>
                        <a class="dropdown-item" href="{{ url($auth_agent_base_url.'teman') }}">Pendaftaran</a>
                        @if(count($res_user_bank_accounts) < 1)
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#add-bank-account">Tarik Saldo</a>
                        @endif
                    </div>
                </div>

                <a href="{{ url($url_agent.'/logout') }}" class="btn app-button"><i class="ft-power"></i> Logout</a>
            </nav>
        </div>

        <section class="account-steps">
            <div class="row user-detail row-design">
                <div class="col-12">
                    <p class="user" style="font-size: 16px !important;">ID: {{ $user_code }}</p>
                    <p class="user-fullname">{{ urldecode($current_user->fullname) }}</p>

                    <span style="float:right; font-size: 14px !important;">
                        <a href="#" data-toggle="modal" data-target="#change-profile">EDIT PROFIL</a>
                    </span>                    
                </div>
            </div>

            <div class="balance row-design">
                <div class="row row-padding border-bottom">
                    <div class="col-sm-8 col-8 ">
                        {{-- <i class="fas fa-wallet" aria-hidden="true" style="margin-left: 20px !important;"></i> --}}
                        <p style="font-size: 12px !important; margin-top: 7px;">
                            <img src="{{ URL::asset("/img/strukDiproses.png") }}" style="height: 30px; margin-left: 20px !important;" alt="Card image">
                            <span style="margin-left: 10px !important;">Diproses</span>
                            <span class="riwayat-cost" style="font-size: 13px !important;"><b>{{ HelperController::formatRupiah(floor($user_balance_standart)) }}</b></span>
                        </p>
                    </div>

                    <div class="col-sm-4 col-4 ">
                        <i class="fa fa-info-circle info-wallet-process" style="border: 0px !important; float:right !important; font-size: 25px !important; margin-right: 25px; margin-top: 10px; color: grey;" onclick="infoVerifikasi()"></i>
                    </div>
                </div>

                <div class="row row-padding">
                    <div class="col-sm-8 col-8">
                        {{-- <i class="fas fa-wallet" aria-hidden="true" style="margin-left: 20px !important;"></i> --}}
                        <p style="font-size: 12px !important; margin-top: 7px;">
                            <img src="{{ URL::asset("/img/saldo.png") }}" style="height: 25px; margin-left: 20px !important;" alt="Card image">
                            <span style="margin-left: 10px !important;">Saldo</span> 
                            <span class="riwayat-cost" style="font-size: 13px !important;"><b>{{ HelperController::formatRupiah(floor($user_active_balance_standart)) }}</b></span>
                        </p>
                    </div>

                    <div class="col-sm-4 col-4 ">
                        <button type="button" data-toggle="modal" data-target="#TukarSaldo" class="btn btn-primary float-right" style="margin-right: 20px !important;">Tukar saldo</button>
                    </div>
                    {{-- @if($current_user->type == "perusahaan")
                        @if(count($res_user_bank_accounts) < 1)
                            <div class="col-sm-4 col-4 ">
                                <button type="button" data-toggle="modal" data-target="#add-bank-account" class="btn btn-primary float-right tukar-saldo-btn" style="margin-right: 20px !important;">Tukar saldo</button>
                            </div>
                        @endif
                    @elseif($current_user->type == "toko")
                        <div class="col-sm-4 col-4 ">
                            <button type="button" data-toggle="modal" data-target="#TukarSaldo" class="btn btn-primary float-right" style="margin-right: 20px !important;">Tukar saldo</button>
                        </div>
                    @endif --}}
                </div>
            </div>

            <div class="riwayat">
                <p class="riwayat-heading" style="font-size: 17px !important;">Penarikan Saldo</p>
                @if(count($res_user_bank_accounts) < 1)
                    <div class="row row-design">
                        <div class="col-6">
                            <p class="riwayat-title" style="font-size: 13px !important;">Data tidak ditemukan</p>
                        </div>
                        
                        <div class="col-6">
                            <p class="riwayat-cost" style="font-size: 13px !important;"></p>
                        </div>
                    </div>
                @endif

                @foreach ($res_user_bank_accounts as $userBankAccount)
                    <div class="row row-design">
                        <div class="col-6">
                            <p class="riwayat-title" style="font-size: 13px !important;">{{ urlencode($userBankAccount->name) }}</p>
                        </div>
                        
                        <div class="col-6">
                            <p class="riwayat-cost" style="font-size: 13px !important;">{{ $userBankAccount->no_rek}}</p>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="riwayat">
                <p class="riwayat-heading" style="font-size: 17px !important;">Recent History Tukar Struk</p>

                @foreach ($recents_history as $recent_history)
                    <div class="row row-design">
                        <div class="col-6">
                            <p class="riwayat-title" style="font-size: 13px !important;">{{ urldecode($recent_history->name) }}</p>
                            <p class="riwayat-date" style="font-size: 13px !important;">{{ date('d F Y H:i:s', strtotime($recent_history->created_at)) }}</p>
                        </div>
                        
                        <div class="col-6">
                            @if($recent_history->status == "1")
                                <p class="text-success riwayat-status" style="font-size: 13px !important;">Sukses</p>
                            @elseif($recent_history->status == "2")
                                <p class="text-danger riwayat-status" style="font-size: 13px !important;">Gagal</p>
                            @else
                                <p class="text-warning riwayat-status" style="font-size: 13px !important;">Proses</p>
                            @endif
                            
                            <p class="riwayat-cost" style="font-size: 13px !important;">{{ HelperController::formatRupiah(floor($recent_history->amount)) }}</p>

                            <p style="font-size: 12px !important;">{{ $recent_history->additional_note }}</p>
                        </div>
                    </div>
                @endforeach

                {{-- <div class="row row-design">
                    <div class="col-8">
                        <p class="riwayat-title">Nama Merchant</p>
                        <p class="riwayat-date">DD/MM/YYYY</p>
                    </div>

                    <div class="col-4">
                        <p class="text-success riwayat-status">sukses</p>
                        <p class="riwayat-cost">Rp 5.0000</p>
                    </div>
                </div>

                <div class="row row-design">
                    <div class="col-8">
                        <p class="riwayat-title">Nama Merchant</p>
                        <p class="riwayat-date">DD/MM/YYYY</p>
                    </div>

                    <div class="col-4">
                        <p class="text-danger riwayat-status">gagal</p>
                        <p class="riwayat-cost">Rp 5.0000</p>
                    </div>
                </div> --}}
            </div>
        </section>

        <!-- Modal -->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Kode AAI</h5>

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <p class="model-text">contoh kode struk</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="TukarSaldo" tabindex="-1" role="dialog" aria-labelledby="TukarSaldo" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <h5 class="modal-title" id="exampleModalLongTitle">Jumlah saldo ditukar</h5>

                        <div id="popup-form">
                            <div class="input-group">
                                <input id="jumlah" type="text" class="form-control" name="jumlah" value="" >                                        
                            </div>
                            
                            <button type="submit" class="btn app-button" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn float-right app-button" onclick="exchange()">Tukar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Change Profile Modal -->
        <div class="modal fade text-xs-left" id="change-profile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                    </div>

                    <form>
                        <div class="modal-body">
                            <label>Nama Lengkap </label>
                            <div class="form-group">
                                <input type="text" placeholder="Nama Lengkap" class="form-control cp_fullname" value="{{ urldecode($current_user->fullname) }}">
                            </div>

                            <label>E-mail </label>
                            <div class="form-group">
                                <input type="text" placeholder="E-mail" class="form-control cp_email" value="{{ urldecode($current_user->email) }}">
                            </div>

                            <label>No HP </label>
                            <div class="form-group">
                                <input type="text" placeholder="No HP" class="form-control cp_phone" value="{{ $current_user->phone }}" disabled>
                            </div>

                            <label>Password (Isi password jika anda ingin mengubahnya)</label>
                            <div class="form-group">
                                <input type="password" placeholder="Password" class="form-control cp_password" value="">
                            </div>

                        </div>

                        <div class="modal-footer">
                            <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                            <input type="submit" class="btn btn-outline-primary btn change-profile-btn" value="Ubah" onclick="change_profile('{{ $auth_agent_base_url }}', '{{ $current_user->id }}')">
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Edit Judul Kartu Modal -->
        <div class="modal fade text-xs-left" id="judul-link" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                        <label class="modal-title text-text-bold-600" id="myModalLabel33">Judul Link</label>
                    </div>

                    <form>
                        <div class="modal-body">
                            <label>Judul Link *</label>
                            <div class="form-group">
                                <textarea placeholder="Judul link..." class="form-control judul-link judul-link-maxlength" id="maxlength-textarea" maxlength="50" rows="8"></textarea>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup" onclick="closeModalJudulLink()">
                            <input type="submit" class="btn btn-outline-primary btn save-judul-link-btn" value="Simpan" onclick="addLinkTitle()">
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Add Bank Account Modal -->
        <div class="modal fade text-xs-left" id="add-bank-account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <label class="modal-title text-text-bold-600" id="myModalLabel33">Data Bank Account</label>

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
        
                    <form action="#">
                        <div class="modal-body">
                            <p>* Demi keamanan, nomor rekening yang disimpan TIDAK DAPAT DIUBAH</p>
                            {{-- <p>* Ketentuan Pencairan Uang: </p>
                            <p>1. Saldo minimal Rp. 25.000</p>
                            <p>2. Pencairan otomatis tanggal 15 setiap bulan</p> --}}

                            <br>

                            <label>Bank / Wallet Account *</label>
                            <div class="form-group">
                                <select class="select2-bank form-control bank" name="bank" style="width: 100%">
                                    @foreach($banks as $bank)
                                        <option value="{{ $bank->uid }}">{{ urldecode($bank->name) }}</option>
                                    @endforeach
                                </select>
                            </div>
        
                            <label>Nomor Account *</label>
                            <div class="form-group">
                                <input type="text" placeholder="Nomor Account" class="form-control no_rek">
                            </div>
                            
                        </div>
        
                        <div class="modal-footer">
                            <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                            <input type="submit" class="btn btn-outline-primary btn save-btn" onclick="addBankAccount()" value="Simpan">
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="{{ URL::asset('admin/js/jquery-3.4.1.js') }}"></script>
        <script src="{{ URL::asset('admin/js/jquery-migrate-1.2.1.min.js') }}"></script>
        <script src="{{ URL::asset('admin/js/jquery-ui.js') }}"></script>
        <script src="{{ URL::asset('admin/js/popper.min.js') }}"></script>
        <script src="{{ URL::asset('admin/js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('admin/js/slick.min.js') }}"></script>
        <script src="{{ URL::asset('admin/js/jquery.smartWizard.js') }}"></script>
        <script src="{{ URL::asset('admin/js/custom.js') }}"></script>
        <script src="{{ URL::asset('admin/app-assets/vendors/js/extensions/toastr.min.js') }}" type="text/javascript"></script>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.info-wallet-process').click(function(e) {
                
            });

            function infoVerifikasi() {
                toastr.info("Value akan dimasukkan ke saldo anda setelah proses verifikasi setiap tanggal 20 pada bulan berikutnya", "Info");
            }

            function change_profile(base_url, id) {
                let args = {};
                args.id = id;
                args.fullname = $('.cp_fullname').val();
                args.email = $('.cp_email').val();
                args.phone = $('.cp_phone').val();
                args.password = $('.cp_password').val();
                args.type = "{{ $current_user->type }}";

                $('.change-profile-btn').prop('disabled', true);
                toastr.info("Harap menunggu, data sedang di proses", "Loading...");
                
                $.ajax({
                    type: "POST",
                    url: base_url+'change-profile',
                    dataType: "json",
                    data: args,
                    cache : false,
                    success: function(data){
                        toastr.clear();
                        
                        if(data.code == 400) {
                            if(Array.isArray(data.message)) {
                                toastr.warning(data.message[0], "Peringatan");
                            } else {
                                toastr.warning(data.message, "Peringatan");
                            }
                        } else if(data.code == 200) {
                            toastr.success(data.message, "Sukses");
                            $('.user-fullname').text(args.fullname);
                            $('#change-profile').modal('hide');
                        }

                        $('.change-profile-btn').prop('disabled', false);
                    } ,error: function(xhr, status, error) {
                        console.log(error);
                        toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                        $('.change-profile-btn').prop('disabled', false);
                    },

                });
            }

            function deleteLogo() {
                let args = {};
                args.user_uid = '{{ $current_user->uid }}';

                $('.delete-logo-btn').prop('disabled', true);
                toastr.info("Harap menunggu, data sedang di proses", "Loading...");

                $.ajax({
                    type: "POST",
                    url: '{{ $auth_agent_base_url }}'+'delete-logo',
                    dataType: "json",
                    data: args,
                    cache : false,
                    success: function(data){
                        toastr.clear();
                        
                        if(data.code == 400) {
                            if(Array.isArray(data.message)) {
                                toastr.warning(data.message[0], "Peringatan");
                            } else {
                                toastr.warning(data.message, "Peringatan");
                            }
                        } else if(data.code == 200) {
                            toastr.success(data.message, "Sukses");

                            $('#my-logo').modal('hide');
                        }

                        location.reload();
                        $('.delete-logo-btn').prop('disabled', false);
                    } ,error: function(xhr, status, error) {
                        console.log(error);
                        toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
                        $('.delete-logo-btn').prop('disabled', false);
                    },

                });
            }

            function myLogo() {
                let data = new FormData($("#upload_logo")[0]);

                $('.my-logo-btn').prop('disabled', true);
                toastr.info("Harap menunggu, data sedang di proses", "Loading...");

                $.ajax({
                    type: "POST",
                    processData: false,
                    contentType: false,
                    url: '{{ $auth_agent_base_url }}'+'add-logo',
                    dataType: "json",
                    data: data,
                    cache : false,
                    success: function(data){
                        toastr.clear();
                        
                        if(data.code == 400) {
                            if(Array.isArray(data.message)) {
                                toastr.warning(data.message[0], "Peringatan");
                            } else {
                                toastr.warning(data.message, "Peringatan");
                            }
                        } else if(data.code == 200) {
                            toastr.success(data.message, "Sukses");

                            $('#my-logo').modal('hide');                        
                        }
                        
                        location.reload();
                        $('.my-logo-btn').prop('disabled', false);
                    } ,error: function(xhr, status, error) {
                        console.log(error);
                        toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
                        $('.my-logo-btn').prop('disabled', false);
                    },

                });
            }

            function getLinkTitle() {
                // let args = {};
                // args.text = $('.greeting-text').val();

                toastr.info("Harap menunggu, data sedang di proses", "Loading...");
                // $('.save-greeting-text-btn').prop('disabled', true);

                $.ajax({
                    type: "GET",
                    url: '{{ $auth_agent_base_url }}'+'link-title',
                    dataType: "json",
                    // data: args,
                    cache : false,
                    success: function(data){
                        toastr.clear();
                    
                        if(data.code == 400) {
                            if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                            } else {
                            toastr.warning(data.message, "Peringatan");
                            }
                        } else if(data.code == 200) {
                            if(data.text != null) {
                                data.text = decodeURIComponent(data.text.replace(/\+/g, ' '))
                            }

                            $('.judul-link').val(data.text);
                            $('#judul-link').modal('show');
                        }

                        $('.save-judul-link-btn').prop('disabled', false);
                    } ,error: function(xhr, status, error) {
                    console.log(error);
                        toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
                        $('.save-judul-link-btn').prop('disabled', false);
                    },

                });
            }

            function addLinkTitle() {
                let args = {};
                args.description = $('.judul-link').val();

                toastr.info("Harap menunggu, data sedang di proses", "Loading...");
                $('.save-judul-link-btn').prop('disabled', true);

                $.ajax({
                    type: "POST",
                    url: '{{ $auth_agent_base_url }}'+'link-title',
                    dataType: "json",
                    data: args,
                    cache : false,
                    success: function(data){
                        toastr.clear();
                    
                        if(data.code == 400) {
                            if(Array.isArray(data.message)) {
                            toastr.warning(data.message[0], "Peringatan");
                            } else {
                            toastr.warning(data.message, "Peringatan");
                            }
                        } else if(data.code == 200) {
                            toastr.success(data.message, "Sukses");

                            $('#judul-link').modal('hide');
                            $(".judul-link-class").remove();
                        }

                        $('.save-judul-link-btn').prop('disabled', false);
                    } ,error: function(xhr, status, error) {
                        console.log(error);
                        toastr.warning("Terjadi kesalahan, silahkan refresh halaman ini", "Error");
                        $('.save-judul-link-btn').prop('disabled', false);
                    },

                });
            }

            function exchange() {
                toastr.warning("Saldo tidak mencukupi", "Peringatan");
            }

            $(".judul-link-maxlength").maxlength({
                alwaysShow: !0,
                warningClass: "tag tag-success judul-link-class",
                limitReachedClass: "tag tag-danger judul-link-class"
            });

            function addBankAccount() {
                let args = {};
                args.user_uid = "{{ $current_user->uid }}";
                args.bank_uid = $('.bank').val();
                args.no_rek = $('.no_rek').val();
                
                $('.save-btn').prop('disabled', true);
                toastr.info("Harap menunggu, data sedang di proses", "Loading...");

                $.ajax({
                    type: "POST",
                    url: '{{ $auth_agent_base_url }}'+'add-bank-account',
                    dataType: "json",
                    data: args,
                    cache : false,
                    success: function(data){
                        toastr.clear();
                        
                        if(data.code == 400) {
                            if(Array.isArray(data.message)) {
                                toastr.warning(data.message[0], "Peringatan");
                            } else {
                                toastr.warning(data.message, "Peringatan");
                            }
                        } else if(data.code == 200) {
                            toastr.success(data.message, "Sukses");

                            $('#add-bank-account').modal('hide');
                            
                            $('.no_rek').val("");
                            $('.tukar-saldo-btn').hide();

                            setTimeout(function() { 
                                location.reload();
                             }, 2000);
                        }

                        $('.save-btn').prop('disabled', false);
                    } ,error: function(xhr, status, error) {
                        console.log(error);
                        toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                        $('.save-btn').prop('disabled', false);
                    },

                });
            }
        </script>
    </body>
</html>