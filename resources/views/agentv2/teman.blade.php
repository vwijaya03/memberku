@extends('agentv2/header')

@section('content')
    <style>
        .img-wa {
            width: 40px;
            float: right;
        }

        @media screen and (max-width: 600px){
            .search-icon i {
                margin-top: 20px !important;
                margin-left: 12px;
            }
        }

    </style>

    <section class="account-steps">
        <div class="members-table-outer">
            <div class="table-top-button">
                <button type="submit" class="btn app-button" data-toggle="modal" data-target="#add-agent">Daftar baru</button>
                <div class="balance row-design">
                    Total Saldo: Rp 0
                </div>
            </div>

            
            
            <table id="members-table" >
                <div class="search-icon"><i class="fa fa-search" aria-hidden="true"></i>
                
                </div>
                
                <thead>
                    <tr>
                        <th>Member</th>
                        <th>Saldo</th>
                    </tr>
                </thead>
            </table>
        </div>
    </section>

    <!-- Add Agent Modal -->
    <div class="modal fade text-xs-left" id="add-agent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="myModalLabel33">DAFTAR BARU</label>
                    
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
    
                <form action="#">
                    <div class="modal-body">
                        <label>Nama Lengkap *</label>
                        <div class="form-group">
                            <input type="text" placeholder="Nama Lengkap" class="form-control fullname">
                        </div>
    
                        <label>No HP *</label>
                        <div class="form-group">
                            <input type="text" placeholder="No HP" class="form-control phone">
                        </div>
    
                        {{-- <label>Tipe User *</label>
                        <div class="form-group">
                            <select class="form-control type">
                                <option value="user_only">Customer Only</option>
                            </select>
                        </div> --}}

                        {{-- <option value="standart" selected>Customer Plus</option> 
                        <option value="perusahaan">Perusahaan</option> 
                        <option value="toko">Toko</option> --}}
                        
                    </div>
    
                    <div class="modal-footer">
                        <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                        <input type="submit" class="btn btn-outline-primary btn save-btn" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <!-- Phone Modal -->
    <div class="modal fade text-xs-left" id="send-wa-with-other-phone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
    
                <form action="#">
                    <div class="modal-body">
    
                        <label>Kirim Ke *</label>
                        <div class="form-group">
                            <input type="text" placeholder="Kirim Ke" class="form-control target-phone">
                        </div>
                        
                    </div>
    
                    <div class="modal-footer">
                        <input type="reset" class="btn btn-outline-secondary btn" data-dismiss="modal" value="Tutup">
                        <button type="button" class="btn btn-outline-primary btn send-btn">Kirim</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('custom_js_script')
    <script>
        let table, agent_id, temp_password, user_code = '';
        let registered_phone = '';

        $(document).ready( function () {
            // $('#members-table').DataTable({
            //     "dom": '<"top">frt<"bottom"l><"clear">',
            //     "language": {
            //         "search": "",
            //         "searchPlaceholder": "Ketik untuk mencari",
            //         "sLengthMenu": "Tampilkan _MENU_ per halaman",
            //     }
            // });
            $('.save-btn').on('click', addAgent);
            $('.send-btn').on('click', sendWA);

            table = $('#members-table').DataTable({
                "dom": '<"top">frt<"bottom"l><"clear">',
                "lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
                "processing": true,
                "serverSide": true,
	            "language": {
                    "search": "",
                    "searchPlaceholder": "Ketik untuk mencari",
                    "sLengthMenu": "Tampilkan _MENU_ per halaman",
                },
                "ajax":{
                    "type": "POST",
                    "url": "{{ url($url_agent.'/teman-ajax') }}",
                    "dataType": "json",
                    "data": function(param) {
                        param.referral_code = "{{ $referral_code }}";
                    }
                },
                "columns": [
                    { "data": "row1" },
                    { "data": "row2" },
                ],
                order: [[0, 'desc']],
                "columnDefs": [
                    // { "orderable": false, "targets": [ 0, 2 ] },
                    //{ "width": "190px", "targets": [ 7 ] },
                    //{ "width": "120px", "targets": [ 5, 6 ] },
                ]
            });

            function addAgent() {
                let args = {};
                args.fullname = $('.fullname').val();
                args.phone = $('.phone').val();
                // args.type = $('.type').val();
                args.type = "standart";

                $('.save-btn').prop('disabled', true);
                toastr.info("Harap menunggu, data sedang di proses", "Loading...");

                $.ajax({
                    type: "POST",
                    url: '{{ $auth_agent_base_url }}'+'add-teman',
                    dataType: "json",
                    data: args,
                    cache : false,
                    success: function(data){
                        toastr.clear();
                        
                        if(data.code == 400) {
                            if(Array.isArray(data.message)) {
                                toastr.warning(data.message[0], "Peringatan");
                            } else {
                                toastr.warning(data.message, "Peringatan");
                            }
                        } else if(data.code == 200) {
                            toastr.success(data.message, "Sukses");
                            temp_password = data.temp_password;
                            user_code = data.registered_user_code;

                            $('#add-agent').modal('hide');
                            
                            $('.fullname').val("");
                            
                            table.ajax.reload(null, false);

                            // $('#send-wa-with-other-phone').modal();
                        }

                        $('.save-btn').prop('disabled', false);
                    } ,error: function(xhr, status, error) {
                        console.log(error);
                        toastr.warning("Terjadi kesalahan, harap hubungi admin", "Error");
                        $('.save-btn').prop('disabled', false);
                    },

                });
            }

            function sendWA() {
                let target_phone = $('.target-phone').val();

                if(target_phone.charAt(0) == "+") {
                    target_phone = target_phone.replace(/\+/g, '');
                }
                
                if(target_phone.charAt(0) == "0") {
                    target_phone = "62" + target_phone.substring(1);;
                }

                let text = `Klik Link dibawah ini untuk menggunakan kartu digital \n\nhttps://memberku.id/u/${user_code} \n\nNo HP untuk login: ${registered_phone} \n\nPassword sementara: ${temp_password} \n\nSegera ubah password sementara setelah login \n\nJika Link tidak aktif, balas pesan ini dengan "Ya" `;
                let encodedText = encodeURIComponent(text);

                window.location.href = `https://api.whatsapp.com/send?phone=${target_phone}&text=${encodedText}`;
            }
        });

        function openWA(obj) {
            registered_phone = obj.phone;
            temp_password = "123456";
            user_code = obj.code;

            $('#send-wa-with-other-phone').modal();
        }
    </script>
@endsection