<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class UserBalanceLogModel extends Model
{
    protected $table = 'user_balance_logs';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'from_user_uid', 'to_user_uid', 'amount', 'rate', 'type', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di proses.';
    private $success_delete_msg = 'Data berhasil di hapus.';
}
