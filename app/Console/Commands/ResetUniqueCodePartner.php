<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\UniqueCodeModel;
use App\SettingUniqueCodePartnerModel;
use DB, Log;

class ResetUniqueCodePartner extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:ResetUniqueCodePartner';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset All Unique Code Per Tanggal 1 Setiap Bulan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UniqueCodeModel $uniqueCodeModel, SettingUniqueCodePartnerModel $settingUniqueCodePartnerModel)
    {
        parent::__construct();
        $this->uniqueCodeModel = $uniqueCodeModel;
        $this->settingUniqueCodePartnerModel = $settingUniqueCodePartnerModel;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info('Running command started at '.date('Y-m-d H:i:s'));

        $this->uniqueCodeModel->cmdResetUniqueCode();
        $this->settingUniqueCodePartnerModel->cmdResetTotalUniqueCode();

        Log::info('Finished at '.date('Y-m-d H:i:s'));
    }
}
