<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Auth, Hash, DB, Log;

class LinkTitleModel extends Model
{
    protected $table = 'link_titles';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'user_uid', 'description', 'approve', 'delete'];

    private $success_approve_msg = 'Judul agent berhasil di approve';
    // private $success_update_msg = 'Data sedang direview.';
    private $success_update_msg = 'Judul kartu akan berganti setelah disetujui';
    private $success_add_msg = 'Data berhasil di simpan.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function getOneLinkTitle($user_uid) 
    {
    	$greeting_text = $this->select('id', 'uid', 'user_uid', 'description')
        ->where('user_uid', $user_uid)
        ->where('delete', 0)
        ->where('approve', 1)
        ->first();

        return $greeting_text;
    }

    public function getOneLinkTitleForReview($user_uid) 
    {
    	$greeting_text = $this->select('id', 'uid', 'user_uid', 'description')
        ->where('user_uid', $user_uid)
        ->where('delete', 0)
        ->first();

        return $greeting_text;
    }

    public function getOneLinkTitleWithoutApprove($user_uid) 
    {
    	$greeting_text = $this->select('id', 'uid', 'user_uid', 'description')
        ->where('user_uid', $user_uid)
        ->where('delete', 0)
        ->first();

        return $greeting_text;
    }

    public function postAddLinkTitle($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('link_titles'),
                'user_uid' => $param['user_uid'],
                'description' => urlencode($param['description']),
                'approve' => 0,
                'delete' => 0
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }

    public function postEditLinkTitle($param, $user_uid)
    {
        DB::transaction(function () use($param, $user_uid) {
            $this->where('user_uid', $user_uid)->where('delete', 0)->update([
                'description' => urlencode($param['description']),
                'approve' => 0
            ]);
        });

        return $this->success_update_msg;
    }

    public function postApproveAgentDescription($user_uid)
    {
        DB::transaction(function () use($user_uid) {
            $this->where('user_uid', $user_uid)->where('delete', 0)->update([
                'approve' => 1
            ]);
        });

        return $this->success_approve_msg;
    }
}
