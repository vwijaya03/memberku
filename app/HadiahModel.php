<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class HadiahModel extends Model
{
    protected $table = 'hadiah';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'merchant_uid', 'title', 'description', 'img', 'expired_date', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_update_sequence_msg = 'Urutan data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di proses.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function countAllHadiah() 
    {
        $count_hadiah = $this->count('hadiah.id');

        return $count_hadiah;
    }

    public function countAllActiveHadiah()
    {
        $count_hadiah = $this->join('merchants', 'merchants.uid', '=', 'hadiah.merchant_uid')
        ->where('hadiah.delete', 0)
        ->where('merchants.delete', 0)
        ->count('hadiah.id');

        return $count_hadiah;
    }

    public function countAllFilteredActiveHadiah($search)
    {
        $count_hadiah = $this->join('merchants', 'merchants.uid', '=', 'hadiah.merchant_uid')
        ->where(function ($q) use($search) {
            $q->where('merchants.name', 'like', '%'.$search.'%');
            $q->orWhere('hadiah.title', 'like', '%'.$search.'%');
            $q->orWhere('hadiah.description', 'like', '%'.$search.'%');
            $q->orWhere('hadiah.img', 'like', '%'.$search.'%');
        })
        ->where('hadiah.delete', 0)
        ->where('merchants.delete', 0)
        ->count('hadiah.id');

        return $count_hadiah;
    }

    public function getHadiah($start, $limit, $order, $dir)
    {
        $hadiah = $this->select('merchants.uid as merchant_uid', 'merchants.name', 'hadiah.uid', 'hadiah.title', 'hadiah.description', 'hadiah.img', 'hadiah.expired_date')
        ->join('merchants', 'merchants.uid', '=', 'hadiah.merchant_uid')
        ->where('hadiah.delete', 0)
        ->where('merchants.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $hadiah;
    }
    
    public function getFilteredHadiah($search, $start, $limit, $order, $dir)
    {
        $hadiah = $this->select('merchants.uid as merchant_uid', 'merchants.name', 'hadiah.uid', 'hadiah.title', 'hadiah.description', 'hadiah.img', 'hadiah.expired_date')
        ->join('merchants', 'merchants.uid', '=', 'hadiah.merchant_uid')
        ->where(function ($q) use($search) {
            $q->where('merchants.name', 'like', '%'.$search.'%');
            $q->orWhere('hadiah.title', 'like', '%'.$search.'%');
            $q->orWhere('hadiah.description', 'like', '%'.$search.'%');
            $q->orWhere('hadiah.img', 'like', '%'.$search.'%');
        })
        ->where('hadiah.delete', 0)
        ->where('merchants.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $hadiah;
    }

    public function getOneHadiah($uid)
    {
        $hadiah = $this->select('merchants.uid as merchant_uid', 'merchants.name', 'hadiah.uid', 'hadiah.title', 'hadiah.description', 'hadiah.img', 'hadiah.expired_date')
        ->join('merchants', 'merchants.uid', '=', 'hadiah.merchant_uid')
        ->where('hadiah.uid', $uid)
        ->where('hadiah.delete', 0)
        ->where('merchants.delete', 0)
        ->first();

        return $hadiah;
    }

    public function postAddHadiah($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('hadiah'),
                'merchant_uid' => $param['merchant_uid'],
                'title' => urlencode($param['title']),
                'description' => urlencode($param['description']),
                'img' => $param['img_path'],
                'expired_date' => $param['expired_date'],
                'delete' => 0
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }

    public function postEditHadiah($param, $uid)
    {
        DB::transaction(function () use($param, $uid) {
            if($param['img_path'] != null || $param['img_path'] != '')
            {
                $this->where('hadiah.uid', $uid)->where('hadiah.delete', 0)->update([
                    'merchant_uid' => $param['merchant_uid'],
                    'title' => urlencode($param['title']),
                    'description' => urlencode($param['description']),
                    'img' => $param['img_path'],
                    'expired_date' => $param['expired_date']
                ]);
            }
            else
            {
                $this->where('hadiah.uid', $uid)->where('hadiah.delete', 0)->update([
                    'merchant_uid' => $param['merchant_uid'],
                    'title' => urlencode($param['title']),
                    'description' => urlencode($param['description']),
                    'expired_date' => $param['expired_date']
                ]);
            }
            
        });

        return $this->success_update_msg;
    }

    public function postDeleteHadiah($uid)
    {
        DB::transaction(function () use($uid) {
            $this->where('hadiah.uid', $uid)->where('hadiah.delete', 0)->update([
                'delete' => 1
            ]);
        });

        return $this->success_delete_msg;
    }
}
