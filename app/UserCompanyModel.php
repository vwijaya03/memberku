<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class UserCompanyModel extends Model
{
    protected $table = 'user_referral_codes';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'user_uid', 'referral_code', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di proses.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    // public function getOneCompanyByUserUID($user_uid) {
    //     $company = $this->select('companies.id', 'companies.uid', 'companies.name', 'companies.code', 'companies.title', 'companies.img')
    //     ->join('companies', 'user_referral_codes.referral_code', '=', 'companies.uid')
    //     ->where('user_referral_codes.user_uid', $user_uid)
    //     ->where('companies.delete', 0)
    //     ->first();

    //     return $company;
    // }

    public function getOneCompanyByUserUID($user_uid) {
        $company = $this->select('user_referral_codes.*')
        // ->join('companies', 'user_referral_codes.referral_code', '=', 'companies.uid')
        ->where('user_referral_codes.user_uid', $user_uid)
        ->where('user_referral_codes.delete', 0)
        ->first();

        return $company;
    }

    public function getOneReferralCodeByUserUID($user_uid) {
        $referral_code = $this->select('user_referral_codes.referral_code')
        // ->join('companies', 'user_referral_codes.referral_code', '=', 'companies.uid')
        ->where('user_referral_codes.user_uid', $user_uid)
        ->where('user_referral_codes.delete', 0)
        ->first();

        return $referral_code;
    }

    public function getUserUIDByCode($referral_code) {
        $referral_code = $this->select('user_referral_codes.user_uid', 'users.type')
        ->join('users', 'users.uid', '=', 'user_referral_codes.user_uid')
        ->where('user_referral_codes.referral_code', $referral_code)
        ->where('user_referral_codes.delete', 0)
        ->first();

        return $referral_code;
    }
}