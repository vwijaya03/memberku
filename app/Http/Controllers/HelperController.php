<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash, Log;

class HelperController extends Controller
{
    public static function getCurrentMilisecond() {
        list($usec, $sec) = explode(" ", microtime());
        $milisecond = ((float)$usec + (float)$sec) * 1000;

        return number_format($milisecond, 0, ',', '');
    }

    public static function getDayDifferenceFromDate($date_from, $date_to)
    {
        $date_from_ts = strtotime($date_from);
        $date_to_ts = strtotime($date_to);

        $diff = $date_to_ts - $date_from_ts;

        return round($diff / 86400);
    }

    public static function uid($args) {
        if($args == null || strlen($args) == 0) {
            $args = 'default';
        }

        $faker = \Faker\Factory::create();
        $uid = $args.HelperController::getCurrentMilisecond().$faker->randomNumber($nbDigits = 8, $strict = false);

        return preg_replace('/[^a-zA-Z0-9_]/', '', Hash::make($uid));
    }

    public static function generateUniqueCode() {
        $faker = \Faker\Factory::create();
        $unique_code = strtoupper($faker->randomLetter()).strtoupper($faker->randomLetter()).$faker->randomNumber($nbDigits = 2, $strict = true).strtoupper($faker->randomLetter()).strtoupper($faker->randomLetter()).$faker->randomNumber($nbDigits = 2, $strict = true);

        return $unique_code;
    }

    public static function generateCompanyCode($id) {
        $faker = \Faker\Factory::create();
        $code = strtoupper($faker->randomLetter()).strtoupper($faker->randomLetter()).$faker->randomNumber($nbDigits = 4, $strict = true).$id;

        return $code;
    }

    public static function generateAgentCode($id) {
        $faker = \Faker\Factory::create();
        $code = "2".$faker->randomNumber($nbDigits = 4, $strict = true).$id;

        return $code;
    }

    public static function generateAgentFromBankCode($id) {
        $faker = \Faker\Factory::create();
        $code = "3".$faker->randomNumber($nbDigits = 4, $strict = true).$id;

        return $code;
    }

    public static function splitUserCode($str) {
        $mod_str = "";

        for ($i = 1; $i <= strlen($str); $i++) { 
            if($i % 3 == 0) {
                if($i == strlen($str)) {
                    $mod_str .= substr($str, $i - 1, 1);
                } else {
                    $mod_str .= substr($str, $i - 1, 1) . "-";
                }
            } 
            else {
                $mod_str .= substr($str, $i - 1, 1);
            }
        }

        return $mod_str;
    }

    public static function formatRupiah($str) {
        $strFormatRupiah = "Rp " . number_format($str, 2, ",", ".");

	    return $strFormatRupiah;
    }

    public static function generateCardNumber() {
        $faker = \Faker\Factory::create();
        $card_number = $faker->randomNumber($nbDigits = 2, $strict = true).date('md').$faker->randomNumber($nbDigits = 2, $strict = true);

        return $card_number;
    }

    public static function generatePromoPin() {
        $faker = \Faker\Factory::create();
        $pin = $faker->randomNumber($nbDigits = 6, $strict = true);

        return $pin;
    }

    public static function getIp() {
        // Get real visitor IP behind CloudFlare network
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
            $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
            $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }

        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if(filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } else if(filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }

        return $ip;
    }

    public static function validatorHelper($validator) {
    	$err_arr = [];

    	if($validator->fails()) {
            foreach ($validator->errors()->all() as $value) {
                array_push($err_arr, $value);
            }

            return $err_arr;
        } else {
        	return $err_arr;
        }
    }

    public static function getReportTukarStrukColumns() {   
        $arr = [];
        array_push($arr, 'users.uid', 'merchants.name', 'users.fullname', 'users.phone', 'claim_gifts.nominal', 'claim_gifts.no_struk', 'users.uid', 'claim_gifts.status', 'claim_gifts.created_at', 'claim_gifts.uid');

        return $arr;
    }

    public static function getReviewAgentDescriptionColumns() {   
        $arr = [];
        array_push($arr, 'users.id', 'users.fullname', 'users.phone', 'users.id', 'users.id');

        return $arr;
    }

    public static function getReviewAgentLogoColumns() {   
        $arr = [];
        array_push($arr, 'users.id', 'users.fullname', 'users.phone', 'users.id', 'users.id');

        return $arr;
    }
    
    public static function getBankColumns() {   
        $arr = [];
        array_push($arr, 'banks.id', 'banks.name', 'banks.id');

        return $arr;
    }
    
    public static function getBankAccountColumns() {   
        $arr = [];
        array_push($arr, 'user_bank_accounts.id', 'banks.name', 'user_bank_accounts.no_rek');

        return $arr;
    }

    public static function getAgentTypeColumns() {   
        $arr = [];
        array_push($arr, 'id', 'name', 'rate', 'allow_upload_logo');

        return $arr;
    }
    
    public static function getTemanColumns() {   
        $arr = [];
        array_push($arr, 'users.created_at', 'users.fullname', 'users.email');

        return $arr;
    }

    public static function getAgentColumns() {   
        $arr = [];
        array_push($arr, 'id', 'fullname', 'email', 'type', 'id');

        return $arr;
    }

    public static function getUserColumns() {   
        $arr = [];
        array_push($arr, 'id', 'fullname', 'name_on_card', 'email', 'phone', 'address');

        return $arr;
    }

    public static function getBookingColumns() {   
        $arr = [];
        array_push($arr, 'id', 'email', 'nama_usaha', 'contact_person', 'alamat_usaha', 'social_media', 'phone');

        return $arr;
    }

    public static function getMerchantColumns() {   
        $arr = [];
        array_push($arr, 'id', 'name', 'email', 'contact_person', 'phone', 'address');

        return $arr;
    }

    public static function getMerchantJoinBonusColumns() {   
        $arr = [];
        array_push($arr, 'id', 'name');

        return $arr;
    }

    public static function getCompanyColumns() {   
        $arr = [];
        array_push($arr, 'uid', 'name', 'code');

        return $arr;
    }

    public static function getVoucherExpiredDateColumns() {   
        $arr = [];
        array_push($arr, 'id', 'description', 'days', 'default');

        return $arr;
    }

    public static function getCategoryPartnerColumns() {   
        $arr = [];
        array_push($arr, 'id', 'name');

        return $arr;
    }

    public static function getPromoColumns() {   
        $arr = [];
        array_push($arr, 'promos.sequence', 'promos.id', 'promos.uid', 'promos.merchant_uid', 'promos.title', 'promos.description', 'promos.img', 'promos.harga_awal', 'promos.harga_akhir', 'promos.expired_date', 'promos.top_promo');

        return $arr;
    }

    public static function getPromoInternalColumns() {   
        $arr = [];
        array_push($arr, 'promos_internal.sequence', 'promos_internal.id', 'promos_internal.uid', 'promos_internal.user_uid', 'promos_internal.title', 'promos_internal.description', 'promos_internal.img', 'promos_internal.harga_awal', 'promos_internal.harga_akhir', 'promos_internal.expired_date', 'promos_internal.top_promo');

        return $arr;
    }

    public static function getSelectedPromoColumns() {   
        $arr = [];
        array_push($arr, 'merchants.name', 'promos.uid', 'promos.title', 'promos.description');

        return $arr;
    }

    public static function getPartnerPromoColumns() {   
        $arr = [];
        array_push($arr, 'promos.created_at', 'promos.title', 'promos.id');

        return $arr;
    }

    public static function getPartnerUniqueCodeColumns() {   
        $arr = [];
        array_push($arr, 'unique_codes.unique_code', 'unique_codes.unique_code', 'unique_codes.created_at', 'unique_codes.created_at');

        return $arr;
    }

    public static function getPinPromoColumns() {   
        $arr = [];
        array_push($arr, 'pin_promos.uid', 'pin_promos.pin');

        return $arr;
    }

    public static function getAdminUniqueCodeColumns() {   
        $arr = [];
        array_push($arr, 'unique_codes.unique_code', 'users.fullname', 'unique_codes.unique_code', 'unique_codes.created_at');

        return $arr;
    }

    public static function getAdminTotalUniqueCodeColumns() {   
        $arr = [];
        array_push($arr, 'unique_codes.unique_code', 'users.fullname', 'total_unique_code');

        return $arr;
    }

    public static function getPartnerMembershipColumns() {   
        $arr = [];
        array_push($arr, 'memberships.created_at', 'memberships.fullname', 'memberships.nama_usaha', 'memberships.address', 'memberships.city', 'memberships.phone', 'memberships.card_number', 'memberships.id');

        return $arr;
    }

    public static function getReportPinColumns() {   
        $arr = [];
        array_push($arr, 'records_use_promo.id', 'promos.title', 'records_use_promo.pin', 'records_use_promo.created_at');

        return $arr;
    }

    public static function getHadiahColumns() {   
        $arr = [];
        array_push($arr, 'hadiah.id', 'merchants.name', 'hadiah.title', 'hadiah.description', 'hadiah.img', 'hadiah.expired_date');

        return $arr;
    }

    public static function errorMessagesAgent() {
        $messages = [
            'id.required' => 'Data tidak di temukan',
            'id.numeric' => 'Data tidak di temukan',
            'fullname.required' => 'Nama lengkap tidak boleh kosong',
            'name_on_card.required' => 'Nama pada kartu tidak boleh kosong',
            'email.required' => 'Email tidak boleh kosong',
            'email.email' => 'Format e-mail salah',
            'email.unique' => 'E-mail telah di gunakan',
            'phone.required' => 'No. HP tidak boleh kosong',
            'phone.numeric' => 'Format nomor telepon salah',
            'phone.digits_between' => 'Format nomor telepon salah',
            'phone.max' => 'Format Telp / IG / Web salah',
            'address.required' => 'Address tidak boleh kosong',
            'address.max' => 'Address maximum 35 karakter',
            'city.required' => 'City tidak boleh kosong',
            'city.max' => 'City maximum 35 karakter',
            'type.required' => 'Tipe agent tidak boleh kosong',
            'type.max' => 'Tipe agent maximum 35 karakter',
            'role.required' => 'Role tidak boleh kosong'
        ];

        return $messages;
    }

    public static function errorMessagesUser() {
        $messages = [
            'id.required' => 'Data tidak di temukan',
            'id.numeric' => 'Data tidak di temukan',
            'fullname.required' => 'Nama lengkap tidak boleh kosong',
            'name_on_card.required' => 'Nama pada kartu tidak boleh kosong',
            'email.required' => 'Email tidak boleh kosong',
            'email.email' => 'Format e-mail salah',
            'email.unique' => 'E-mail telah di gunakan',
            'phone.unique' => 'Nomor telah di gunakan',
            'phone.required' => 'No. HP tidak boleh kosong',
            'phone.numeric' => 'Format nomor telepon salah',
            'phone.digits_between' => 'Format nomor telepon salah',
            'phone.max' => 'Format Telp / IG / Web salah',
            'address.required' => 'Address tidak boleh kosong',
            'address.max' => 'Address maximum 35 karakter',
            'city.required' => 'City tidak boleh kosong',
            'city.max' => 'City maximum 35 karakter',
            'role.required' => 'Role tidak boleh kosong'
        ];

        return $messages;
    }

    public static function errorMessagesBooking() {
        $messages = [
            'id.required' => 'Data tidak di temukan',
            'id.numeric' => 'Data tidak di temukan',
            'email.required' => 'Email tidak boleh kosong',
            'email.email' => 'Format e-mail salah',
            'email.unique' => 'E-mail telah di gunakan',
            'nama_usaha.required' => 'Nama usaha tidak boleh kosong',
            'contact_person.required' => 'Nama contact person tidak boleh kosong',
            'alamat_usaha.required' => 'Alamat usaha tidak boleh kosong',
            'social_media.required' => 'IG / FB / Website tidak boleh kosong',
            'phone.required' => 'No. HP tidak boleh kosong',
            'phone.numeric' => 'Format nomor telepon salah',
            'phone.digits_between' => 'Format nomor telepon salah'
        ];

        return $messages;
    }

    public static function errorMessagesSetting() {
        $messages = [
            'user_uid.required' => 'Data user tidak di temukan',
            'user_uid.max' => 'Data user salah',
            'max_total_uc.required' => 'Jumlah limit kirim benefit tidak boleh kosong',
            'max_total_uc.numeric' => 'Format jumlah limit kirim benefit salah',
            'voucher_price.required' => 'Harga per voucher tidak boleh kosong',
            'voucher_price.numeric' => 'Format harga per voucher salah',
            'voucher_click_limit.required' => 'Jumlah maximum voucher di klik tidak boleh kosong',
            'voucher_click_limit.numeric' => 'Format jumlah maximum voucher di klik salah',
            'voucher_generate_limit.required' => 'Jumlah maximum partner generate voucher tidak boleh kosong',
            'voucher_generate_limit.numeric' => 'Format jumlah maximum partner generate voucher salah',
            'server_maintenance.required' => 'Server maintenace tidak boleh kosong'
        ];

        return $messages;
    }
    
    public static function errorMessagesAgentType() {
        $messages = [
            'uid.required' => 'Data agent type tidak di temukan',
            'uid.max' => 'Data agent type salah',
            'name.required' => 'Tipe agent tidak boleh kosong',
            'name.max' => 'Format tipe agent salah',
            'rate.required' => 'Rate tidak boleh kosong',
            'rate.numeric' => 'Format rate salah',
        ];

        return $messages;
    }
    
    public static function errorMessagesBank() {
        $messages = [
            'uid.required' => 'Data bank tidak di temukan',
            'uid.max' => 'Data bank salah',
            'name.required' => 'Nama bank tidak boleh kosong',
            'name.max' => 'Format nama bank salah'
        ];

        return $messages;
    }
    
    public static function errorMessagesBankAccount() {
        $messages = [
            'uid.required' => 'Data bank tidak di temukan',
            'uid.max' => 'Data bank salah',
            'user_uid.required' => 'User tidak boleh kosong',
            'user_uid.max' => 'Format user salah',
            'bank_uid.required' => 'Bank tidak boleh kosong',
            'bank_uid.max' => 'Format bank salah',
            'no_rek.required' => 'Nomor rekening tidak boleh kosong',
            'no_rek.max' => 'Format nomor rekening salah'
        ];

        return $messages;
    }

    public static function errorMessagesUserEWallet() {
        $messages = [
            'uid.required' => 'Data bank tidak di temukan',
            'uid.max' => 'Data bank salah',
            'user_uid.required' => 'User tidak boleh kosong',
            'user_uid.max' => 'Format user salah',
            'bank_uid.required' => 'Bank tidak boleh kosong',
            'bank_uid.max' => 'Format bank salah',
            'no_rek.required' => 'Nomor rekening tidak boleh kosong',
            'no_rek.max' => 'Format nomor rekening salah'
        ];

        return $messages;
    }

    public static function errorMessagesSettingPromoPartner() {
        $messages = [
            'promo_uid.required' => 'Promo tidak ada yang di pilih',
            'promo_uid.max' => 'Promo tidak di temukan',
            'user_uid.required' => 'Partner tidak ada yang di pilih',
            'user_uid.max' => 'Partner tidak di temukan'
        ];

        return $messages;
    }

    public static function errorMessagesPinPromo() {
        $messages = [
            'promo_uid.required' => 'Promo tidak ada yang di pilih',
            'promo_uid.max' => 'Promo tidak di temukan',
            'pin.required' => 'Pin tidak boleh kosong',
            'pin.max' => 'Format pin salah'
        ];

        return $messages;
    }

    public static function errorMessagesMerchant() {
        $messages = [
            'id.required' => 'Data tidak di temukan',
            'id.numeric' => 'Data tidak di temukan',
            'contact_person.required' => 'Nama contact person tidak boleh kosong',
            'name.required' => 'Nama merchant tidak boleh kosong',
            'email.required' => 'Email tidak boleh kosong',
            'email.email' => 'Format e-mail salah',
            'email.unique' => 'E-mail telah di gunakan',
            'phone.required' => 'No. HP tidak boleh kosong',
            'phone.numeric' => 'Format nomor telepon salah',
            'phone.digits_between' => 'Format nomor telepon salah',
            'address.required' => 'Address tidak boleh kosong',
        ];

        return $messages;
    }

    public static function errorMessagesVoucherExpired() {
        $messages = [
            'id.required' => 'Data tidak di temukan',
            'id.numeric' => 'Data tidak di temukan',
            'user_uid.required' => 'Data partner belum ada yang di pilih',
            'user_uid.max' => 'Format data partner salah',
            'description.required' => 'Description tidak boleh kosong',
            'days.required' => 'Satuan hari tidak boleh kosong',
            'days.numeric' => 'Format satuan hari salah',
            'days.digits_between' => 'Format satuan hari salah'
        ];

        return $messages;
    }

    public static function errorMessagesCategoryPartner() {
        $messages = [
            'id.required' => 'Data tidak di temukan',
            'id.numeric' => 'Data tidak di temukan',
            'name.required' => 'Nama kategori partner tidak boleh kosong'
        ];

        return $messages;
    }

    public static function errorMessagesUniqueCode() {
        $messages = [
            'user_uid.required' => 'Data partner tidak di temukan',
            'user_uid.max' => 'Format data partner salah',
            'category.required' => 'Kategori tidak di temukan',
            'category.max' => 'Format kategori salah',
            'unique_code.required' => 'Data unique code tidak di temukan',
            'unique_code.max' => 'Format unique code salah',
            'page.required' => 'Jumlah halaman tidak boleh kosong',
            'page.max' => 'Jumlah halaman melebihi limit'
        ];

        return $messages;
    }

    public static function errorMessagesPromo() {
        $messages = [
            'promo_uid.required' => 'Data promo belum di pilih',
            'id.required' => 'Data tidak di temukan',
            'id.numeric' => 'Data tidak di temukan',
            'merchant.required' => 'Nama merchant tidak boleh kosong',
            'title.required' => 'Judul tidak boleh kosong',
            'description.required' => 'Description tidak boleh kosong',
            'img.required' => 'Gambar tidak boleh kosong',
            'img.image' => 'Format gambar salah',
            'harga_awal.required' => 'Harga awal tidak boleh kosong',
            'harga_awal.numeric' => 'Format harga awal salah',
            'harga_akhir.required' => 'Harga akhir tidak boleh kosong',
            'harga_akhir.numeric' => 'Format harga akhir salah',
            'pin.required' => 'Pin tidak boleh kosong',
            'pin.numeric' => 'Pin harus angka',
            'pin.digits_between' => 'Pin harus 6 digit',
            'expired_date.required' => 'Tanggal expired promo tidak boleh kosong'
        ];

        return $messages;
    }

    public static function errorMessagesPromoInternal() {
        $messages = [
            'promo_uid.required' => 'Data promo belum di pilih',
            'id.required' => 'Data tidak di temukan',
            'id.numeric' => 'Data tidak di temukan',
            'partner.required' => 'Partner tidak boleh kosong',
            'title.required' => 'Judul tidak boleh kosong',
            'description.required' => 'Description tidak boleh kosong',
            'img.required' => 'Gambar tidak boleh kosong',
            'img.image' => 'Format gambar salah',
            'harga_awal.required' => 'Harga awal tidak boleh kosong',
            'harga_awal.numeric' => 'Format harga awal salah',
            'harga_akhir.required' => 'Harga akhir tidak boleh kosong',
            'harga_akhir.numeric' => 'Format harga akhir salah',
            'pin.required' => 'Pin tidak boleh kosong',
            'pin.numeric' => 'Pin harus angka',
            'pin.digits_between' => 'Pin harus 6 digit',
            'expired_date.required' => 'Tanggal expired promo tidak boleh kosong'
        ];

        return $messages;
    }

    public static function errorMessagesPartnerLogo() {
        $messages = [
            'user_uid.required' => 'Data partner tidak di temukan',
            'user_uid.max' => 'Format data partner salah',
            'img.required' => 'Gambar tidak boleh kosong',
            'img.image' => 'Format gambar salah'
        ];

        return $messages;
    }

    public static function errorMessagesDataCustomer() {
        $messages = [
            'id.required' => 'Data tidak di temukan',
            'id.numeric' => 'Data tidak di temukan',
            'user_uid.required' => 'Data partner tidak di temukan',
            'user_uid.max' => 'Format data partner salah',
            'fullname.required' => 'Nama lengkap tidak boleh kosong',
            'fullname.max' => 'Format nama lengkap salah',
            'address.required' => 'Alamat tidak boleh kosong',
            'address.max' => 'Format alamat salah',
            'email.required' => 'Email tidak boleh kosong',
            'email.email' => 'Format e-mail salah',
            'email.max' => 'Format e-mail salah',
            'phone.required' => 'No. HP tidak boleh kosong',
            'phone.max' => 'Format no. HP salah',
            'phone.min' => 'Format no. HP salah',
        ];

        return $messages;
    }

    public static function errorMessagesUsePromo() {
        $messages = [
            'promo_uid.required' => 'Promo tidak di temukan',
            'promo_uid.max' => 'Format data promo salah',
            'unique_code.required' => 'Kode unik tidak di temukan',
            'unique_code.max' => 'Format kode unik salah',
            'pin.required' => 'Pin tidak boleh kosong',
            'pin.numeric' => 'Pin harus angka',
            'pin.digits_between' => 'Pin harus 6 digit',
            'user_uid.required' => 'Data tidak di temukan',
            'user_uid.max' => 'Format data salah'
        ];

        return $messages;
    }

    public static function errorMessagesGreetingText() {
        $messages = [
            'description.required' => 'Text tidak boleh kosong',
            'description.max' => 'Format text salah'
        ];

        return $messages;
    }

    public static function errorMessagesMembership() {
        $messages = [
            'user_uid.required' => 'Data partner tidak di temukan',
            'user_uid.max' => 'Format data partner salah',
            'nama_usaha.required' => 'Nama usaha tidak boleh kosong',
            'nama_usaha.max' => 'Format nama usaha salah',
            'address.required' => 'Alamat tidak boleh kosong',
            'address.max' => 'Format alamat salah',
            'city.required' => 'Kota tidak boleh kosong',
            'city.max' => 'Format kota salah',
            'phone.required' => 'No. HP tidak boleh kosong',
            'phone.max' => 'Format no. HP salah',
            'phone.min' => 'Format no. HP salah',
            'fullname.required' => 'Nama lengkap tidak boleh kosong',
            'fullname.max' => 'Format nama lengkap salah'
        ];

        return $messages;
    }

    public static function errorMessagesVerifyUniqueCode() {
        $messages = [
            'unique_code.required' => 'Kode voucher tidak valid',
            'unique_code.max' => 'Kode voucher tidak valid'
        ];

        return $messages;
    }

    public static function errorMessagesReportPin() {
        $messages = [
            'pin.required' => 'Pin tidak boleh kosong',
            'pin.max' => 'Format pin salag'
        ];

        return $messages;
    }

    public static function errorMessagesCompany() {
        $messages = [
            'uid.required' => 'Data perusahaan tidak di temukan',
            'uid.max' => 'Format data perusahaan salah',
            'name.required' => 'Nama perusahaan tidak boleh kosong',
            'name.max' => 'Format nama perusahaan salah',
            'code.required' => 'Kode perusahaan tidak boleh kosong',
            'code.max' => 'Format kode perusahaan salah',
            'title.max' => 'Judul maksimal 200 karakter',
            'img.image' => 'Format logo perusahaan salah'
        ];

        return $messages;
    }

    public static function errorMessagesHadiah() {
        $messages = [
            'uid.required' => 'Data hadiah tidak di temukan',
            'uid.max' => 'Format data hadiah salah',
            'merchant.required' => 'Nama merchant tidak boleh kosong',
            'merchant.max' => 'Data nama merchant salah',
            'title.required' => 'Judul tidak boleh kosong',
            'description.required' => 'Description tidak boleh kosong',
            'img.required' => 'Gambar tidak boleh kosong',
            'img.image' => 'Format gambar salah',
            'expired_date.required' => 'Tanggal expired data hadiah tidak boleh kosong'
        ];

        return $messages;
    }
    
    public static function errorMessagesClaimGift() {
        $messages = [
            'merchant_uid.required' => 'Data merchant tidak di temukan',
            'merchant_uid.max' => 'Format data merchant salah',
            'cgUid.required' => 'Data merchant tidak di temukan',
            'cgUid.max' => 'Format data merchant salah',
            'additional_note.max' => 'Catatan tambahan terlalu banyak',
            'user_uid.required' => 'Data agent tidak boleh kosong',
            'user_uid.max' => 'Format data agent salah',
            'amount.required' => 'Jumlah nominal tidak boleh kosong',
            'amount.numeric' => 'Format jumlah nominal salah',
            'amount.digits_between' => 'Jumlah nominal terlalu besar',
            'jumlah_redeem.required' => 'Jumlah redeem tidak boleh kosong',
            'jumlah_redeem.numeric' => 'Format jumlah redeem salah',
            'jumlah_redeem.digits_between' => 'Jumlah redeem terlalu besar',
            'img_struk.required' => 'Foto struk tidak boleh kosong',
            'img_struk.image' => 'Format foto struk salah',
            'img_struk.max' => 'Ukuran file foto struk terlalu besar',
        ];

        return $messages;
    }
}
