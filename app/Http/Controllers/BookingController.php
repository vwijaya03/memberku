<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\BookingModel;
use Auth, Hash, DB, Log, Validator;

class BookingController extends Controller
{
    public function __construct(BookingModel $bookingModel)
    {
        $this->bookingModel = $bookingModel;
        $this->page_title = 'Booking';
    }

    public function getBooking()
    {
        return view('admin/booking', ['current_user' => Auth::user(), 'page_title' => $this->page_title]);
    }

    public function postAjaxBooking(Request $request)
    {
        $data = array();

        $columns = HelperController::getBookingColumns();
  
        $totalData = $this->bookingModel->countAllActiveBooking();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $bookings = $this->bookingModel->getBooking($start, $limit, $order, $dir);
        }
        else 
        {
            $bookings = $this->bookingModel->getFilteredBooking($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->bookingModel->countAllFilteredActiveBooking($search);
        }

        if(!empty($bookings))
        {
            foreach ($bookings as $booking)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['res'] = $booking;
                $nestedData['action_btn'] = "
                    <button onclick='editBooking(".$booking.")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-target='#edit-booking'><i class='ft-edit'></i></button>
                    <button onclick='deleteBooking(".$booking.")' type='button' class='btn btn-danger mr-1 mb-1' data-toggle='modal' data-target='#delete-booking'><i class='ft-trash-2'></i></button>
                ";
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function postAddBooking()
    {
        $validator = Validator::make(request()->all(), [
            'email' => 'required|email|unique:bookings',
            'nama_usaha' => 'required',
            'contact_person' => 'required',
            'alamat_usaha' => 'required',
            'social_media' => 'required',
            'phone' => 'required|numeric|digits_between:9,13'
        ], HelperController::errorMessagesBooking());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

    	$result = $this->bookingModel->postAddBooking($requested);
    	
        return response()->json(['code' => 200, 'message' => $result[1]]);
    }

    public function postEditBooking()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|numeric',
            'email' => 'required|email|unique:bookings,email,'.request()['id'],
            'nama_usaha' => 'required',
            'contact_person' => 'required',
            'alamat_usaha' => 'required',
            'social_media' => 'required',
            'phone' => 'required|numeric|digits_between:9,13'
        ], HelperController::errorMessagesBooking());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

    	$requested = request();

    	$result = $this->bookingModel->postEditBooking($requested, $requested['id']);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postDeleteBooking()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|numeric',
        ], HelperController::errorMessagesBooking());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->bookingModel->postDeleteBooking($requested['id']);

        return response()->json(['code' => 200, 'message' => $result]);
    }
}
