<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\Http\Controllers\ImageController;
use App\CompanyModel;
use Auth, Hash, DB, Log, Validator;

class CompanyController extends Controller
{
    public function __construct(CompanyModel $companyModel)
    {
        $this->companyModel = $companyModel;
        $this->page_title = 'Perusahaan';
        $this->dir = '/company-logo/';
    }

    public function getCompany()
    {
        return view('admin/company', ['current_user' => Auth::user(), 'page_title' => $this->page_title]);
    }

    public function postAjaxCompany(Request $request)
    {
        $data = array();

        $columns = HelperController::getCompanyColumns();
  
        $totalData = $this->companyModel->countAllActiveCompany();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $companies = $this->companyModel->getCompany($start, $limit, $order, $dir);
        }
        else 
        {
            $companies = $this->companyModel->getFilteredCompany($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->companyModel->countAllFilteredActiveCompany($search);
        }

        if(!empty($companies))
        {
            foreach ($companies as $company)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['res'] = $company;
                $nestedData['name'] = urldecode($company->name);
                $nestedData['title'] = urldecode(strip_tags(substr($company->title, 0, 35))."...");;

                if($company->img != null || $company->img != "") {
                    $nestedData['img'] = "<a href='".url('/').$company->img."' target='_blank'>Klik di sini</a>";
                } else {
                    $nestedData['img'] = "";
                }

                $nestedData['action_btn'] = "
                    <button onclick='editCompany(".$company.")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-target='#edit-company'><i class='ft-edit'></i></button>
                    <button onclick='deleteCompany(".$company.")' type='button' class='btn btn-danger mr-1 mb-1' data-toggle='modal' data-target='#delete-company'><i class='ft-trash-2'></i></button>
                ";
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function postAddCompany()
    {
        $validator = Validator::make(request()->all(), [
            'name' => 'required|max:50',
            'code' => 'max:50',
            'img' => 'image',
            'title' => 'max:200'
        ], HelperController::errorMessagesCompany());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

        if($requested['img'] != null) {
            $requested['img_path'] = ImageController::createImage($requested['img'], $this->dir, '');
        }
        
    	$result = $this->companyModel->postAddCompany($requested);
    	
        return response()->json(['code' => 200, 'message' => $result[1]]);
    }

    public function postEditCompany()
    {
        $validator = Validator::make(request()->all(), [
        	'uid' => 'required|max:100',
        	'name' => 'required|max:50',
        	'code' => 'required|max:50',
        	'title' => 'max:200',
        	'img' => 'image',
        ], HelperController::errorMessagesCompany());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

        $res_promo = $this->companyModel->getOneCompanyByUID(request()['uid']);

        if($res_promo == null) {
            return response()->json(['code' => 400, 'message' => 'Data perusahaan tidak di temukan']);
        }

        if($requested['img'] != null) {
            ImageController::deleteImage($res_promo->img, "");
            $requested['img_path'] = ImageController::createImage($requested['img'], $this->dir, '');
        } else {
            $requested['img_path'] = null;
        }
        
    	$result = $this->companyModel->postEditCompany($requested, $requested['uid']);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postDeleteCompany()
    {
        $validator = Validator::make(request()->all(), [
            'uid' => 'required|max:100',
        ], HelperController::errorMessagesBooking());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->companyModel->postDeleteCompany($requested['uid']);

        return response()->json(['code' => 200, 'message' => $result]);
    }
}
