<?php

namespace App\Http\Controllers;

use App\Http\Controllers\HelperController;
use Illuminate\Http\Request;
use Auth, Hash, DB, Log, Carbon;

class AdminDashboardController extends Controller
{
    public function getAdminDashboard()
    {    	
        return view('admin/dashboard', ['current_user' => Auth::user()]);
    }
}
