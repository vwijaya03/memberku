<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\ImageManager;
use App\Http\Controllers\HelperController;
use App\Http\Controllers\ImageController;
use App\PromoModel;
use App\MerchantModel;
use App\VoucherExpiredModel;
use App\GreetingTextModel;
use App\LinkTitleModel;
use Auth, Hash, DB, Log, Validator;

class PromoController extends Controller
{
    public function __construct(PromoModel $promoModel, MerchantModel $merchantModel, VoucherExpiredModel $voucherExpiredModel, GreetingTextModel $greetingTextModel, LinkTitleModel $linkTitleModel)
    {
        $this->promoModel = $promoModel;
        $this->merchantModel = $merchantModel;
        $this->voucherExpiredModel = $voucherExpiredModel;
        $this->greetingTextModel = $greetingTextModel;
        $this->linkTitleModel = $linkTitleModel;
        $this->page_title = 'Promo';
        $this->dir = '/promo-image/';
    }

    public function getPromo()
    {
        $search_merchant_url = url('/admin-access/search-merchant');

        return view('admin/promo', ['current_user' => Auth::user(), 'page_title' => $this->page_title, 'search_merchant_url' => $search_merchant_url]);
    }

    public function postAjaxPromo(Request $request)
    {
        $data = array();

        $columns = HelperController::getPromoColumns();
  
        $totalData = $this->promoModel->countAllActivePromo();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $promos = $this->promoModel->getPromo($start, $limit, $order, $dir);
        }
        else 
        {
            $promos = $this->promoModel->getFilteredPromo($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->promoModel->countAllFilteredActivePromo($search);
        }

        if(!empty($promos))
        {
            foreach ($promos as $promo)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['res'] = $promo;
                $nestedData['name'] = urldecode($promo->name);
                $nestedData['title'] = urldecode($promo->title);
                $nestedData['expired_date'] = date('d F Y', strtotime($promo->expired_date));
                $nestedData['img'] = "<a href='".url('/').$promo->img."' target='_blank'>Klik di sini</a>";
                $nestedData['description'] = urldecode(strip_tags(substr($promo->description, 0, 35))."...");
                $nestedData['action_btn'] = "
                    <button onclick='editPromo(".$promo.")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-target='#edit-promo'><i class='ft-edit'></i></button>
                    <button onclick='deletePromo(".$promo->id.")' type='button' class='btn btn-danger mr-1 mb-1' data-toggle='modal' data-target='#delete-promo'><i class='ft-trash-2'></i></button>
                ";
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function getPartnerPromo()
    {
        return view('partner/benefit', ['current_user' => Auth::user()]);
    }

    public function postAjaxPartnerPromo(Request $request)
    {
        $data = array();
        $current_user_uid = Auth::user()->uid;

        $columns = HelperController::getPartnerPromoColumns();
  
        $totalData = $this->promoModel->countAllActiveSelectedPromoPartnerInBenefit($current_user_uid);
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;


        if(empty($search))
        {            
            $promos = $this->promoModel->getSelectedPromoPartnerInBenefit($start, $limit, $order, $dir, $current_user_uid);
        }
        else 
        {
            $promos = $this->promoModel->getFilteredSelectedPromoPartnerInBenefit($search, $start, $limit, $order, $dir, $current_user_uid);
            $totalFiltered = $this->promoModel->countAllFilteredActiveSelectedPromoPartnerInBenefit($search, $current_user_uid);
        }

        if(!empty($promos))
        {
            foreach ($promos as $promo)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['res'] = $promo;
                $nestedData['name'] = urldecode($promo->name);
                $nestedData['title'] = urldecode($promo->name).' - '.urldecode($promo->title);
                $nestedData['expired_date'] = date('d F Y', strtotime($promo->expired_date));
                $nestedData['img'] = "<a href='".url('/').$promo->img."' target='_blank'>Klik di sini</a>";
                $nestedData['description'] = urldecode(strip_tags(substr($promo->description, 0, 60))."...");
                $nestedData['action_btn'] = "
                    <button onclick='detailPromo(".$promo.")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-target='#data-benefit'><i class='ft-info'></i></button>
                ";
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function postTopPromo() {
        $user_uid = Auth::user()->uid;

        $result = $this->greetingTextModel->getOneGreetingText($user_uid);
        $result_link_title = $this->linkTitleModel->getOneLinkTitle($user_uid);

        $greeting_text = '';
        $link_title = '';

        if($result != null) {
            $greeting_text = $result->description;
        }

        if($result_link_title != null) {
            $link_title = $result_link_title->description;
        }

        $promos = $this->promoModel->getTopPromo();

        return response()->json(['result' => $promos, 'greeting_text' => $greeting_text, 'link_title' => $link_title]);
    }

    public function postAddPromo()
    {
        $validator = Validator::make(request()->all(), [
            'merchant' => 'required',
            'title' => 'required',
            'description' => 'required',
            'img' => 'required|image',
            'harga_awal' => 'required|numeric',
            'harga_akhir' => 'required|numeric',
            'expired_date' => 'required',
            'top_promo' => ''
        ], HelperController::errorMessagesPromo());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $res_merchant = $this->merchantModel->getOneMerchant(request()['merchant']);

        if($res_merchant == null) {
            return response()->json(['code' => 400, 'message' => 'Data merchant tidak di temukan']);
        }

        $requested = request();
        $requested['merchant_uid'] = $this->merchantModel->getOneMerchant($requested['merchant'])->uid;
        $requested['img_path'] = ImageController::createImage($requested['img'], $this->dir, '');
        $requested['top_promo'] = ($requested['top_promo'] == 'on') ? 'yes' : 'no';
        $requested['expired_date'] = date('Y-m-d 23:59:59', strtotime($requested['expired_date']));
        // $requested['pin'] = HelperController::generatePromoPin();
        $requested['sequence'] = $this->promoModel->countAllPromo() + 1;
        $requested['use_pin'] = 'no';

    	$result = $this->promoModel->postAddPromo($requested);
    	
        return response()->json(['code' => 200, 'message' => $result[1]]);
    }

    public function postEditPromo()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|numeric',
            'merchant' => 'required',
            'title' => 'required',
            'description' => 'required',
            'img' => 'image',
            'harga_awal' => 'required|numeric',
            'harga_akhir' => 'required|numeric',
            'use_pin' => '',
            'expired_date' => 'required',
            'top_promo' => ''
        ], HelperController::errorMessagesPromo());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $res_merchant = $this->merchantModel->getOneMerchant(request()['merchant']);
        $res_promo = $this->promoModel->getOnePromo(request()['id']);

        if($res_merchant == null) {
            return response()->json(['code' => 400, 'message' => 'Data merchant tidak di temukan']);
        }

        if($res_promo == null) {
            return response()->json(['code' => 400, 'message' => 'Data promo tidak di temukan']);
        }

    	$requested = request();
        $requested['merchant_uid'] = $res_merchant->uid;
        $requested['top_promo'] = ($requested['top_promo'] == 'on') ? 'yes' : 'no';
        $requested['use_pin'] = ($requested['use_pin'] == 'on') ? 'yes' : 'no';
        $requested['expired_date'] = date('Y-m-d 23:59:59', strtotime($requested['expired_date']));

        // if($requested['use_pin'] == 'yes') {
        //     if($requested['pin'] == null || $requested['pin'] == '' || empty($requested['pin'])) {
        //         return response()->json(['code' => 400, 'message' => 'Pin tidak boleh kosong']);
        //     }

        //     if(!is_numeric($requested['pin'])) {
        //         return response()->json(['code' => 400, 'message' => 'Pin harus angka']);
        //     }
            
        //     if(strlen($requested['pin']) != 6) {
        //        return response()->json(['code' => 400, 'message' => 'Pin harus 6 digit']); 
        //     }
        // }

        if($requested['img'] != null) {
            ImageController::deleteImage($res_promo->img, "");
            $requested['img_path'] = ImageController::createImage($requested['img'], $this->dir, '');
        } else {
            $requested['img_path'] = null;
        }

    	$result = $this->promoModel->postEditPromo($requested, $requested['id']);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postUpdateSequencePromo()
    {
        $validator = Validator::make(request()->all(), [
            'promo_uid' => 'required',
            'new_position' => 'required',
        ], HelperController::errorMessagesPromo());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->promoModel->postUpdateSequencePromo($requested);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postDeletePromo()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|numeric',
        ], HelperController::errorMessagesPromo());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->promoModel->postDeletePromo($requested['id']);

        return response()->json(['code' => 200, 'message' => $result]);
    }
}
