<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\UserBalanceModel;
use App\UserCodeModel;
use App\UserCompanyModel;
use App\UserActiveBalanceLogModel;
use App\ClaimGiftModel;
use Auth, Hash, DB, Log, Validator;

class ClaimGiftController extends Controller
{
    public function __construct(UserCodeModel $userCodeModel, ClaimGiftModel $claimGiftModel, UserBalanceModel $userBalanceModel, UserActiveBalanceLogModel $userActiveBalanceLogModel, UserCompanyModel $userCompanyModel)
    {
        $this->userCodeModel = $userCodeModel;
        $this->userBalanceModel = $userBalanceModel;
        $this->claimGiftModel = $claimGiftModel;
        $this->userActiveBalanceLogModel = $userActiveBalanceLogModel;
        $this->userCompanyModel = $userCompanyModel;
        $this->page_title = 'Ambil Hadiah';
    }

    public function getClaimGift()
    {
        $mod_user_code = "";
        $user_code = "";

        $current_user = Auth::user();
        $result = $this->userCodeModel->getOneUserCodeByUserUID($current_user->uid);

        if($result != null) {
            $mod_user_code = HelperController::splitUserCode($result->code);
            $user_code = $result->code;
        }

        return redirect()->route('getInfoClaimGift', ['user_code' => $user_code]);
    	// return view('agent/claim-gift', ['current_user' => $current_user, 'mod_user_code' => $mod_user_code, 'user_code' => $user_code, 'page_title' => $this->page_title]);
    }

    public function postClaimGift()
    {
        $validator = Validator::make(request()->all(), [
            'merchant_uid' => 'required|max:100',
            'user_uid' => 'required|max:100',
            'amount' => 'required|numeric|digits_between:1,9'
        ], HelperController::errorMessagesClaimGift());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        $requested['user_agent'] = request()->header('User-Agent');
        $requested['ip'] = request()->ip();
        
        $result = $this->claimGiftModel->postAddClaimGift($requested);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postApproveTukarStruk()
    {
        $validator = Validator::make(request()->all(), [
            'cgUid' => 'required|max:100',
            'additional_note' => 'max:100',
        ], HelperController::errorMessagesClaimGift());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $resTukarStruk = $this->claimGiftModel->getOneTukarStrukToVerify($requested['cgUid']);

        if($resTukarStruk == null) {
            return response()->json(['code' => 400, 'message' => 'Data tukar struk tidak ditemukan']);
        }

        if($resTukarStruk->status == "2" || $resTukarStruk->status == "1") {
            return response()->json(['code' => 400, 'message' => 'Data tukar struk sudah direject atau approve']);
        }

        $result = $this->claimGiftModel->postApproveTukarStruk($requested);

        $this->userBalanceModel->postUpdatePlusUserActiveBalance($resTukarStruk->user_uid, $resTukarStruk->amount, $type = "standart");
        $this->userBalanceModel->postUpdateMinusUserBalance($resTukarStruk->user_uid, $resTukarStruk->amount, $type = "standart");

        UserActiveBalanceLogModel::create([
            'uid' => HelperController::uid('user_balance_logs'),
            'from_user_uid' => $resTukarStruk->user_uid,
            'to_user_uid' => $resTukarStruk->user_uid,
            'amount' => floor($resTukarStruk->amount),
            'rate' => 0.1,
            'type' => "self",
            'additional_note' => "Struk sukses diproses",
            'delete' => 0,
        ]);
        
        $this->getParentAgent($resTukarStruk->user_uid, floor($resTukarStruk->amount));

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postRejectTukarStruk()
    {
        $validator = Validator::make(request()->all(), [
            'cgUid' => 'required|max:100',
            'additional_note' => 'max:100',
        ], HelperController::errorMessagesClaimGift());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $resTukarStruk = $this->claimGiftModel->getOneTukarStrukToVerify($requested['cgUid']);

        if($resTukarStruk == null) {
            return response()->json(['code' => 400, 'message' => 'Data tukar struk tidak ditemukan']);
        }

        if($resTukarStruk->status == "2" || $resTukarStruk->status == "1") {
            return response()->json(['code' => 400, 'message' => 'Data tukar struk sudah direject atau approve']);
        }

        $result = $this->claimGiftModel->postRejectTukarStruk($requested);

        $this->userBalanceModel->postUpdateMinusUserBalance($resTukarStruk->user_uid, $resTukarStruk->amount, $type = "standart");

        return response()->json(['code' => 200, 'message' => $result]);
    }

    private function getParentAgent($user_uid, $amount) {
        $referralCode = "";
        $parentUserUID = $user_uid;
        $userType = "standart";

        $referralParent = $this->userCompanyModel->getOneReferralCodeByUserUID($parentUserUID);

        if($referralParent->referral_code != null || $referralParent->referral_code != "") {
            $referralCode = $referralParent->referral_code;
        }
        
        if(substr($referralCode, 0, 3) != "AAI") {
            $amount = $amount * 0.1;

            $referralParentUser = $this->userCodeModel->getOneUserCodeByCodeForRecursive($referralCode);
        
            if($referralParentUser->user_uid != null || $referralParentUser->user_uid != "") {
                $parentUserUID = $referralParentUser->user_uid;
                
                if($referralParentUser->type == "perusahaan") {
                    UserActiveBalanceLogModel::create([
                        'uid' => HelperController::uid('user_balance_logs'),
                        'from_user_uid' => $user_uid,
                        'to_user_uid' => $parentUserUID,
                        'amount' => floor($amount),
                        'rate' => 10,
                        'type' => "perusahaan",
                        'additional_note' => "Bonus referral",
                        'delete' => 0,
                    ]);

                    $this->userBalanceModel->postUpdatePlusUserActiveBalance($parentUserUID, floor($amount), $userType);
                } else if($referralParentUser->type == "standart") {
                    UserActiveBalanceLogModel::create([
                        'uid' => HelperController::uid('user_balance_logs'),
                        'from_user_uid' => $user_uid,
                        'to_user_uid' => $parentUserUID,
                        'amount' => floor($amount),
                        'rate' => 10,
                        'type' => "standart",
                        'additional_note' => "Bonus referral",
                        'delete' => 0,
                    ]);

                    $this->userBalanceModel->postUpdatePlusUserActiveBalance($parentUserUID, floor($amount), $userType);
                } else if($referralParentUser->type == "user_only") {
                    UserActiveBalanceLogModel::create([
                        'uid' => HelperController::uid('user_balance_logs'),
                        'from_user_uid' => $user_uid,
                        'to_user_uid' => $parentUserUID,
                        'amount' => floor($amount),
                        'rate' => 10,
                        'type' => "user_only",
                        'additional_note' => "Bonus referral",
                        'delete' => 0,
                    ]);

                    $this->userBalanceModel->postUpdatePlusUserActiveBalance($parentUserUID, floor($amount), $userType);
                }
            }
            
            if(substr($referralCode, 0, 3) == "AAI") {
                return "";
            } else {
                return $this->getParentAgent($parentUserUID, $amount);
            }
        }
    }
}