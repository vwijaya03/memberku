<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\ImageManager;
use App\Http\Controllers\HelperController;
use App\Http\Controllers\ImageController;
use App\PromoModel;
use App\PromoInternalModel;
use App\UserModel;
use App\VoucherExpiredModel;
use App\GreetingTextModel;
use App\LinkTitleModel;
use Auth, Hash, DB, Log, Validator;

class PromoInternalController extends Controller
{
    public function __construct(PromoInternalModel $promoInternalModel, PromoModel $promoModel, UserModel $userModel, VoucherExpiredModel $voucherExpiredModel, GreetingTextModel $greetingTextModel, LinkTitleModel $linkTitleModel)
    {
        $this->promoModel = $promoModel;
        $this->promoInternalModel = $promoInternalModel;
        $this->userModel = $userModel;
        $this->voucherExpiredModel = $voucherExpiredModel;
        $this->greetingTextModel = $greetingTextModel;
        $this->linkTitleModel = $linkTitleModel;
        $this->page_title = 'Promo Internal';
        $this->dir = '/promo-internal-image/';
    }

    public function getPromoInternal()
    {
        $search_partner_url = url('/admin-access/search-partner');

        return view('admin/promo-internal', ['current_user' => Auth::user(), 'page_title' => $this->page_title, 'search_partner_url' => $search_partner_url]);
    }

    public function postAjaxPromoInternal(Request $request)
    {
        $data = array();

        $columns = HelperController::getPromoInternalColumns();
  
        $totalData = $this->promoInternalModel->countAllActivePromoInternal();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $promos = $this->promoInternalModel->getPromoInternal($start, $limit, $order, $dir);
        }
        else 
        {
            $promos = $this->promoInternalModel->getFilteredPromoInternal($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->promoInternalModel->countAllFilteredActivePromoInternal($search);
        }

        if(!empty($promos))
        {
            foreach ($promos as $promo)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['res'] = $promo;
                $nestedData['fullname'] = urldecode($promo->fullname);
                $nestedData['name'] = urldecode($promo->name);
                $nestedData['title'] = urldecode($promo->title);
                $nestedData['expired_date'] = date('d F Y', strtotime($promo->expired_date));
                $nestedData['img'] = "<a href='".url('/').$promo->img."' target='_blank'>Klik di sini</a>";
                $nestedData['description'] = urldecode(strip_tags(substr($promo->description, 0, 35))."...");
                $nestedData['action_btn'] = "
                    <button onclick='editPromoInternal(".$promo.")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-target='#edit-promo-internal'><i class='ft-edit'></i></button>
                    <button onclick='deletePromoInternal(".$promo->id.")' type='button' class='btn btn-danger mr-1 mb-1' data-toggle='modal' data-target='#delete-promo-internal'><i class='ft-trash-2'></i></button>
                ";
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function postTopPromoInternal() {
        $user_uid = Auth::user()->uid;

        $result = $this->greetingTextModel->getOneGreetingText($user_uid);
        $result_link_title = $this->linkTitleModel->getOneLinkTitle($user_uid);

        $greeting_text = '';
        $link_title = '';

        if($result != null) {
            $greeting_text = $result->description;
        }

        if($result_link_title != null) {
            $link_title = $result_link_title->description;
        }

        $promos = $this->promoInternalModel->getTopPromo();

        return response()->json(['result' => $promos, 'greeting_text' => $greeting_text, 'link_title' => $link_title]);
    }

    public function postAddPromoInternal()
    {
        $validator = Validator::make(request()->all(), [
            'partner' => 'required',
            'title' => 'required',
            'description' => 'required',
            'img' => 'required|image',
            'harga_awal' => 'required|numeric',
            'harga_akhir' => 'required|numeric',
            'expired_date' => 'required',
            'top_promo' => ''
        ], HelperController::errorMessagesPromoInternal());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $res_partner = $this->userModel->getOneUserByUID(request()['partner']);

        if($res_partner == null) {
            return response()->json(['code' => 400, 'message' => 'Data partner tidak di temukan']);
        }

        $requested = request();
        $requested['merchant_uid'] = $this->userModel->getOneUserByUID($requested['partner'])->uid;
        $requested['img_path'] = ImageController::createImage($requested['img'], $this->dir, '');
        $requested['top_promo'] = ($requested['top_promo'] == 'on') ? 'yes' : 'no';
        $requested['expired_date'] = date('Y-m-d 23:59:59', strtotime($requested['expired_date']));
        // $requested['pin'] = HelperController::generatePromoPin();
        $requested['sequence'] = $this->promoInternalModel->countAllPromoInternal($requested['partner']) + 1;
        $requested['use_pin'] = 'no';

    	$result = $this->promoInternalModel->postAddPromoInternal($requested);
    	
        return response()->json(['code' => 200, 'message' => $result[1]]);
    }

    public function postEditPromoInternal()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|numeric',
            'partner' => 'required',
            'title' => 'required',
            'description' => 'required',
            'img' => 'image',
            'harga_awal' => 'required|numeric',
            'harga_akhir' => 'required|numeric',
            'use_pin' => '',
            'expired_date' => 'required',
            'top_promo' => ''
        ], HelperController::errorMessagesPromo());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $res_partner = $this->userModel->getOneUserByUID(request()['partner']);
        $res_promo = $this->promoInternalModel->getOnePromoInternal(request()['id']);

        if($res_partner == null) {
            return response()->json(['code' => 400, 'message' => 'Data partner tidak di temukan']);
        }

        if($res_promo == null) {
            return response()->json(['code' => 400, 'message' => 'Data promo tidak di temukan']);
        }

    	$requested = request();
        $requested['merchant_uid'] = $res_partner->uid;
        $requested['top_promo'] = ($requested['top_promo'] == 'on') ? 'yes' : 'no';
        $requested['use_pin'] = ($requested['use_pin'] == 'on') ? 'yes' : 'no';
        $requested['expired_date'] = date('Y-m-d 23:59:59', strtotime($requested['expired_date']));

        // if($requested['use_pin'] == 'yes') {
        //     if($requested['pin'] == null || $requested['pin'] == '' || empty($requested['pin'])) {
        //         return response()->json(['code' => 400, 'message' => 'Pin tidak boleh kosong']);
        //     }

        //     if(!is_numeric($requested['pin'])) {
        //         return response()->json(['code' => 400, 'message' => 'Pin harus angka']);
        //     }
            
        //     if(strlen($requested['pin']) != 6) {
        //        return response()->json(['code' => 400, 'message' => 'Pin harus 6 digit']); 
        //     }
        // }

        if($requested['img'] != null) {
            ImageController::deleteImage($res_promo->img, "");
            $requested['img_path'] = ImageController::createImage($requested['img'], $this->dir, '');
        } else {
            $requested['img_path'] = null;
        }

    	$result = $this->promoInternalModel->postEditPromoInternal($requested, $requested['id']);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postUpdateSequencePromoInternal()
    {
        $validator = Validator::make(request()->all(), [
            'promo_uid' => 'required',
            'new_position' => 'required',
        ], HelperController::errorMessagesPromo());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->promoInternalModel->postUpdateSequencePromoInternal($requested);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postDeletePromoInternal()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|numeric',
        ], HelperController::errorMessagesPromoInternal());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->promoInternalModel->postDeletePromoInternal($requested['id']);

        return response()->json(['code' => 200, 'message' => $result]);
    }
}
