<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\ImageManager;
use App\Http\Controllers\HelperController;
use App\Http\Controllers\ImageController;
use App\VoucherBonusModel;
use Auth, Hash, DB, Log, Validator;

class VoucherBonusController extends Controller
{
    public function __construct(VoucherBonusModel $voucherBonusModel)
    {
        $this->voucherBonusModel = $voucherBonusModel;
        $this->page_title = 'Voucher Bonus';
        $this->dir = '/voucher-bonus-image/';
    }
}
