<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\ImageManager;
use File, Log;

class ImageController extends Controller
{
    public function __construct()
    {

    }
    
    public static function createImage($img, $dir, $additional_path_name)
    {
        $manager = new ImageManager(array('driver' => 'gd'));
        $path = $dir; //jika di hosting sungguhan pathnya pake yang ini $path
        $name = sha1(\Carbon\Carbon::now().$img->getClientOriginalName().$additional_path_name).'.'.$img->guessExtension();

        if(!File::exists($path))
        {
            File::makeDirectory(getcwd().$path, $mode = 0755, true, true);
        }
        
        $compressedImage = $manager->make($img->getRealPath());
        $compressedImage->resize(500, 400, function ($constraint) {
            $constraint->aspectRatio();
        })
        ->save(getcwd().$path.'/'.$name);

        return $path.$name;
    }

    public static function deleteImage($dir, $img_path)
    {
        if($dir != null || $dir != '')
        {
            if(File::exists(getcwd().$dir))
            {
                $dir_files = getcwd().$dir;
                unlink(realpath($dir_files));   
            }
            else
            {

            }
        }
    }
}
