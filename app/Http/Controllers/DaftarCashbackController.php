<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\UserCompanyModel;
use App\UserCodeModel;
use Auth, Hash, DB, Log, Validator;

class DaftarCashbackController extends Controller
{
    public function __construct(UserCompanyModel $userCompanyModel, UserCodeModel $userCodeModel)
    {
        $this->userCompanyModel = $userCompanyModel;
        $this->userCodeModel = $userCodeModel;
        $this->page_title = 'Daftar Uang';
    }

    public function getDaftarCashback()
    {
        $current_user = Auth::user();
        $res_atasan_user = "";

        $result_user_company = $this->userCompanyModel->getOneCompanyByUserUID($current_user->uid);
        $res_atasan_user = $this->userCodeModel->getOneUserCodeAndUserData($result_user_company->referral_code);

        if($result_user_company != null) {
            $res_atasan_user = $this->userCodeModel->getOneUserCodeAndUserData($result_user_company->referral_code);
        }

        return view('agent/daftar-cashback', ['current_user' => $current_user, 'page_title' => $this->page_title, 'res_atasan_user' => $res_atasan_user]);
    }
}
