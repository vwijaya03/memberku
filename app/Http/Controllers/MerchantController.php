<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\Http\Controllers\ImageController;
use App\MerchantModel;
use Auth, Hash, DB, Log, Validator;

class MerchantController extends Controller
{
    public function __construct(MerchantModel $merchantModel)
    {
        $this->merchantModel = $merchantModel;
        $this->page_title = 'Merchant';
        $this->example_struk_image_dir = '/example-struk-image/';
        $this->logo_merchant_image_dir = '/logo-merchant/';
    }

    public function getMerchant()
    {
        return view('admin/merchant', ['current_user' => Auth::user(), 'page_title' => $this->page_title]);
    }

    public function postAjaxMerchant(Request $request)
    {
        $data = array();

        $columns = HelperController::getMerchantColumns();
  
        $totalData = $this->merchantModel->countAllActiveMerchant();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $merchants = $this->merchantModel->getMerchant($start, $limit, $order, $dir);
        }
        else 
        {
            $merchants = $this->merchantModel->getFilteredMerchant($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->merchantModel->countAllFilteredActiveMerchant($search);
        }

        if(!empty($merchants))
        {
            foreach ($merchants as $merchant)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['res'] = $merchant;
                $nestedData['name'] = urldecode($merchant->name);
                if($merchant->img != null || $merchant->img != "") {
                    $nestedData['img'] = "<a href='".url('/').$merchant->img."' target='_blank'>Klik di sini</a>";
                } else {
                    $nestedData['img'] = "";
                }
                Log::info($merchant->logo);
                if($merchant->logo != null || $merchant->logo != "") {
                    $nestedData['logo'] = "<a href='".url('/').$merchant->logo."' target='_blank'>Klik di sini</a>";
                } else {
                    $nestedData['logo'] = "";
                }
                $nestedData['action_btn'] = "
                    <button onclick='editMerchant(".$merchant.")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-target='#edit-merchant'><i class='ft-edit'></i></button>
                    <button onclick='deleteMerchant(".$merchant.")' type='button' class='btn btn-danger mr-1 mb-1' data-toggle='modal' data-target='#delete-merchant'><i class='ft-trash-2'></i></button>
                ";
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function postAjaxMerchantJoinBonus(Request $request)
    {
        $data = array();

        $columns = HelperController::getMerchantJoinBonusColumns();
  
        $totalData = $this->merchantModel->countAllActiveMerchantJoinBonus();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = urlencode($request->input('search.value')); 
        $number = 1;

        if(empty($search))
        {            
            $merchants = $this->merchantModel->getMerchantJoinBonus($start, $limit, $order, $dir);
        }
        else 
        {
            $merchants = $this->merchantModel->getFilteredMerchantJoinBonus($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->merchantModel->countAllFilteredActiveMerchantJoinBonus($search);
        }

        if(!empty($merchants))
        {
            foreach ($merchants as $merchant)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['res'] = $merchant;
                $nestedData['name'] = urldecode($merchant->name);

                if($merchant->img != null || $merchant->img != "") {
                    $nestedData['img'] = "<a href='".url('/').$merchant->img."' target='_blank'>Klik di sini</a>";
                } else {
                    $nestedData['img'] = "";
                }

                if($merchant->available == "yes") {
                    $nestedData['action_btn'] = "
                        <button onclick='detailBonus(".$merchant.")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-target='#edit-merchant'>Pilih</button>
                    ";
                } else {
                    $nestedData['action_btn'] = "";
                }
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function getSearchMerchant(Request $request) 
    {
        $merchants = $this->merchantModel->getSearchSelect2Merchant($request->input('q'));

        return response()->json(['result' => $merchants]);
    }

    public function getSearchMerchantJoinPromo(Request $request) 
    {
        $merchants = $this->merchantModel->getSearchSelect2MerchantJoinPromo($request->input('q'));

        return response()->json(['result' => $merchants]);
    }

    public function getSearchMerchantJoinBonus(Request $request) 
    {
        $merchants = $this->merchantModel->getSearchSelect2MerchantJoinBonus($request->input('q'));

        return response()->json(['result' => $merchants]);
    }

    public function postAddMerchant()
    {
        $validator = Validator::make(request()->all(), [
            'email' => 'required|email|unique:merchants',
            'name' => 'required',
            'contact_person' => 'required',
            'phone' => 'required|numeric|digits_between:9,13',
            'address' => 'required',
            'join_promo' => '',
            'join_bonus' => '',
            'available' => '',
            'rate' => 'required|numeric',
            'img' => 'required|image',
            'logo_merchant' => 'required|image',
        ], HelperController::errorMessagesMerchant());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        $requested['join_promo'] = ($requested['join_promo'] == 'on') ? 'yes' : 'no';
        $requested['join_bonus'] = ($requested['join_bonus'] == 'on') ? 'yes' : 'no';
        $requested['available'] = ($requested['available'] == 'on') ? 'yes' : 'no';
        $requested['img_path'] = ImageController::createImage($requested['img'], $this->example_struk_image_dir, '');
        $requested['logo_merchant_path'] = ImageController::createImage($requested['logo_merchant'], $this->logo_merchant_image_dir, '');

    	$result = $this->merchantModel->postAddMerchant($requested);
    	
        return response()->json(['code' => 200, 'message' => $result[1]]);
    }

    public function postEditMerchant()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|numeric',
            'email' => 'required|email|unique:merchants,email,'.request()['id'],
            'name' => 'required',
            'contact_person' => 'required',
            'phone' => 'required|numeric|digits_between:9,13',
            'address' => 'required',
            'join_promo' => '',
            'join_bonus' => '',
            'rate' => 'required|numeric'
        ], HelperController::errorMessagesMerchant());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }
        
        $requested = request();
        $requested['join_promo'] = ($requested['join_promo'] == 'on') ? 'yes' : 'no';
        $requested['join_bonus'] = ($requested['join_bonus'] == 'on') ? 'yes' : 'no';
        $requested['available'] = ($requested['available'] == 'on') ? 'yes' : 'no';

        $res_merchant = $this->merchantModel->getOneMerchant($requested['id']);

        if($res_merchant == null) {
            return response()->json(['code' => 400, 'message' => 'Data merchant tidak di temukan']);
        }

        if($requested['img'] != null) {
            ImageController::deleteImage($res_merchant->img, "");
            $requested['img_path'] = ImageController::createImage($requested['img'], $this->example_struk_image_dir, '');
        } else {
            $requested['img_path'] = null;
        }

        if($requested['logo_merchant'] != null) {
            ImageController::deleteImage($res_merchant->logo, "");
            $requested['logo_merchant_path'] = ImageController::createImage($requested['logo_merchant'], $this->logo_merchant_image_dir, '');
        } else {
            $requested['logo_merchant_path'] = null;
        }

    	$result = $this->merchantModel->postEditMerchant($requested, $requested['id']);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postDeleteMerchant()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|numeric',
        ], HelperController::errorMessagesBooking());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->merchantModel->postDeleteMerchant($requested['id']);

        return response()->json(['code' => 200, 'message' => $result]);
    }
}
