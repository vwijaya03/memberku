<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\UserCodeModel;
use Auth, Hash, DB, Log, Validator;

class VerifyUserFromBankController extends Controller
{
    public function __construct(UserCodeModel $userCodeModel)
    {
        $this->page_title = '';
        $this->userCodeModel = $userCodeModel;
    }

    public function getVerifyUserFromBank()
    {
        return view('public/verify-user-from-bank', ['page_title' => $this->page_title]);
    }
    
    public function postVerifyUserFromBank()
    {
        $validator = Validator::make(request()->all(), [
            'user_code' => 'required|max:100'
        ], HelperController::errorMessagesUsePromo());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

        $res_user_code = $this->userCodeModel->getOneUserCodeByCode($requested['user_code']);

        if($res_user_code == null) {
            return response()->json(['code' => 400, 'message' => 'Kode User Tidak Ditemukan']);
        }

        return response()->json(['code' => 200, 'message' => 'berhasil', 'user_code' => $requested['user_code']]);
    }
}
