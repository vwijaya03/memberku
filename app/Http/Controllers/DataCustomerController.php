<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\DataCustomerModel;
use Auth, Hash, DB, Log, Validator;

class DataCustomerController extends Controller
{
    public function __construct(DataCustomerModel $dataCustomerModel)
    {
        $this->dataCustomerModel = $dataCustomerModel;
        $this->page_title = 'Data Customer';
    }

    public function postAddDataCustomerWA()
    {
        $validator = Validator::make(request()->all(), [
            'user_uid' => 'required|max:100',
            'fullname' => 'max:100',
            'email' => 'max:100',
            'phone' => 'required|min:9|max:13',
            'address' => 'max:100',
        ], HelperController::errorMessagesDataCustomer());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

    	$result = $this->dataCustomerModel->postAddDataCustomer($requested);
    	
        return response()->json(['code' => 200, 'message' => $result[1]]);
    }
}
