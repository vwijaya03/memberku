<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\UserModel;
use App\CompanyModel;
use App\UserCompanyModel;
use Auth, Hash, DB, Log, Validator;

class UserController extends Controller
{
    public function __construct(UserModel $userModel, CompanyModel $companyModel, UserCompanyModel $userCompanyModel)
    {
        $this->userModel = $userModel;
        $this->companyModel = $companyModel;
        $this->userCompanyModel = $userCompanyModel;
        $this->page_title_admin = 'Admin';
        $this->page_title_customer = 'Customer';
        $this->page_title_superadmin = 'Superadmin';
        $this->role = '';
    }

    public function getUser()
    {
        return view('admin/user', ['current_user' => Auth::user(), 'page_title' => $this->page_title_superadmin]);
    }

    public function postAjaxUser(Request $request)
    {
        $data = array();

        $columns = HelperController::getUserColumns();
  
        $totalData = $this->userModel->countAllActiveUser($this->role = 'superadmin');
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $users = $this->userModel->getUser($start, $limit, $order, $dir, $this->role = 'superadmin');
        }
        else 
        {
            $users = $this->userModel->getFilteredUser($search, $start, $limit, $order, $dir, $this->role = 'superadmin');
            $totalFiltered = $this->userModel->countAllFilteredActiveUser($search, $this->role = 'superadmin');
        }

        if(!empty($users))
        {
            foreach ($users as $user)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['fullname'] = urldecode($user->fullname);
                $nestedData['res'] = $user;
                $nestedData['action_btn'] = "
                    <button onclick='editUser(".$user.")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-target='#edit-user'><i class='ft-edit'></i></button>
                    <button onclick='deleteUser(".$user.")' type='button' class='btn btn-danger mr-1 mb-1' data-toggle='modal' data-target='#delete-user'><i class='ft-trash-2'></i></button>
                ";
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function getAuthUser()
    {
        $user =  $this->userModel->getOnePartnerUserByUID(Auth::user()->uid);
        
        return response()->json(['code' => 200, 'result' => $user]);
    }

    public function getAddUser()
    {
    	return view('admin/add-user', ['current_user' => Auth::user(), 'page_title' => $this->page_title_superadmin]);
    }

    public function postAddAgent()
    {
        $validator = Validator::make(request()->all(), [
            'fullname' => 'required',
            'name_on_card' => 'max:100',
            'email' => 'required|email|unique:users',
            'phone' => 'max:100',
            'address' => 'max:35',
            'city' => 'max:35',
            'referral_code' => 'max:35',
            'role' => 'required'
        ], HelperController::errorMessagesUser());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

        if($requested['password'] == null || $requested['password'] == "") {
            $requested['password'] = '123456';
        }

        if($requested['role'] ==  "agent") {
            $requested['company_uid'] = "";

            if($requested['referral_code'] != null || $requested['referral_code'] != "") {
                $res_company = $this->companyModel->getOneCompany($requested['referral_code']);

                if($res_company == null) {
                    return response()->json(['code' => 400, 'message' => "Data perusahaan tidak di temukan"]);
                } else {
                    $requested['company_uid'] = $res_company->uid;
                }
            } else {
                $requested['company_uid'] = "";
            }
        }

        $result = $this->userModel->postAddUser($requested);
        
        if(Auth::attempt(['email' => $requested['email'], 'password' => $requested['password'], 'role' => 'agent', 'approve' => 1, 'delete' => 0])) {
            return response()->json(['code' => 200, 'message' => $result[1]]);
        } else {
            return response()->json(['code' => 400, 'message' => "Register gagal, silahkan coba lagi"]);
        }
    }

    public function postAddUser()
    {
        $validator = Validator::make(request()->all(), [
            'fullname' => 'required',
            'name_on_card' => 'max:100',
            'email' => 'required|email|unique:users',
            'phone' => 'max:100',
            'address' => 'max:35',
            'city' => 'max:35',
            'referral_code' => 'max:35',
            'role' => 'required'
        ], HelperController::errorMessagesUser());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

        if($requested['password'] == null || $requested['password'] == "") {
            $requested['password'] = '123456';
        }

        if($requested['role'] ==  "partner") {
            $requested['type'] = "partner";
        }

        if($requested['role'] ==  "agent") {
            $requested['company_uid'] = "";

            if($requested['referral_code'] != null || $requested['referral_code'] != "") {
                $res_company = $this->companyModel->getOneCompany($requested['referral_code']);

                if($res_company == null) {
                    return response()->json(['code' => 400, 'message' => "Data perusahaan tidak di temukan"]);
                } else {
                    $requested['company_uid'] = $res_company->uid;
                }
            } else {
                $requested['company_uid'] = "";
            }
        }

    	$result = $this->userModel->postAddUser($requested);
    	
        return response()->json(['code' => 200, 'message' => $result[1]]);
    }

    public function postEditUser()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|numeric',
            'fullname' => 'required|max:100',
            'name_on_card' => 'max:100',
            'email' => 'required|unique:users,email,'.request()['id'],
            'phone' => 'max:100|unique:users,phone,'.request()['id'],
            'address' => 'max:35',
            'city' => 'max:35'
        ], HelperController::errorMessagesUser());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

    	$requested = request();

    	$result = $this->userModel->postEditUser($requested, $requested['id']);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postDeleteUser()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|numeric',
        ], HelperController::errorMessagesUser());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->userModel->postDeleteUser($requested['id']);

        return response()->json(['code' => 200, 'message' => $result]);
    }
}
