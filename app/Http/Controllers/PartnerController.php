<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\UserModel;
use Auth, Hash, DB, Log, Validator;

class PartnerController extends Controller
{
    public function __construct(UserModel $userModel)
    {
        $this->userModel = $userModel;
        $this->page_title = 'Partner Pending';
        $this->role = '';
    }

    public function getPartner($status)
    {
    	if($status == 'approved') {
        	$this->page_title = 'Partner Approved';
        } else if($status == 'rejected') {
            $this->page_title = 'Partner Rejected';
        }

        return view('admin/partner', ['current_user' => Auth::user(), 'page_title' => $this->page_title, 'status' => $status]);
    }

    public function postAjaxPartner(Request $request, $status)
    {
    	$approved = 0;
        $data = array();

        if($status == 'approved') {
        	$approved = 1;
        } else if($status == 'rejected') {
            $approved = 2;
        }

        $columns = HelperController::getUserColumns();
  
        $totalData = $this->userModel->countAllActiveUser($this->role = 'partner', $approved);
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $partners = $this->userModel->getUser($start, $limit, $order, $dir, $this->role = 'partner', $approved);
        }
        else 
        {
            $partners = $this->userModel->getFilteredUser($search, $start, $limit, $order, $dir, $this->role = 'partner', $approved);
            $totalFiltered = $this->userModel->countAllFilteredActiveUser($search, $this->role = 'partner', $approved);
        }

        if(!empty($partners))
        {
            foreach ($partners as $partner)
            {
                $action_btn = "";

                if($status == 'pending') {
                    $action_btn = "
                        <button onclick='approvePartner(".$partner->id.")' type='button' class='btn btn-success mr-1 mb-1' data-toggle='modal' data-target='#approve-partner' title='Approve'><i class='ft-check'></i></button>
                        <button onclick='editPartner(".$partner.")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-target='#edit-partner' title='Ubah'><i class='ft-edit'></i></button>
                        <button onclick='deletePartner(".$partner.")' type='button' class='btn btn-danger mr-1 mb-1' data-toggle='modal' data-target='#delete-partner' title='Hapus'><i class='ft-trash-2'></i></button>
                    ";
                } else if($status == 'rejected') {
                    $action_btn = "
                        <button onclick='approvePartner(".$partner->id.")' type='button' class='btn btn-success mr-1 mb-1' data-toggle='modal' data-target='#approve-partner' title='Approve'><i class='ft-check'></i></button>
                        <button onclick='editPartner(".$partner.")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-target='#edit-partner' title='Ubah'><i class='ft-edit'></i></button>
                        <button onclick='deletePartner(".$partner.")' type='button' class='btn btn-danger mr-1 mb-1' data-toggle='modal' data-target='#delete-partner' title='Hapus'><i class='ft-trash-2'></i></button>
                    ";
                } else {
                    $action_btn = "
                        <button onclick='rejectPartner(".$partner->id.")' type='button' class='btn btn-danger mr-1 mb-1' data-toggle='modal' data-target='#reject-partner' title='Reject'><i class='ft-x'></i></button>
                        <button onclick='editPartner(".$partner.")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-target='#edit-partner' title='Ubah'><i class='ft-edit'></i></button>
                        <button onclick='deletePartner(".$partner.")' type='button' class='btn btn-danger mr-1 mb-1' data-toggle='modal' data-target='#delete-partner' title='Hapus'><i class='ft-trash-2'></i></button>
                    ";
                }

                $nestedData['no'] = $start+$number;
                $nestedData['fullname'] = urldecode($partner->fullname);
                $nestedData['res'] = $partner;
                $nestedData['action_btn'] = $action_btn;
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function getSearchPartner(Request $request) 
    {
        $partner = $this->userModel->getSearchSelect2Partner($request->input('q'));

        return response()->json(['result' => $partner]);
    }

    public function postAddPartner()
    {
        $validator = Validator::make(request()->all(), [
            'fullname' => 'required',
            'name_on_card' => 'max:100',
            'email' => 'required|email|unique:users',
            'phone' => 'max:100',
            'address' => 'max:35',
            'city' => 'max:35',
            'role' => 'required'
        ], HelperController::errorMessagesUser());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        $requested['password'] = '123456';

    	$result = $this->userModel->postAddUser($requested);
    	
        return response()->json(['code' => 200, 'message' => $result[1]]);
    }

    public function postEditPartner()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|numeric',
            'fullname' => 'required',
            'name_on_card' => 'max:100',
            'email' => 'required|unique:users,email,'.request()['id'],
            'phone' => 'max:100',
            'address' => 'max:35',
            'city' => 'max:35'
        ], HelperController::errorMessagesUser());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

    	$requested = request();

    	$result = $this->userModel->postEditUser($requested, $requested['id']);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postDeletePartner()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|numeric',
        ], HelperController::errorMessagesUser());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->userModel->postDeleteUser($requested['id']);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postApprovePartner()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|numeric',
        ], HelperController::errorMessagesUser());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->userModel->postApproveUser($requested['id']);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postRejectPartner()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|numeric',
        ], HelperController::errorMessagesUser());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->userModel->postRejectUser($requested['id']);

        return response()->json(['code' => 200, 'message' => $result]);
    }
}
