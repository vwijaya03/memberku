<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\UserEWalletModel;
use Auth, Hash, DB, Log, Validator;

class UserEWalletController extends Controller
{
    public function __construct(UserEWalletModel $userEWalletModel)
    {
        $this->userEWalletModel = $userEWalletModel;
        $this->page_title = 'GoPay / OVO';
    }

    public function getUserEWallet()
    {
        $res_user_ewallet = $this->userEWalletModel->getOneUserEWalletByUserUID(Auth::user()->uid);

        return view('agent/ewallet', ['current_user' => Auth::user(), 'page_title' => $this->page_title, 'res_user_ewallet' => $res_user_ewallet]);
    }

    public function postAjaxUserEWallet(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            'user_uid' => 'required|max:100',
        ], HelperController::errorMessagesUserEWallet());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

        $data = array();
        
        $columns = HelperController::getUserEWalletColumns();
  
        $totalData = $this->userEWalletModel->countAllActiveUserEWallet($requested['user_uid']);
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $ewallets = $this->userEWalletModel->getUserEWallet($start, $limit, $order, $dir, $requested['user_uid']);
        }
        else 
        {
            $ewallets = $this->userEWalletModel->getFilteredUserEWallet($search, $start, $limit, $order, $dir, $requested['user_uid']);
            $totalFiltered = $this->userEWalletModel->countAllFilteredActiveUserEWallet($search, $requested['user_uid']);
        }

        if(!empty($ewallets))
        {
            foreach ($ewallets as $ewallet)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['res'] = $ewallet;
                $nestedData['no_hp'] = urldecode($ewallet->no_hp);
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function postAddUserEWallet()
    {
        $validator = Validator::make(request()->all(), [
            'user_uid' => 'required|max:60',
            'no_hp' => 'required|max:60'
        ], HelperController::errorMessagesUserEWallet());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

        $res_user_ewallet = $this->userEWalletModel->getOneUserEWalletByUserUID($requested['user_uid']);

        if($res_user_ewallet != null) {
            return response()->json(['code' => 400, 'message' => "Limit no hp untuk GoPay / OVO hanya 1"]);
        }

    	$result = $this->userEWalletModel->postAddUserEWallet($requested);
    	
        return response()->json(['code' => 200, 'message' => $result[1]]);
    }

    public function postEditUserEWallet()
    {
        $validator = Validator::make(request()->all(), [
            'uid' => 'required|max:100',
            'user_uid' => 'required|max:60',
            'no_hp' => 'required|max:60'
        ], HelperController::errorMessagesUserEWallet());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

    	$result = $this->userEWalletModel->postEditUserEWallet($requested, $requested['uid']);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postDeleteUserEWallet()
    {
        $validator = Validator::make(request()->all(), [
            'uid' => 'required|max:100',
        ], HelperController::errorMessagesUserEWallet());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->userEWalletModel->postDeleteUserEWallet($requested['uid']);

        return response()->json(['code' => 200, 'message' => $result]);
    }
}
