<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class PartnerDashboardController extends Controller
{
    public function getPartnerDashboard()
    {    	
        return view('partner/dashboard', ['current_user' => Auth::user()]);
    }
}
