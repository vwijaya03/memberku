<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\ImageManager;
use App\Http\Controllers\HelperController;
use App\Http\Controllers\ImageController;
use App\HadiahModel;
use App\MerchantModel;
use Auth, Hash, DB, Log, Validator;

class HadiahController extends Controller
{
    public function __construct(HadiahModel $hadiahModel, MerchantModel $merchantModel)
    {
        $this->hadiahModel = $hadiahModel;
        $this->merchantModel = $merchantModel;
        $this->page_title = 'Hadiah';
        $this->dir = '/hadiah-image/';
    }

    public function getHadiah()
    {
        $search_merchant_url = url('/admin-access/search-merchant-join-bonus');

        return view('admin/hadiah', ['current_user' => Auth::user(), 'page_title' => $this->page_title, 'search_merchant_url' => $search_merchant_url]);
    }

    public function postAjaxHadiah(Request $request)
    {
        $data = array();

        $columns = HelperController::getHadiahColumns();
  
        $totalData = $this->hadiahModel->countAllActiveHadiah();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $gifts = $this->hadiahModel->getHadiah($start, $limit, $order, $dir);
        }
        else 
        {
            $gifts = $this->hadiahModel->getFilteredHadiah($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->hadiahModel->countAllFilteredActiveHadiah($search);
        }

        if(!empty($gifts))
        {
            foreach ($gifts as $hadiah)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['res'] = $hadiah;
                $nestedData['name'] = urldecode($hadiah->name);
                $nestedData['title'] = urldecode($hadiah->title);
                $nestedData['expired_date'] = date('d F Y', strtotime($hadiah->expired_date));
                $nestedData['img'] = "<a href='".url('/').$hadiah->img."' target='_blank'>Klik di sini</a>";
                $nestedData['description'] = urldecode(strip_tags(substr($hadiah->description, 0, 35))."...");
                $nestedData['action_btn'] = "
                    <button onclick='editHadiah(".$hadiah.")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-target='#edit-hadiah'><i class='ft-edit'></i></button>
                    <button onclick='deleteHadiah(".$hadiah.")' type='button' class='btn btn-danger mr-1 mb-1' data-toggle='modal' data-target='#delete-hadiah'><i class='ft-trash-2'></i></button>
                ";
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function postAddHadiah()
    {
        $validator = Validator::make(request()->all(), [
            'merchant' => 'required|max:100',
            'title' => 'required',
            'description' => 'required',
            'img' => 'required|image',
            'expired_date' => 'required'
        ], HelperController::errorMessagesHadiah());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $res_merchant = $this->merchantModel->getOneMerchantByUID(request()['merchant']);

        if($res_merchant == null) {
            return response()->json(['code' => 400, 'message' => 'Data merchant tidak di temukan']);
        }

        $requested = request();
        $requested['merchant_uid'] = $res_merchant->uid;
        $requested['img_path'] = ImageController::createImage($requested['img'], $this->dir, '');
        $requested['expired_date'] = date('Y-m-d 23:59:59', strtotime($requested['expired_date']));

    	$result = $this->hadiahModel->postAddHadiah($requested);
    	
        return response()->json(['code' => 200, 'message' => $result[1]]);
    }

    public function postEditHadiah()
    {
        $validator = Validator::make(request()->all(), [
            'uid' => 'required|max:100',
            'merchant' => 'required|max:100',
            'title' => 'required',
            'description' => 'required',
            'img' => 'image',
            'expired_date' => 'required'
        ], HelperController::errorMessagesHadiah());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }
        
        $res_merchant = $this->merchantModel->getOneMerchantByUID(request()['merchant']);
        $res_hadiah = $this->hadiahModel->getOneHadiah(request()['uid']);

        if($res_merchant == null) {
            return response()->json(['code' => 400, 'message' => 'Data merchant tidak di temukan']);
        }

        if($res_hadiah == null) {
            return response()->json(['code' => 400, 'message' => 'Data hadiah tidak di temukan']);
        }

    	$requested = request();
        $requested['merchant_uid'] = $res_merchant->uid;
        $requested['expired_date'] = date('Y-m-d 23:59:59', strtotime($requested['expired_date']));

        if($requested['img'] != null) {
            ImageController::deleteImage($res_hadiah->img, "");
            $requested['img_path'] = ImageController::createImage($requested['img'], $this->dir, '');
        } else {
            $requested['img_path'] = null;
        }

    	$result = $this->hadiahModel->postEditHadiah($requested, $requested['uid']);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postDeleteHadiah()
    {
        $validator = Validator::make(request()->all(), [
            'uid' => 'required|max:100',
        ], HelperController::errorMessagesHadiah());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->hadiahModel->postDeleteHadiah($requested['uid']);

        return response()->json(['code' => 200, 'message' => $result]);
    }
}
