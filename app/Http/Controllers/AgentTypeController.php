<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\AgentTypeModel;
use Auth, Hash, DB, Log, Validator;

class AgentTypeController extends Controller
{
    public function __construct(AgentTypeModel $agentTypeModel)
    {
        $this->agentTypeModel = $agentTypeModel;
        $this->page_title = 'Tipe Agent';
    }

    public function getAgentType()
    {
        return view('admin/agent-type', ['current_user' => Auth::user(), 'page_title' => $this->page_title]);
    }

    public function postAjaxAgentType(Request $request)
    {
        $data = array();

        $columns = HelperController::getAgentTypeColumns();
  
        $totalData = $this->agentTypeModel->countAllActiveAgentType();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $agent_types = $this->agentTypeModel->getAgentType($start, $limit, $order, $dir);
        }
        else 
        {
            $agent_types = $this->agentTypeModel->getFilteredAgentType($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->agentTypeModel->countAllFilteredActiveAgentType($search);
        }

        if(!empty($agent_types))
        {
            foreach ($agent_types as $agent_type)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['res'] = $agent_type;
                $nestedData['name'] = urldecode($agent_type->name);
                $nestedData['action_btn'] = "
                    <button onclick='editAgentType(".$agent_type.")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-target='#edit-agent-type'><i class='ft-edit'></i></button>
                    <button onclick='deleteAgentType(".$agent_type.")' type='button' class='btn btn-danger mr-1 mb-1' data-toggle='modal' data-target='#delete-agent-type'><i class='ft-trash-2'></i></button>
                ";
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function postAddAgentType()
    {
        $validator = Validator::make(request()->all(), [
            'name' => 'required|max:50',
            'rate' => 'required|numeric',
            'allow_upload_logo' => '',
        ], HelperController::errorMessagesAgentType());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        $requested['allow_upload_logo'] = ($requested['allow_upload_logo'] == 'on') ? 'yes' : 'no';

    	$result = $this->agentTypeModel->postAddAgentType($requested);
    	
        return response()->json(['code' => 200, 'message' => $result[1]]);
    }

    public function postEditAgentType()
    {
        $validator = Validator::make(request()->all(), [
            'uid' => 'required|max:100',
            'name' => 'required|max:50',
            'rate' => 'required|numeric',
            'allow_upload_logo' => '',
        ], HelperController::errorMessagesAgentType());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        $requested['allow_upload_logo'] = ($requested['allow_upload_logo'] == 'on') ? 'yes' : 'no';

    	$result = $this->agentTypeModel->postEditAgentType($requested, $requested['uid']);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postDeleteAgentType()
    {
        $validator = Validator::make(request()->all(), [
            'uid' => 'required|max:100',
        ], HelperController::errorMessagesAgentType());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->agentTypeModel->postDeleteAgentType($requested['uid']);

        return response()->json(['code' => 200, 'message' => $result]);
    }
}
