<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\Http\Controllers\ImageController;
use App\PromoModel;
use App\PromoInternalModel;
use App\PartnerPromoModel;
use App\UniqueCodeModel;
use App\RecordUsePromoModel;
use App\MembershipModel;
use App\UserModel;
use App\PartnerLogoModel;
use App\LinkTitleModel;
use App\PinPromoModel;
use App\UserCodeModel;
use App\ClaimGiftModel;
use App\MerchantModel;
use App\UserCompanyModel;
use App\UserBalanceModel;
use App\UserBalanceLogModel;
use Auth, Hash, DB, Log, Validator, Redirect;

class PublicController extends Controller
{
    public function __construct(PromoModel $promoModel, PromoInternalModel $promoInternalModel, PartnerPromoModel $partnerPromoModel, UniqueCodeModel $uniqueCodeModel, RecordUsePromoModel $recordUsePromoModel, MembershipModel $membershipModel, UserModel $userModel, PartnerLogoModel $partnerLogoModel, LinkTitleModel $linkTitleModel, PinPromoModel $pinPromoModel, UserCodeModel $userCodeModel, ClaimGiftModel $claimGiftModel, MerchantModel $merchantModel, UserCompanyModel $userCompanyModel, UserBalanceModel $userBalanceModel)
    {
        $this->promoModel = $promoModel;
        $this->promoInternalModel = $promoInternalModel;
        $this->partnerPromoModel = $partnerPromoModel;
        $this->uniqueCodeModel = $uniqueCodeModel;
        $this->recordUsePromoModel = $recordUsePromoModel;
        $this->membershipModel = $membershipModel;
        $this->userModel = $userModel;
        $this->partnerLogoModel = $partnerLogoModel;
        $this->linkTitleModel = $linkTitleModel;
        $this->pinPromoModel = $pinPromoModel;
        $this->userCodeModel = $userCodeModel;
        $this->claimGiftModel = $claimGiftModel;
        $this->merchantModel = $merchantModel;
        $this->userCompanyModel = $userCompanyModel;
        $this->userBalanceModel = $userBalanceModel;

        $this->img_struk_dir = '/bukti-struk/';
    }

    public function getTACClaimGift()
    {
        return view('public/tac-claim-gift');
    }

    public function getInfoClaimGift($user_code = "")
    {
        return Redirect::to('/claim-gift/u/'.$user_code);
        // return view('public/info-claim-gift', ['user_code' => $user_code]);
    }

    public function getPromo($promo_uid, $unique_code, $user_uid, $name_on_card = "")
    {
    	$promo = $this->promoModel->getOnePromoByUID($promo_uid);
    	$voucher = $this->uniqueCodeModel->getOneUniqueCodeByUniqueCode($unique_code, $user_uid);

    	if($promo == null || $voucher == null) {
    		return view('error/promo-expired');
    	}

    	if(date('Y-m-d H:i:s') >= $promo->expired_date) {
    		return view('error/promo-expired');
    	}

    	if(date('Y-m-d H:i:s') >= $voucher->expired_date) {
    		return view('error/promo-expired');
    	}

        return view('public/promo', ['promo' => $promo, 'voucher' => $voucher, 'name_on_card' => $name_on_card]);
    }

    public function postUsePromo()
    {
    	$validator = Validator::make(request()->all(), [
            'promo_uid' => 'required|max:100',
            'unique_code' => 'required|max:100',
            'pin' => 'required|max:100',
            'user_uid' => 'max:100'
        ], HelperController::errorMessagesUsePromo());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        $requested['user_agent'] = request()->header('User-Agent');
        $requested['ip'] = request()->ip();
        
        $record = $this->recordUsePromoModel->getOneRecord($requested['unique_code'], $requested['promo_uid']);
        $voucher = $this->uniqueCodeModel->getOneUniqueCodeByUniqueCode($requested['unique_code'], $requested['user_uid']);
        $promo = $this->promoModel->verifyPromoExpiredDate($requested['promo_uid']);
        $verify_pin_promo = $this->pinPromoModel->verifyPromoPin($requested['promo_uid'], $requested['pin']);

        if($verify_pin_promo == null) {
            return response()->json(['code' => 400, 'message' => 'Pin salah']);
        }

        if(date('Y-m-d H:i:s') >= $voucher->expired_date) {
            return response()->json(['code' => 400, 'message' => 'Promo / Link Telah Expired']);
        }

        if(date('Y-m-d H:i:s') >= $promo->expired_date) {
            return response()->json(['code' => 400, 'message' => 'Promo / Link Telah Expired']);
        }

        if($record != null) {
            return response()->json(['code' => 400, 'message' => 'Kode voucher telah di gunakan']);
        }

        $remaining_click = $voucher->click_limit - 1;

        if($remaining_click < 0) {
        	return response()->json(['code' => 400, 'message' => 'Melebihi jumlah penggunaan']);
        }

        $result = $this->uniqueCodeModel->postUpdateRemainingClick($requested['unique_code'], $requested['user_uid'], $remaining_click);

        $this->recordUsePromoModel->postAddRecordUsePromo($requested);
    	
        return response()->json(['code' => 200, 'message' => $result, 'remaining_click' => $remaining_click]);
    }

    public function postUsePromoInternal()
    {
        $validator = Validator::make(request()->all(), [
            'promo_uid' => 'required|max:100',
            'unique_code' => 'required|max:100',
            'pin' => 'required|max:100',
            'user_uid' => 'max:100'
        ], HelperController::errorMessagesUsePromo());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        $requested['user_agent'] = request()->header('User-Agent');
        $requested['promo_type'] = 'internal';
        $requested['ip'] = request()->ip();
        
        $record = $this->recordUsePromoModel->getOneRecord($requested['unique_code'], $requested['promo_uid']);
        $voucher = $this->uniqueCodeModel->getOneUniqueCodeByUniqueCode($requested['unique_code'], $requested['user_uid']);
        $promo_internal = $this->promoInternalModel->verifyPromoInternalExpiredDate($requested['promo_uid']);
        $verify_pin_promo_internal = $this->pinPromoModel->verifyPromoPin($requested['promo_uid'], $requested['pin']);

        if($verify_pin_promo_internal == null) {
            return response()->json(['code' => 400, 'message' => 'Pin salah']);
        }

        if(date('Y-m-d H:i:s') >= $voucher->expired_date) {
            return response()->json(['code' => 400, 'message' => 'Promo / Link Telah Expired']);
        }

        if(date('Y-m-d H:i:s') >= $promo_internal->expired_date) {
            return response()->json(['code' => 400, 'message' => 'Promo / Link Telah Expired']);
        }

        if($record != null) {
            return response()->json(['code' => 400, 'message' => 'Kode voucher telah di gunakan']);
        }

        $remaining_click = $voucher->click_limit - 1;

        if($remaining_click < 0) {
            return response()->json(['code' => 400, 'message' => 'Melebihi jumlah penggunaan']);
        }

        $result = $this->uniqueCodeModel->postUpdateRemainingClick($requested['unique_code'], $requested['user_uid'], $remaining_click);

        $this->recordUsePromoModel->postAddRecordUsePromo($requested);
        
        return response()->json(['code' => 200, 'message' => $result, 'remaining_click' => $remaining_click]);
    }

    public function postDirectUsePromo()
    {
        $validator = Validator::make(request()->all(), [
            'promo_uid' => 'required|max:100',
            'unique_code' => 'required|max:100',
            'user_uid' => 'max:100'
        ], HelperController::errorMessagesUsePromo());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        $requested['user_agent'] = request()->header('User-Agent');
        $requested['ip'] = request()->ip();
        
        $record = $this->recordUsePromoModel->getOneRecord($requested['unique_code'], $requested['promo_uid']);
        $voucher = $this->uniqueCodeModel->getOneUniqueCodeByUniqueCode($requested['unique_code'], $requested['user_uid']);
        $promo = $this->promoModel->verifyPromoExpiredDate($requested['promo_uid']);

        if(date('Y-m-d H:i:s') >= $voucher->expired_date) {
            return response()->json(['code' => 400, 'message' => 'Promo / Link Telah Expired']);
        }

        if(date('Y-m-d H:i:s') >= $promo->expired_date) {
            return response()->json(['code' => 400, 'message' => 'Promo / Link Telah Expired']);
        }

        if($record != null) {
            return response()->json(['code' => 400, 'message' => 'Kode voucher telah di gunakan']);
        }

        $remaining_click = $voucher->click_limit - 1;

        if($remaining_click < 0) {
            return response()->json(['code' => 400, 'message' => 'Melebihi jumlah penggunaan']);
        }

        $result = $this->uniqueCodeModel->postUpdateRemainingClick($requested['unique_code'], $requested['user_uid'], $remaining_click);

        $this->recordUsePromoModel->postAddRecordUsePromo($requested);
        
        return response()->json(['code' => 200, 'message' => $result, 'remaining_click' => $remaining_click]);
    }

    public function postDirectUsePromoInternal()
    {
        $validator = Validator::make(request()->all(), [
            'promo_uid' => 'required|max:100',
            'unique_code' => 'required|max:100',
            'user_uid' => 'max:100'
        ], HelperController::errorMessagesUsePromo());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        $requested['user_agent'] = request()->header('User-Agent');
        $requested['promo_type'] = 'internal';
        $requested['ip'] = request()->ip();
        
        $record = $this->recordUsePromoModel->getOneRecord($requested['unique_code'], $requested['promo_uid']);
        $voucher = $this->uniqueCodeModel->getOneUniqueCodeByUniqueCode($requested['unique_code'], $requested['user_uid']);
        $promo_internal = $this->promoInternalModel->verifyPromoInternalExpiredDate($requested['promo_uid']);

        if(date('Y-m-d H:i:s') >= $voucher->expired_date) {
            return response()->json(['code' => 400, 'message' => 'Promo / Link Telah Expired']);
        }

        if(date('Y-m-d H:i:s') >= $promo_internal->expired_date) {
            return response()->json(['code' => 400, 'message' => 'Promo / Link Telah Expired']);
        }

        if($record != null) {
            return response()->json(['code' => 400, 'message' => 'Kode voucher telah di gunakan']);
        }

        $remaining_click = $voucher->click_limit - 1;

        if($remaining_click < 0) {
            return response()->json(['code' => 400, 'message' => 'Melebihi jumlah penggunaan']);
        }

        $result = $this->uniqueCodeModel->postUpdateRemainingClick($requested['unique_code'], $requested['user_uid'], $remaining_click);

        $this->recordUsePromoModel->postAddRecordUsePromo($requested);
        
        return response()->json(['code' => 200, 'message' => $result, 'remaining_click' => $remaining_click]);
    }

    public function getRedirectMain()
    {
        return redirect()->route('getPartnerLogin');
    }

    public function getOneMembership($membership_uid, $user_uid)
    {
        $member = $this->membershipModel->getOneMembershipByUID($membership_uid);
        $partner = $this->userModel->getOnePartnerUserByUID($user_uid);
        $logo = $this->partnerLogoModel->getOnePartnerLogo($user_uid);

        return view('public/member', ['member' => $member, 'partner' => $partner, 'logo' => $logo]);
    }

    public function getOneMembershipPathB($noc = '', $address = '', $kota = '', $phone = '', $user_uid = '', $membership_uid = '')
    {
        $member = $this->membershipModel->getOneMembershipByUID($membership_uid);
        $logo = $this->partnerLogoModel->getOnePartnerLogo($user_uid);

        return view('public/member-b', ['member' => $member, 'logo' => $logo, 'noc' => $noc, 'address' => $address, 'kota' => $kota, 'phone' => $phone]);
    }

    public function getMainPage()
    {
        return view('public/main');
    }

    public function postVerifyUniqueCode()
    {
        $validator = Validator::make(request()->all(), [
            'unique_code' => 'required|max:100'
        ], HelperController::errorMessagesVerifyUniqueCode());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        $voucher = $this->uniqueCodeModel->verifyUniqueCode($requested['unique_code']);

        if($voucher == null) {
            return response()->json(['code' => 400, 'message' => 'Kode voucher tidak valid']);
        }

        if(date('Y-m-d H:i:s') >= $voucher->expired_date) {
            return response()->json(['code' => 400, 'message' => 'Kode voucher telah expired']);
        }

        return response()->json(['code' => 200, 'message' => 'Data berhasil di proses']);
    }

    public function getAllPromo($uc = '')
    {
        $voucher = $this->uniqueCodeModel->getOneUniqueCodeByUniqueCodeOnly($uc);
        $link_title = '';

        if($voucher == null) {
            return redirect()->route('getMainPage')->with('err', 'Kode voucher tidak valid');
        }

        if(date('Y-m-d H:i:s') >= $voucher->expired_date) {
            return view('error/promo-expired');
        }

        $promos = $this->partnerPromoModel->getForPublicSelectedPromo($voucher->user_uid);
        $promos_internal = $this->promoInternalModel->getForPublicPromoInternal($voucher->user_uid);

        foreach ($promos as $promo) {
            $promo->is_record_exist = 'no';
            
            $record = $this->recordUsePromoModel->getOneRecord($uc, $promo->uid);

            if($record != null) {
                $promo->is_record_exist = 'yes';
            }
        }

        foreach ($promos_internal as $promo_internal) {
            $promo_internal->is_record_exist = 'no';
            
            $record = $this->recordUsePromoModel->getOneRecord($uc, $promo_internal->uid);

            if($record != null) {
                $promo_internal->is_record_exist = 'yes';
            }
        }

        // $promos = $this->promoModel->getAllPromo();

        $result_link_title = $this->linkTitleModel->getOneLinkTitle($voucher->user_uid);

        if($result_link_title != null) {
            $link_title = $result_link_title->description;
        }

        return view('public/all-promo', ['uc' => $uc, 'promos' => $promos, 'promos_internal' => $promos_internal, 'voucher' => $voucher, 'link_title' => $link_title]);
    }

    public function getAllPromoByWa($uc = '', $noc = '')
    {
        // $promos = $this->promoModel->getAllPromo();
        $voucher = $this->uniqueCodeModel->getOneUniqueCodeByUniqueCodeOnly($uc);
        $link_title = $noc;

        if($voucher == null) {
            return redirect()->route('getMainPage')->with('err', 'Kode voucher tidak valid');
        }

        if(date('Y-m-d H:i:s') >= $voucher->expired_date) {
            return view('error/promo-expired');
        }

        $promos = $this->partnerPromoModel->getForPublicSelectedPromo($voucher->user_uid);
        $promos_internal = $this->promoInternalModel->getForPublicPromoInternal($voucher->user_uid);

        foreach ($promos as $promo) {
            $promo->is_record_exist = 'no';
            
            $record = $this->recordUsePromoModel->getOneRecord($uc, $promo->uid);

            if($record != null) {
                $promo->is_record_exist = 'yes';
            }
        }

        foreach ($promos_internal as $promo_internal) {
            $promo_internal->is_record_exist = 'no';
            
            $record = $this->recordUsePromoModel->getOneRecord($uc, $promo_internal->uid);

            if($record != null) {
                $promo_internal->is_record_exist = 'yes';
            }
        }
        
        return view('public/all-promo', ['uc' => $uc, 'promos' => $promos, 'promos_internal' => $promos_internal, 'voucher' => $voucher, 'link_title' => $link_title]);
    }

    public function getAgentProfile($user_code = "")
    {
        $company_logo = "";
        $company_link_title = "";
        $img_path = "";
        $company_title = "";
        $res_atasan_user = "";
        $logo_untuk_bawahan = "";
        $link_title = "";
        $user_balance_standart = 0;

        $res_user_code = $this->userCodeModel->getOneUserCodeByCode($user_code);

        if($res_user_code == null) {
            return redirect()->route('getAgentLogin');;
        }

        $res_user = $this->userModel->getOneAgentByUID($res_user_code->user_uid);
        $result_user_company = $this->userCompanyModel->getOneCompanyByUserUID($res_user_code->user_uid);
        
        $result_user_balance_standart = $this->userBalanceModel->getCurrentUserBalance($res_user_code->user_uid, "standart");

        if($result_user_balance_standart != null) {
            $user_balance_standart = $result_user_balance_standart->balance;
        }
        
        if($result_user_company != null) {
            $img_path = $result_user_company->img;
            $company_title = $result_user_company->title;
            $res_atasan_user = $this->userCodeModel->getOneUserCodeAndUserData($result_user_company->referral_code);
        }

        if($res_atasan_user != null) {
            $logo_untuk_bawahan = $this->partnerLogoModel->getOnePartnerLogoUntukBawahan($res_atasan_user->user_uid);
            $link_title = $this->linkTitleModel->getOneLinkTitle($res_atasan_user->user_uid);
        }

        $res_logo = $this->partnerLogoModel->getOnePartnerLogo($res_user_code->user_uid);
        $res_company_link_title = $this->linkTitleModel->getOneLinkTitle($res_user_code->user_uid);
        
        if($res_logo != null) {
            $company_logo = $res_logo->img;
        }

        if($res_company_link_title != null) {
            $company_link_title = $res_company_link_title->description;
        }

        $merchants = $this->merchantModel->getAllMerchant();
        
        return view('publicv2/user-profile', ['user' => $res_user, 'user_code_data' => $res_user_code, 'user_code' => $res_user_code->code, 'img_path' => $img_path, 'company_title' => $company_title, 'logo_untuk_bawahan' => $logo_untuk_bawahan, 'res_atasan_user' => $res_atasan_user, 'link_title' => $link_title, 'company_logo' => $company_logo, 'user_balance_standart' => $user_balance_standart, 'company_link_title' => $company_link_title, 'merchants' => $merchants]);
    }

    public function getClaimGiftViaBank($user_code = "")
    {
        $mod_user_code = "";
        $user_uid = "";
        $total_claim = 0;

        $result_user_code = $this->userCodeModel->getOneUserCodeByCode($user_code);
        
        if($result_user_code != null) {
            $user_uid = $result_user_code->user_uid;
            $mod_user_code = HelperController::splitUserCode($result_user_code->code);
        }

        $current_user = $this->userModel->getOneAgentByUID($user_uid);

        if($current_user != null) {
            $user_uid = $current_user->uid;
        }

        if($this->claimGiftModel->where('user_uid', $user_uid)->count() != 0) {
            $total_claim = $this->claimGiftModel->where('user_uid', $user_uid)->where('redeemed', 0)->count();
        }

    	return view('public/claim-gift-via-bank', ['current_user' => $current_user, 'mod_user_code' => $mod_user_code, 'user_code' => $user_code, 'user_uid' => $user_uid, 'total_claim' => $total_claim]);
    }

    public function getClaimGift($user_code = "")
    {
        $mod_user_code = "";
        $user_uid = "";
        $total_claim = 0;

        $result_user_code = $this->userCodeModel->getOneUserCodeByCode($user_code);
        
        if($result_user_code != null) {
            $user_uid = $result_user_code->user_uid;
            $mod_user_code = HelperController::splitUserCode($result_user_code->code);
        }

        $current_user = $this->userModel->getOneAgentByUID($user_uid);

        if($current_user != null) {
            $user_uid = $current_user->uid;
        }

        if($this->claimGiftModel->where('user_uid', $user_uid)->count() != 0) {
            $total_claim = $this->claimGiftModel->where('user_uid', $user_uid)->where('redeemed', 0)->count();
        }

    	return view('public/claim-gift', ['current_user' => $current_user, 'mod_user_code' => $mod_user_code, 'user_code' => $user_code, 'user_uid' => $user_uid, 'total_claim' => $total_claim]);
    }

    public function postClaimGift()
    {
        $validator = Validator::make(request()->all(), [
            'merchant_uid' => 'required|max:100',
            'user_uid' => 'required|max:100',
            'nominal' => 'required|numeric|digits_between:1,9',
            'no_struk' => 'required|max:20',
            'no_reg' => 'required|max:20',
            'img_struk' => 'required|image|max:20240',
        ], HelperController::errorMessagesClaimGift());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        $amount = 0;
        
        $res_user = $this->userModel->getOneAgentByUID($requested['user_uid']);
        $res_merchant = $this->merchantModel->getOneMerchantByUID($requested['merchant_uid']);

        if($res_user == null) {
            return response()->json(['code' => 400, 'message' => 'Data agent tidak di temukan']);
        }

        if($res_merchant == null) {
            return response()->json(['code' => 400, 'message' => 'Data merchant tidak di temukan']);
        }

        if($res_merchant->rate != null || $res_merchant->rate != 0 || $res_merchant->rate != "") {
            $amount = $requested['nominal'] * ($res_merchant->rate / 100);
        }
        
        $requested['user_agent'] = request()->header('User-Agent');
        $requested['ip'] = request()->ip();
        $requested['amount'] = floor($amount);
        $requested['img_struk_path'] = ImageController::createImage($requested['img_struk'], $this->img_struk_dir, '');
        
        $result = $this->claimGiftModel->postAddClaimGift($requested);

        $this->userBalanceModel->postUpdateUserBalance($requested['user_uid'], floor($amount), $type = "standart");

        UserBalanceLogModel::create([
            'uid' => HelperController::uid('user_balance_logs'),
            'from_user_uid' => $requested['user_uid'],
            'to_user_uid' => "",
            'amount' => floor($amount),
            'rate' => $res_merchant->rate / 100,
            'type' => "self",
            'delete' => 0,
        ]);

        // $this->getParentAgent($requested['user_uid'], $amount);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postRedeemClaimGift()
    {
        $validator = Validator::make(request()->all(), [
            'user_uid' => 'required|max:100',
            'jumlah_redeem' => 'required|numeric|digits_between:1,9'
        ], HelperController::errorMessagesClaimGift());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        $user_uid = $requested['user_uid'];
        $request_redeem = $requested['jumlah_redeem'];
        $amount = 0;
        $total_claim = 0;
        $array_claim_gifts_uid = [];

        $total_claim = $this->claimGiftModel->where('user_uid', $user_uid)->where('redeemed', 0)->count();

        if($request_redeem < 1) {
            return response()->json(['code' => 400, 'message' => 'Jumlah redeem tidak boleh 0']);
        }

        if($request_redeem > $total_claim) {
            return response()->json(['code' => 400, 'message' => 'Jumlah redeem melebihi jumlah struk']);
        }

        $claim_gifts_uid = $this->claimGiftModel->where('user_uid', $user_uid)->where('redeemed', 0)->limit($request_redeem)->get();

        foreach ($claim_gifts_uid as $claim_gift_uid) {
            array_push($array_claim_gifts_uid, $claim_gift_uid->uid);
        }
        
        ClaimGiftModel::whereIn('uid', $array_claim_gifts_uid)->update(['redeemed' => 1]);

        $total_claim = $total_claim - $request_redeem;

        return response()->json(['code' => 200, 'message' => 'Redeem berhasil', 'total_claim' => $total_claim]);
    }

    private function getParentAgent($user_uid, $amount) {
        $referralCode = "";
        $parentUserUID = $user_uid;
        $userType = "standart";

        $referralParent = $this->userCompanyModel->getOneReferralCodeByUserUID($parentUserUID);

        if($referralParent->referral_code != null || $referralParent->referral_code != "") {
            $referralCode = $referralParent->referral_code;
        }
        
        if(substr($referralCode, 0, 3) != "AAI") {
            $amount = $amount * 0.1;

            $referralParentUser = $this->userCodeModel->getOneUserCodeByCodeForRecursive($referralCode);
        
            if($referralParentUser->user_uid != null || $referralParentUser->user_uid != "") {
                $parentUserUID = $referralParentUser->user_uid;
                
                // if($referralParentUser->type == "toko") {
                //     $userType = "toko";
                    
                //     UserBalanceLogModel::create([
                //         'uid' => HelperController::uid('user_balance_logs'),
                //         'from_user_uid' => $parentUserUID,
                //         'to_user_uid' => $user_uid,
                //         'amount' => floor($amount),
                //         'rate' => 10,
                //         'type' => "toko",
                //         'delete' => 0,
                //     ]);

                //     $this->userBalanceModel->postUpdateUserBalance($user_uid, floor($amount), $userType);
                // }
                 
                if($referralParentUser->type == "perusahaan") {
                    UserBalanceLogModel::create([
                        'uid' => HelperController::uid('user_balance_logs'),
                        'from_user_uid' => $user_uid,
                        'to_user_uid' => $parentUserUID,
                        'amount' => floor($amount),
                        'rate' => 10,
                        'type' => "perusahaan",
                        'delete' => 0,
                    ]);

                    $this->userBalanceModel->postUpdateUserBalance($parentUserUID, floor($amount), $userType);
                } else if($referralParentUser->type == "standart") {
                    UserBalanceLogModel::create([
                        'uid' => HelperController::uid('user_balance_logs'),
                        'from_user_uid' => $user_uid,
                        'to_user_uid' => $parentUserUID,
                        'amount' => floor($amount),
                        'rate' => 10,
                        'type' => "standart",
                        'delete' => 0,
                    ]);

                    $this->userBalanceModel->postUpdateUserBalance($parentUserUID, floor($amount), $userType);
                } else if($referralParentUser->type == "user_only") {
                    UserBalanceLogModel::create([
                        'uid' => HelperController::uid('user_balance_logs'),
                        'from_user_uid' => $user_uid,
                        'to_user_uid' => $parentUserUID,
                        'amount' => floor($amount),
                        'rate' => 10,
                        'type' => "user_only",
                        'delete' => 0,
                    ]);

                    $this->userBalanceModel->postUpdateUserBalance($parentUserUID, floor($amount), $userType);
                }
            }
            
            if(substr($referralCode, 0, 3) == "AAI") {
                return "";
            } else {
                return $this->getParentAgent($parentUserUID, $amount);
            }
        }
    }
}
