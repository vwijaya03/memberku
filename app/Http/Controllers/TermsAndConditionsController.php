<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;

class TermsAndConditionsController extends Controller
{
    public function getTermsAndConditions()
    {
        $destinationUrl = url('/')."/terms-and-conditions/terms.pdf";
        
        return Redirect::to($destinationUrl);
    }
}
