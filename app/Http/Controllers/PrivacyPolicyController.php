<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;

class PrivacyPolicyController extends Controller
{
    public function getPrivacyPolicy()
    {
        $destinationUrl = url('/')."/privacy-policy/privacy.pdf";
        
        return Redirect::to($destinationUrl);
    }
}
