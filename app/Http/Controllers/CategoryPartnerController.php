<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\CategoryPartnerModel;
use Auth, Hash, DB, Log, Validator;

class CategoryPartnerController extends Controller
{
    public function __construct(CategoryPartnerModel $categoryPartnerModel)
    {
        $this->categoryPartnerModel = $categoryPartnerModel;
        $this->page_title = 'Kategori Partner';
    }

    public function getCategoryPartner()
    {
        return view('admin/category-partner', ['current_user' => Auth::user(), 'page_title' => $this->page_title]);
    }

    public function postAjaxCategoryPartner(Request $request)
    {
        $data = array();

        $columns = HelperController::getCategoryPartnerColumns();
  
        $totalData = $this->categoryPartnerModel->countAllActiveCategoryPartner();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $category_partners = $this->categoryPartnerModel->getCategoryPartner($start, $limit, $order, $dir);
        }
        else 
        {
            $category_partners = $this->categoryPartnerModel->getFilteredCategoryPartner($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->categoryPartnerModel->countAllFilteredActiveCategoryPartner($search);
        }

        if(!empty($category_partners))
        {
            foreach ($category_partners as $category_partner)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['res'] = $category_partner;
                $nestedData['action_btn'] = "
                    <button onclick='editCategoryPartner(".$category_partner.")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-target='#edit-category-partner'><i class='ft-edit'></i></button>
                    <button onclick='deleteCategoryPartner(".$category_partner.")' type='button' class='btn btn-danger mr-1 mb-1' data-toggle='modal' data-target='#delete-category-partner'><i class='ft-trash-2'></i></button>
                ";
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function postAddCategoryPartner()
    {
        $validator = Validator::make(request()->all(), [
            'name' => 'required'
        ], HelperController::errorMessagesCategoryPartner());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

    	$result = $this->categoryPartnerModel->postAddCategoryPartner($requested);
    	
        return response()->json(['code' => 200, 'message' => $result[1]]);
    }

    public function postEditCategoryPartner()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|numeric',
            'name' => 'required',
        ], HelperController::errorMessagesCategoryPartner());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

    	$requested = request();

    	$result = $this->categoryPartnerModel->postEditCategoryPartner($requested, $requested['id']);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postDeleteCategoryPartner()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|numeric',
        ], HelperController::errorMessagesCategoryPartner());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->categoryPartnerModel->postDeleteCategoryPartner($requested['id']);

        return response()->json(['code' => 200, 'message' => $result]);
    }
}
