<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\VoucherExpiredModel;
use Auth, Hash, DB, Log, Validator;

class VoucherExpiredController extends Controller
{
    public function __construct(VoucherExpiredModel $voucherExpiredModel)
    {
        $this->voucherExpiredModel = $voucherExpiredModel;
        $this->page_title = 'Promo Expired Date';
    }

    public function getVoucherExpiredDate()
    {
        $search_partner_url = url('/admin-access/search-partner');

        return view('admin/voucher-expired-date', ['current_user' => Auth::user(), 'page_title' => $this->page_title, 'search_partner_url' => $search_partner_url]);
    }

    public function postAjaxVoucherExpiredDate(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            'user_uid' => 'required|max:100'
        ], HelperController::errorMessagesSettingPromoPartner());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

        $data = array();

        $columns = HelperController::getVoucherExpiredDateColumns();
  
        $totalData = $this->voucherExpiredModel->countAllActiveVoucherExpired($requested['user_uid']);
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $voucherExpiredDates = $this->voucherExpiredModel->getVoucherExpired($start, $limit, $order, $dir, $requested['user_uid']);
        }
        else 
        {
            $voucherExpiredDates = $this->voucherExpiredModel->getFilteredVoucherExpired($search, $start, $limit, $order, $dir, $requested['user_uid']);
            $totalFiltered = $this->voucherExpiredModel->countAllFilteredActiveVoucherExpired($search, $requested['user_uid']);
        }

        if(!empty($voucherExpiredDates))
        {
            foreach ($voucherExpiredDates as $voucherExpiredDate)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['res'] = $voucherExpiredDate;
                $nestedData['action_btn'] = "
                    <button onclick='editVoucherExpiredDate(".$voucherExpiredDate.")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-target='#edit-voucher-expired-date'><i class='ft-edit'></i></button>
                    <button onclick='deleteVoucherExpiredDate(".$voucherExpiredDate.")' type='button' class='btn btn-danger mr-1 mb-1' data-toggle='modal' data-target='#delete-voucher-expired-date'><i class='ft-trash-2'></i></button>
                ";
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function postAddVoucherExpiredDate()
    {
        $validator = Validator::make(request()->all(), [
            'user_uid' => 'required|max:100',
            'description' => 'required',
            'days' => 'required|numeric|digits_between:1,5',
            'default' => ''
        ], HelperController::errorMessagesVoucherExpired());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        $requested['default'] = ($requested['default'] == 'on') ? 'yes' : 'no';

    	$result = $this->voucherExpiredModel->postAddVoucherExpired($requested);
    	
        return response()->json(['code' => 200, 'message' => $result[1]]);
    }

    public function postEditVoucherExpiredDate()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|numeric',
            'user_uid' => 'required|max:100',
            'description' => 'required',
            'days' => 'required|numeric|digits_between:1,5',
            'default' => ''
        ], HelperController::errorMessagesVoucherExpired());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

    	$requested = request();
        $requested['default'] = ($requested['default'] == 'on') ? 'yes' : 'no';

    	$result = $this->voucherExpiredModel->postEditVoucherExpired($requested, $requested['id']);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postDeleteVoucherExpiredDate()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|numeric',
        ], HelperController::errorMessagesBooking());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->voucherExpiredModel->postDeleteVoucherExpired($requested['id']);

        return response()->json(['code' => 200, 'message' => $result]);
    }
}
