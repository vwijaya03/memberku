<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\ImageManager;
use App\Http\Controllers\HelperController;
use App\Http\Controllers\ImageController;
use App\PromoInternalModel;
use Auth, Hash, DB, Log, Validator;

class PromoInternalPartnerController extends Controller
{
    public function __construct(PromoInternalModel $promoInternalModel)
    {
        $this->promoInternalModel = $promoInternalModel;
    }

    public function postAjaxPromoInternal(Request $request, $user_uid)
    {
        $data = array();

        $columns = HelperController::getPromoInternalColumns();
  
        $totalData = $this->promoInternalModel->countAllActivePromoInternalPartner($user_uid);
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $promos = $this->promoInternalModel->getPromoInternalPartner($start, $limit, $order, $dir, $user_uid);
        }
        else 
        {
            $promos = $this->promoInternalModel->getFilteredPromoInternalPartner($search, $start, $limit, $order, $dir, $user_uid);
            $totalFiltered = $this->promoInternalModel->countAllFilteredActivePromoInternalPartner($search, $user_uid);
        }

        if(!empty($promos))
        {
            foreach ($promos as $promo)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['res'] = $promo;
                $nestedData['name'] = urldecode($promo->name);
                $nestedData['title'] = urldecode($promo->fullname).' - '.urldecode($promo->title);
                $nestedData['expired_date'] = date('d F Y', strtotime($promo->expired_date));
                $nestedData['img'] = "<a href='".url('/').$promo->img."' target='_blank'>Klik di sini</a>";
                $nestedData['description'] = urldecode(strip_tags(substr($promo->description, 0, 35))."...");
                $nestedData['action_btn'] = "
                	<button onclick='detailPromoInternalPartner(".$promo.")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-target='#data-benefit'><i class='ft-info'></i></button>
                ";
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }
}
