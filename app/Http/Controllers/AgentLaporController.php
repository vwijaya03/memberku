<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use Auth, Hash, DB, Log, Validator;

class AgentLaporController extends Controller
{
    public function __construct()
    {
        $this->page_title = 'Setting Kirim Benefit Partner';
    }

    public function getClaimBonus()
    {
    	return view('agent/lapor');
    }
}
