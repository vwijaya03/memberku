<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\GreetingTextModel;
use App\LinkTitleModel;
use Auth, Hash, DB, Log, Validator;

class GreetingTextController extends Controller
{
    public function __construct(GreetingTextModel $greetingTextModel, LinkTitleModel $linkTitleModel)
    {
        $this->greetingTextModel = $greetingTextModel;
    	$this->linkTitleModel = $linkTitleModel;
    }

    public function getGreetingText() 
    {
    	$text = '';
    	$result = $this->greetingTextModel->getOneGreetingText(Auth::user()->uid);

    	if($result != null) {
    		$text = $result->description;
    	}

    	return response()->json(['code' => 200, 'text' => $text]);
    }

    public function postAddGreetingText() 
    {
    	$validator = Validator::make(request()->all(), [
            'description' => 'max:500'
        ], HelperController::errorMessagesGreetingText());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $user_uid = Auth::user()->uid;

        $requested = request();
        $requested['user_uid'] = $user_uid;

        $result = $this->greetingTextModel->getOneGreetingText($user_uid);

        $msg = '';

    	if($result != null) {
    		$result = $this->greetingTextModel->postEditGreetingText($requested, $user_uid);
    		$msg = $result;
    	} else {
    		$result = $this->greetingTextModel->postAddGreetingText($requested);
    		$msg = $result[1];
    	}

        return response()->json(['code' => 200, 'message' => $msg]);
    }

    public function getLinkTitle() 
    {
        $text = '';
        $result = $this->linkTitleModel->getOneLinkTitle(Auth::user()->uid);

        if($result != null) {
            $text = $result->description;
        }

        return response()->json(['code' => 200, 'text' => $text]);
    }

    public function postAddLinkTitle() 
    {
        $validator = Validator::make(request()->all(), [
            'description' => 'max:50'
        ], HelperController::errorMessagesGreetingText());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $user_uid = Auth::user()->uid;

        $requested = request();
        $requested['user_uid'] = $user_uid;

        $result = $this->linkTitleModel->getOneLinkTitleForReview($user_uid);

        $msg = '';
        
        if($result != null) {
            $result = $this->linkTitleModel->postEditLinkTitle($requested, $user_uid);
            $msg = $result;
        } else {
            $result = $this->linkTitleModel->postAddLinkTitle($requested);
            $msg = $result[1];
        }

        return response()->json(['code' => 200, 'message' => $msg]);
    }
}
