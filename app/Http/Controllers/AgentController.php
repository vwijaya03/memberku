<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Http\Controllers\HelperController;
use App\UserModel;
use App\CompanyModel;
use App\UserCompanyModel;
use Auth, Hash, DB, Log, Validator;

class AgentController extends Controller
{
    public function __construct(UserModel $userModel, CompanyModel $companyModel, UserCompanyModel $userCompanyModel)
    {
        $this->userModel = $userModel;
        $this->companyModel = $companyModel;
        $this->userCompanyModel = $userCompanyModel;
        $this->page_title = 'Agent';
    }

    public function getAgent()
    {
        return view('admin/agent', ['current_user' => Auth::user(), 'page_title' => $this->page_title]);
    }

    public function postAjaxAgent(Request $request)
    {
        $data = array();

        $columns = HelperController::getAgentColumns();
  
        $totalData = $this->userModel->countAllActiveAgent();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $agents = $this->userModel->getAgent($start, $limit, $order, $dir);
        }
        else 
        {
            $agents = $this->userModel->getFilteredAgent($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->userModel->countAllFilteredActiveAgent($search);
        }

        if(!empty($agents))
        {
            foreach ($agents as $agent)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['fullname'] = urldecode($agent->fullname);
                $nestedData['email'] = urldecode($agent->email);
                $nestedData['res'] = $agent;
                $nestedData['action_btn'] = "
                    <button onclick='editLogoAgent(".$agent.")' class='btn btn-secondary mr-1 mb-1 logo-btn'><i class='ft-image'></i></button>
                    <button onclick='editAgent(".$agent.")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-target='#edit-agent'><i class='ft-edit'></i></button>
                    <button onclick='deleteAgent(".$agent.")' type='button' class='btn btn-danger mr-1 mb-1' data-toggle='modal' data-target='#delete-agent'><i class='ft-trash-2'></i></button>
                ";
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function postAddAgent()
    {
        $validator = Validator::make(request()->all(), [
            'fullname' => 'required|max:50',
            'phone' => 'required|max:20',
            'password' => 'max:100',
            'type' => 'required|max:100',
            'referral_code' => 'max:35'
        ], HelperController::errorMessagesAgent());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        $requested['role'] = "agent";

        if($requested['email'] == null || $requested['email'] == "") {
            $requested['email'] = str_replace(" ", "_", $requested['fullname'])."_".$requested['phone']."@mail.com";
        }
        
        if($requested['password'] == null || $requested['password'] == "") {
            $requested['password'] = '123456';
        }

        if(substr($requested['phone'], 0, 3) == "+62") {
            $requested['phone'] = "0".substr($requested['phone'], 3);
        } else if(substr($requested['phone'], 0, 2) == "62") {
            $requested['phone'] = "0".substr($requested['phone'], 2);
        }

        $requested['phone'] = preg_replace('/[^\w\d]+/', '', $requested['phone']);

        $exist_phone_number = $this->userModel->getOneAgentByPhone($requested['phone']);

        if($exist_phone_number || $exist_phone_number != null) {
            return response()->json(['code' => 400, 'message' => "Nomor HP telah digunakan"]);
        }

        if($requested['role'] ==  "agent") {
            $requested['company_uid'] = "";

            if($requested['referral_code'] != null || $requested['referral_code'] != "") {
                $res_company = $this->companyModel->getOneCompany($requested['referral_code']);

                if($res_company == null) {
                    return response()->json(['code' => 400, 'message' => "Data perusahaan tidak di temukan"]);
                } else {
                    $requested['company_uid'] = $res_company->uid;
                }
            } else {
                $requested['company_uid'] = "";
            }
        }

        $result = $this->userModel->postAddUser($requested);
        
        return response()->json(['code' => 200, 'message' => $result[1]]);
    }
    
    public function postGenerateCode()
    {
        $requested = request()->validate([
    		'total_code' => 'required',
        ]);
        
        $faker = \Faker\Factory::create();
        $sheetArray = array();
        $filename = "data-".date('Ymd-H:i:s');

        for ($i=0; $i < $requested['total_code']; $i++) { 
            $requested['role'] = "agent";
            $requested['fullname'] = $faker->name." ".$faker->randomNumber($nbDigits = 7, $strict = true);
            $requested['name_on_card'] = "";
            $requested['email'] = $faker->randomNumber($nbDigits = 7, $strict = true)."@mail.com";
            $requested['password'] = '123456';
            $requested['phone'] = $faker->randomNumber($nbDigits = 7, $strict = true);
            $requested['city'] = "";
            $requested['address'] = "";
            $requested['type'] = "user_only";
            $requested['role'] = "agent";
            $requested['approve'] = "1";
            $requested['from_bank'] = "yes";

            if($requested['role'] ==  "agent") {
                $requested['company_uid'] = "";
            }

            $result = $this->userModel->postAddUser($requested);

            $sheetArray[] = array('No' => $i+1, 'Kode' => $result[2]);
        }
        
        return Excel::create($filename, function($excel) use($sheetArray) {

            $excel->sheet('sheet1', function($sheet) use($sheetArray) {
        
                $sheet->fromArray($sheetArray);
        
            });
        
        })->export('xlsx');
    }

    // public function postGenerateCode()
    // {
    //     $requested = request()->validate([
    // 		'total_code' => 'required',
    //     ]);
        
    //     $faker = \Faker\Factory::create();
    //     $code = "3".$faker->randomNumber($nbDigits = 4, $strict = true);

    //     $sheetArray = array();

    //     for ($i=0; $i < $requested['total_code']; $i++) { 
    //         $requested['role'] = "agent";
    //         $requested['fullname'] = $faker->name." ".$faker->randomNumber($nbDigits = 7, $strict = true);
    //         $requested['name_on_card'] = "";
    //         $requested['email'] = $faker->randomNumber($nbDigits = 7, $strict = true)."@mail.com";
    //         $requested['password'] = '123456';
    //         $requested['phone'] = $faker->randomNumber($nbDigits = 7, $strict = true);
    //         $requested['city'] = "";
    //         $requested['address'] = "";
    //         $requested['type'] = "user_only";
    //         $requested['role'] = "agent";
    //         $requested['approve'] = "1";
    //         $requested['from_bank'] = "yes";

    //         if($requested['role'] ==  "agent") {
    //             $requested['company_uid'] = "";
    //         }

    //         $result = $this->userModel->postAddUser($requested);

    //         $sheetArray[] = array('No' => $i+1, 'Kode' => $result[2]);
    //     }
        
    //     return Excel::create('data', function($excel) use($sheetArray) {
    //         $excel->sheet('sheet1', function($sheet) use($sheetArray) {
    //             $sheet->fromArray($sheetArray);
    //         });
    //     })->export('xls');
    // }

    public function postEditAgent()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|max:50',
            'email' => 'max:50|unique:users,email,'.request()['id'],
            'fullname' => 'required|max:50',
            'phone' => 'required|max:20',
            'password' => 'max:100',
            'type' => 'required|max:100',
            'referral_code' => 'max:35'
        ], HelperController::errorMessagesAgent());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

    	$requested = request();
        $requested['role'] = "agent";

        if(substr($requested['phone'], 0, 3) == "+62") {
            $requested['phone'] = "0".substr($requested['phone'], 3);
        } else if(substr($requested['phone'], 0, 2) == "62") {
            $requested['phone'] = "0".substr($requested['phone'], 2);
        }

        $requested['phone'] = preg_replace('/[^\w\d]+/', '', $requested['phone']);

        $exist_phone_number = $this->userModel->getOneAgentByPhoneExceptSelf($requested['phone'], $requested['id']);

        if($exist_phone_number || $exist_phone_number != null) {
            return response()->json(['code' => 400, 'message' => "Nomor HP telah digunakan"]);
        }

    	$result = $this->userModel->postEditAgent($requested, $requested['id']);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postDeleteAgent()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|numeric',
        ], HelperController::errorMessagesAgent());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->userModel->postDeleteUser($requested['id']);

        return response()->json(['code' => 200, 'message' => $result]);
    }
}
