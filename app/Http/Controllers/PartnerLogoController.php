<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\Http\Controllers\ImageController;
use App\PartnerLogoModel;
use Auth, Hash, DB, Log, Validator;

class PartnerLogoController extends Controller
{
    public function __construct(PartnerLogoModel $partnerLogoModel)
    {
    	$this->partnerLogoModel = $partnerLogoModel;
        $this->dir = '/partner-logo/';
    }

    public function postAddPartnerLogo() 
    {
    	$validator = Validator::make(request()->all(), [
            'img' => 'required|image'
        ], HelperController::errorMessagesPartnerLogo());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $user_uid = Auth::user()->uid;

        $requested = request();
        $requested['user_uid'] = $user_uid;
        $requested['img_path'] = ImageController::createImage($requested['img'], $this->dir, '');

        $result = $this->partnerLogoModel->getOnePartnerLogo($user_uid);

        $msg = '';

    	if($result != null) {
            ImageController::deleteImage($result->img, "");
            $requested['img_path'] = ImageController::createImage($requested['img'], $this->dir, '');
    		$result = $this->partnerLogoModel->postEditPartnerLogo($requested, $user_uid);
    		$msg = $result;
    	} else {
    		$result = $this->partnerLogoModel->postAddPartnerLogo($requested);
    		$msg = $result[1];
    	}

        return response()->json(['code' => 200, 'message' => $msg]);
    }

    public function postAddPartnerLogoBySuperadmin() 
    {
    	$validator = Validator::make(request()->all(), [
            'img' => 'required|image',
            'agent_uid' => 'required|max:100'
        ], HelperController::errorMessagesPartnerLogo());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        $user_uid = $requested['agent_uid'];
        $requested['user_uid'] = $user_uid;
        $requested['img_path'] = ImageController::createImage($requested['img'], $this->dir, '');

        $result = $this->partnerLogoModel->getOneAgentLogo($user_uid);
        
        $msg = '';

    	if($result != null) {
            ImageController::deleteImage($result->img, "");
            $requested['img_path'] = ImageController::createImage($requested['img'], $this->dir, '');
    		$result = $this->partnerLogoModel->postEditPartnerLogoBySuperadmin($requested, $user_uid);
    		$msg = $result;
    	} else {
    		$result = $this->partnerLogoModel->postAddPartnerLogoBySuperadmin($requested);
    		$msg = $result[1];
    	}

        return response()->json(['code' => 200, 'message' => $msg]);
    }

    public function postDeletePartnerLogo()
    {
        $validator = Validator::make(request()->all(), [
            'user_uid' => 'required|max:100'
        ], HelperController::errorMessagesPartnerLogo());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->partnerLogoModel->postDeletePartnerLogo($requested['user_uid']);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function getOneLogo() 
    {
        $validator = Validator::make(request()->all(), [
            'agent_uid' => 'required|max:100'
        ], HelperController::errorMessagesPartnerLogo());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

        $resultLogo = $this->partnerLogoModel->getOneAgentLogo($requested['agent_uid']);

        return response()->json(['code' => 200, 'message' => 'OK', 'result' => $resultLogo, 'agent_uid' => $requested['agent_uid']]);
    }
}
