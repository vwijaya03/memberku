<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\UserModel;
use App\CompanyModel;
use App\UserCompanyModel;
use App\UserCodeModel;
use App\PartnerLogoModel;
use App\LinkTitleModel;
use Auth, Hash, DB, Log, Validator;

class PublicAgentRegistrationController extends Controller
{
    public function __construct(UserModel $userModel, CompanyModel $companyModel, UserCompanyModel $userCompanyModel, UserCodeModel $userCodeModel, PartnerLogoModel $partnerLogoModel, LinkTitleModel $linkTitleModel)
    {
        $this->userModel = $userModel;
        $this->companyModel = $companyModel;
        $this->userCompanyModel = $userCompanyModel;
        $this->userCodeModel = $userCodeModel;
        $this->partnerLogoModel = $partnerLogoModel;
        $this->linkTitleModel = $linkTitleModel;
        $this->page_title = 'Pendaftaran';
    }

    public function getRegistration($user_code = "") 
    {
        $company_logo = "";
        $link_title = "";
        $res_logo = null;
        $res_link_title = null;

        $result_user_code = $this->userCodeModel->getOneUserCodeByCode($user_code);

        if($result_user_code != null) {
            $res_logo = $this->partnerLogoModel->getOnePartnerLogo($result_user_code->user_uid);
            $res_link_title = $this->linkTitleModel->getOneLinkTitle($result_user_code->user_uid);
        }

        if($res_logo != null) {
            $company_logo = $res_logo->img;
        }

        if($res_link_title != null) {
            $link_title = $res_link_title->description;
        }

        return view('public/agent-registration', ['user_code' => $user_code, 'company_logo' => $company_logo, 'link_title' => $link_title]);
    }

    public function postRegistration()
    {
        $validator = Validator::make(request()->all(), [
            'fullname' => 'required|max:30',
            'phone' => 'required|max:20',
            'password' => 'required|max:30',
            'code' => 'required|max:100'
        ], HelperController::errorMessagesAgent());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $current_user = Auth::user();
        $requested = request();
        $requested['role'] = "agent";
        $requested['type'] = "standart";

        if($requested['email'] == null || $requested['email'] == "") {
            $requested['email'] = str_replace(" ", "_", $requested['fullname'])."_".$requested['phone']."@mail.com";
        }

        if($requested['password'] == null || $requested['password'] == "") {
            $requested['password'] = '123456';
        }

        if(substr($requested['phone'], 0, 3) == "+62") {
            $requested['phone'] = "0".substr($requested['phone'], 3);
        } else if(substr($requested['phone'], 0, 2) == "62") {
            $requested['phone'] = "0".substr($requested['phone'], 2);
        }

        $requested['phone'] = preg_replace('/[^\w\d]+/', '', $requested['phone']);

        $exist_phone_number = $this->userModel->getOneAgentByPhone($requested['phone']);

        if($exist_phone_number || $exist_phone_number != null) {
            return response()->json(['code' => 400, 'message' => "Nomor HP telah digunakan"]);
        }
        
        $res_user_code = $this->userCodeModel->getOneUserCodeByCode($requested['code']);
        
        if($res_user_code == null) {
            return response()->json(['code' => 400, 'message' => "Link pendaftaran salah"]);
        }

        $requested['company_uid'] = $res_user_code->code;

        Log::info($requested);

        $result = $this->userModel->postAddUser($requested);
        
        return response()->json(['code' => 200, 'message' => $result[1]]);
    }
}
