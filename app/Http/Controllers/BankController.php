<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\BankModel;
use Auth, Hash, DB, Log, Validator;

class BankController extends Controller
{
    public function __construct(BankModel $bankModel)
    {
        $this->bankModel = $bankModel;
        $this->page_title = 'Bank';
    }

    public function getBank()
    {
        return view('admin/bank', ['current_user' => Auth::user(), 'page_title' => $this->page_title]);
    }

    public function postAjaxBank(Request $request)
    {
        $data = array();

        $columns = HelperController::getBankColumns();
  
        $totalData = $this->bankModel->countAllActiveBank();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = urlencode($request->input('search.value')); 
        $number = 1;

        if(empty($search))
        {            
            $banks = $this->bankModel->getBank($start, $limit, $order, $dir);
        }
        else 
        {
            $banks = $this->bankModel->getFilteredBank($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->bankModel->countAllFilteredActiveBank($search);
        }

        if(!empty($banks))
        {
            foreach ($banks as $bank)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['res'] = $bank;
                $nestedData['name'] = urldecode($bank->name);
                $nestedData['type'] = $bank->type;
                $nestedData['action_btn'] = "
                    <button onclick='editBank(".$bank.")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-target='#edit-bank'><i class='ft-edit'></i></button>
                    <button onclick='deleteBank(".$bank.")' type='button' class='btn btn-danger mr-1 mb-1' data-toggle='modal' data-target='#delete-bank'><i class='ft-trash-2'></i></button>
                ";
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function getSearchBank(Request $request) 
    {
        $banks = $this->bankModel->getSearchSelect2Bank(urlencode($request->input('q')));

        return response()->json(['result' => $banks]);
    }

    public function postAddBank()
    {
        $validator = Validator::make(request()->all(), [
            'name' => 'required|max:60',
            'type' => 'required|max:20'
        ], HelperController::errorMessagesBank());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

    	$result = $this->bankModel->postAddBank($requested);
    	
        return response()->json(['code' => 200, 'message' => $result[1]]);
    }

    public function postEditBank()
    {
        $validator = Validator::make(request()->all(), [
            'uid' => 'required|max:100',
            'name' => 'required|max:60',
            'type' => 'required|max:20'
        ], HelperController::errorMessagesBank());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

    	$result = $this->bankModel->postEditBank($requested, $requested['uid']);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postDeleteBank()
    {
        $validator = Validator::make(request()->all(), [
            'uid' => 'required|max:100',
        ], HelperController::errorMessagesBank());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->bankModel->postDeleteBank($requested['uid']);

        return response()->json(['code' => 200, 'message' => $result]);
    }
}
