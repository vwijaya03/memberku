<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\UserModel;
use App\UserCodeModel;
use App\UserCompanyModel;
use App\UserBalanceModel;
use App\PartnerLogoModel;
use App\LinkTitleModel;
use App\Mail\ResetPasswordMail;
use Auth, Hash, DB, Log, Carbon, Validator;
use Mail;

class AgentLoginController extends Controller
{
    public function __construct(UserModel $userModel, UserCodeModel $userCodeModel, UserCompanyModel $userCompanyModel, PartnerLogoModel $partnerLogoModel, LinkTitleModel $linkTitleModel)
    {
        $this->userModel = $userModel;
        $this->userCodeModel = $userCodeModel;
        $this->userCompanyModel = $userCompanyModel;
        $this->partnerLogoModel = $partnerLogoModel;
        $this->linkTitleModel = $linkTitleModel;
    }

    public function getAgentLogin(Request $request)
    {
        if(Auth::check())
        {
            return redirect()->route('getAgentDashboard');
        }
                
        return view('agentv2/login');
    }

    public function postAgentLogin()
    {
        $requested = request()->validate([
    		'email' => 'required|max:100',
    		'password' => 'required|max:100'
    	]);
        
        if(Auth::attempt(['email' => $requested['email'], 'password' => $requested['password'], 'role' => 'agent', 'approve' => 1, 'delete' => 0]) || Auth::attempt(['phone' => $requested['email'], 'password' => $requested['password'], 'role' => 'agent', 'approve' => 1, 'delete' => 0]))
        {
            return redirect()->route('getAgentDashboard');
        }
        else if(!Auth::attempt(['email' => $requested['email'], 'password' => $requested['password'], 'role' => 'agent', 'approve' => 1, 'delete' => 0]))
        {
            return redirect()->route('getAgentLogin')->with(['err' => 'No. HP atau password salah.']);
        }
        else
        {
            return redirect()->route('getAgentLogin')->with(['err' => 'Akses ditolak.']);
        }
    }

    public function getAgentChangePassword()
    {
        $res_atasan_user = "";
        $logo_untuk_bawahan = "";
        $logo_toko_or_perusahaan = "";
        $link_title = "";

        $current_user = Auth::user();
        $result_user_company = $this->userCompanyModel->getOneCompanyByUserUID($current_user->uid);

        if($result_user_company != null) {
            //$img_path = $result_user_company->img;
            //$company_title = $result_user_company->title;
            $res_atasan_user = $this->userCodeModel->getOneUserCodeAndUserData($result_user_company->referral_code);
        }

        if($res_atasan_user != null) {
            $logo_untuk_bawahan = $this->partnerLogoModel->getOnePartnerLogoUntukBawahan($res_atasan_user->user_uid);
            $link_title = $this->linkTitleModel->getOneLinkTitle($res_atasan_user->user_uid);
        }

        return view('agent/change-password', ['current_user' => Auth::user(), 'logo_untuk_bawahan' => $logo_untuk_bawahan, 'res_atasan_user' => $res_atasan_user, 'link_title' => $link_title]);
    }

    public function postAgentChangePassword(Request $request)
    {
        if(Auth::user()->delete == "1" || Auth::user()->delete == 1)
        {   
            return redirect()->route('getPartnerLogin')->with(['err' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.']);
        }

        $current_password = $request->get('old_password');
        $new_password = $request->get('new_password');
        $email = Auth::user()->email;

        if(Hash::check($current_password, Auth::user()->password))
        {
            UserModel::where('email', $email)->where('delete', 0)
            ->update([
                'password' => Hash::make($new_password)
            ]);
            
            return redirect()->route('getAgentChangePassword')->with(['done' => 'Password berhasil di ubah.']);
        }
        else
        {
            return redirect()->route('getAgentChangePassword')->with(['err' => 'Password gagal di ubah.']);
        }
    }

    public function getAgentLogout()
    {
        Auth::logout();

        return redirect()->route('getAgentLogin');
    }

    public function postResetPassword(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            'email' => 'required|email|max:100'
        ], HelperController::errorMessagesClaimGift());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $faker = \Faker\Factory::create();
        $reset_password = $faker->randomNumber($nbDigits = 6, $strict = false);
        $msg = '';
        $status_json = '';

        $email = request()['email'];
        Log::info($email.' npass: '.$reset_password);

        $result = UserModel::select('id', 'email')->where('email', $email)->where('delete', 0)->first();

        if($result != null)
        {
            UserModel::where('email', $result->email)->where('delete', 0)
            ->update([
                'password' => Hash::make($reset_password)
            ]);

            $to_email = $result->email;
            // $to_email = 'viko_wijaya@yahoo.co.id';

            Mail::to($to_email)->send(new ResetPasswordMail($reset_password));
            $msg = 'Password Baru Anda Berhasil Di Kirim Ke Email.';

            //return redirect()->route('getPartnerResetPassword')->with(['done' => $msg]);
            return response()->json(['code' => 200, 'message' => $msg]);
        }
        else 
        {
            return response()->json(['code' => 400, 'message' => $msg]);
        }
    }
}
