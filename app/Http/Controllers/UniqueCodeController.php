<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\UniqueCodeModel;
use App\SettingModel;
use App\PromoModel;
use App\VoucherExpiredModel;
use App\GreetingTextModel;
use App\SettingUniqueCodePartnerModel;
use App\LinkTitleModel;
use App\TaskModel;
use Auth, Hash, DB, Log, Validator, PDF;

class UniqueCodeController extends Controller
{
    public function __construct(UniqueCodeModel $uniqueCodeModel, SettingModel $settingModel, PromoModel $promoModel, VoucherExpiredModel $voucherExpiredModel, GreetingTextModel $greetingTextModel, SettingUniqueCodePartnerModel $settingUniqueCodePartnerModel, LinkTitleModel $linkTitleModel, TaskModel $taskModel)
    {
        $this->uniqueCodeModel = $uniqueCodeModel;
        $this->settingModel = $settingModel;
        $this->promoModel = $promoModel;
        $this->voucherExpiredModel = $voucherExpiredModel;
        $this->greetingTextModel = $greetingTextModel;
        $this->settingUniqueCodePartnerModel = $settingUniqueCodePartnerModel;
        $this->linkTitleModel = $linkTitleModel;
        $this->taskModel = $taskModel;
        $this->page_title = 'Unique Code';
        $this->task_type = 'uc';
    }

    public function getPrintVoucherV1()
    {
        $url_post_print_voucher = url('/partner/print-voucher-v1');
        $title_jumlah_per_halaman = 'Ukuran kertas A4, Jumlah Voucher Per 1 Halaman Adalah 28';

        return view('partner/print-voucher', ['current_user' => Auth::user(), 'url_post_print_voucher' => $url_post_print_voucher, 'title_jumlah_per_halaman' => $title_jumlah_per_halaman]);
    }

    public function postPrintVoucherV1()
    {
        $validator = Validator::make(request()->all(), [
            'page' => 'required|max:2',
        ], HelperController::errorMessagesUniqueCode());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

        if($requested['page'] > 15) {
            return redirect()->route('getPrintVoucherV1')->with(['err' => 'Maximum 15 halaman' ]);
        }

        // $total_data = 16 * $requested['page']; // versi 2
        $total_data = 28 * $requested['page']; // versi 1
        $arr_unique_code = [];
        $auth_user = Auth::user();

        $result_task = $this->taskModel->getOneTask($auth_user->uid, $this->task_type);
        
        if($result_task != null) {
            if($result_task->status == 0) {
                return redirect()->route('getPrintVoucherV1')->with(['err' => 'Proses print voucher sebelum nya belum selesai' ]);
            }
        }

        $total_unique_code = $this->uniqueCodeModel->getCountUniqueCodeIsResetFalse($auth_user->uid);
        $setting_unique_code = $this->settingUniqueCodePartnerModel->getOne($auth_user->uid);
        $setting = $this->settingModel->getSetting();

        $limit_generate_unique_code = 0;
        $click_limit = 0;
        $check_sum_total_data = $total_data + $total_unique_code;

        if($setting_unique_code != null) {
            $limit_generate_unique_code = $setting_unique_code->max_total_uc;
        }

        if($limit_generate_unique_code == null) {
            $limit_generate_unique_code = 0;
        }
        
        if($setting != null) {
            $click_limit = $setting->voucher_click_limit;
        }

        $expired = $this->voucherExpiredModel->getDefault($auth_user->uid);
        $today = date('Y-m-d');
        $expired_date = '';

        if($expired == null) {
            $expired_date = date('Y-m-d 23:59:59', strtotime($today . "30 day") );
        } else {
            $str = $expired->days." day";
            $expired_date = date('Y-m-d 23:59:59', strtotime($today . $str) );
        }

        $judul_link = '';
        $result = $this->linkTitleModel->getOneLinkTitle($auth_user->uid);

        if($result != null) {
            $judul_link = $result->description;
        }

        // Log::info('total data '.$total_data);
        // Log::info('exist total data di db '.$total_unique_code);
        // Log::info('max limit '.$limit_generate_unique_code);
        // Log::info('max limit '.$limit_generate_unique_code.' - exist total data di db '.$total_unique_code);

        if($check_sum_total_data >= $limit_generate_unique_code) {
            $total_data = $limit_generate_unique_code - $total_unique_code;

            if($total_data < 1) {
                // return response()->json(['code' => 400, 'message' => 'Melebihi jumlah limit generate kode unik']);
                return redirect()->route('getPrintVoucherV1')->with(['err' => 'Melebihi jumlah limit print voucher' ]);
            }
        }

        // Log::info($total_data);
        // Log::info('');
        $task_param = [];
        $task_param['user_uid'] = $auth_user->uid;
        $task_param['type'] = $this->task_type;
        $task_param['status'] = 0;
        $task_param['description'] = '';

        $task = $this->taskModel->postAddTask($task_param);

        for($i = 0; $i < $total_data; $i++) {
            $arr_unique_code[] = [
                'uid' => HelperController::uid('unique_codes'),
                'user_uid' => $auth_user->uid,
                'unique_code' => HelperController::generateUniqueCode(),
                'is_send_email' => 0,
                'is_send_wa' => 1,
                'expired_date' => $expired_date,
                'click_limit' => $click_limit,
                'is_reset' => 'no',
                'delete' => 0
            ];
        }

        $this->uniqueCodeModel->postAddBulkUniqueCode($arr_unique_code);
        // Log::info($arr_unique_code[0]['unique_code']);

        $total_uc = $total_unique_code + $total_data;

        $this->settingUniqueCodePartnerModel->postEditSettingUniqueCodePartnerTotalUc($total_uc, $auth_user->uid);

        $total_row = ceil($total_data / 4);
        
        // versi 2

        // $pdf = PDF::loadView('export/pdf-unique-code-v2', ['unique_codes' => $arr_unique_code, 'total_row' => $total_row, 'total_data' => $total_data, 'judul_link' => $judul_link, 'task_id' => $task[0], 'current_user' => Auth::user()])->setPaper('a4', 'portrait');

        // versi 1

        $pdf = PDF::loadView('export/pdf-unique-code', ['unique_codes' => $arr_unique_code, 'total_row' => $total_row, 'total_data' => $total_data, 'judul_link' => $judul_link, 'task_id' => $task[0], 'current_user' => Auth::user()])->setPaper('a4', 'portrait');

        return $pdf->stream();
    }

    public function getPrintVoucherV2()
    {
        $url_post_print_voucher = url('/partner/print-voucher-v2');
        $title_jumlah_per_halaman = 'Ukuran kertas A4, Jumlah Voucher Per 1 Halaman Adalah 25';

        return view('partner/print-voucher', ['current_user' => Auth::user(), 'url_post_print_voucher' => $url_post_print_voucher, 'title_jumlah_per_halaman' => $title_jumlah_per_halaman]);
    }

    public function postPrintVoucherV2()
    {
        $validator = Validator::make(request()->all(), [
            'page' => 'required|max:2',
        ], HelperController::errorMessagesUniqueCode());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

        if($requested['page'] > 15) {
            return redirect()->route('getPrintVoucherV2')->with(['err' => 'Maximum 15 halaman' ]);
        }

        // $total_data = 16 * $requested['page']; // versi 2
        $total_data = 25 * $requested['page']; // versi 1
        $arr_unique_code = [];
        $auth_user = Auth::user();

        $result_task = $this->taskModel->getOneTask($auth_user->uid, $this->task_type);
        
        if($result_task != null) {
            if($result_task->status == 0) {
                return redirect()->route('getPrintVoucherV2')->with(['err' => 'Proses print voucher sebelum nya belum selesai' ]);
            }
        }

        $total_unique_code = $this->uniqueCodeModel->getCountUniqueCodeIsResetFalse($auth_user->uid);
        $setting_unique_code = $this->settingUniqueCodePartnerModel->getOne($auth_user->uid);
        $setting = $this->settingModel->getSetting();

        $limit_generate_unique_code = 0;
        $click_limit = 0;
        $check_sum_total_data = $total_data + $total_unique_code;

        if($setting_unique_code != null) {
            $limit_generate_unique_code = $setting_unique_code->max_total_uc;
        }

        if($limit_generate_unique_code == null) {
            $limit_generate_unique_code = 0;
        }
        
        if($setting != null) {
            $click_limit = $setting->voucher_click_limit;
        }

        $expired = $this->voucherExpiredModel->getDefault($auth_user->uid);
        $today = date('Y-m-d');
        $expired_date = '';

        if($expired == null) {
            $expired_date = date('Y-m-d 23:59:59', strtotime($today . "30 day") );
        } else {
            $str = $expired->days." day";
            $expired_date = date('Y-m-d 23:59:59', strtotime($today . $str) );
        }

        $judul_link = '';
        $result = $this->linkTitleModel->getOneLinkTitle($auth_user->uid);

        if($result != null) {
            $judul_link = $result->description;
        }

        // Log::info('total data '.$total_data);
        // Log::info('exist total data di db '.$total_unique_code);
        // Log::info('max limit '.$limit_generate_unique_code);
        // Log::info('max limit '.$limit_generate_unique_code.' - exist total data di db '.$total_unique_code);

        if($check_sum_total_data >= $limit_generate_unique_code) {
            $total_data = $limit_generate_unique_code - $total_unique_code;

            if($total_data < 1) {
                // return response()->json(['code' => 400, 'message' => 'Melebihi jumlah limit generate kode unik']);
                return redirect()->route('getPrintVoucherV2')->with(['err' => 'Melebihi jumlah limit print voucher' ]);
            }
        }

        // Log::info($total_data);
        // Log::info('');
        $task_param = [];
        $task_param['user_uid'] = $auth_user->uid;
        $task_param['type'] = $this->task_type;
        $task_param['status'] = 0;
        $task_param['description'] = '';

        $task = $this->taskModel->postAddTask($task_param);

        for($i = 0; $i < $total_data; $i++) {
            $arr_unique_code[] = [
                'uid' => HelperController::uid('unique_codes'),
                'user_uid' => $auth_user->uid,
                'unique_code' => HelperController::generateUniqueCode(),
                'is_send_email' => 0,
                'is_send_wa' => 1,
                'expired_date' => $expired_date,
                'click_limit' => $click_limit,
                'is_reset' => 'no',
                'delete' => 0
            ];
        }

        $this->uniqueCodeModel->postAddBulkUniqueCode($arr_unique_code);
        // Log::info($arr_unique_code[0]['unique_code']);

        $total_uc = $total_unique_code + $total_data;

        $this->settingUniqueCodePartnerModel->postEditSettingUniqueCodePartnerTotalUc($total_uc, $auth_user->uid);

        $total_row = ceil($total_data / 5);
        
        // versi 2

        $pdf = PDF::loadView('export/pdf-unique-code-v2', ['unique_codes' => $arr_unique_code, 'total_row' => $total_row, 'total_data' => $total_data, 'judul_link' => $judul_link, 'task_id' => $task[0], 'current_user' => Auth::user()])->setPaper('a4', 'portrait');

        // versi 1

        // $pdf = PDF::loadView('export/pdf-unique-code', ['unique_codes' => $arr_unique_code, 'total_row' => $total_row, 'total_data' => $total_data, 'judul_link' => $judul_link, 'task_id' => $task[0], 'current_user' => Auth::user()])->setPaper('a4', 'portrait');

        return $pdf->stream();
    }

    public function getPrintVoucherV3()
    {
        $url_post_print_voucher = url('/partner/print-voucher-v3');
        $title_jumlah_per_halaman = 'Ukuran kertas A4, Jumlah Voucher Per 1 Halaman Adalah 35';

        return view('partner/print-voucher', ['current_user' => Auth::user(), 'url_post_print_voucher' => $url_post_print_voucher, 'title_jumlah_per_halaman' => $title_jumlah_per_halaman]);
    }

    public function postPrintVoucherV3()
    {
        $validator = Validator::make(request()->all(), [
            'page' => 'required|max:2',
        ], HelperController::errorMessagesUniqueCode());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

        if($requested['page'] > 15) {
            return redirect()->route('getPrintVoucherV3')->with(['err' => 'Maximum 15 halaman' ]);
        }

        // $total_data = 16 * $requested['page']; // versi 2
        $total_data = 35 * $requested['page']; // versi 1
        $arr_unique_code = [];
        $auth_user = Auth::user();

        $result_task = $this->taskModel->getOneTask($auth_user->uid, $this->task_type);
        
        if($result_task != null) {
            if($result_task->status == 0) {
                return redirect()->route('getPrintVoucherV3')->with(['err' => 'Proses print voucher sebelum nya belum selesai' ]);
            }
        }

        $total_unique_code = $this->uniqueCodeModel->getCountUniqueCodeIsResetFalse($auth_user->uid);
        $setting_unique_code = $this->settingUniqueCodePartnerModel->getOne($auth_user->uid);
        $setting = $this->settingModel->getSetting();

        $limit_generate_unique_code = 0;
        $click_limit = 0;
        $check_sum_total_data = $total_data + $total_unique_code;

        if($setting_unique_code != null) {
            $limit_generate_unique_code = $setting_unique_code->max_total_uc;
        }

        if($limit_generate_unique_code == null) {
            $limit_generate_unique_code = 0;
        }
        
        if($setting != null) {
            $click_limit = $setting->voucher_click_limit;
        }

        $expired = $this->voucherExpiredModel->getDefault($auth_user->uid);
        $today = date('Y-m-d');
        $expired_date = '';

        if($expired == null) {
            $expired_date = date('Y-m-d 23:59:59', strtotime($today . "30 day") );
        } else {
            $str = $expired->days." day";
            $expired_date = date('Y-m-d 23:59:59', strtotime($today . $str) );
        }

        $judul_link = '';
        $result = $this->linkTitleModel->getOneLinkTitle($auth_user->uid);

        if($result != null) {
            $judul_link = $result->description;
        }

        // Log::info('total data '.$total_data);
        // Log::info('exist total data di db '.$total_unique_code);
        // Log::info('max limit '.$limit_generate_unique_code);
        // Log::info('max limit '.$limit_generate_unique_code.' - exist total data di db '.$total_unique_code);

        if($check_sum_total_data >= $limit_generate_unique_code) {
            $total_data = $limit_generate_unique_code - $total_unique_code;

            if($total_data < 1) {
                // return response()->json(['code' => 400, 'message' => 'Melebihi jumlah limit generate kode unik']);
                return redirect()->route('getPrintVoucherV3')->with(['err' => 'Melebihi jumlah limit print voucher' ]);
            }
        }

        // Log::info($total_data);
        // Log::info('');
        $task_param = [];
        $task_param['user_uid'] = $auth_user->uid;
        $task_param['type'] = $this->task_type;
        $task_param['status'] = 0;
        $task_param['description'] = '';

        $task = $this->taskModel->postAddTask($task_param);

        for($i = 0; $i < $total_data; $i++) {
            $arr_unique_code[] = [
                'uid' => HelperController::uid('unique_codes'),
                'user_uid' => $auth_user->uid,
                'unique_code' => HelperController::generateUniqueCode(),
                'is_send_email' => 0,
                'is_send_wa' => 1,
                'expired_date' => $expired_date,
                'click_limit' => $click_limit,
                'is_reset' => 'no',
                'delete' => 0
            ];
        }

        $this->uniqueCodeModel->postAddBulkUniqueCode($arr_unique_code);
        // Log::info($arr_unique_code[0]['unique_code']);

        $total_uc = $total_unique_code + $total_data;

        $this->settingUniqueCodePartnerModel->postEditSettingUniqueCodePartnerTotalUc($total_uc, $auth_user->uid);

        $total_row = ceil($total_data / 5);
        
        // versi 2

        $pdf = PDF::loadView('export/pdf-unique-code-v3', ['unique_codes' => $arr_unique_code, 'total_row' => $total_row, 'total_data' => $total_data, 'judul_link' => $judul_link, 'task_id' => $task[0], 'current_user' => Auth::user()])->setPaper('a4', 'portrait');

        // versi 1

        // $pdf = PDF::loadView('export/pdf-unique-code', ['unique_codes' => $arr_unique_code, 'total_row' => $total_row, 'total_data' => $total_data, 'judul_link' => $judul_link, 'task_id' => $task[0], 'current_user' => Auth::user()])->setPaper('a4', 'portrait');

        return $pdf->stream();
    }

    public function getUniqueCode()
    {
        $result = $this->settingUniqueCodePartnerModel->getOne(Auth::user()->uid);
        $total_uc = 0;
        $max_total_uc = 0;

        if($result != null) {
            $total_uc = $result->total_uc;
            $max_total_uc = $result->max_total_uc;
        }

        if(Auth::user()->email == 'pm@mail.com') {
            return view('partner/old-unique-code', ['current_user' => Auth::user(), 'page_title' => $this->page_title, 'total_uc' => $total_uc, 'max_total_uc' => $max_total_uc]);
        }

        return view('partner/unique-code', ['current_user' => Auth::user(), 'page_title' => $this->page_title, 'total_uc' => $total_uc, 'max_total_uc' => $max_total_uc]);
    }

    public function postAjaxUniqueCode(Request $request, $user_uid)
    {
        $data = array();

        $columns = HelperController::getPartnerUniqueCodeColumns();

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = 'unique_codes.created_at';
        $dir = 'desc';
        $number = 1;

        $totalData = $this->uniqueCodeModel->countAllActivePartnerUniqueCode($start, $limit, $order, $dir, $user_uid);
        $totalFiltered = $totalData; 

        $unique_codes = $this->uniqueCodeModel->getPartnerUniqueCode($start, $limit, $order, $dir, $user_uid);

        if(!empty($unique_codes))
        {
            foreach ($unique_codes as $unique_code)
            {
            	// $email_btn = "<button onclick='shareEmail(".$unique_code.")' type='button' class='btn btn-success mr-1 mb-1' data-toggle='modal' data-target='#share-email' title='Share Via E-mail'><i class='ft-mail'></i></button>";

            	$wa_btn = "<button onclick='shareWA(".$unique_code.")' type='button' class='btn btn-success mr-1 mb-1' title='Share Via Whats App'><i class='fa fa-whatsapp'></i></button>";

                $nestedData['res'] = $unique_code;
                $nestedData['share_btn'] = $wa_btn;

                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function getHistoryUniqueCode()
    {
    	$page_title = 'History Kode Unik';

        return view('partner/history-unique-code', ['current_user' => Auth::user(), 'page_title' => $page_title]);
    }

    public function postAjaxHistoryUniqueCode(Request $request, $user_uid)
    {
        $data = array();

        $columns = HelperController::getPartnerUniqueCodeColumns();
  
        $totalData = $this->uniqueCodeModel->countAllActiveHistoryUniqueCode($user_uid);
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $unique_codes = $this->uniqueCodeModel->getHistoryUniqueCode($start, $limit, $order, $dir, $user_uid);
        }
        else 
        {
            $unique_codes = $this->uniqueCodeModel->getFilteredHistoryUniqueCode($search, $start, $limit, $order, $dir, $user_uid);
            $totalFiltered = $this->uniqueCodeModel->countAllFilteredActiveHistoryUniqueCode($search, $user_uid);
        }

        if(!empty($unique_codes))
        {
            foreach ($unique_codes as $unique_code)
            {
            	$wa_btn = "<button onclick='shareEmail(".$unique_code.")' type='button' class='btn btn-success mr-1 mb-1' data-toggle='modal' data-target='#share-email' title='Share Via E-mail'><i class='ft-mail'></i></button>";

            	$email_btn = "<button onclick='shareWA(".$unique_code.")' type='button' class='btn btn-success mr-1 mb-1' title='Share Via Whats App'><i class='fa fa-whatsapp'></i></button>";

                $nestedData['no'] = $start+$number;
                $nestedData['res'] = $unique_code;
                $nestedData['created_at'] = date('d F Y H:i:s', strtotime($unique_code->created_at));
                $nestedData['share_btn'] = $wa_btn.$email_btn;

                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function getAllUniqueCode()
    {
    	$page_title = 'Kode Unik';

        return view('admin/unique-code', ['current_user' => Auth::user(), 'page_title' => $page_title]);
    }

    public function postAjaxAllUniqueCode(Request $request)
    {
        $data = array();

        $columns = HelperController::getAdminUniqueCodeColumns();
  
        $totalData = $this->uniqueCodeModel->countAllActiveAllUniqueCode();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $unique_codes = $this->uniqueCodeModel->getAllUniqueCode($start, $limit, $order, $dir);
        }
        else 
        {
            $unique_codes = $this->uniqueCodeModel->getFilteredAllUniqueCode($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->uniqueCodeModel->countAllFilteredActiveAllUniqueCode($search);
        }

        if(!empty($unique_codes))
        {
            foreach ($unique_codes as $unique_code)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['res'] = $unique_code;
                $nestedData['created_at'] = date('d F Y H:i:s', strtotime($unique_code->created_at));

                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function postAjaxAllUniqueCodeTotal(Request $request)
    {
        $data = array();

        $columns = HelperController::getAdminTotalUniqueCodeColumns();
  
        $totalData = $this->uniqueCodeModel->countAllActiveAllTotalUniqueCode();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $unique_codes = $this->uniqueCodeModel->getAllTotalUniqueCode($start, $limit, $order, $dir);
        }
        else 
        {
            $unique_codes = $this->uniqueCodeModel->getFilteredAllTotalUniqueCode($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->uniqueCodeModel->countAllFilteredActiveAllTotalUniqueCode($search);
        }

        if(!empty($unique_codes))
        {
            foreach ($unique_codes as $unique_code)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['res'] = $unique_code;

                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function postAddUniqueCode()
    {
        $validator = Validator::make(request()->all(), [
            'user_uid' => 'required|max:100',
        ], HelperController::errorMessagesUniqueCode());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        $requested['unique_code'] = HelperController::generateUniqueCode();
        
        $total_unique_code = $this->uniqueCodeModel->getCountUniqueCodeIsResetFalse($requested['user_uid']);

        $setting = $this->settingModel->getSetting();
        $setting_unique_code = $this->settingUniqueCodePartnerModel->getOne($requested['user_uid']);

        $expired = $this->voucherExpiredModel->getDefault($requested['user_uid']);
        $today = date('Y-m-d');

        if($expired == null) {
        	$requested['expired_date'] = date('Y-m-d 23:59:59', strtotime($today . "30 day") );
        } else {
        	$str = $expired->days." day";
        	$requested['expired_date'] = date('Y-m-d 23:59:59', strtotime($today . $str) );
        }

        $limit_generate_unique_code = 0;

        if($setting_unique_code != null) {
            $limit_generate_unique_code = $setting_unique_code->max_total_uc;
        }

        if($limit_generate_unique_code == null) {
        	$limit_generate_unique_code = 0;
        }

        $requested['click_limit'] = 0;

        if($setting != null) {
            $requested['click_limit'] = $setting->voucher_click_limit;
        }

        if($total_unique_code >= $limit_generate_unique_code) {
        	return response()->json(['code' => 400, 'message' => 'Melebihi jumlah limit generate kode unik']);
        }

        $exist_unique_code = $this->uniqueCodeModel->getExistUniqueCode($requested['user_uid'], $requested['unique_code']);

        if(sizeof($exist_unique_code) > 0) {
        	return response()->json(['code' => 400, 'message' => 'Ulangi generate kode unik']);
        }

    	$result = $this->uniqueCodeModel->postAddUniqueCode($requested);
    	
        $total_uc = $total_unique_code + 1;

        $this->settingUniqueCodePartnerModel->postEditSettingUniqueCodePartnerTotalUc($total_uc, $requested['user_uid']);

        return response()->json(['code' => 200, 'message' => $result[1], 'total_uc' => $total_uc, 'max_total_uc' => $limit_generate_unique_code]);
    }

    public function postDeleteUniqueCode()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|numeric',
        ], HelperController::errorMessagesUniqueCode());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->uniqueCodeModel->postDeleteUniqueCode($requested['id']);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postUpdateSend() 
    {
    	$validator = Validator::make(request()->all(), [
            'user_uid' => 'required|max:100',
            'category' => 'required|max:100',
            'unique_code' => 'required|max:100'
        ], HelperController::errorMessagesUniqueCode());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

        $result = $this->uniqueCodeModel->postUpdateSend($requested);

        return response()->json(['code' => 200, 'message' => $result]);
    }
}
