<?php

namespace App\Http\Controllers;

use App\Http\Controllers\HelperController;
use Illuminate\Http\Request;
use App\UserModel;
use App\UserCodeModel;
use App\UserCompanyModel;
use App\UserBalanceModel;
use App\PartnerLogoModel;
use App\LinkTitleModel;
use App\ClaimGiftModel;
use App\BankModel;
use App\UserBankAccountModel;
use Auth, Hash, DB, Log, Carbon;

class AgentDashboardController extends Controller
{
    public function __construct(UserModel $userModel, UserCodeModel $userCodeModel, UserCompanyModel $userCompanyModel, UserBalanceModel $userBalanceModel, PartnerLogoModel $partnerLogoModel, LinkTitleModel $linkTitleModel, ClaimGiftModel $claimGiftModel, BankModel $bankModel, UserBankAccountModel $userBankAccountModel)
    {
        $this->userModel = $userModel;
        $this->userCodeModel = $userCodeModel;
        $this->userCompanyModel = $userCompanyModel;
        $this->userBalanceModel = $userBalanceModel;
        $this->partnerLogoModel = $partnerLogoModel;
        $this->linkTitleModel = $linkTitleModel;
        $this->claimGiftModel = $claimGiftModel;
        $this->bankModel = $bankModel;
        $this->userBankAccountModel = $userBankAccountModel;
    }

    public function getAgentDashboard()
    {    	
        $user_code = "";
        $mod_user_code = "";
        $user_balance_standart = 0;
        $user_active_balance_standart = 0;
        $user_balance_toko = 0;

        $current_user = Auth::user();
        $result_user_code = $this->userCodeModel->getOneUserCodeByUserUID($current_user->uid);
        $result_user_company = $this->userCompanyModel->getOneCompanyByUserUID($current_user->uid);
        $result_user_balance_standart = $this->userBalanceModel->getCurrentUserBalance($current_user->uid, "standart");
        $result_user_balance_toko = $this->userBalanceModel->getCurrentUserBalance($current_user->uid, "toko");
        
        if($result_user_code != null) {
            $user_code = $result_user_code->code;
            //$mod_user_code = HelperController::splitUserCode($result_user_code->code);
        }

        if($result_user_balance_standart != null) {
            $user_balance_standart = $result_user_balance_standart->balance;
            $user_active_balance_standart = $result_user_balance_standart->activeBalance;
        }

        if($result_user_balance_toko != null) {
            $user_balance_toko = $result_user_balance_toko->balance;
        }

        $recents_history = $this->claimGiftModel->getRecentHistoryClaimGift($current_user->uid);
        $banks = $this->bankModel->getAllBank();
        $res_user_bank_accounts = $this->userBankAccountModel->getBankAccountByUserUID($current_user->uid);
        
        return view('agentv2/dashboard', [
            'current_user' => $current_user, 
            'user_code' => $user_code, 
            'user_balance_standart' => $user_balance_standart, 
            'user_active_balance_standart' => $user_active_balance_standart, 
            'user_balance_toko' => $user_balance_toko, 
            'recents_history' => $recents_history, 
            'res_user_bank_accounts' => $res_user_bank_accounts, 
            'banks' => $banks
            ]
        );
    }
}
