<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\MembershipModel;
use App\PartnerLogoModel;
use Auth, Hash, DB, Log, Validator;

class MembershipController extends Controller
{
    public function __construct(MembershipModel $membershipModel, PartnerLogoModel $partnerLogoModel)
    {
        $this->membershipModel = $membershipModel;
        $this->partnerLogoModel = $partnerLogoModel;
        $this->page_title = 'Kartu Member';
    }

    public function getPartnerMembership()
    {
        $logo = $this->partnerLogoModel->getOnePartnerLogo(Auth::user()->uid);

        return view('partner/membership', ['current_user' => Auth::user(), 'page_title' => $this->page_title, 'logo' => $logo]);
    }

    public function postAjaxPartnerMembership(Request $request)
    {
        $data = array();
        $user_uid = Auth::user()->uid;

        $columns = HelperController::getPartnerMembershipColumns();
        
        $totalData = $this->membershipModel->countAllActivePartnerMembership($user_uid);
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $memberships = $this->membershipModel->getPartnerMembership($start, $limit, $order, $dir, $user_uid);
        }
        else 
        {
            $memberships = $this->membershipModel->getFilteredPartnerMembership($search, $start, $limit, $order, $dir, $user_uid);
            $totalFiltered = $this->membershipModel->countAllFilteredActivePartnerMembership($search, $user_uid);
        }

        if(!empty($memberships))
        {
            foreach ($memberships as $membership)
            {
                // $email_btn = "<button onclick='shareEmail(".$membership.")' type='button' class='btn btn-success mr-1 mb-1' data-toggle='modal' data-target='#share-email' title='Share Via E-mail'><i class='ft-mail'></i></button>";

                $wa_btn = "<button onclick='shareWA(".$membership.")' type='button' class='btn btn-success mr-1 mb-1' title='Share Via Whats App'><i class='fa fa-whatsapp'></i></button>";

            	$nestedData['no'] = $start+$number;
                $nestedData['res'] = $membership;
                $nestedData['created_at'] = date('d F Y H:i:s', strtotime($membership->created_at));
                $nestedData['share_btn'] = $wa_btn;
                $nestedData['action_btn'] = "
                    <button onclick='editMembership(".$membership.")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-target='#edit-membership'><i class='ft-edit'></i></button>
                    <button onclick='deleteMembership(".$membership.")' type='button' class='btn btn-danger mr-1 mb-1' data-toggle='modal' data-target='#delete-membership'><i class='ft-trash-2'></i></button>
                ";

                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function postAddPartnerMembership()
    {
        $validator = Validator::make(request()->all(), [
            'fullname' => 'required|max:100',
            'nama_usaha' => 'max:100',
            'address' => 'max:100',
            'city' => 'max:100',
            // 'phone' => 'required|min:9|max:13'
        ], HelperController::errorMessagesMembership());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $user_uid = Auth::user()->uid;

        $requested = request();
        $requested['user_uid'] = $user_uid;
        $requested['card_number'] = HelperController::generateCardNumber();

        if($this->membershipModel->getOneMembershipByCardNumber($requested['card_number']) != null) {
            return response()->json(['code' => 400, 'message' => 'Please try again']);
        }

    	$result = $this->membershipModel->postAddMembership($requested);
    	
        return response()->json(['code' => 200, 'message' => $result[1]]);
    }

    public function postEditPartnerMembership()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|numeric',
            'fullname' => 'required|max:100',
            'nama_usaha' => 'max:100',
            'address' => 'max:100',
            'city' => 'max:100',
            // 'phone' => 'required|min:9|max:13'
        ], HelperController::errorMessagesMembership());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $user_uid = Auth::user()->uid;
    	$requested = request();

    	$result = $this->membershipModel->postEditPartnerMembership($requested, $requested['id'], $user_uid);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postDeletePartnerMembership()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|numeric',
        ], HelperController::errorMessagesMembership());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $user_uid = Auth::user()->uid;
        $requested = request();
        
        $result = $this->membershipModel->postDeletePartnerMembership($requested['id'], $user_uid);

        return response()->json(['code' => 200, 'message' => $result]);
    }
}
