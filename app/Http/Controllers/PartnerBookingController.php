<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\BookingModel;
use Auth, Hash, DB, Log, Validator;

class PartnerBookingController extends Controller
{
    public function __construct(BookingModel $bookingModel)
    {
        $this->bookingModel = $bookingModel;
        $this->page_title = 'Booking';
    }

    public function getPartnerBooking()
    {
        return view('partner/booking', ['page_title' => $this->page_title]);
    }

    public function postPartnerBooking() 
    {
    	$validator = Validator::make(request()->all(), [
            'email' => 'required|email|unique:bookings',
            'nama_usaha' => 'required',
            'contact_person' => 'required',
            'alamat_usaha' => 'required',
            'social_media' => 'required',
            'phone' => 'required|numeric|digits_between:9,13'
        ], HelperController::errorMessagesBooking());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

    	$result = $this->bookingModel->postAddBooking($requested);
    	
        return response()->json(['code' => 200, 'message' => $result[1]]);
    }
}
