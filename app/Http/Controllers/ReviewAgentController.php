<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\UserModel;
use App\PartnerLogoModel;
use App\LinkTitleModel;
use Auth, Hash, DB, Log, Validator;

class ReviewAgentController extends Controller
{
    public function __construct(UserModel $userModel, PartnerLogoModel $partnerLogoModel, LinkTitleModel $linkTitleModel)
    {
        $this->userModel = $userModel;
        $this->partnerLogoModel = $partnerLogoModel;
        $this->linkTitleModel = $linkTitleModel;
        $this->page_title = "Review Agent Logo";
    }

    public function getReviewAgentLogo()
    {
        return view('admin/review-agent-logo', ['current_user' => Auth::user(), 'page_title' => $this->page_title]);
    }

    public function postAjaxReviewAgentLogo(Request $request)
    {
        $data = array();

        $columns = HelperController::getReviewAgentLogoColumns();
  
        $totalData = $this->userModel->countAllActiveReviewAgentLogo();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = urlencode($request->input('search.value')); 
        $number = 1;

        if(empty($search))
        {            
            $users = $this->userModel->getReviewAgentLogo($start, $limit, $order, $dir);
        }
        else 
        {
            $users = $this->userModel->getFilteredReviewAgentLogo($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->userModel->countAllFilteredActiveReviewAgentLogo($search);
        }

        if(!empty($users))
        {
            foreach ($users as $user)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['fullname'] = urldecode($user->fullname);
                $nestedData['res'] = $user;
                $nestedData['img'] = "<a href='".url('/').$user->img."' target='_blank'>Klik di sini</a>";
                $nestedData['action_btn'] = "
                    <button onclick='approveAgentLogo(".$user.")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-target='#approve-agent-logo'>Approve</button>
                ";
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function postApproveAgentLogo()
    {
        $validator = Validator::make(request()->all(), [
            'uid' => 'required|max:100',
        ], HelperController::errorMessagesUser());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->partnerLogoModel->postApproveAgentLogo($requested['uid']);

        return response()->json(['code' => 200, 'message' => $result]);
    }
    
    /* -------------------------------------------------------------- Review Agent Description -------------------------------------------------- */
    
    public function getReviewAgentDescription()
    {
        $this->page_title = "Review Agent Description";
        
        return view('admin/review-agent-description', ['current_user' => Auth::user(), 'page_title' => $this->page_title]);
    }

    public function postAjaxReviewAgentDescription(Request $request)
    {
        $data = array();

        $columns = HelperController::getReviewAgentDescriptionColumns();
  
        $totalData = $this->userModel->countAllActiveReviewAgentDescription();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = urlencode($request->input('search.value')); 
        $number = 1;

        if(empty($search))
        {            
            $users = $this->userModel->getReviewAgentDescription($start, $limit, $order, $dir);
        }
        else 
        {
            $users = $this->userModel->getFilteredReviewAgentDescription($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->userModel->countAllFilteredActiveReviewAgentDescription($search);
        }

        if(!empty($users))
        {
            foreach ($users as $user)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['fullname'] = urldecode($user->fullname);
                $nestedData['res'] = $user;
                $nestedData['description'] = urldecode($user->description);
                $nestedData['action_btn'] = "
                    <button onclick='approveAgentDescription(".$user.")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-target='#approve-agent-description'>Approve</button>
                ";
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function postApproveAgentDescription()
    {
        $validator = Validator::make(request()->all(), [
            'uid' => 'required|max:100',
        ], HelperController::errorMessagesUser());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->linkTitleModel->postApproveAgentDescription($requested['uid']);

        return response()->json(['code' => 200, 'message' => $result]);
    }
}
