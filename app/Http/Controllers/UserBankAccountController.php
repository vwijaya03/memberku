<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\BankModel;
use App\UserBankAccountModel;
use App\UserCompanyModel;
use App\UserCodeModel;
use Auth, Hash, DB, Log, Validator;

class UserBankAccountController extends Controller
{
    public function __construct(BankModel $bankModel, UserBankAccountModel $userBankAccountModel, UserCompanyModel $userCompanyModel, UserCodeModel $userCodeModel)
    {
        $this->bankModel = $bankModel;
        $this->userBankAccountModel = $userBankAccountModel;
        $this->userCompanyModel = $userCompanyModel;
        $this->userCodeModel = $userCodeModel;
        $this->page_title = 'Pencairan Uang';
    }

    public function getBankAccount()
    {
        $current_user = Auth::user();
        $res_atasan_user = "";

        $result_user_company = $this->userCompanyModel->getOneCompanyByUserUID($current_user->uid);
        $res_atasan_user = $this->userCodeModel->getOneUserCodeAndUserData($result_user_company->referral_code);

        if($result_user_company != null) {
            $res_atasan_user = $this->userCodeModel->getOneUserCodeAndUserData($result_user_company->referral_code);
        }

        $banks = $this->bankModel->getAllBank();
        $res_user_bank_account = $this->userBankAccountModel->getOneBankAccountByUserUID(Auth::user()->uid);

        $search_bank_url = url('/client/search-bank');

        return view('agent/bank-account', ['current_user' => Auth::user(), 'page_title' => $this->page_title, 'search_bank_url' => $search_bank_url, 'res_user_bank_account' => $res_user_bank_account, 'banks' => $banks, 'res_atasan_user' => $res_atasan_user]);
    }

    public function postAjaxBankAccount(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            'user_uid' => 'required|max:100',
        ], HelperController::errorMessagesBankAccount());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

        $data = array();
        
        $columns = HelperController::getBankAccountColumns();
  
        $totalData = $this->userBankAccountModel->countAllActiveBankAccount($requested['user_uid']);
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $bank_accounts = $this->userBankAccountModel->getBankAccount($start, $limit, $order, $dir, $requested['user_uid']);
        }
        else 
        {
            $bank_accounts = $this->userBankAccountModel->getFilteredBankAccount($search, $start, $limit, $order, $dir, $requested['user_uid']);
            $totalFiltered = $this->userBankAccountModel->countAllFilteredActiveBankAccount($search, $requested['user_uid']);
        }

        if(!empty($bank_accounts))
        {
            foreach ($bank_accounts as $bank_account)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['res'] = $bank_account;
                $nestedData['name'] = urldecode($bank_account->name);
                $nestedData['action_btn'] = "
                    <button onclick='editBankAccount(".$bank_account.")' type='button' class='btn btn-info mr-1 mb-1' data-toggle='modal' data-target='#edit-bank'><i class='ft-edit'></i></button>
                    <button onclick='deleteBankAccount(".$bank_account.")' type='button' class='btn btn-danger mr-1 mb-1' data-toggle='modal' data-target='#delete-bank'><i class='ft-trash-2'></i></button>
                ";
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function postAddBankAccount()
    {
        $validator = Validator::make(request()->all(), [
            'user_uid' => 'required|max:60',
            'bank_uid' => 'required|max:60',
            'no_rek' => 'required|max:60'
        ], HelperController::errorMessagesBankAccount());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

        $res_user_bank_account = $this->userBankAccountModel->getOneBankAccountByUserUID($requested['user_uid']);

        if($res_user_bank_account != null) {
            return response()->json(['code' => 400, 'message' => "Limit bank account hanya 1"]);
        }

    	$result = $this->userBankAccountModel->postAddBankAccount($requested);
    	
        return response()->json(['code' => 200, 'message' => $result[1]]);
    }

    public function postEditBankAccount()
    {
        $validator = Validator::make(request()->all(), [
            'uid' => 'required|max:100',
            'user_uid' => 'required|max:60',
            'bank_uid' => 'required|max:60',
            'no_rek' => 'required|max:60'
        ], HelperController::errorMessagesBankAccount());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

    	$result = $this->userBankAccountModel->postEditBankAccount($requested, $requested['uid']);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postDeleteBankAccount()
    {
        $validator = Validator::make(request()->all(), [
            'uid' => 'required|max:100',
        ], HelperController::errorMessagesBankAccount());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->userBankAccountModel->postDeleteBankAccount($requested['uid']);

        return response()->json(['code' => 200, 'message' => $result]);
    }
}
