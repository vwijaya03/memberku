<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\RecordUsePromoModel;
use Auth, Hash, DB, Log, Validator;

class ReportPinController extends Controller
{
    public function __construct(RecordUsePromoModel $recordUsePromoModel)
    {
        $this->recordUsePromoModel = $recordUsePromoModel;
        $this->page_title = 'Report Pin';
    }

    public function getReportPin($pin) 
    {
        if($pin == null || empty($pin)) {
            return redirect()->route('getSettingPinPromo');
        }

        return view('report/pin', ['current_user' => Auth::user(), 'current_pin' => $pin, 'page_title' => $this->page_title]);
    }

    public function postReportPin() 
    {
        $validator = Validator::make(request()->all(), [
            // 'promo_uid' => 'required|max:100',
            'pin' => 'required|max:100'
        ], HelperController::errorMessagesReportPin());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = $request = request();

        $data = array();

        $columns = HelperController::getReportPinColumns();
  
        $totalData = $this->recordUsePromoModel->countAllActiveReportPin($requested['pin']);
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $reports = $this->recordUsePromoModel->getReportPin($requested['pin'], $start, $limit, $order, $dir);
        }
        else 
        {
            $reports = $this->recordUsePromoModel->getFilteredReportPin($requested['pin'], $search, $start, $limit, $order, $dir);
            $totalFiltered = $this->recordUsePromoModel->countAllFilteredActiveReportPin($requested['pin'], $search);
        }

        if(!empty($reports))
        {
            foreach ($reports as $report)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['created_at'] = date('d F Y H:i:s', strtotime($report->created_at));
                $nestedData['title'] = urldecode($report->title);
                $nestedData['pin'] = urldecode($report->pin);
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }
}
