<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClaimGiftModel;
use Auth, Hash, DB, Log, Validator;

class ReportTukarStrukController extends Controller
{
    public function __construct(ClaimGiftModel $claimGiftModel)
    {
        $this->claimGiftModel = $claimGiftModel;

        $this->page_title = 'Report Tukar Struk';
    }

    public function getReportTukarStruk() {
        return view('admin/report-tukar-struk', ['current_user' => Auth::user(), 'page_title' => $this->page_title]);
    }

    public function postAjaxReportTukarStruk(Request $request) {
        $data = array();

        $columns = HelperController::getReportTukarStrukColumns();
  
        $totalData = $this->claimGiftModel->countAllActiveTukarStruk();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = urlencode($request->input('search.value')); 
        $number = 1;

        if(empty($search))
        {            
            $tukarStruks = $this->claimGiftModel->getTukarStruk($start, $limit, $order, $dir);
        }
        else 
        {
            $tukarStruks = $this->claimGiftModel->getFilteredTukarStruk($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->claimGiftModel->countAllFilteredActiveTukarStruk($search);
        }

        

        if(!empty($tukarStruks))
        {
            foreach ($tukarStruks as $tukarStruk)
            {
                $action = "";

                if($tukarStruk->status == "1") {
                    $status = "Sukses";
                } else if($tukarStruk->status == "2") {
                    $status = "Gagal";
                } else {
                    $status = "Pending";
                    $action = "
                        <div class='form-group'>
                            <div class='btn-group mr-1 mb-1'>
                                <button type='button' class='btn btn-secondary btn-min-width dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>Action</button>
                                <div class='dropdown-menu'>
                                    <button type='button' class='dropdown-item' onclick=approveTukarStrukTemp('$tukarStruk->uid')>Approve</button>
                                    <button type='button' class='dropdown-item' onclick=rejectTukarStrukTemp('$tukarStruk->uid')>Reject</button>
                                </div>
                            </div>
                        </div>
                    ";
                }

                $linkFotoStruk = "";

                if($tukarStruk->img_struk != null) {
                    $linkFotoStruk = "<a href='".url('/').$tukarStruk->img_struk."' target='_blank'>Klik di sini</a>";
                }
                
                $nestedData['no'] = $start+$number;
                $nestedData['res'] = $tukarStruk;
                $nestedData['fullname'] = urldecode($tukarStruk->fullname);
                $nestedData['merchant'] = urldecode($tukarStruk->name);
                $nestedData['status'] = $status;
                $nestedData['fotoStruk'] = $linkFotoStruk;
                $nestedData['action'] = $action;
                $nestedData['tukarStrukCreatedAt'] = date('d F Y H:i:s', strtotime($tukarStruk->created_at));
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }
}
