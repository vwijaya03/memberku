<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\SettingModel;
use App\UserModel;
use App\SettingUniqueCodePartnerModel;
use App\ResetUniqueCodePartnerHistoryModel;
use App\UniqueCodeModel;
use App\PartnerPromoModel;
use App\PromoModel;
use App\PromoInternalModel;
use App\PinPromoModel;
use Auth, Hash, DB, Log, Validator;

class SettingController extends Controller
{
    public function __construct(SettingModel $settingModel, UserModel $userModel, SettingUniqueCodePartnerModel $settingUniqueCodePartnerModel, ResetUniqueCodePartnerHistoryModel $resetUniqueCodePartnerHistoryModel, UniqueCodeModel $uniqueCodeModel, PartnerPromoModel $partnerPromoModel, PromoModel $promoModel, PromoInternalModel $promoInternalModel, PinPromoModel $pinPromoModel)
    {
        $this->settingModel = $settingModel;
        $this->userModel = $userModel;
        $this->settingUniqueCodePartnerModel = $settingUniqueCodePartnerModel;
        $this->resetUniqueCodePartnerHistoryModel = $resetUniqueCodePartnerHistoryModel;
        $this->uniqueCodeModel = $uniqueCodeModel;
        $this->partnerPromoModel = $partnerPromoModel;
        $this->promoModel = $promoModel;
        $this->promoInternalModel = $promoInternalModel;
        $this->pinPromoModel = $pinPromoModel;
        $this->page_title = 'Setting Kirim Benefit Partner';
    }

    public function postAjaxSetting()
    {
    	if($this->settingModel->getSetting() == null) {
    		$validator = Validator::make(request()->all(), [
	            'voucher_price' => 'required|numeric',
	            'voucher_click_limit' => 'required|numeric',
	            'voucher_generate_limit' => 'required|numeric',
	            'server_maintenance' => 'required|max:100',
	            'visibility_remaining_click' => 'required|max:100',
	        ], HelperController::errorMessagesSetting());

	    	if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
	            return response()->json(['code' => 400, 'message' => $err]);
	        }

	        $requested = request();

	    	$result = $this->settingModel->postAddSetting($requested);
	    	
	        return response()->json(['code' => 200, 'message' => $result[1]]);
    	} else {
    		$validator = Validator::make(request()->all(), [
	            'voucher_price' => 'required|numeric',
	            'voucher_click_limit' => 'required|numeric',
	            'voucher_generate_limit' => 'required|numeric',
	            'server_maintenance' => 'required|max:100',
	            'visibility_remaining_click' => 'required|max:100',
	        ], HelperController::errorMessagesSetting());

	    	if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
	            return response()->json(['code' => 400, 'message' => $err]);
	        }

	        $setting = $this->settingModel->getSetting();

	        $requested = request();

	        $result = $this->settingModel->postEditSetting($requested, $setting->id);

        	return response()->json(['code' => 200, 'message' => $result]);
    	}
    	
    }

    public function getSettingKirimBenefitPartner()
    {
        return view('admin/setting-kirim-benefit-partner', ['current_user' => Auth::user(), 'page_title' => $this->page_title]);
    }

    public function getSettingPromoPartner()
    {
        $this->page_title = 'Setting Promo Partner';
        $search_partner_url = url('/admin-access/search-partner');
        
    	return view('admin/setting-promo-partner', ['current_user' => Auth::user(), 'page_title' => $this->page_title, 'search_partner_url' => $search_partner_url]);
    }

    public function getPartnerSelectedPromo(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            // 'promo_uid' => 'required|max:100',
            'user_uid' => 'required|max:100'
        ], HelperController::errorMessagesSettingPromoPartner());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

        $data = array();

        $columns = HelperController::getSelectedPromoColumns();
  
        $totalData = $this->partnerPromoModel->countAllActiveSelectedPromo($requested['user_uid']);
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        // $order = $columns[$request->input('order.0.column')];
        // $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $selected_promos = $this->partnerPromoModel->getSelectedPromo($requested['user_uid'], $start, $limit);
        }
        else 
        {
            $selected_promos = $this->partnerPromoModel->getFilteredSelectedPromo($requested['user_uid'], $search, $start, $limit);
            $totalFiltered = $this->partnerPromoModel->countAllFilteredActiveSelectedPromo($requested['user_uid'], $search);
        }

        if(!empty($selected_promos))
        {
            foreach ($selected_promos as $selected_promo)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['name'] = $selected_promo->name;
                $nestedData['title'] = $selected_promo->title;
                $nestedData['description'] = $selected_promo->description;
                $nestedData['uid'] = $selected_promo->uid;
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function getPartnerUnselectedPromo(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            'user_uid' => 'required|max:100',
            'selected_promo_uid' => ''
        ], HelperController::errorMessagesSettingPromoPartner());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

        if(is_array($requested['selected_promo_uid'])) {
            if(count($requested['selected_promo_uid']) < 1) {
                $requested['selected_promo_uid'] = [];
            }
        } else {
            $requested['selected_promo_uid'] = [];
        }

        $data = array();

        $columns = HelperController::getSelectedPromoColumns();
  
        $totalData = $this->partnerPromoModel->countAllActiveUnselectedPromo($requested['selected_promo_uid']);
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        // $order = $columns[$request->input('order.0.column')];
        // $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $unselected_promos = $this->partnerPromoModel->getUnselectedPromo($requested['selected_promo_uid'], $start, $limit);
        }
        else 
        {
            $unselected_promos = $this->partnerPromoModel->getFilteredUnselectedPromo($requested['selected_promo_uid'], $search, $start, $limit);
            $totalFiltered = $this->partnerPromoModel->countAllFilteredActiveUnselectedPromo($requested['selected_promo_uid'], $search);
        }

        if(!empty($unselected_promos))
        {
            foreach ($unselected_promos as $unselected_promo)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['name'] = $unselected_promo->name;
                $nestedData['title'] = $unselected_promo->title;
                $nestedData['description'] = $unselected_promo->description;
                $nestedData['uid'] = $unselected_promo->uid;
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function postAddPartnerPromo()
    {
        $validator = Validator::make(request()->all(), [
            'user_uid' => 'required|max:100',
            'promo_uid' => 'required|array|min:1'
        ], HelperController::errorMessagesSettingPromoPartner());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $arrExistPromo = [];
        $arrDataPartnerPromo = [];
        $requested = request();

        $existPartnerPromos = $this->partnerPromoModel->getExistPartnerPromo($requested['user_uid'], $requested['promo_uid']);

        foreach ($existPartnerPromos as $existPartnerPromo) {
            array_push($arrExistPromo, $existPartnerPromo->promo_uid);
        }

        $arrayDiffPartnerPromos = array_diff($requested['promo_uid'], $arrExistPromo);
        
        foreach ($arrayDiffPartnerPromos as $arrayDiffPartnerPromo) {
            $arrDataPartnerPromo[] = [
                'uid' => HelperController::uid(''),
                'user_uid' => $requested['user_uid'],
                'promo_uid' => $arrayDiffPartnerPromo,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'delete' => 0,
            ];
        }
        
        $result = $this->partnerPromoModel->postAddPartnerPromo($arrDataPartnerPromo);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postDeletePartnerPromo()
    {
        $validator = Validator::make(request()->all(), [
            'user_uid' => 'required|max:100',
            'promo_uid' => 'required|array|min:1'
        ], HelperController::errorMessagesSettingPromoPartner());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->partnerPromoModel->postDeletePartnerPromo($requested['user_uid'], $requested['promo_uid']);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function postSettingUniqueCodePartner(Request $request, $status)
    {
        $approved = 0;
        $data = array();

        if($status == 'approved') {
            $approved = 1;
        } else if($status == 'rejected') {
            $approved = 2;
        }

        $columns = HelperController::getUserColumns();
  
        $totalData = $this->userModel->countAllActiveUser($this->role = 'partner', $approved);
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $partners = $this->userModel->getUser($start, $limit, $order, $dir, $this->role = 'partner', $approved);
        }
        else 
        {
            $partners = $this->userModel->getFilteredUser($search, $start, $limit, $order, $dir, $this->role = 'partner', $approved);
            $totalFiltered = $this->userModel->countAllFilteredActiveUser($search, $this->role = 'partner', $approved);
        }

        if(!empty($partners))
        {
            foreach ($partners as $partner)
            {
                $action_btn = "
                    <button onclick='getSettingUniqueCodePartner(".$partner.")' type='button' class='btn btn-success mr-1 mb-1 setting-unique-code-partner-btn' data-toggle='modal' data-target='#setting-unique-code-partner-modal' title='Setting'><i class='ft-settings'></i></button>
                    <button onclick='confirmResetTotalUc(".$partner.")' type='button' class='btn btn-success mr-1 mb-1 setting-unique-code-partner-btn' data-toggle='modal' data-target='#reset-total-uc-modal' title='Setting'><i class='ft-rotate-ccw'></i></button>
                ";

                $nestedData['no'] = $start+$number;
                $nestedData['res'] = $partner;
                $nestedData['action_btn'] = $action_btn;
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function postGetDataSettingUniqueCodePartner()
    {
        $validator = Validator::make(request()->all(), [
            'user_uid' => 'required|max:100'
        ], HelperController::errorMessagesSetting());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

        $result = $this->settingUniqueCodePartnerModel->getOne($requested['user_uid']);
        
        return response()->json(['code' => 200, 'result' => $result]);
    }

    public function postAddDataSettingUniqueCodePartner()
    {
        $validator = Validator::make(request()->all(), [
            'user_uid' => 'required|max:100',
            'max_total_uc' => 'required|numeric',
        ], HelperController::errorMessagesSetting());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $msg = '';
        $requested = request();
        $result_setting = $this->settingUniqueCodePartnerModel->getOne($requested['user_uid']);
        
        if($result_setting == null) {
            $result = $this->settingUniqueCodePartnerModel->postAddSettingUniqueCodePartner($requested);
            $msg = $result[1];
        } else {
            $result = $this->settingUniqueCodePartnerModel->postEditSettingUniqueCodePartner($requested, $requested['user_uid']);
            $msg = $result;
        }

        return response()->json(['code' => 200, 'message' => $msg]);
    }

    public function postResetTotalUc()
    {
        $validator = Validator::make(request()->all(), [
            'user_uid' => 'required|max:100'
        ], HelperController::errorMessagesSetting());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $auth_user = Auth::user();

        $requested = request();
        $requested['reset_date'] = date('Y-m-d H:i:s');
        $requested['reset_by'] = $auth_user->fullname;
        $requested['reset_by_uid'] = $auth_user->uid;

        $result = $this->settingUniqueCodePartnerModel->postResetTotalUniqueCodePartner($requested['user_uid']);
        $this->uniqueCodeModel->postUpdateIsResetUniqueCode($requested['user_uid']);
        $this->resetUniqueCodePartnerHistoryModel->postAddResetUniqueCodePartnerHistory($requested);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function getSettingPinPromo()
    {
        $this->page_title = 'Setting Pin Promo';

        $global_promos = $this->promoModel->getAllPromoForSettingPin();

        return view('admin/pin-promo', ['current_user' => Auth::user(), 'page_title' => $this->page_title, 'global_promos' => $global_promos]);
    }

    public function getPinSelectedPromo()
    {
        $validator = Validator::make(request()->all(), [
            // 'promo_uid' => 'required|max:100',
            'promo_uid' => 'required|max:100'
        ], HelperController::errorMessagesSettingPromoPartner());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = $request = request();

        $data = array();

        $columns = HelperController::getPinPromoColumns();
  
        $totalData = $this->pinPromoModel->countAllActivePinPromo($requested['promo_uid']);
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        // $order = $columns[$request->input('order.0.column')];
        // $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $selected_pins = $this->pinPromoModel->getPinPromo($requested['promo_uid'], $start, $limit);
        }
        else 
        {
            $selected_pins = $this->pinPromoModel->getFilteredPinPromo($requested['promo_uid'], $search, $start, $limit);
            $totalFiltered = $this->pinPromoModel->countAllFilteredActivePinPromo($requested['promo_uid'], $search);
        }

        if(!empty($selected_pins))
        {
            foreach ($selected_pins as $selected_pin)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['pin'] = $selected_pin->pin;
                $nestedData['uid'] = $selected_pin->uid;
                $nestedData['action_btn'] = "
                    <a href='".url('/admin-access/report-pin/'.$selected_pin->pin)."' class='btn btn-info mr-1 mb-1' target='_blank'>Lihat Report</a>
                ";
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function postAddPinPromo()
    {
        $validator = Validator::make(request()->all(), [
            'promo_uid' => 'required|max:100',
            'pin' => 'required|max:100'
        ], HelperController::errorMessagesPinPromo());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

        $result = $this->pinPromoModel->postAddPinPromo($requested);
        $msg = $result[1];

        return response()->json(['code' => 200, 'message' => $msg]);
    }

    public function postDeletePinPromo()
    {
        $validator = Validator::make(request()->all(), [
            'promo_uid' => 'required|max:100',
            'pin_promo_uid' => 'required|array|min:1'
        ], HelperController::errorMessagesPinPromo());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->pinPromoModel->postDeletePinPromo($requested['promo_uid'], $requested['pin_promo_uid']);

        return response()->json(['code' => 200, 'message' => $result]);
    }

    public function getSettingPinPromoInternal()
    {
        $this->page_title = 'Setting Pin Promo Internal';

        $internal_promos = $this->promoInternalModel->getAllPromoInternalForSettingPin();

        return view('admin/pin-promo-internal', ['current_user' => Auth::user(), 'page_title' => $this->page_title, 'internal_promos' => $internal_promos]);
    }

    public function getPinSelectedPromoInternal()
    {
        $validator = Validator::make(request()->all(), [
            // 'promo_uid' => 'required|max:100',
            'promo_uid' => 'required|max:100'
        ], HelperController::errorMessagesSettingPromoPartner());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = $request = request();

        $data = array();

        $columns = HelperController::getPinPromoColumns();
  
        $totalData = $this->pinPromoModel->countAllActivePinPromo($requested['promo_uid']);
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        // $order = $columns[$request->input('order.0.column')];
        // $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $selected_pins = $this->pinPromoModel->getPinPromo($requested['promo_uid'], $start, $limit);
        }
        else 
        {
            $selected_pins = $this->pinPromoModel->getFilteredPinPromo($requested['promo_uid'], $search, $start, $limit);
            $totalFiltered = $this->pinPromoModel->countAllFilteredActivePinPromo($requested['promo_uid'], $search);
        }

        if(!empty($selected_pins))
        {
            foreach ($selected_pins as $selected_pin)
            {
                $nestedData['no'] = $start+$number;
                $nestedData['pin'] = $selected_pin->pin;
                $nestedData['uid'] = $selected_pin->uid;
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function postAddPinPromoInternal()
    {
        $validator = Validator::make(request()->all(), [
            'promo_uid' => 'required|max:100',
            'pin' => 'required|max:100'
        ], HelperController::errorMessagesPinPromo());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();

        $result = $this->pinPromoModel->postAddPinPromo($requested);
        $msg = $result[1];

        return response()->json(['code' => 200, 'message' => $msg]);
    }

    public function postDeletePinPromoInternal()
    {
        $validator = Validator::make(request()->all(), [
            'promo_uid' => 'required|max:100',
            'pin_promo_uid' => 'required|array|min:1'
        ], HelperController::errorMessagesPinPromo());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->pinPromoModel->postDeletePinPromo($requested['promo_uid'], $requested['pin_promo_uid']);

        return response()->json(['code' => 200, 'message' => $result]);
    }
}
