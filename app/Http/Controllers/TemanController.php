<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\UserModel;
use App\CompanyModel;
use App\UserCompanyModel;
use App\UserCodeModel;
use Auth, Hash, DB, Log, Validator;

class TemanController extends Controller
{
    public function __construct(UserModel $userModel, CompanyModel $companyModel, UserCompanyModel $userCompanyModel, UserCodeModel $userCodeModel)
    {
        $this->userModel = $userModel;
        $this->companyModel = $companyModel;
        $this->userCompanyModel = $userCompanyModel;
        $this->userCodeModel = $userCodeModel;
        $this->page_title = 'Daftar';
    }

    public function getTeman()
    {   
        $current_user = Auth::user();
        $referral_code = "";
        $res_atasan_user = "";
        
        $res_user_code = $this->userCodeModel->getOneUserCodeByUserUID($current_user->uid);
        $result_user_company = $this->userCompanyModel->getOneCompanyByUserUID($current_user->uid);
        $res_atasan_user = $this->userCodeModel->getOneUserCodeAndUserData($result_user_company->referral_code);

        if($res_user_code != null) {
            $referral_code = $res_user_code->code;
        }

        if($result_user_company != null) {
            $res_atasan_user = $this->userCodeModel->getOneUserCodeAndUserData($result_user_company->referral_code);
        }

        if($current_user->type == "toko") {
            // return view('agent/teman-with-type-toko', ['current_user' => $current_user, 'page_title' => $this->page_title, 'referral_code' => $referral_code, 'res_atasan_user' => $res_atasan_user, 'user_code' => $referral_code]);

            return view('agentv2/teman', ['current_user' => $current_user, 'page_title' => $this->page_title, 'referral_code' => $referral_code, 'res_atasan_user' => $res_atasan_user, 'user_code' => $referral_code]);
        } else {
            // return view('agent/teman-with-type-standart', ['current_user' => $current_user, 'page_title' => $this->page_title, 'referral_code' => $referral_code, 'res_atasan_user' => $res_atasan_user]);

            return view('agentv2/teman', ['current_user' => $current_user, 'page_title' => $this->page_title, 'referral_code' => $referral_code, 'res_atasan_user' => $res_atasan_user, 'user_code' => $referral_code]);
        }
    }

    public function postAjaxTeman(Request $request)
    {
        $data = array();
        $referral_code = $request->get('referral_code');

        $columns = HelperController::getTemanColumns();
  
        $totalData = $this->userModel->countAllActiveAgentByCurrentUserUID($referral_code, "toko");
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 
        $number = 1;

        if(empty($search))
        {            
            $agents = $this->userModel->getAgentByCurrentUserUID($start, $limit, $order, $dir, $referral_code, "toko");
        }
        else 
        {
            $agents = $this->userModel->getFilteredAgentByCurrentUserUID($search, $start, $limit, $order, $dir, $referral_code, "toko");
            $totalFiltered = $this->userModel->countAllFilteredActiveAgentByCurrentUserUID($search, $referral_code, "toko");
        }

        if(!empty($agents))
        {
            foreach ($agents as $agent)
            {
                $wa_btn = "<button onclick='openWA(".$agent.")' type='button' class='btn btn-success mr-1 mb-1' title='Share Via Whats App'><i class='fa fa-whatsapp'></i></button>";

                // $nestedData['no'] = $start+$number;
                // $nestedData['fullname'] = urldecode($agent->fullname);
                // $nestedData['res'] = $agent;
                // $nestedData['struk_yang_bisa_ditukarkan'] = "Rp. 0";
                // $nestedData['share_btn'] = $wa_btn;

                $nestedData['row1'] = urldecode($agent->fullname)." <img src='".url("/img/wa.png")."' class='img-wa' onclick='openWA(".$agent.")'><br>".$agent->phone." ";
                $nestedData['row2'] = "Rp 0";
                
                $data[] = $nestedData;
                $number++;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function postAddTeman()
    {
        $validator = Validator::make(request()->all(), [
            'fullname' => 'required|max:50',
            'phone' => 'required|max:20',
            'password' => 'max:100',
            'type' => 'required|max:100',
            'referral_code' => 'max:35'
        ], HelperController::errorMessagesAgent());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $current_user = Auth::user();
        $requested = request();
        $requested['role'] = "agent";

        if($requested['email'] == null || $requested['email'] == "") {
            $requested['email'] = str_replace(" ", "_", $requested['fullname'])."_".$requested['phone']."@mail.com";
        }

        if($requested['password'] == null || $requested['password'] == "") {
            $requested['password'] = '123456';
        }

        if(substr($requested['phone'], 0, 3) == "+62") {
            $requested['phone'] = "0".substr($requested['phone'], 3);
        } else if(substr($requested['phone'], 0, 2) == "62") {
            $requested['phone'] = "0".substr($requested['phone'], 2);
        }

        $requested['phone'] = preg_replace('/[^\w\d]+/', '', $requested['phone']);

        $exist_phone_number = $this->userModel->getOneAgentByPhone($requested['phone']);

        if($exist_phone_number || $exist_phone_number != null) {
            return response()->json(['code' => 400, 'message' => "Nomor HP telah digunakan"]);
        }

        $res_user_code = $this->userCodeModel->getOneUserCodeByUserUID($current_user->uid);

        if($res_user_code == null) {
            return response()->json(['code' => 400, 'message' => "Kode referral tidak ditemukan"]);
        }

        $requested['company_uid'] = $res_user_code->code;

        $result = $this->userModel->postAddUser($requested);
        
        return response()->json(['code' => 200, 'message' => $result[1], 'temp_password' => "123456", 'registered_user_code' => $result[2]]);
    }

    public function postDeleteTeman()
    {
        $validator = Validator::make(request()->all(), [
            'id' => 'required|numeric',
        ], HelperController::errorMessagesAgent());

        if(sizeof($err = HelperController::validatorHelper($validator)) > 0 ) {
            return response()->json(['code' => 400, 'message' => $err]);
        }

        $requested = request();
        
        $result = $this->userModel->postDeleteUser($requested['id']);

        return response()->json(['code' => 200, 'message' => $result]);
    }
}
