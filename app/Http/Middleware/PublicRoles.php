<?php

namespace App\Http\Middleware;

use Closure, Log, Redirect;
use App\SettingModel;

class PublicRoles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Check jika main domain, domain nya = kartu digital maka langsung redirect ke /agent

        $kd_url = $request->fullUrl();

        if (!$request->secure()) {
            
            if($kd_url == "http://memberku.id" || $kd_url == "http://www.memberku.id") {

                if($request->getRequestUri() == '/') {
                    $kd_url = "https://memberku.id/client";
                } else {
                    $kd_url = "https://memberku.id/".$request->getRequestUri();
                }

                return Redirect::to($kd_url);
            } else if($kd_url == "http://tukarstruk.id" || $kd_url == "http://www.tukarstruk.id") {

                if($request->getRequestUri() == '/') {
                    $kd_url = "https://tukarstruk.id/verify";
                } else {
                    $kd_url = "https://tukarstruk.id/".$request->getRequestUri();
                }

                return Redirect::to($kd_url);
            } else {
                return redirect()->secure($request->getRequestUri());
            }
        } else {
            if($kd_url == "https://memberku.id" || $kd_url == "https://www.memberku.id") {
                $kd_url = "https://memberku.id/client";

                return Redirect::to($kd_url);
            } else if($kd_url == "https://tukarstruk.id" || $kd_url == "https://www.tukarstruk.id") {
                $kd_url = "https://tukarstruk.id/verify";

                return Redirect::to($kd_url);
            }
        }
        
        $settingModel = new SettingModel();
        $setting = $settingModel->getSetting();

        if($setting != null) {
            if($setting->server_maintenance == 'yes') {
                if($request->ajax()) {
                    return response()->json(['code' => 400, 'message' => 'Server sedang maintenance']);
                }
                return response()->view('error/503');
            }
        }

        return $next($request);
    }
}
