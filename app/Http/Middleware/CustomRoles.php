<?php

namespace App\Http\Middleware;

use Closure, Auth, Log;
use App\SettingModel;

class CustomRoles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$args)
    {
        if (!$request->secure()) {
            return redirect()->secure($request->getRequestUri());
        }

        $settingModel = new SettingModel();
        $setting = $settingModel->getSetting();

        if($setting != null) {
            if($setting->server_maintenance == 'yes') {
                if($request->ajax()) {
                    return response()->json(['code' => 400, 'message' => 'Server sedang maintenance']);
                }
                return response()->view('error/503');
            }
        }

        $last_user_data = '';

        if(Auth::check()) {
            $user_role = [];
            $user_role[] = auth()->user()->role;

            $authorizedRoles = array_intersect($args, $user_role);

            if(count($authorizedRoles) > 0 && auth()->user()->delete == 0 && auth()->user()->approve == 1) {
                return $next($request);
            } else {
                if(auth()->user()->role == 'partner') {
                    if($request->ajax()) {
                        return response()->json(['code' => 400, 'message' => 'Unauthorized !']);
                    }
                    
                    return redirect()->route('getPartnerLogin')->with(['err' => 'Unauthorized !']);
                } else {
                    if($request->ajax()) {
                        return response()->json(['code' => 400, 'message' => 'Unauthorized !']);
                    }
                    
                    if(auth()->user()->role == 'partner') {
                        Auth::logout();
                        return redirect()->route('getPartnerLogin')->with(['err' => 'Unauthorized !']);
                    } else if(auth()->user()->role == 'agent') {
                        Auth::logout();
                        return redirect()->route('getAgentLogin')->with(['err' => 'Unauthorized !']);
                    } else {
                        Auth::logout();
                        return redirect()->route('getAdminLogin')->with(['err' => 'Unauthorized !']);
                    }
                }
            }
        } else {
            Auth::logout();
            return redirect()->route('getAdminLogin')->with(['err' => 'Please login again !']);
        }
    }
}
