<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class UniqueCodeModel extends Model
{
    protected $table = 'unique_codes';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'user_uid', 'unique_code', 'is_send_email', 'is_send_wa', 'expired_date', 'click_limit', 'is_reset', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di proses.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    private $success_use_promo = 'Promo berhasil di gunakan';

    public function countAllActivePartnerUniqueCode($start, $limit, $order, $dir, $user_uid)
    {
        $count_unique_code = $this->select('unique_codes.id')
        ->join('users', 'users.uid', '=', 'unique_codes.user_uid')
        ->where('unique_codes.user_uid', $user_uid)
        ->where('unique_codes.is_send_wa', 0)
        ->where('unique_codes.is_send_email', 0)
        ->where('unique_codes.is_reset', 'no')
        ->where('unique_codes.delete', 0)
        ->where('users.delete', 0)
        ->where('users.role', 'partner')
        ->limit($limit)
        ->count();

        return $count_unique_code;
    }

    public function getPartnerUniqueCode($start, $limit, $order, $dir, $user_uid)
    {
        $unique_code = $this->select('users.id as user_id', 'users.fullname', 'users.email', 'unique_codes.user_uid', 'unique_codes.unique_code', 'unique_codes.expired_date', 'unique_codes.created_at')
        ->join('users', 'users.uid', '=', 'unique_codes.user_uid')
        ->where('unique_codes.user_uid', $user_uid)
        ->where('unique_codes.is_send_wa', 0)
        ->where('unique_codes.is_send_email', 0)
        ->where('unique_codes.is_reset', 'no')
        ->where('unique_codes.delete', 0)
        ->where('users.delete', 0)
        ->where('users.role', 'partner')
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $unique_code;
    }

    public function countAllActiveHistoryUniqueCode($user_uid)
    {
        $count_unique_code = $this->select('unique_codes.id')
        ->join('users', 'users.uid', '=', 'unique_codes.user_uid')
        ->where('unique_codes.user_uid', $user_uid)
        ->where('unique_codes.delete', 0)
        ->where('users.delete', 0)
        ->where('users.role', 'partner')
        ->count();

        return $count_unique_code;
    }

    public function countAllFilteredActiveHistoryUniqueCode($search, $user_uid)
    {
        $count_unique_code = $this->select('unique_codes.id')
        ->join('users', 'users.uid', '=', 'unique_codes.user_uid')
        ->where(function ($q) use($search) {
            $q->where('users.fullname', 'like', '%'.$search.'%');
            $q->orWhere('users.email', 'like', '%'.$search.'%');
            $q->orWhere('unique_codes.unique_code', 'like', '%'.$search.'%');
        })
        ->where('unique_codes.user_uid', $user_uid)
        ->where('unique_codes.delete', 0)
        ->where('users.delete', 0)
        ->where('users.role', 'partner')
        ->count();

        return $count_unique_code;
    }

    public function getHistoryUniqueCode($start, $limit, $order, $dir, $user_uid)
    {
        $unique_code = $this->select('users.id as user_id', 'users.fullname', 'users.email', 'unique_codes.user_uid', 'unique_codes.unique_code', 'unique_codes.created_at')
        ->join('users', 'users.uid', '=', 'unique_codes.user_uid')
        ->where('unique_codes.user_uid', $user_uid)
        ->where('unique_codes.delete', 0)
        ->where('users.delete', 0)
        ->where('users.role', 'partner')
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $unique_code;
    }
    
    public function getFilteredHistoryUniqueCode($search, $start, $limit, $order, $dir, $user_uid)
    {
        $unique_code = $this->select('users.id as user_id', 'users.fullname', 'users.email', 'unique_codes.user_uid', 'unique_codes.unique_code', 'unique_codes.created_at')
        ->join('users', 'users.uid', '=', 'unique_codes.user_uid')
        ->where(function ($q) use($search) {
            $q->where('users.fullname', 'like', '%'.$search.'%');
            $q->orWhere('users.email', 'like', '%'.$search.'%');
            $q->orWhere('unique_codes.unique_code', 'like', '%'.$search.'%');
        })
        ->where('unique_codes.user_uid', $user_uid)
        ->where('unique_codes.delete', 0)
        ->where('users.delete', 0)
        ->where('users.role', 'partner')
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $unique_code;
    }

    public function countAllActiveAllUniqueCode()
    {
        $count_unique_code = $this->select('unique_codes.id')
        ->join('users', 'users.uid', '=', 'unique_codes.user_uid')
        ->where('unique_codes.delete', 0)
        ->where('users.delete', 0)
        ->where('users.role', 'partner')
        ->count();

        return $count_unique_code;
    }

    public function countAllFilteredActiveAllUniqueCode($search)
    {
        $count_unique_code = $this->select('unique_codes.id')
        ->join('users', 'users.uid', '=', 'unique_codes.user_uid')
        ->where(function ($q) use($search) {
            $q->where('users.fullname', 'like', '%'.$search.'%');
            $q->orWhere('users.email', 'like', '%'.$search.'%');
            $q->orWhere('unique_codes.unique_code', 'like', '%'.$search.'%');
        })
        ->where('unique_codes.delete', 0)
        ->where('users.delete', 0)
        ->where('users.role', 'partner')
        ->count();

        return $count_unique_code;
    }

    public function getAllUniqueCode($start, $limit, $order, $dir)
    {
        $unique_code = $this->select('users.id as user_id', 'users.fullname', 'users.email', 'unique_codes.user_uid', 'unique_codes.unique_code', 'unique_codes.created_at')
        ->join('users', 'users.uid', '=', 'unique_codes.user_uid')
        ->where('unique_codes.delete', 0)
        ->where('users.delete', 0)
        ->where('users.role', 'partner')
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $unique_code;
    }
    
    public function getFilteredAllUniqueCode($search, $start, $limit, $order, $dir)
    {
        $unique_code = $this->select('users.id as user_id', 'users.fullname', 'users.email', 'unique_codes.user_uid', 'unique_codes.unique_code', 'unique_codes.created_at')
        ->join('users', 'users.uid', '=', 'unique_codes.user_uid')
        ->where(function ($q) use($search) {
            $q->where('users.fullname', 'like', '%'.$search.'%');
            $q->orWhere('users.email', 'like', '%'.$search.'%');
            $q->orWhere('unique_codes.unique_code', 'like', '%'.$search.'%');
        })
        ->where('unique_codes.delete', 0)
        ->where('users.delete', 0)
        ->where('users.role', 'partner')
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $unique_code;
    }

    public function countAllActiveAllTotalUniqueCode()
    {
        $count_unique_code = $this->distinct('unique_codes.user_uid')
        ->join('users', 'users.uid', '=', 'unique_codes.user_uid')
        ->where('unique_codes.delete', 0)
        ->where('users.delete', 0)
        ->where('users.role', 'partner')
        ->count('unique_codes.user_uid');

        return $count_unique_code;
    }

    public function countAllFilteredActiveAllTotalUniqueCode($search)
    {
        $count_unique_code = $this->distinct('unique_codes.user_uid')
        ->join('users', 'users.uid', '=', 'unique_codes.user_uid')
        ->where(function ($q) use($search) {
            $q->where('users.fullname', 'like', '%'.$search.'%');
            $q->orWhere('unique_codes.unique_code', 'like', '%'.$search.'%');
        })
        ->where('unique_codes.delete', 0)
        ->where('users.delete', 0)
        ->where('users.role', 'partner')
        ->count('unique_codes.user_uid');

        return $count_unique_code;
    }

    public function getAllTotalUniqueCode($start, $limit, $order, $dir)
    {
        $unique_code = $this->select(DB::raw('users.fullname, count(unique_codes.unique_code) as total_unique_code'))
        ->join('users', 'users.uid', '=', 'unique_codes.user_uid')
        ->where('unique_codes.delete', 0)
        ->where('users.delete', 0)
        ->where('users.role', 'partner')
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->groupBy('unique_codes.user_uid', 'users.fullname')
        ->get();

        return $unique_code;
    }
    
    public function getFilteredAllTotalUniqueCode($search, $start, $limit, $order, $dir)
    {
        $unique_code = $this->select(DB::raw('users.fullname, count(unique_codes.unique_code) as total_unique_code'))
        ->join('users', 'users.uid', '=', 'unique_codes.user_uid')
        ->where(function ($q) use($search) {
            $q->where('users.fullname', 'like', '%'.$search.'%');
            $q->orWhere('unique_codes.unique_code', 'like', '%'.$search.'%');
        })
        ->where('unique_codes.delete', 0)
        ->where('users.delete', 0)
        ->where('users.role', 'partner')
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->groupBy('unique_codes.user_uid', 'users.fullname')
        ->get();

        return $unique_code;
    }

    public function getOneUniqueCode($id, $user_uid)
    {
        $unique_code = $this->select('users.id as user_id', 'users.fullname', 'users.email', 'unique_codes.user_uid', 'unique_codes.unique_code', 'unique_codes.created_at')
        ->where('unique_codes.id', $id)
        ->where('unique_codes.user_uid', $user_uid)
        ->where('unique_codes.delete', 0)
        ->where('users.delete', 0)
        ->where('users.role', 'partner')
        ->first();

        return $unique_code;
    }

    public function getOneUniqueCodeByUniqueCode($unique_code, $user_uid)
    {
        $unique_code = $this->select('unique_codes.user_uid', 'unique_codes.unique_code', 'unique_codes.created_at', 'expired_date', 'click_limit')
        ->where('unique_codes.unique_code', $unique_code)
        ->where('unique_codes.user_uid', $user_uid)
        ->where('unique_codes.delete', 0)
        ->first();

        return $unique_code;
    }

    public function getOneUniqueCodeByUniqueCodeOnly($unique_code)
    {
        $unique_code = $this->select('unique_codes.user_uid', 'unique_codes.unique_code', 'unique_codes.created_at', 'expired_date', 'click_limit')
        ->where('unique_codes.unique_code', $unique_code)
        ->where('unique_codes.delete', 0)
        ->first();

        return $unique_code;
    }

    public function verifyUniqueCode($unique_code)
    {
        $unique_code = $this->select('unique_codes.user_uid', 'unique_codes.unique_code', 'unique_codes.created_at', 'expired_date', 'click_limit')
        ->where('unique_codes.unique_code', $unique_code)
        ->where('unique_codes.delete', 0)
        ->first();

        return $unique_code;
    }

    public function getCountUniqueCodeIsResetFalse($user_uid) {
        $unique_code = $this->select('unique_codes.uid as total')
        ->where('unique_codes.user_uid', $user_uid)
        ->where('unique_codes.is_reset', 'no')
        ->where('unique_codes.delete', 0)
        ->count();

        return $unique_code;
    }

    public function getCountUniqueCode($user_uid) {
        $unique_code = $this->select('unique_codes.uid as total')
        ->where('unique_codes.user_uid', $user_uid)
        ->where('unique_codes.delete', 0)
        ->count();

        return $unique_code;
    }

    public function getExistUniqueCode($user_uid, $unique_code) {
        $unique_code = $this->select('unique_codes.uid')
        ->where('unique_codes.user_uid', $user_uid)
        ->where('unique_codes.unique_code', $unique_code)
        ->where('unique_codes.delete', 0)
        ->get();

        return $unique_code;
    }

    public function postAddUniqueCode($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('unique_codes'),
                'user_uid' => $param['user_uid'],
                'unique_code' => $param['unique_code'],
                'is_send_email' => 0,
                'is_send_wa' => 0,
                'expired_date' => $param['expired_date'],
                'click_limit' => $param['click_limit'],
                'is_reset' => 'no',
                'delete' => 0
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }

    public function postAddBulkUniqueCode($data)
    {
        $final = DB::transaction(function () use($data) {
            $this->insert($data);

            return "done";
        });

        return $final;
    }

    public function postUpdateIsResetUniqueCode($user_uid)
    {
        DB::transaction(function () use($user_uid) {
            $this->where('unique_codes.user_uid', $user_uid)->where('unique_codes.delete', 0)->update([
                'is_reset' => 'yes'
            ]);
        });

        return $this->success_update_msg;
    }

    public function postUpdateRemainingClick($unique_code, $user_uid, $remaining_click)
    {
        DB::transaction(function () use($unique_code, $user_uid, $remaining_click) {
            $this->where('unique_codes.unique_code', $unique_code)->where('unique_codes.user_uid', $user_uid)->where('unique_codes.delete', 0)->update([
                'click_limit' => $remaining_click
            ]);
        });

        return $this->success_use_promo;
    }

    public function cmdResetUniqueCode()
    {
        DB::transaction(function () {
            $this->where('unique_codes.delete', 0)->update([
                'is_reset' => 'yes'
            ]);
        });
    }

    public function postEditUniqueCode($param, $id, $user_uid)
    {
        DB::transaction(function () use($param, $id, $user_uid) {
            $this->where('unique_codes.id', $id)->where('unique_codes.user_uid', $user_uid)->where('unique_codes.delete', 0)->update([
                'unique_code' => $param['unique_code']
            ]);
        });

        return $this->success_update_msg;
    }

    public function postUpdateSend($param)
    {
        if($param['category'] == 'wa') {
            DB::transaction(function () use($param) {
                $this->where('unique_codes.unique_code', $param['unique_code'])->where('unique_codes.user_uid', $param['user_uid'])->where('unique_codes.delete', 0)->update([
                    'is_send_wa' => 1
                ]);
            });
        } else if($param['category'] == 'email') {
            DB::transaction(function () use($param) {
                $this->where('unique_codes.unique_code', $param['unique_code'])->where('unique_codes.user_uid', $param['user_uid'])->where('unique_codes.delete', 0)->update([
                    'is_send_email' => 1
                ]);
            });
        }

        return $this->success_update_msg;
    }

    public function postDeleteUniqueCode($id, $user_uid)
    {
        DB::transaction(function () use($id, $user_uid) {
            $this->where('id', $id)->where('unique_codes.user_uid', $user_uid)->where('unique_codes.delete', 0)->update([
                'delete' => 1
            ]);
        });

        return $this->success_delete_msg;
    }
}
