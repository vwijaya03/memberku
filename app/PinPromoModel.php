<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class PinPromoModel extends Model
{
    protected $table = 'pin_promos';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'promo_uid', 'pin', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_update_sequence_msg = 'Urutan data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di proses.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function countAllActivePinPromo($promo_uid)
    {
        $count_pin_promo = $this->where('pin_promos.promo_uid', $promo_uid)
        ->where('pin_promos.delete', 0)
        ->count('pin_promos.uid');

        return $count_pin_promo;
    }

    public function countAllFilteredActivePinPromo($promo_uid, $search)
    {
        $count_pin_promo = $this->where(function ($q) use($search) {
            $q->where('pin_promos.pin', 'like', '%'.$search.'%');
        })
        ->where('pin_promos.promo_uid', $promo_uid)
        ->where('pin_promos.delete', 0)
        ->count('pin_promos.uid');

        return $count_pin_promo;
    }

    public function getPinPromo($promo_uid, $start, $limit)
    {
        $pin_promo = $this->select('pin_promos.uid', 'pin_promos.pin')
        ->where('pin_promos.promo_uid', $promo_uid)
        ->where('pin_promos.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->get();

        return $pin_promo;
    }

    public function getFilteredPinPromo($promo_uid, $search, $start, $limit)
    {
        $pin_promo = $this->select('pin_promos.uid', 'pin_promos.pin')
        ->where(function ($q) use($search) {
            $q->where('pin_promos.pin', 'like', '%'.$search.'%');
        })
        ->where('pin_promos.promo_uid', $promo_uid)
        ->where('pin_promos.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->get();

        return $pin_promo;
    }

    public function verifyPromoPin($promo_uid, $pin)
    {
        $pin_promo = $this->select('pin_promos.uid')
        ->where('pin_promos.promo_uid', $promo_uid)
        ->where('pin_promos.pin', $pin)
        ->where('pin_promos.delete', 0)
        ->first();

        return $pin_promo;
    }

    public function postAddPinPromo($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('pin_promo'),
                'promo_uid' => $param['promo_uid'],
                'pin' => urlencode($param['pin']),
                'delete' => 0
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }

    public function postDeletePinPromo($promo_uid, $uid)
    {
    	$final = DB::transaction(function () use($promo_uid, $uid) {
            $this->where('pin_promos.promo_uid', $promo_uid)->whereIn('pin_promos.uid', $uid)->where('delete', 0)->update([
                'delete' => 1
            ]);

            return $this->success_delete_msg;
        });

        return $final;
    }
}
