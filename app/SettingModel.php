<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class SettingModel extends Model
{
    protected $table = 'settings';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'voucher_price', 'voucher_click_limit', 'voucher_generate_limit', 'visibility_remaining_click', 'server_maintenance', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di proses.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function getSetting()
    {
        $setting = $this->select('id', 'uid', 'voucher_price', 'voucher_click_limit', 'voucher_generate_limit', 'visibility_remaining_click', 'server_maintenance')
        ->where('delete', 0)
        ->first();

        return $setting;
    }

    public function getOneSetting($id)
    {
        $setting = $this->select('id', 'uid', 'voucher_price', 'voucher_click_limit', 'voucher_generate_limit', 'visibility_remaining_click', 'server_maintenance')
        ->where('id', $id)
        ->where('delete', 0)
        ->first();

        return $setting;
    }

    public function postAddSetting($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('settings'),
                'voucher_price' => $param['voucher_price'],
                'voucher_click_limit' => $param['voucher_click_limit'],
                'voucher_generate_limit' => $param['voucher_generate_limit'],
                'server_maintenance' => $param['server_maintenance'],
                'visibility_remaining_click' => $param['visibility_remaining_click'],
                'delete' => 0
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }

    public function postEditSetting($param, $id)
    {
        DB::transaction(function () use($param, $id) {
            $this->where('id', $id)->where('delete', 0)->update([
                'voucher_price' => $param['voucher_price'],
                'voucher_click_limit' => $param['voucher_click_limit'],
                'voucher_generate_limit' => $param['voucher_generate_limit'],
                'server_maintenance' => $param['server_maintenance'],
                'visibility_remaining_click' => $param['visibility_remaining_click'],
            ]);
        });

        return $this->success_update_msg;
    }
}
