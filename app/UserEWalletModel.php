<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class UserEWalletModel extends Model
{
    protected $table = 'user_ewallets';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'user_uid', 'no_hp', 'default', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di proses.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function countAllActiveUserEWallet($user_uid)
    {
        $count_user_ewallet = $this->where('user_ewallets.user_uid', $user_uid)
        ->where('user_ewallets.delete', 0)
        ->count('user_ewallets.uid');

        return $count_user_ewallet;
    }

    public function countAllFilteredActiveUserEWallet($search, $user_uid)
    {
        $count_user_ewallet = $this->where(function ($q) use($search) {
            $q->where('user_ewallets.no_hp', 'like', '%'.$search.'%');
        })
        ->where('user_ewallets.user_uid', $user_uid)
        ->where('user_ewallets.delete', 0)
        ->count('user_ewallets.uid');

        return $count_user_ewallet;
    }

    public function getUserEWallet($start, $limit, $order, $dir, $user_uid)
    {
        $bank_account = $this->select('user_ewallets.uid', 'user_ewallets.user_uid', 'user_ewallets.no_hp')
        
        ->where('user_ewallets.user_uid', $user_uid)
        ->where('user_ewallets.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $bank_account;
    }
    
    public function getFilteredUserEWallet($search, $start, $limit, $order, $dir, $user_uid)
    {
        $bank_account = $this->select('user_ewallets.uid', 'user_ewallets.user_uid', 'user_ewallets.no_hp')
        
        ->where(function ($q) use($search) {
            $q->where('user_ewallets.no_hp', 'like', '%'.$search.'%');
        })
        ->where('user_ewallets.user_uid', $user_uid)
        ->where('user_ewallets.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $bank_account;
    }

    public function getOneUserEWalletByUserUID($user_uid)
    {
        $bank_account = $this->select('user_ewallets.user_uid')
        ->where('user_ewallets.user_uid', $user_uid)
        ->where('user_ewallets.delete', 0)
        ->first();

        return $bank_account;
    }

    public function postAddUserEWallet($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('user_ewallets'),
                'user_uid' => $param['user_uid'],
                'no_hp' => $param['no_hp'],
                'default' => 0,
                'delete' => 0
            ]);
            
            $this->where('user_ewallets.user_uid', $param['user_uid'])->update([
                'default' => 0
            ]);

            $this->where('user_ewallets.id', $data->id)->where('user_ewallets.user_uid', $param['user_uid'])->update([
                'default' => 1
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }

    public function postEditUserEWallet($param, $uid)
    {
        DB::transaction(function () use($param, $uid) {
            $this->where('user_ewallets.uid', $uid)->where('user_ewallets.delete', 0)->update([
                'user_uid' => $param['user_uid'],
                'no_hp' => $param['no_hp'],
            ]);
        });

        return $this->success_update_msg;
    }

    public function postDeleteUserEWallet($uid)
    {
        DB::transaction(function () use($uid) {
            $this->where('user_ewallets.uid', $uid)->where('user_ewallets.delete', 0)->update([
                'user_ewallets.delete' => 1
            ]);
        });

        return $this->success_delete_msg;
    }
}
