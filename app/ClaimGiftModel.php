<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Auth, Hash, DB, Log;

class ClaimGiftModel extends Model
{
    protected $table = 'claim_gifts';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'user_uid', 'merchant_uid', 'hadiah_uid', 'nominal', 'amount', 'no_struk', 'no_reg', 'additional_note', 'img_struk', 'status', 'user_agent', 'ip', 'delete'];

    private $success_claim_gift_msg = 'Hadiah berhasil di proses';
    private $success_approve_tukar_struk_msg = 'Data tukar struk berhasil diubah';

    public function countAllActiveTukarStruk()
    {
        $countTukarStruk = $this->join('users', 'users.uid', '=', 'claim_gifts.user_uid')
        ->join('merchants', 'merchants.uid', '=', 'claim_gifts.merchant_uid')
        ->where('users.delete', 0)
        ->where('merchants.delete', 0)
        ->where('claim_gifts.delete', 0)
        ->count('claim_gifts.uid');

        return $countTukarStruk;
    }

    public function countAllFilteredActiveTukarStruk($search)
    {
        $countTukarStruk = $this->join('users', 'users.uid', '=', 'claim_gifts.user_uid')
        ->join('merchants', 'merchants.uid', '=', 'claim_gifts.merchant_uid')
        ->where(function ($q) use($search) {
            $q->where('users.fullname', 'like', '%'.$search.'%');
            $q->orWhere('users.phone', 'like', '%'.$search.'%');
            $q->orWhere('merchants.name', 'like', '%'.$search.'%');
            $q->orWhere('claim_gifts.no_struk', 'like', '%'.$search.'%');
        })
        ->where('users.delete', 0)
        ->where('merchants.delete', 0)
        ->where('claim_gifts.delete', 0)
        ->count('claim_gifts.uid');

        return $countTukarStruk;
    }

    public function getTukarStruk($start, $limit, $order, $dir)
    {
        $tukarStruk = $this->select('merchants.name', 'users.fullname', 'users.phone', 'claim_gifts.uid', 'claim_gifts.nominal', 'claim_gifts.amount', 'claim_gifts.no_struk', 'claim_gifts.img_struk', 'claim_gifts.status', 'claim_gifts.created_at')
        ->join('users', 'users.uid', '=', 'claim_gifts.user_uid')
        ->join('merchants', 'merchants.uid', '=', 'claim_gifts.merchant_uid')
        ->where('users.delete', 0)
        ->where('merchants.delete', 0)
        ->where('claim_gifts.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $tukarStruk;
    }
    
    public function getFilteredTukarStruk($search, $start, $limit, $order, $dir)
    {
        $tukarStruk = $this->select('merchants.name', 'users.fullname', 'users.phone', 'claim_gifts.uid', 'claim_gifts.nominal', 'claim_gifts.amount', 'claim_gifts.no_struk', 'claim_gifts.img_struk', 'claim_gifts.status', 'claim_gifts.created_at')
        ->join('users', 'users.uid', '=', 'claim_gifts.user_uid')
        ->join('merchants', 'merchants.uid', '=', 'claim_gifts.merchant_uid')
        ->where(function ($q) use($search) {
            $q->where('users.fullname', 'like', '%'.$search.'%');
            $q->orWhere('users.phone', 'like', '%'.$search.'%');
            $q->orWhere('merchants.name', 'like', '%'.$search.'%');
            $q->orWhere('claim_gifts.no_struk', 'like', '%'.$search.'%');
        })
        ->where('users.delete', 0)
        ->where('merchants.delete', 0)
        ->where('claim_gifts.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $tukarStruk;
    }

    public function getOneTukarStrukToVerify($cgUid)
    {
        $tukarStruk = $this->select('claim_gifts.uid', 'claim_gifts.user_uid', 'claim_gifts.merchant_uid', 'claim_gifts.nominal', 'claim_gifts.amount', 'claim_gifts.no_struk', 'claim_gifts.status')
        ->where('claim_gifts.uid', $cgUid)
        ->where('claim_gifts.delete', 0)
        ->first();

        return $tukarStruk;
    }

    public function getRecentHistoryClaimGift($user_uid)
    {
        $user = $this->select('merchants.name', 'claim_gifts.amount', 'claim_gifts.created_at', 'claim_gifts.status', 'claim_gifts.additional_note')
        ->join('merchants', 'merchants.uid', '=', 'claim_gifts.merchant_uid')
        ->where('claim_gifts.user_uid', $user_uid)
        ->limit(10)
        ->orderBy('created_at', 'desc')
        ->get();

        return $user;
    }

    public function postAddClaimGift($param)
    {
        DB::transaction(function () use($param) {
            $this->create([
                'uid' => HelperController::uid('claim_gifts'),
                'user_uid' => $param['user_uid'],
                'merchant_uid' => $param['merchant_uid'],
                'hadiah_uid' => $param['hadiah_uid'],
                'nominal' => $param['nominal'],
                'amount' => $param['amount'],
                'no_struk' => $param['no_struk'],
                'no_reg' => $param['no_reg'],
                'img_struk' => $param['img_struk_path'],
                'status' => 0,
                'user_agent' => $param['user_agent'],
                'ip' => $param['ip'],
                'delete' => 0
            ]);
        });

        return $this->success_claim_gift_msg;
    }

    public function postUpdateRedeemClaimGift($param)
    {
        DB::transaction(function () use($id) {
            $this->where('user_uid', $param['user_uid'])->where('redeem', 0)->update([
                'redeem' => 1
            ]);
        });

        return $this->success_approve_msg;
    }

    public function postApproveTukarStruk($param)
    {
        DB::transaction(function () use($param) {
            $this->where('claim_gifts.uid', $param['cgUid'])->where('delete', 0)->update([
                'status' => 1,
                'additional_note' => $param['additional_note']
            ]);
        });

        return $this->success_approve_tukar_struk_msg;
    }

    public function postRejectTukarStruk($param)
    {
        DB::transaction(function () use($param) {
            $this->where('claim_gifts.uid', $param['cgUid'])->where('delete', 0)->update([
                'status' => 2,
                'additional_note' => $param['additional_note']
            ]);
        });

        return $this->success_approve_tukar_struk_msg;
    }
}
