<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Auth, Hash, DB, Log;

class TaskModel extends Model
{
    protected $table = 'tasks';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'user_uid', 'type', 'status', 'description', 'delete'];

    public function getOneTask($user_uid, $type) 
    {
    	$task = $this->select('status')
        ->where('user_uid', $user_uid)
        ->where('type', $type)
        ->where('status', 0)
        ->where('delete', 0)
        ->first();

        return $task;
    }

    public function postAddTask($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('tasks'),
                'user_uid' => $param['user_uid'],
                'type' => $param['type'],
                'status' => $param['status'],
                'description' => $param['description'],
                'delete' => 0
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }

    public function postUpdateStatusTask($task_id)
    {
        DB::transaction(function () use($task_id) {
            $this->where('id', $task_id)->where('delete', 0)->update([
                'status' => 1
            ]);
        });

        return $this->success_update_msg;
    }
}
