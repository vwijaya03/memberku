<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class DataCustomerModel extends Model
{
    protected $table = 'data_customers';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'user_uid', 'fullname', 'email', 'phone', 'address', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di proses.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function postAddDataCustomer($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('unique_codes'),
                'user_uid' => $param['user_uid'],
                'fullname' => $param['fullname'],
                'email' => $param['email'],
                'phone' => $param['phone'],
                'address' => $param['address'],
                'delete' => 0
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }
}
