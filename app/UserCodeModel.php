<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class UserCodeModel extends Model
{
    protected $table = 'user_codes';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'user_uid', 'code', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di proses.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function getOneUserCodeByUserUID($user_uid) {
        $user_code = $this->select('id', 'uid', 'user_uid', 'code')
        ->where('user_uid', $user_uid)
        ->where('delete', 0)
        ->first();

        return $user_code;
    }

    public function getOneUserCodeByCode($code) {
        $user_code = $this->select('id', 'uid', 'user_uid', 'code')
        ->where('code', $code)
        ->where('delete', 0)
        ->first();

        return $user_code;
    }

    public function getOneUserCodeAndUserData($code) {
        $user_code = $this->select('user_codes.id', 'user_codes.uid', 'user_codes.user_uid', 'user_codes.code', 'users.type')
        ->join('users', 'users.uid', '=', 'user_codes.user_uid')
        ->where('user_codes.code', $code)
        ->where('user_codes.delete', 0)
        ->where('users.delete', 0)
        ->first();

        return $user_code;
    }

    public function getOneUserCodeByCodeForRecursive($code) {
        $user_code = $this->select('user_codes.id', 'user_codes.uid', 'user_codes.user_uid', 'user_codes.code', 'users.type')
        ->join('users', 'users.uid', '=', 'user_codes.user_uid')
        ->where('user_codes.code', $code)
        ->where('user_codes.delete', 0)
        ->where('users.delete', 0)
        ->first();

        return $user_code;
    }

    public function postAddResetUserCode($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('user_codes'),
                'user_uid' => $param['user_uid'],
                'code' => HelperController::generateAgentCode($param['id']),
                'delete' => 0
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }
}
