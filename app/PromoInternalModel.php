<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class PromoInternalModel extends Model
{
    protected $table = 'promos_internal';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'user_uid', 'title', 'description', 'img', 'harga_awal', 'harga_akhir', 'use_pin', 'expired_date', 'top_promo', 'sequence', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_update_sequence_msg = 'Urutan data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di proses.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function countAllPromoInternal($user_uid) 
    {
        $count_promo = $this
        ->where('promos_internal.user_uid', $user_uid)
        ->count('promos_internal.id');

        return $count_promo;
    }

    public function countAllActivePromoInternal()
    {
        $count_promo = $this
        ->join('users', 'users.uid', '=', 'promos_internal.user_uid')
        ->where('promos_internal.delete', 0)
        ->where('users.delete', 0)
        ->count('promos_internal.id');

        return $count_promo;
    }

    public function countAllFilteredActivePromoInternal($search)
    {
        $count_promo = $this
        ->join('users', 'users.uid', '=', 'promos_internal.user_uid')
        ->where(function ($q) use($search) {
            $q->where('users.fullname', 'like', '%'.$search.'%');
            $q->orWhere('promos_internal.title', 'like', '%'.$search.'%');
            $q->orWhere('promos_internal.description', 'like', '%'.$search.'%');
            $q->orWhere('promos_internal.img', 'like', '%'.$search.'%');
            $q->orWhere('promos_internal.harga_awal', 'like', '%'.$search.'%');
            $q->orWhere('promos_internal.harga_akhir', 'like', '%'.$search.'%');
        })
        ->where('promos_internal.delete', 0)
        ->where('users.delete', 0)
        ->count('promos_internal.id');

        return $count_promo;
    }

    public function getPromoInternal($start, $limit, $order, $dir)
    {
        $promo = $this->select('users.uid as user_uid', 'users.fullname', 'promos_internal.id', 'promos_internal.uid', 'promos_internal.user_uid', 'promos_internal.title', 'promos_internal.description', 'promos_internal.img', 'promos_internal.harga_awal', 'promos_internal.harga_akhir', 'promos_internal.pin', 'promos_internal.use_pin', 'promos_internal.sequence', 'promos_internal.expired_date', 'promos_internal.top_promo')
        ->join('users', 'users.uid', '=', 'promos_internal.user_uid')
        ->where('promos_internal.delete', 0)
        ->where('users.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $promo;
    }
    
    public function getFilteredPromoInternal($search, $start, $limit, $order, $dir)
    {
        $promo = $this->select('users.uid as user_uid', 'users.fullname', 'promos_internal.id', 'promos_internal.uid', 'promos_internal.user_uid', 'promos_internal.title', 'promos_internal.description', 'promos_internal.img', 'promos_internal.harga_awal', 'promos_internal.harga_akhir', 'promos_internal.pin', 'promos_internal.use_pin', 'promos_internal.sequence', 'promos_internal.expired_date', 'promos_internal.top_promo')
        ->join('users', 'users.uid', '=', 'promos_internal.user_uid')
        ->where(function ($q) use($search) {
            $q->where('users.fullname', 'like', '%'.$search.'%');
            $q->orWhere('promos_internal.title', 'like', '%'.$search.'%');
            $q->orWhere('promos_internal.description', 'like', '%'.$search.'%');
            $q->orWhere('promos_internal.img', 'like', '%'.$search.'%');
            $q->orWhere('promos_internal.harga_awal', 'like', '%'.$search.'%');
            $q->orWhere('promos_internal.harga_akhir', 'like', '%'.$search.'%');
        })
        ->where('promos_internal.delete', 0)
        ->where('users.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $promo;
    }

    /* ------------------------------------------ Promo Internal Per Partner ------------------------------------------- */

    public function countAllActivePromoInternalPartner($user_uid)
    {
        $count_promo = $this
        ->join('users', 'users.uid', '=', 'promos_internal.user_uid')
        ->where('promos_internal.user_uid', $user_uid)
        ->where('promos_internal.delete', 0)
        ->where('users.delete', 0)
        ->count('promos_internal.id');

        return $count_promo;
    }

    public function countAllFilteredActivePromoInternalPartner($search, $user_uid)
    {
        $count_promo = $this
        ->join('users', 'users.uid', '=', 'promos_internal.user_uid')
        ->where(function ($q) use($search) {
            $q->where('users.fullname', 'like', '%'.$search.'%');
            $q->orWhere('promos_internal.title', 'like', '%'.$search.'%');
            $q->orWhere('promos_internal.description', 'like', '%'.$search.'%');
            $q->orWhere('promos_internal.img', 'like', '%'.$search.'%');
            $q->orWhere('promos_internal.harga_awal', 'like', '%'.$search.'%');
            $q->orWhere('promos_internal.harga_akhir', 'like', '%'.$search.'%');
        })
        ->where('promos_internal.user_uid', $user_uid)
        ->where('promos_internal.delete', 0)
        ->where('users.delete', 0)
        ->count('promos_internal.id');

        return $count_promo;
    }

    public function getPromoInternalPartner($start, $limit, $order, $dir, $user_uid)
    {
        $promo = $this->select('users.uid as user_uid', 'users.fullname', 'promos_internal.id', 'promos_internal.uid', 'promos_internal.user_uid', 'promos_internal.title', 'promos_internal.description', 'promos_internal.img', 'promos_internal.harga_awal', 'promos_internal.harga_akhir', 'promos_internal.pin', 'promos_internal.use_pin', 'promos_internal.sequence', 'promos_internal.expired_date', 'promos_internal.top_promo')
        ->join('users', 'users.uid', '=', 'promos_internal.user_uid')
        ->where('promos_internal.user_uid', $user_uid)
        ->where('promos_internal.delete', 0)
        ->where('users.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $promo;
    }
    
    public function getFilteredPromoInternalPartner($search, $start, $limit, $order, $dir, $user_uid)
    {
        $promo = $this->select('users.uid as user_uid', 'users.fullname', 'promos_internal.id', 'promos_internal.uid', 'promos_internal.user_uid', 'promos_internal.title', 'promos_internal.description', 'promos_internal.img', 'promos_internal.harga_awal', 'promos_internal.harga_akhir', 'promos_internal.pin', 'promos_internal.use_pin', 'promos_internal.sequence', 'promos_internal.expired_date', 'promos_internal.top_promo')
        ->join('users', 'users.uid', '=', 'promos_internal.user_uid')
        ->where(function ($q) use($search) {
            $q->where('users.fullname', 'like', '%'.$search.'%');
            $q->orWhere('promos_internal.title', 'like', '%'.$search.'%');
            $q->orWhere('promos_internal.description', 'like', '%'.$search.'%');
            $q->orWhere('promos_internal.img', 'like', '%'.$search.'%');
            $q->orWhere('promos_internal.harga_awal', 'like', '%'.$search.'%');
            $q->orWhere('promos_internal.harga_akhir', 'like', '%'.$search.'%');
        })
        ->where('promos_internal.user_uid', $user_uid)
        ->where('promos_internal.delete', 0)
        ->where('users.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $promo;
    }

    /* ----------------------------------------------------------------------------------------------------------------- */

    public function getOnePromoInternal($id)
    {
        $promo = $this->select('users.uid as user_uid', 'users.fullname', 'promos_internal.id', 'promos_internal.uid', 'promos_internal.user_uid', 'promos_internal.title', 'promos_internal.description', 'promos_internal.img', 'promos_internal.harga_awal', 'promos_internal.harga_akhir', 'promos_internal.pin', 'promos_internal.use_pin', 'promos_internal.sequence', 'promos_internal.expired_date', 'promos_internal.top_promo')
        ->join('users', 'users.uid', '=', 'promos_internal.user_uid')
        ->where('promos_internal.id', $id)
        ->where('promos_internal.delete', 0)
        ->where('users.delete', 0)
        ->first();

        return $promo;
    }

    public function getOnePromoInternalByUID($uid)
    {
        $promo = $this->select('users.uid as user_uid', 'users.fullname', 'promos_internal.id', 'promos_internal.uid', 'promos_internal.user_uid', 'promos_internal.title', 'promos_internal.description', 'promos_internal.img', 'promos_internal.harga_awal', 'promos_internal.harga_akhir', 'promos_internal.pin', 'promos_internal.use_pin', 'promos_internal.sequence', 'promos_internal.expired_date', 'promos_internal.top_promo')
        ->join('users', 'users.uid', '=', 'promos_internal.user_uid')
        ->where('promos_internal.uid', $uid)
        ->where('promos_internal.delete', 0)
        ->where('users.delete', 0)
        ->first();

        return $promo;
    }

    public function verifyPromoInternalExpiredDate($uid)
    {
        $promo = $this->select('promos_internal.expired_date')
        ->where('promos_internal.uid', $uid)
        ->where('promos_internal.delete', 0)
        ->first();

        return $promo;
    }

    public function verifyPromoInternalPin($uid, $pin)
    {
        $promo = $this->select('promos_internal.uid')
        ->where('promos_internal.uid', $uid)
        ->where('promos_internal.pin', $pin)
        ->where('promos_internal.delete', 0)
        ->first();

        return $promo;
    }

    public function getTopPromoInternal() {
        $promo = $this->select('users.fullname', 'promos_internal.uid', 'promos_internal.title')
        ->join('users', 'users.uid', '=', 'promos_internal.user_uid')
        ->where('promos_internal.top_promo', 'yes')
        ->where('promos_internal.delete', 0)
        ->where('promos_internal.expired_date', '>=', date('Y-m-d H:i:s'))
        ->where('users.delete', 0)
        ->get();

        return $promo;
    }

    public function getForPublicPromoInternal($user_uid) {
        $promo = $this->select('users.uid as user_uid', 'users.fullname', 'promos_internal.id', 'promos_internal.uid', 'promos_internal.user_uid', 'promos_internal.title', 'promos_internal.description', 'promos_internal.img', 'promos_internal.harga_awal', 'promos_internal.harga_akhir', 'promos_internal.pin', 'promos_internal.use_pin', 'promos_internal.sequence', 'promos_internal.expired_date', 'promos_internal.top_promo')
        ->join('users', 'users.uid', '=', 'promos_internal.user_uid')
        // ->where('promos_internal.top_promo', 'yes')
        ->where('promos_internal.delete', 0)
        ->where('promos_internal.expired_date', '>=', date('Y-m-d H:i:s'))
        ->where('users.uid', $user_uid)
        ->where('users.delete', 0)
        ->get();

        return $promo;
    }

    public function getAllPromoInternal() {
        $promo = $this->select('users.uid as user_uid', 'users.fullname', 'promos_internal.id', 'promos_internal.uid', 'promos_internal.user_uid', 'promos_internal.title', 'promos_internal.description', 'promos_internal.img', 'promos_internal.harga_awal', 'promos_internal.harga_akhir', 'promos_internal.expired_date', 'promos_internal.top_promo', 'promos_internal.use_pin')
        ->join('users', 'users.uid', '=', 'promos_internal.user_uid')
        ->where('promos_internal.top_promo', 'yes')
        ->where('promos_internal.delete', 0)
        ->where('promos_internal.expired_date', '>=', date('Y-m-d H:i:s'))
        ->where('users.delete', 0)
        ->orderBy('promos_internal.sequence', 'asc')
        ->get();

        return $promo;
    }

    public function getAllPromoInternalForSettingPin() {
        $promo = $this->select('users.fullname', 'promos_internal.uid', 'promos_internal.title')
        ->join('users', 'users.uid', '=', 'promos_internal.user_uid')
        ->where('promos_internal.delete', 0)
        ->where('promos_internal.expired_date', '>=', date('Y-m-d H:i:s'))
        ->where('users.delete', 0)
        ->orderBy('users.fullname', 'asc')
        ->get();

        return $promo;
    }

    public function postAddPromoInternal($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('promos_internal'),
                'user_uid' => $param['partner'],
                'title' => urlencode($param['title']),
                'description' => urlencode($param['description']),
                'img' => $param['img_path'],
                'harga_awal' => $param['harga_awal'],
                'harga_akhir' => $param['harga_akhir'],
                'use_pin' => $param['use_pin'],
                'expired_date' => $param['expired_date'],
                'top_promo' => $param['top_promo'],
                'sequence' => $param['sequence'],
                'delete' => 0
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }

    public function postEditPromoInternal($param, $id)
    {
        DB::transaction(function () use($param, $id) {
            if($param['img_path'] != null || $param['img_path'] != '')
            {
                $this->where('promos_internal.id', $id)->where('promos_internal.delete', 0)->update([
                    'user_uid' => $param['partner'],
                    'title' => urlencode($param['title']),
                    'description' => urlencode($param['description']),
                    'img' => $param['img_path'],
                    'harga_awal' => $param['harga_awal'],
                    'harga_akhir' => $param['harga_akhir'],
                    'use_pin' => $param['use_pin'],
                    'expired_date' => $param['expired_date'],
                    'top_promo' => $param['top_promo']
                ]);
            }
            else
            {
                $this->where('promos_internal.id', $id)->where('promos_internal.delete', 0)->update([
                    'user_uid' => $param['partner'],
                    'title' => urlencode($param['title']),
                    'description' => urlencode($param['description']),
                    'harga_awal' => $param['harga_awal'],
                    'harga_akhir' => $param['harga_akhir'],
                    'use_pin' => $param['use_pin'],
                    'expired_date' => $param['expired_date'],
                    'top_promo' => $param['top_promo']
                ]);
            }
            
        });

        return $this->success_update_msg;
    }

    public function postUpdateSequencePromoInternal($param)
    {
        DB::transaction(function () use($param) {

            for ($i = 0; $i < count($param['promo_uid']); $i++) {
                
                $this->where('uid', $param['promo_uid'][$i])->where('promos_internal.delete', 0)->update([
                    'sequence' => $param['new_position'][$i]
                ]);
            }
            
        });

        return $this->success_update_sequence_msg;
    }

    public function postDeletePromoInternal($id)
    {
        DB::transaction(function () use($id) {
            $this->where('id', $id)->where('promos_internal.delete', 0)->update([
                'delete' => 1
            ]);
        });

        return $this->success_delete_msg;
    }
}
