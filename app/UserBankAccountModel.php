<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class UserBankAccountModel extends Model
{
    protected $table = 'user_bank_accounts';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'user_uid', 'bank_uid', 'no_rek', 'default', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di proses.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function countAllActiveBankAccount($user_uid)
    {
        $count_user_bank_account = $this->join('banks', 'banks.uid', '=', 'user_bank_accounts.bank_uid')
        ->where('user_bank_accounts.user_uid', $user_uid)
        ->where('user_bank_accounts.delete', 0)
        ->where('banks.delete', 0)
        ->count('user_bank_accounts.uid');

        return $count_user_bank_account;
    }

    public function countAllFilteredActiveBankAccount($search, $user_uid)
    {
        $count_user_bank_account = $this->join('banks', 'banks.uid', '=', 'user_bank_accounts.bank_uid')
        ->where(function ($q) use($search) {
            $q->where('user_bank_accounts.no_rek', 'like', '%'.$search.'%');
            $q->orWhere('banks.name', 'like', '%'.urlencode($search).'%');
        })
        ->where('user_bank_accounts.user_uid', $user_uid)
        ->where('user_bank_accounts.delete', 0)
        ->where('banks.delete', 0)
        ->count('user_bank_accounts.uid');

        return $count_user_bank_account;
    }

    public function getBankAccount($start, $limit, $order, $dir, $user_uid)
    {
        $bank_account = $this->select('user_bank_accounts.uid', 'user_bank_accounts.user_uid', 'user_bank_accounts.no_rek', 'banks.name')
        ->join('banks', 'banks.uid', '=', 'user_bank_accounts.bank_uid')
        ->where('user_bank_accounts.user_uid', $user_uid)
        ->where('user_bank_accounts.delete', 0)
        ->where('banks.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $bank_account;
    }
    
    public function getFilteredBankAccount($search, $start, $limit, $order, $dir, $user_uid)
    {
        $bank_account = $this->select('user_bank_accounts.uid', 'user_bank_accounts.user_uid', 'user_bank_accounts.no_rek', 'banks.name')
        ->join('banks', 'banks.uid', '=', 'user_bank_accounts.bank_uid')
        ->where(function ($q) use($search) {
            $q->where('user_bank_accounts.no_rek', 'like', '%'.$search.'%');
            $q->orWhere('banks.name', 'like', '%'.urlencode($search).'%');
        })
        ->where('user_bank_accounts.user_uid', $user_uid)
        ->where('user_bank_accounts.delete', 0)
        ->where('banks.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $bank_account;
    }

    public function getOneBankAccountByUserUID($user_uid)
    {
        $bank_account = $this->select('user_bank_accounts.user_uid')
        ->join('banks', 'banks.uid', '=', 'user_bank_accounts.bank_uid')
        ->where('user_bank_accounts.user_uid', $user_uid)
        ->where('user_bank_accounts.delete', 0)
        ->where('banks.delete', 0)
        ->first();

        return $bank_account;
    }

    public function getBankAccountByUserUID($user_uid)
    {
        $bank_account = $this->select('user_bank_accounts.user_uid', 'user_bank_accounts.no_rek', 'banks.name')
        ->join('banks', 'banks.uid', '=', 'user_bank_accounts.bank_uid')
        ->where('user_bank_accounts.user_uid', $user_uid)
        ->where('user_bank_accounts.delete', 0)
        ->where('banks.delete', 0)
        ->get();

        return $bank_account;
    }

    public function postAddBankAccount($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('bank_accounts'),
                'user_uid' => $param['user_uid'],
                'bank_uid' => $param['bank_uid'],
                'no_rek' => $param['no_rek'],
                'default' => 0,
                'delete' => 0
            ]);
            
            $this->where('user_bank_accounts.user_uid', $param['user_uid'])->update([
                'default' => 0
            ]);

            $this->where('user_bank_accounts.id', $data->id)->where('user_bank_accounts.user_uid', $param['user_uid'])->update([
                'default' => 1
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }

    public function postEditBankAccount($param, $uid)
    {
        DB::transaction(function () use($param, $uid) {
            $this->where('user_bank_accounts.uid', $uid)->where('user_bank_accounts.delete', 0)->update([
                'user_uid' => $param['user_uid'],
                'bank_uid' => $param['bank_uid'],
                'no_rek' => $param['no_rek']
            ]);
        });

        return $this->success_update_msg;
    }

    public function postDeleteBankAccount($uid)
    {
        DB::transaction(function () use($uid) {
            $this->where('user_bank_accounts.uid', $uid)->where('user_bank_accounts.delete', 0)->update([
                'user_bank_accounts.delete' => 1
            ]);
        });

        return $this->success_delete_msg;
    }
}
