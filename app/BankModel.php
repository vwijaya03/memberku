<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class BankModel extends Model
{
    protected $table = 'banks';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'name', 'type', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di proses.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function countAllActiveBank()
    {
        $count_bank = $this->where('delete', 0)
        ->count('id');

        return $count_bank;
    }

    public function countAllFilteredActiveBank($search)
    {
        $count_bank = $this->where(function ($q) use($search) {
            $q->where('name', 'like', '%'.$search.'%');
            $q->orWhere('type', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->count('id');

        return $count_bank;
    }

    public function getBank($start, $limit, $order, $dir)
    {
        $bank = $this->select('uid', 'name', 'type')
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $bank;
    }
    
    public function getFilteredBank($search, $start, $limit, $order, $dir)
    {
        $bank = $this->select('uid', 'name', 'type')
        ->where(function ($q) use($search) {
            $q->where('name', 'like', '%'.$search.'%');
            $q->orWhere('type', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $bank;
    }

    public function getAllBank()
    {
        $bank = $this->select('uid', 'name')
        ->where('delete', 0)
        ->get();

        return $bank;
    }

    public function getSearchSelect2Bank($search)
    {
        $bank = $this->select('uid', 'name')
        ->where('name', 'like', '%'.$search.'%')
        ->where('delete', 0)
        ->orderBy('name','asc')
        ->paginate(30);

        return $bank;
    }

    public function getOneBankByUID($uid)
    {
        $bank = $this->select('uid', 'name')
        ->where('uid', $uid)
        ->where('delete', 0)
        ->first();

        return $bank;
    }

    public function postAddBank($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('banks'),
                'name' => urlencode($param['name']),
                'type' => $param['type'],
                'delete' => 0
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }

    public function postEditBank($param, $uid)
    {
        DB::transaction(function () use($param, $uid) {
            $this->where('uid', $uid)->where('delete', 0)->update([
                'name' => urlencode($param['name']),
                'type' => $param['type'],
            ]);
        });

        return $this->success_update_msg;
    }

    public function postDeleteBank($uid)
    {
        DB::transaction(function () use($uid) {
            $this->where('uid', $uid)->where('delete', 0)->update([
                'delete' => 1
            ]);
        });

        return $this->success_delete_msg;
    }
}
