<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Http\Controllers\HelperController;
use App\SettingUniqueCodePartnerModel;
use App\UserCodeModel;
use App\UserCompanyModel;
use App\UserBalanceModel;
use Auth, Hash, DB, Log;

class UserModel extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';

    protected $primaryKey = 'id';

    protected $fillable = [
        'uid', 'fullname', 'name_on_card', 'email', 'password', 'phone', 'address', 'city', 'type', 'role', 'approve', 'delete',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di tambahkan.';
    private $success_delete_msg = 'Data berhasil di hapus.';
    private $success_approve_msg = 'Data berhasil di approve.';
    private $success_reject_msg = 'Data berhasil di reject.';

    public function getSearchSelect2Partner($search)
    {
        $partner = $this->select('id', 'uid', 'fullname')
        ->where('fullname', 'like', '%'.$search.'%')
        ->where('role', 'partner')
        ->where('delete', 0)
        ->orderBy('fullname','asc')
        ->paginate(30);

        return $partner;
    }

    public function countAllActiveUser($role, $approve = 1)
    {
        $count_user = $this->select('id')
        ->where('role', $role)
        ->where('approve', $approve)
        ->where('delete', 0)
        ->count();

        return $count_user;
    }

    public function countAllFilteredActiveUser($search, $role, $approve = 1)
    {
        $count_user = $this->select('id')
        ->where(function ($q) use($search) {
            $q->where('fullname', 'like', '%'.$search.'%');
            $q->orWhere('name_on_card', 'like', '%'.$search.'%');
            $q->orWhere('email', 'like', '%'.$search.'%');
            $q->orWhere('phone', 'like', '%'.$search.'%');
            $q->orWhere('address', 'like', '%'.$search.'%');
            $q->orWhere('city', 'like', '%'.$search.'%');
        })
        ->where('role', $role)
        ->where('approve', $approve)
        ->where('delete', 0)
        ->count();

        return $count_user;
    }

    public function getUser($start, $limit, $order, $dir, $role, $approve = 1)
    {
        $user = $this->select('id', 'uid', 'fullname', 'name_on_card', 'email', 'phone', 'address', 'city', 'role')
        ->where('role', $role)
        ->where('approve', $approve)
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $user;
    }
    
    public function getFilteredUser($search, $start, $limit, $order, $dir, $role, $approve = 1)
    {
        $user = $this->select('id', 'uid', 'fullname', 'name_on_card', 'email', 'phone', 'address', 'city', 'role')
        ->where(function ($q) use($search) {
            $q->where('fullname', 'like', '%'.$search.'%');
            $q->orWhere('name_on_card', 'like', '%'.$search.'%');
            $q->orWhere('email', 'like', '%'.$search.'%');
            $q->orWhere('phone', 'like', '%'.$search.'%');
            $q->orWhere('address', 'like', '%'.$search.'%');
            $q->orWhere('city', 'like', '%'.$search.'%');
        })
        ->where('role', $role)
        ->where('approve', $approve)
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $user;
    }

    /* ---------------------------------------------------------- Agent ---------------------------------------------------------------- */ 

    public function countAllActiveAgent()
    {
        $count_user = $this->select('id')
        ->where('role', 'agent')
        ->where('delete', 0)
        ->count();

        return $count_user;
    }

    public function countAllFilteredActiveAgent($search)
    {
        $count_user = $this->select('id')
        ->where(function ($q) use($search) {
            $q->where('fullname', 'like', '%'.$search.'%');
            $q->orWhere('email', 'like', '%'.$search.'%');
            $q->orWhere('type', 'like', '%'.$search.'%');
        })
        ->where('role', 'agent')
        ->where('delete', 0)
        ->count();

        return $count_user;
    }

    public function getAgent($start, $limit, $order, $dir)
    {
        $user = $this->select('id', 'uid', 'fullname', 'email', 'phone', 'type', 'role')
        ->where('role', 'agent')
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $user;
    }
    
    public function getFilteredAgent($search, $start, $limit, $order, $dir)
    {
        $user = $this->select('id', 'uid', 'fullname', 'email', 'phone', 'type', 'role')
        ->where(function ($q) use($search) {
            $q->where('fullname', 'like', '%'.$search.'%');
            $q->orWhere('email', 'like', '%'.$search.'%');
            $q->orWhere('type', 'like', '%'.$search.'%');
        })
        ->where('role', 'agent')
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $user;
    }

    public function countAllActiveAgentByCurrentUserUID($referral_code, $type)
    {
        $count_user = $this->join('user_referral_codes', 'user_referral_codes.user_uid', '=', 'users.uid')
        ->join('user_balance', 'user_balance.user_uid', '=', 'users.uid')
        ->join('user_codes', 'user_codes.user_uid', '=', 'users.uid')
        ->where('users.role', 'agent')
        ->where('user_referral_codes.referral_code', $referral_code)
        ->where('user_balance.type', $type)
        ->where('users.delete', 0)
        ->count('users.id');

        return $count_user;
    }

    public function countAllFilteredActiveAgentByCurrentUserUID($search, $referral_code, $type)
    {
        $count_user = $this->join('user_referral_codes', 'user_referral_codes.user_uid', '=', 'users.uid')
        ->join('user_balance', 'user_balance.user_uid', '=', 'users.uid')
        ->join('user_codes', 'user_codes.user_uid', '=', 'users.uid')
        ->where(function ($q) use($search) {
            $q->where('users.fullname', 'like', '%'.$search.'%');
            $q->orWhere('users.phone', 'like', '%'.$search.'%');
            // $q->orWhere('users.email', 'like', '%'.$search.'%');
        })
        ->where('users.role', 'agent')
        ->where('user_referral_codes.referral_code', $referral_code)
        ->where('user_balance.type', $type)
        ->where('users.delete', 0)
        ->count('users.id');

        return $count_user;
    }

    public function getAgentByCurrentUserUID($start, $limit, $order, $dir, $referral_code, $type)
    {
        $user = $this->select('users.id', 'users.uid', 'users.fullname', 'users.email', 'users.phone', 'users.type', 'users.role', 'user_balance.balance', 'user_codes.code')
        ->join('user_balance', 'user_balance.user_uid', '=', 'users.uid')
        ->join('user_referral_codes', 'user_referral_codes.user_uid', '=', 'users.uid')
        ->join('user_codes', 'user_codes.user_uid', '=', 'users.uid')
        ->where('users.role', 'agent')
        ->where('user_referral_codes.referral_code', $referral_code)
        ->where('user_balance.type', $type)
        ->where('users.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $user;
    }
    
    public function getFilteredAgentByCurrentUserUID($search, $start, $limit, $order, $dir, $referral_code, $type)
    {
        $user = $this->select('users.id', 'users.uid', 'users.fullname', 'users.email', 'users.phone', 'users.type', 'users.role', 'user_balance.balance', 'user_codes.code')
        ->join('user_balance', 'user_balance.user_uid', '=', 'users.uid')
        ->join('user_referral_codes', 'user_referral_codes.user_uid', '=', 'users.uid')
        ->join('user_codes', 'user_codes.user_uid', '=', 'users.uid')
        ->where(function ($q) use($search) {
            $q->where('users.fullname', 'like', '%'.$search.'%');
            $q->orWhere('users.phone', 'like', '%'.$search.'%');
            // $q->orWhere('users.email', 'like', '%'.$search.'%');
        })
        ->where('users.role', 'agent')
        ->where('user_referral_codes.referral_code', $referral_code)
        ->where('user_balance.type', $type)
        ->where('users.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $user;
    }

    /* ---------------------------------------------------------- End Of Agent ---------------------------------------------------------------- */ 

    /* ---------------------------------------------------------- Review Agent Logo ------------------------------------------------------------- */
    public function countAllActiveReviewAgentLogo()
    {
        $count_user = $this->join('logos', 'logos.user_uid', '=', 'users.uid')
        ->where('users.role', 'agent')
        ->where('logos.approve', 0)
        ->where('users.delete', 0)
        ->where('logos.delete', 0)
        ->count('users.id');

        return $count_user;
    }

    public function countAllFilteredActiveReviewAgentLogo($search)
    {
        $count_user = $this->join('logos', 'logos.user_uid', '=', 'users.uid')
        ->where(function ($q) use($search) {
            $q->where('users.fullname', 'like', '%'.$search.'%');
            $q->orWhere('users.email', 'like', '%'.$search.'%');
        })
        ->where('users.role', 'agent')
        ->where('logos.approve', 0)
        ->where('users.delete', 0)
        ->where('logos.delete', 0)
        ->count('users.id');

        return $count_user;
    }

    public function getReviewAgentLogo($start, $limit, $order, $dir)
    {
        $user = $this->select('users.id', 'users.uid', 'users.fullname', 'users.email', 'users.phone', 'logos.img')
        ->join('logos', 'logos.user_uid', '=', 'users.uid')
        ->where('users.role', 'agent')
        ->where('logos.approve', 0)
        ->where('users.delete', 0)
        ->where('logos.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $user;
    }
    
    public function getFilteredReviewAgentLogo($search, $start, $limit, $order, $dir)
    {
        $user = $this->select('users.id', 'users.uid', 'users.fullname', 'users.email', 'users.phone', 'logos.img')
        ->join('logos', 'logos.user_uid', '=', 'users.uid')
        ->where(function ($q) use($search) {
            $q->where('users.fullname', 'like', '%'.$search.'%');
            $q->orWhere('users.email', 'like', '%'.$search.'%');
        })
        ->where('users.role', 'agent')
        ->where('logos.approve', 0)
        ->where('users.delete', 0)
        ->where('logos.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $user;
    }

    /* ---------------------------------------------------------- End Of Review Agent Logo ---------------------------------------------------- */

    /* ---------------------------------------------------------- Review Agent Description ------------------------------------------------------ */
    public function countAllActiveReviewAgentDescription()
    {
        $count_user = $this->join('link_titles', 'link_titles.user_uid', '=', 'users.uid')
        ->where('users.role', 'agent')
        ->where('link_titles.approve', 0)
        ->where('users.delete', 0)
        ->where('link_titles.delete', 0)
        ->count('users.id');

        return $count_user;
    }

    public function countAllFilteredActiveReviewAgentDescription($search)
    {
        $count_user = $this->join('link_titles', 'link_titles.user_uid', '=', 'users.uid')
        ->where(function ($q) use($search) {
            $q->where('users.fullname', 'like', '%'.$search.'%');
            $q->orWhere('users.email', 'like', '%'.$search.'%');
        })
        ->where('users.role', 'agent')
        ->where('link_titles.approve', 0)
        ->where('users.delete', 0)
        ->where('link_titles.delete', 0)
        ->count('users.id');

        return $count_user;
    }

    public function getReviewAgentDescription($start, $limit, $order, $dir)
    {
        $user = $this->select('users.id', 'users.uid', 'users.fullname', 'users.email', 'users.phone', 'link_titles.description')
        ->join('link_titles', 'link_titles.user_uid', '=', 'users.uid')
        ->where('users.role', 'agent')
        ->where('link_titles.approve', 0)
        ->where('users.delete', 0)
        ->where('link_titles.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $user;
    }
    
    public function getFilteredReviewAgentDescription($search, $start, $limit, $order, $dir)
    {
        $user = $this->select('users.id', 'users.uid', 'users.fullname', 'users.email', 'users.phone', 'link_titles.description')
        ->join('link_titles', 'link_titles.user_uid', '=', 'users.uid')
        ->where(function ($q) use($search) {
            $q->where('users.fullname', 'like', '%'.$search.'%');
            $q->orWhere('users.email', 'like', '%'.$search.'%');
        })
        ->where('users.role', 'agent')
        ->where('link_titles.approve', 0)
        ->where('users.delete', 0)
        ->where('link_titles.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $user;
    }

    /* ---------------------------------------------------------- End Of Review Agent Description ----------------------------------------------- */

    public function getOneUser($id, $role, $approve = 1)
    {
        if($role != null || $role != '')
        {
            $admin = $this->select('id', 'uid', 'fullname', 'name_on_card', 'email', 'phone', 'address', 'city', 'role')
            ->where('role', $role)
            ->where('approve', $approve)
            ->where('id', $id)
            ->where('delete', 0)
            ->first();

            return $admin;
        }
        else
        {
            $admin = $this->select('id', 'uid', 'fullname', 'name_on_card', 'email', 'phone', 'address', 'city', 'role')
            ->where('id', $id)
            ->where('approve', $approve)
            ->where('delete', 0)
            ->first();

            return $admin;
        }
    }

    public function getOnePartnerUserByUID($uid)
    {
        $user = $this->select('name_on_card', 'address', 'city', 'phone')
        ->where('uid', $uid)
        ->where('approve', '1')
        ->where('delete', 0)
        ->first();

        return $user;
    }

    public function getOneUserByEmail($email, $approve = 1)
    {
        $user = $this->select('id', 'email')
        ->where('email', $email)
        ->where('approve', $approve)
        ->where('delete', 0)
        ->first();

        return $user;
    }

    public function getOneUserByEmailAndId($email, $id, $approve = 1)
    {
        $user = $this->select('id', 'email')
        ->where('email', $email)
        ->where('approve', $approve)
        ->where('id', '!=', $id)
        ->where('delete', 0)
        ->first();

        return $user;
    }

    public function getOneUserByUID($uid)
    {
        $user = $this->select('id', 'uid', 'fullname', 'email')
        ->where('uid', $uid)
        ->where('approve', 1)
        ->where('role', 'partner')
        ->where('delete', 0)
        ->first();

        return $user;
    }

    public function getOneAgentByUID($uid)
    {
        $user = $this->select('id', 'uid', 'fullname', 'email', 'phone')
        ->where('uid', $uid)
        ->where('approve', 1)
        ->where('role', 'agent')
        ->where('delete', 0)
        ->first();

        return $user;
    }

    public function getOneAgentByPhone($phone)
    {
        $user = $this->select('id', 'uid', 'fullname', 'email')
        ->where('phone', $phone)
        ->where('approve', 1)
        ->where('role', 'agent')
        ->where('delete', 0)
        ->first();

        return $user;
    }

    public function getOneAgentByPhoneExceptSelf($phone, $id)
    {
        $user = $this->select('id', 'uid', 'fullname', 'email')
        ->where('phone', $phone)
        ->where('id', "!=", $id)
        ->where('approve', 1)
        ->where('role', 'agent')
        ->where('delete', 0)
        ->first();

        return $user;
    }

    public function postAddUser($param)
    {
        $result = [];

        // if($param['role'] != 'superadmin') {
            // $param['approve'] = 0;
        // } else {
            $param['approve'] = 1;
        // }

        $user_uid = HelperController::uid('users');

        $final = DB::transaction(function () use($param, $user_uid, $result) {
            $user_code = "";

            $data = $this->create([
                'uid' => $user_uid,
                'fullname' => urlencode($param['fullname']),
                'name_on_card' => $param['name_on_card'],
                'email' => $param['email'],
                'password' => Hash::make($param['password']),
                'phone' => $param['phone'],
                'address' => $param['address'],
                'city' => $param['city'],
                'type' => $param['type'],
                'role' => $param['role'],
                'approve' => $param['approve'],
                'delete' => 0
            ]);

            if($param['role'] == 'partner') {
                $param = [];
                $param['user_uid'] = $user_uid;
                $param['max_total_uc'] = 100;

                $settingUniqueCodePartnerModel = new SettingUniqueCodePartnerModel();

                $settingUniqueCodePartnerModel->postAddSettingUniqueCodePartner($param);
            } else if($param['role'] == "agent") {
                if($param['from_bank'] == "yes") {
                    $user_code = HelperController::generateAgentFromBankCode($data->id);
                } else {
                    $user_code = HelperController::generateAgentCode($data->id);
                }
                
                UserCodeModel::create([
                    'uid' => HelperController::uid('user_code'),
                    'user_uid' => $user_uid,
                    'code' => $user_code,
                    'delete' => 0
                ]);
                
                if($param['company_uid'] == null || $param['company_uid'] == "") {
                    $param['company_uid'] = "AAI00".$data->id;
                }

                UserCompanyModel::create([
                    'uid' => HelperController::uid('user_referral_codes'),
                    'user_uid' => $user_uid,
                    'referral_code' => $param['company_uid'],
                    'delete' => 0,
                ]);

                UserBalanceModel::create([
                    'uid' => HelperController::uid('user_balance'),
                    'user_uid' => $user_uid,
                    'balance' => 0,
                    'activeBalance' => 0,
                    'type' => "standart",
                    'delete' => 0,
                ]);

                UserBalanceModel::create([
                    'uid' => HelperController::uid('user_balance'),
                    'user_uid' => $user_uid,
                    'balance' => 0,
                    'activeBalance' => 0,
                    'type' => "toko",
                    'delete' => 0,
                ]);
            }

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            if($user_code != "") {
                $result[2] = $user_code;
            }

            return $result;
        });

        return $final;
    }

    public function postEditUser($param, $id)
    {
        DB::transaction(function () use($param, $id) {
            if($param['password'] != null || $param['password'] != '')
            {
                $this->where('id', $id)->where('delete', 0)->update([
                    'fullname' => urlencode($param['fullname']),
                    'name_on_card' => $param['name_on_card'],
                    'email' => $param['email'],
                    'password' => Hash::make($param['password']),
                    'phone' => $param['phone'],
                    'address' => $param['address'],
                    'city' => $param['city']
                ]);
            }
            else
            {
                $this->where('id', $id)->where('delete', 0)->update([
                    'fullname' => urlencode($param['fullname']),
                    'name_on_card' => $param['name_on_card'],
                    'email' => $param['email'],
                    'phone' => $param['phone'],
                    'address' => $param['address'],
                    'city' => $param['city']
                ]);
            }
        });

        return $this->success_update_msg;
    }

    public function postEditAgent($param, $id)
    {
        DB::transaction(function () use($param, $id) {
            if($param['password'] != null || $param['password'] != '')
            {
                $this->where('id', $id)->where('delete', 0)->update([
                    'email' => urlencode($param['email']),
                    'fullname' => urlencode($param['fullname']),
                    'phone' => $param['phone'],
                    'type' => $param['type'],
                    'password' => Hash::make($param['password']),
                ]);
            }
            else
            {
                $this->where('id', $id)->where('delete', 0)->update([
                    'email' => urlencode($param['email']),
                    'fullname' => urlencode($param['fullname']),
                    'phone' => $param['phone'],
                    'type' => $param['type'],
                ]);
            }
        });

        return $this->success_update_msg;
    }

    public function postDeleteUser($id)
    {
        $result = [];

        DB::transaction(function () use($id) {
            $this->where('id', $id)->where('delete', 0)->update([
                'delete' => 1
            ]);
        });

        return $this->success_delete_msg;
    }

    public function postApproveUser($id)
    {
        $result = [];

        DB::transaction(function () use($id) {
            $this->where('id', $id)->where('role', 'partner')->where('delete', 0)->update([
                'approve' => 1
            ]);
        });

        return $this->success_approve_msg;
    }

    public function postRejectUser($id)
    {
        $result = [];

        DB::transaction(function () use($id) {
            $this->where('id', $id)->where('role', 'partner')->where('delete', 0)->update([
                'approve' => 2
            ]);
        });

        return $this->success_reject_msg;
    }
}
