<?php

namespace App\Providers;

use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Auth, Schema;
use App\SettingModel;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        
        $url_admin = '/admin-access';
        $url_partner = '/partner';
        $url_agent = '/client';
        $base_url = url('/admin-access').'/';
        $public_base_url = url('/');
        $partner_base_url = url('/').'/';
        $auth_partner_base_url = url('/partner').'/';
        $auth_agent_base_url = url('/client').'/';
        $setting = null;

        if(Schema::hasTable('settings')) {
            $settingModel = new SettingModel();
            $setting = $settingModel->getSetting();
        }

        View::share(['url_admin' => $url_admin, 'base_url' => $base_url, 'partner_base_url' => $partner_base_url, 'setting' => $setting, 'url_partner' => $url_partner, 'auth_partner_base_url' => $auth_partner_base_url, 'public_base_url' => $public_base_url, 'url_agent' => $url_agent, 'auth_agent_base_url' => $auth_agent_base_url]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
