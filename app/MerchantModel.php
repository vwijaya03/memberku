<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class MerchantModel extends Model
{
    protected $table = 'merchants';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'name', 'email', 'contact_person', 'phone', 'address', 'join_promo', 'join_bonus', 'rate', 'available', 'img', 'logo', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di proses.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function getSearchSelect2Merchant($search)
    {
        $merchant = $this->select('id', 'uid', 'name')
        ->where('name', 'like', '%'.$search.'%')
        ->where('delete', 0)
        ->orderBy('name','asc')
        ->paginate(30);

        return $merchant;
    }
    
    public function getSearchSelect2MerchantJoinPromo($search)
    {
        $merchant = $this->select('id', 'uid', 'name')
        ->where('name', 'like', '%'.$search.'%')
        ->where('join_promo', "yes")
        ->where('delete', 0)
        ->orderBy('name','asc')
        ->paginate(30);

        return $merchant;
    }

    public function getSearchSelect2MerchantJoinBonus($search)
    {
        $merchant = $this->select('id', 'uid', 'name')
        ->where('name', 'like', '%'.$search.'%')
        ->where('join_bonus', "yes")
        ->where('delete', 0)
        ->orderBy('name','asc')
        ->paginate(30);

        return $merchant;
    }

    public function getOneMerchant($id)
    {
        $merchant = $this->select('uid', 'name', 'img')
        ->where('id', $id)
        ->where('delete', 0)
        ->first();

        return $merchant;
    }

    public function getOneMerchantByUID($uid)
    {
        $merchant = $this->select('uid', 'name', 'rate')
        ->where('uid', $uid)
        ->where('delete', 0)
        ->first();

        return $merchant;
    }

    public function countAllActiveMerchant()
    {
        $count_merchant = $this->select('id')
        ->where('delete', 0)
        ->count();

        return $count_merchant;
    }

    public function countAllFilteredActiveMerchant($search)
    {
        $count_merchant = $this->select('id')
        ->where(function ($q) use($search) {
            $q->where('email', 'like', '%'.$search.'%');
            $q->orWhere('name', 'like', '%'.$search.'%');
            $q->orWhere('contact_person', 'like', '%'.$search.'%');
            $q->orWhere('phone', 'like', '%'.$search.'%');
            $q->orWhere('address', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->count();

        return $count_merchant;
    }

    public function getAllMerchant()
    {
        $merchant = $this->select('id', 'uid', 'name', 'email', 'contact_person', 'phone', 'address', 'join_promo', 'join_bonus', 'rate', 'available', 'img', 'logo')
        ->where('delete', 0)
        ->get();

        return $merchant;
    }

    public function getMerchant($start, $limit, $order, $dir)
    {
        $merchant = $this->select('id', 'uid', 'name', 'email', 'contact_person', 'phone', 'address', 'join_promo', 'join_bonus', 'rate', 'available', 'img', 'logo')
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $merchant;
    }
    
    public function getFilteredMerchant($search, $start, $limit, $order, $dir)
    {
        $merchant = $this->select('id', 'uid', 'name', 'email', 'contact_person', 'phone', 'address', 'join_promo', 'join_bonus', 'rate', 'available', 'img', 'logo')
        ->where(function ($q) use($search) {
            $q->where('email', 'like', '%'.$search.'%');
            $q->orWhere('name', 'like', '%'.$search.'%');
            $q->orWhere('contact_person', 'like', '%'.$search.'%');
            $q->orWhere('phone', 'like', '%'.$search.'%');
            $q->orWhere('address', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $merchant;
    }

    public function countAllActiveMerchantJoinBonus()
    {
        $count_merchant = $this->select('id')
        ->where('join_bonus', 'yes')
        ->where('delete', 0)
        ->count();

        return $count_merchant;
    }

    public function countAllFilteredActiveMerchantJoinBonus($search)
    {
        $count_merchant = $this->select('id')
        ->where(function ($q) use($search) {
            $q->where('email', 'like', '%'.$search.'%');
            $q->orWhere('name', 'like', '%'.$search.'%');
            $q->orWhere('contact_person', 'like', '%'.$search.'%');
            $q->orWhere('phone', 'like', '%'.$search.'%');
            $q->orWhere('address', 'like', '%'.$search.'%');
        })
        ->where('join_bonus', 'yes')
        ->where('delete', 0)
        ->count();

        return $count_merchant;
    }

    public function getMerchantJoinBonus($start, $limit, $order, $dir)
    {
        $merchant = $this->select('id', 'uid', 'name', 'email', 'contact_person', 'phone', 'address', 'join_promo', 'join_bonus', 'rate', 'available', 'img', 'logo')
        ->where('join_bonus', 'yes')
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $merchant;
    }
    
    public function getFilteredMerchantJoinBonus($search, $start, $limit, $order, $dir)
    {
        $merchant = $this->select('id', 'uid', 'name', 'email', 'contact_person', 'phone', 'address', 'join_promo', 'join_bonus', 'rate', 'available', 'img', 'logo')
        ->where(function ($q) use($search) {
            $q->where('email', 'like', '%'.$search.'%');
            $q->orWhere('name', 'like', '%'.$search.'%');
            $q->orWhere('contact_person', 'like', '%'.$search.'%');
            $q->orWhere('phone', 'like', '%'.$search.'%');
            $q->orWhere('address', 'like', '%'.$search.'%');
        })
        ->where('join_bonus', 'yes')
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $merchant;
    }

    public function postAddMerchant($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('merchants'),
                'name' => urlencode($param['name']),
                'email' => $param['email'],
                'contact_person' => $param['contact_person'],
                'phone' => $param['phone'],
                'address' => $param['address'],
                'join_promo' => $param['join_promo'],
                'join_bonus' => $param['join_bonus'],
                'rate' => $param['rate'],
                'available' => $param['available'],
                'img' => $param['img_path'],
                'logo' => $param['logo_merchant_path'],
                'delete' => 0
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }

    public function postEditMerchant($param, $id)
    {
        DB::transaction(function () use($param, $id) {
            if($param['img_path'] != null || $param['img_path'] != '') {
                $this->where('id', $id)->where('delete', 0)->update([
                    'name' => urlencode($param['name']),
                    'email' => $param['email'],
                    'contact_person' => $param['contact_person'],
                    'phone' => $param['phone'],
                    'address' => $param['address'],
                    'join_promo' => $param['join_promo'],
                    'join_bonus' => $param['join_bonus'],
                    'available' => $param['available'],
                    'img' => $param['img_path'],
                    'rate' => $param['rate']
                ]);
            } if($param['logo_merchant_path'] != null || $param['logo_merchant_path'] != '') {
                $this->where('id', $id)->where('delete', 0)->update([
                    'name' => urlencode($param['name']),
                    'email' => $param['email'],
                    'contact_person' => $param['contact_person'],
                    'phone' => $param['phone'],
                    'address' => $param['address'],
                    'join_promo' => $param['join_promo'],
                    'join_bonus' => $param['join_bonus'],
                    'available' => $param['available'],
                    'logo' => $param['logo_merchant_path'],
                    'rate' => $param['rate']
                ]);
            } else {
                $this->where('id', $id)->where('delete', 0)->update([
                    'name' => urlencode($param['name']),
                    'email' => $param['email'],
                    'contact_person' => $param['contact_person'],
                    'phone' => $param['phone'],
                    'address' => $param['address'],
                    'join_promo' => $param['join_promo'],
                    'join_bonus' => $param['join_bonus'],
                    'available' => $param['available'],
                    'rate' => $param['rate']
                ]);
            }
        });

        return $this->success_update_msg;
    }

    public function postDeleteMerchant($id)
    {
        DB::transaction(function () use($id) {
            $this->where('id', $id)->where('delete', 0)->update([
                'delete' => 1
            ]);
        });

        return $this->success_delete_msg;
    }
}
