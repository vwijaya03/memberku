<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Auth, Hash, DB, Log;

class PartnerLogoModel extends Model
{
    protected $table = 'logos';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'user_uid', 'img', 'approve', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_approve_msg = 'Logo berhasil di approve';
    private $success_add_msg = 'Logo akan berganti setelah disetujui';
    private $success_add_msg_by_superadmin = 'Data berhasil di simpan.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function getOnePartnerLogo($user_uid) 
    {
    	$logo = $this->select('id', 'uid', 'user_uid', 'img')
        ->where('user_uid', $user_uid)
        ->where('delete', 0)
        ->where('approve', 1)
        ->first();

        return $logo;
    }

    public function getOneAgentLogo($user_uid) 
    {
    	$logo = $this->select('id', 'uid', 'user_uid', 'img')
        ->where('user_uid', $user_uid)
        ->where('delete', 0)
        ->first();

        return $logo;
    }

    public function getOnePartnerLogoUntukBawahan($user_uid) 
    {
    	$logo = $this->select('id', 'uid', 'user_uid', 'img')
        ->where('user_uid', $user_uid)
        ->where('delete', 0)
        ->where('approve', 1)
        ->first();

        return $logo;
    }

    public function postAddPartnerLogo($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('logos'),
                'user_uid' => $param['user_uid'],
                'img' => $param['img_path'],
                'approve' => 0,
                'delete' => 0
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }

    public function postAddPartnerLogoBySuperadmin($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('logos'),
                'user_uid' => $param['user_uid'],
                'img' => $param['img_path'],
                'approve' => 1,
                'delete' => 0
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg_by_superadmin;

            return $result;
        });

        return $final;
    }

    public function postEditPartnerLogo($param, $user_uid)
    {
        DB::transaction(function () use($param, $user_uid) {
            $this->where('user_uid', $user_uid)->where('delete', 0)->update([
                'img' => $param['img_path'],
                'approve' => 0,
            ]);
        });

        return $this->success_update_msg;
    }

    public function postEditPartnerLogoBySuperadmin($param, $user_uid)
    {
        DB::transaction(function () use($param, $user_uid) {
            $this->where('user_uid', $user_uid)->where('delete', 0)->update([
                'img' => $param['img_path'],
                'approve' => 1,
            ]);
        });

        return $this->success_update_msg;
    }

    public function postApproveAgentLogo($user_uid)
    {
        DB::transaction(function () use($user_uid) {
            $this->where('user_uid', $user_uid)->where('delete', 0)->update([
                'approve' => 1,
            ]);
        });

        return $this->success_approve_msg;
    }

    public function postDeletePartnerLogo($user_uid)
    {
        DB::transaction(function () use($user_uid) {
            $this->where('user_uid', $user_uid)->where('delete', 0)->update([
                'delete' => 1
            ]);
        });

        return $this->success_delete_msg;
    }
}
