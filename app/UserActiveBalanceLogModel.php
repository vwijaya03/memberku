<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class UserActiveBalanceLogModel extends Model
{
    protected $table = 'user_active_balance_logs';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'from_user_uid', 'to_user_uid', 'amount', 'rate', 'type', 'additional_note', 'delete'];
}
