<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class RecordUsePromoModel extends Model
{
    protected $table = 'records_use_promo';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'promo_uid', 'user_uid', 'unique_code', 'pin', 'user_agent', 'ip', 'promo_type', 'delete'];

    public function countAllActiveReportPin($pin)
    {
        $count_report_pin = $this->join('promos', 'promos.uid', '=', 'records_use_promo.promo_uid')
        ->where('records_use_promo.pin', $pin)
        ->where('records_use_promo.delete', 0)
        ->where('promos.delete', 0)
        ->count('records_use_promo.id');

        return $count_report_pin;
    }

    public function countAllFilteredActiveReportPin($pin, $search)
    {
        $count_report_pin = $this->join('promos', 'promos.uid', '=', 'records_use_promo.promo_uid')
        ->where('records_use_promo.pin', $pin)
        ->where('records_use_promo.delete', 0)
        ->where('promos.delete', 0)
        ->count('records_use_promo.id');

        return $count_report_pin;
    }

    public function getReportPin($pin, $start, $limit, $order, $dir)
    {
        $report_pin = $this->select('promos.title', 'records_use_promo.id', 'records_use_promo.pin', 'records_use_promo.created_at')
        ->join('promos', 'promos.uid', '=', 'records_use_promo.promo_uid')
        ->where('records_use_promo.pin', $pin)
        ->where('records_use_promo.delete', 0)
        ->where('promos.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order, $dir)
        ->get();

        return $report_pin;
    }
    
    public function getFilteredReportPin($pin, $search, $start, $limit, $order, $dir)
    {
        $report_pin = $this->select('promos.title', 'records_use_promo.id', 'records_use_promo.pin', 'records_use_promo.created_at')
        ->join('promos', 'promos.uid', '=', 'records_use_promo.promo_uid')
        ->where('records_use_promo.pin', $pin)
        ->where('records_use_promo.delete', 0)
        ->where('promos.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order, $dir)
        ->get();

        return $report_pin;
    }

    public function getOneRecord($unique_code, $promo_uid)
    {
        $record = $this->select('uid')
        ->where('unique_code', $unique_code)
        ->where('promo_uid', $promo_uid)
        ->where('delete', 0)
        ->first();

        return $record;
    }

    public function postAddRecordUsePromo($param)
    {
        DB::transaction(function () use($param) {
            $this->create([
                'uid' => HelperController::uid('records_use_promo'),
                'promo_uid' => $param['promo_uid'],
                'user_uid' => $param['user_uid'],
                'unique_code' => $param['unique_code'],
                'pin' => $param['pin'],
                'user_agent' => $param['user_agent'],
                'ip' => $param['ip'],
                'promo_type' => $param['promo_type'],
                'delete' => 0
            ]);
        });

        return "";
    }
}
