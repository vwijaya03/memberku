<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class CategoryPartnerModel extends Model
{
    protected $table = 'category_partners';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'name', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di proses.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function countAllActiveCategoryPartner()
    {
        $count_category_partner = $this->select('id')
        ->where('delete', 0)
        ->count();

        return $count_category_partner;
    }

    public function countAllFilteredActiveCategoryPartner($search)
    {
        $count_category_partner = $this->select('id')
        ->where(function ($q) use($search) {
            $q->where('name', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->count();

        return $count_category_partner;
    }

    public function getCategoryPartner($start, $limit, $order, $dir)
    {
        $category_partner = $this->select('id', 'uid', 'name')
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $category_partner;
    }
    
    public function getFilteredCategoryPartner($search, $start, $limit, $order, $dir)
    {
        $category_partner = $this->select('id', 'uid', 'name')
        ->where(function ($q) use($search) {
            $q->where('name', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $category_partner;
    }

    public function postAddCategoryPartner($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('category_partners'),
                'name' => $param['name'],
                'delete' => 0
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }

    public function postEditCategoryPartner($param, $id)
    {
        DB::transaction(function () use($param, $id) {
            $this->where('id', $id)->where('delete', 0)->update([
                'name' => $param['name']
            ]);
        });

        return $this->success_update_msg;
    }

    public function postDeleteCategoryPartner($id)
    {
        DB::transaction(function () use($id) {
            $this->where('id', $id)->where('delete', 0)->update([
                'delete' => 1
            ]);
        });

        return $this->success_delete_msg;
    }
}
