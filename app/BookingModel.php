<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class BookingModel extends Model
{
    protected $table = 'bookings';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'nama_usaha', 'alamat_usaha', 'social_media', 'contact_person', 'phone', 'email', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di proses.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function countAllActiveBooking()
    {
        $count_booking = $this->select('id')
        ->where('delete', 0)
        ->count();

        return $count_booking;
    }

    public function countAllFilteredActiveBooking($search)
    {
        $count_booking = $this->select('id')
        ->where(function ($q) use($search) {
            $q->where('nama_usaha', 'like', '%'.$search.'%');
            $q->orWhere('alamat_usaha', 'like', '%'.$search.'%');
            $q->orWhere('social_media', 'like', '%'.$search.'%');
            $q->orWhere('contact_person', 'like', '%'.$search.'%');
            $q->orWhere('phone', 'like', '%'.$search.'%');
            $q->orWhere('email', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->count();

        return $count_booking;
    }

    public function getBooking($start, $limit, $order, $dir)
    {
        $booking = $this->select('id', 'nama_usaha', 'alamat_usaha', 'social_media', 'contact_person', 'phone', 'email')
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $booking;
    }
    
    public function getFilteredBooking($search, $start, $limit, $order, $dir)
    {
        $booking = $this->select('id', 'nama_usaha', 'alamat_usaha', 'social_media', 'contact_person', 'phone', 'email')
        ->where(function ($q) use($search) {
            $q->where('nama_usaha', 'like', '%'.$search.'%');
            $q->orWhere('alamat_usaha', 'like', '%'.$search.'%');
            $q->orWhere('social_media', 'like', '%'.$search.'%');
            $q->orWhere('contact_person', 'like', '%'.$search.'%');
            $q->orWhere('phone', 'like', '%'.$search.'%');
            $q->orWhere('email', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $booking;
    }

    public function postAddBooking($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('bookings'),
                'email' => $param['email'],
                'nama_usaha' => $param['nama_usaha'],
                'contact_person' => $param['contact_person'],
                'alamat_usaha' => $param['alamat_usaha'],
                'social_media' => $param['social_media'],
                'phone' => $param['phone'],
                'delete' => 0
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }

    public function postEditBooking($param, $id)
    {
        DB::transaction(function () use($param, $id) {
            $this->where('id', $id)->where('delete', 0)->update([
                'email' => $param['email'],
                'nama_usaha' => $param['nama_usaha'],
                'contact_person' => $param['contact_person'],
                'alamat_usaha' => $param['alamat_usaha'],
                'social_media' => $param['social_media'],
                'phone' => $param['phone'],
            ]);
        });

        return $this->success_update_msg;
    }

    public function postDeleteBooking($id)
    {
        DB::transaction(function () use($id) {
            $this->where('id', $id)->where('delete', 0)->update([
                'delete' => 1
            ]);
        });

        return $this->success_delete_msg;
    }
}
