<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Auth, Hash, DB, Log;

class GreetingTextModel extends Model
{
    protected $table = 'greeting_texts';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'user_uid', 'description', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di simpan.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function getOneGreetingText($user_uid) 
    {
    	$greeting_text = $this->select('id', 'uid', 'user_uid', 'description')
        ->where('user_uid', $user_uid)
        ->where('delete', 0)
        ->first();

        return $greeting_text;
    }

    public function postAddGreetingText($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('greeting_texts'),
                'user_uid' => $param['user_uid'],
                'description' => $param['description'],
                'delete' => 0
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }

    public function postEditGreetingText($param, $user_uid)
    {
        DB::transaction(function () use($param, $user_uid) {
            $this->where('user_uid', $user_uid)->where('delete', 0)->update([
                'description' => $param['description']
            ]);
        });

        return $this->success_update_msg;
    }
}
