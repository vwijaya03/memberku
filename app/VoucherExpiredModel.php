<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class VoucherExpiredModel extends Model
{
    protected $table = 'vouchers_expired_date';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'user_uid', 'description', 'days', 'default', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di proses.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function countAllActiveVoucherExpired($user_uid)
    {
        $count_voucher_expired = $this->join('users', 'users.uid', '=', 'vouchers_expired_date.user_uid')
        ->where('vouchers_expired_date.user_uid', $user_uid)
        ->where('vouchers_expired_date.delete', 0)
        ->where('users.delete', 0)
        ->count('vouchers_expired_date.id');

        return $count_voucher_expired;
    }

    public function countAllFilteredActiveVoucherExpired($search, $user_uid)
    {
        $count_voucher_expired = $this->join('users', 'users.uid', '=', 'vouchers_expired_date.user_uid')
        ->where(function ($q) use($search) {
            $q->where('vouchers_expired_date.description', 'like', '%'.$search.'%');
            $q->orWhere('vouchers_expired_date.days', 'like', '%'.$search.'%');
        })
        ->where('vouchers_expired_date.user_uid', $user_uid)
        ->where('vouchers_expired_date.delete', 0)
        ->where('users.delete', 0)
        ->count('vouchers_expired_date.id');

        return $count_voucher_expired;
    }

    public function getVoucherExpired($start, $limit, $order, $dir, $user_uid)
    {
        $voucher_expired = $this->select('vouchers_expired_date.id', 'vouchers_expired_date.uid', 'vouchers_expired_date.description', 'vouchers_expired_date.default', 'vouchers_expired_date.days')
        ->join('users', 'users.uid', '=', 'vouchers_expired_date.user_uid')
        ->where('vouchers_expired_date.user_uid', $user_uid)
        ->where('vouchers_expired_date.delete', 0)
        ->where('users.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $voucher_expired;
    }
    
    public function getFilteredVoucherExpired($search, $start, $limit, $order, $dir, $user_uid)
    {
        $voucher_expired = $this->select('vouchers_expired_date.id', 'vouchers_expired_date.uid', 'vouchers_expired_date.description', 'vouchers_expired_date.default', 'vouchers_expired_date.days')
        ->join('users', 'users.uid', '=', 'vouchers_expired_date.user_uid')
        ->where(function ($q) use($search) {
            $q->where('vouchers_expired_date.description', 'like', '%'.$search.'%');
            $q->orWhere('vouchers_expired_date.days', 'like', '%'.$search.'%');
        })
        ->where('vouchers_expired_date.user_uid', $user_uid)
        ->where('vouchers_expired_date.delete', 0)
        ->where('users.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $voucher_expired;
    }

    public function getDefault($user_uid) 
    {
        $voucher_expired = $this->select('description', 'days')
        ->where('default', 'yes')
        ->where('vouchers_expired_date.user_uid', $user_uid)
        ->where('vouchers_expired_date.delete', 0)
        ->first();

        return $voucher_expired;
    }

    public function postAddVoucherExpired($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('voucher_expired'),
                'user_uid' => $param['user_uid'],
                'description' => $param['description'],
                'default' => $param['default'],
                'days' => $param['days'],
                'delete' => 0
            ]);

            if($param['default'] == 'yes') {
                $this->where('vouchers_expired_date.user_uid', $param['user_uid'])->where('vouchers_expired_date.delete', 0)->update([
                    'default' => 'no'
                ]);

                $this->where('id', $data->id)->where('vouchers_expired_date.user_uid', $param['user_uid'])->where('vouchers_expired_date.delete', 0)->update([
                    'default' => 'yes'
                ]);
            }

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }

    public function postEditVoucherExpired($param, $id)
    {
        DB::transaction(function () use($param, $id) {
            $this->where('id', $id)->where('vouchers_expired_date.delete', 0)->update([
                'user_uid' => $param['user_uid'],
                'description' => $param['description'],
                'default' => $param['default'],
                'days' => $param['days']
            ]);

            if($param['default'] == 'yes') {
                $this->where('vouchers_expired_date.user_uid', $param['user_uid'])->where('vouchers_expired_date.delete', 0)->update([
                    'default' => 'no'
                ]);

                $this->where('id', $id)->where('vouchers_expired_date.user_uid', $param['user_uid'])->where('vouchers_expired_date.delete', 0)->update([
                    'default' => 'yes'
                ]);
            }
        });

        return $this->success_update_msg;
    }

    public function postDeleteVoucherExpired($id)
    {
        DB::transaction(function () use($id) {
            $this->where('id', $id)->where('vouchers_expired_date.delete', 0)->update([
                'delete' => 1
            ]);
        });

        return $this->success_delete_msg;
    }
}
