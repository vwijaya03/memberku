<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class UserBalanceModel extends Model
{
    protected $table = 'user_balance';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'user_uid', 'balance', 'activeBalance', 'type', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di proses.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function getCurrentUserBalance($user_uid, $type) {
        $currentUserBalance = $this->select('balance', 'activeBalance')
        ->where('type', $type)
        ->where('user_uid', $user_uid)
        ->where('delete', 0)
        ->first();

        return $currentUserBalance;
    }

    public function getCurrentUserActiveBalance($user_uid, $type) {
        $currentUserBalance = $this->select('balance', 'activeBalance')
        ->where('type', $type)
        ->where('user_uid', $user_uid)
        ->where('delete', 0)
        ->first();

        return $currentUserBalance;
    }

    public function postUpdateUserBalance($user_uid, $amount, $type)
    {
        DB::transaction(function () use($user_uid, $amount, $type) {
            $currentBalance = 0;

            $currentUserBalance = $this->getCurrentUserBalance($user_uid, $type);

            if($currentUserBalance != null) {
                $currentBalance = $currentUserBalance->balance;
            }

            $this->where('user_uid', $user_uid)->where('type', $type)->where('delete', 0)->update([
                'balance' => $currentBalance + $amount,
            ]);
        });

        return $this->success_update_msg;
    }
    
    public function postUpdateMinusUserBalance($user_uid, $amount, $type)
    {
        DB::transaction(function () use($user_uid, $amount, $type) {
            $currentBalance = 0;

            $currentUserBalance = $this->getCurrentUserBalance($user_uid, $type);

            if($currentUserBalance != null) {
                $currentBalance = $currentUserBalance->balance;
            }
            
            $this->where('user_uid', $user_uid)->where('type', $type)->where('delete', 0)->update([
                'balance' => $currentBalance - $amount,
            ]);
        });

        return $this->success_update_msg;
    }

    public function postUpdatePlusUserActiveBalance($user_uid, $amount, $type)
    {
        DB::transaction(function () use($user_uid, $amount, $type) {
            $currentBalance = 0;

            $currentUserBalance = $this->getCurrentUserActiveBalance($user_uid, $type);

            if($currentUserBalance != null) {
                $currentBalance = $currentUserBalance->activeBalance;
            }

            $this->where('user_uid', $user_uid)->where('type', $type)->where('delete', 0)->update([
                'activeBalance' => $currentBalance + $amount,
            ]);
        });

        return $this->success_update_msg;
    }

    public function postUpdateMinusUserActiveBalance($user_uid, $amount, $type)
    {
        DB::transaction(function () use($user_uid, $amount, $type) {
            $currentBalance = 0;

            $currentUserBalance = $this->getCurrentUserActiveBalance($user_uid, $type);

            if($currentUserBalance != null) {
                $currentBalance = $currentUserBalance->activeBalance;
            }

            $this->where('user_uid', $user_uid)->where('type', $type)->where('delete', 0)->update([
                'activeBalance' => $currentBalance - $amount,
            ]);
        });

        return $this->success_update_msg;
    }
}
