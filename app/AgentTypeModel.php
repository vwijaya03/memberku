<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class AgentTypeModel extends Model
{
    protected $table = 'agent_types';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'name', 'rate', 'allow_upload_logo', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di proses.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function countAllActiveAgentType()
    {
        $count_agent_type = $this->where('delete', 0)
        ->count('id');

        return $count_agent_type;
    }

    public function countAllFilteredActiveAgentType($search)
    {
        $count_agent_type = $this->where(function ($q) use($search) {
            $q->where('name', 'like', '%'.$search.'%');
            $q->orWhere('rate', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->count('id');

        return $count_agent_type;
    }

    public function getAgentType($start, $limit, $order, $dir)
    {
        $agent_type = $this->select('uid', 'name', 'rate', 'allow_upload_logo')
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $agent_type;
    }
    
    public function getFilteredAgentType($search, $start, $limit, $order, $dir)
    {
        $agent_type = $this->select('uid', 'name', 'rate', 'allow_upload_logo')
        ->where(function ($q) use($search) {
            $q->where('name', 'like', '%'.$search.'%');
            $q->orWhere('rate', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $agent_type;
    }

    public function postAddAgentType($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('agent_types'),
                'name' => urlencode($param['name']),
                'rate' => $param['rate'],
                'allow_upload_logo' => $param['allow_upload_logo'],
                'delete' => 0
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }

    public function postEditAgentType($param, $uid)
    {
        DB::transaction(function () use($param, $uid) {
            $this->where('uid', $uid)->where('delete', 0)->update([
                'name' => urlencode($param['name']),
                'rate' => $param['rate'],
                'allow_upload_logo' => $param['allow_upload_logo'],
            ]);
        });

        return $this->success_update_msg;
    }

    public function postDeleteAgentType($uid)
    {
        DB::transaction(function () use($uid) {
            $this->where('uid', $uid)->where('delete', 0)->update([
                'delete' => 1
            ]);
        });

        return $this->success_delete_msg;
    }
}
