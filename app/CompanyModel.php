<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class CompanyModel extends Model
{
    protected $table = 'companies';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'name', 'code', 'title', 'img', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di proses.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function countAllActiveCompany()
    {
        $count_company = $this->where('delete', 0)
        ->count('id');

        return $count_company;
    }

    public function countAllFilteredActiveCompany($search)
    {
        $count_company = $this->where(function ($q) use($search) {
            $q->where('name', 'like', '%'.$search.'%');
            $q->orWhere('code', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->count('id');

        return $count_company;
    }

    public function getCompany($start, $limit, $order, $dir)
    {
        $company = $this->select('id', 'uid', 'name', 'code', 'title', 'img')
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $company;
    }
    
    public function getFilteredCompany($search, $start, $limit, $order, $dir)
    {
        $company = $this->select('id', 'uid', 'name', 'code', 'title', 'img')
        ->where(function ($q) use($search) {
            $q->where('name', 'like', '%'.$search.'%');
            $q->orWhere('code', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $company;
    }

    public function getOneCompanyByUID($uid) {
        $company = $this->select('id', 'uid', 'name', 'code', 'title', 'img')
        ->where('uid', $uid)
        ->where('delete', 0)
        ->first();

        return $company;
    }

    public function getOneCompany($code) {
        $company = $this->select('id', 'uid', 'name', 'code', 'title', 'img')
        ->where('code', $code)
        ->where('delete', 0)
        ->first();

        return $company;
    }

    public function postAddCompany($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('companies'),
                'name' => urlencode($param['name']),
                'title' => $param['title'],
                'img' => $param['img_path'],
                'delete' => 0
            ]);

            if($data->id != null || $data->id != "") {
                if($param['code'] != null) {
                    $this->where('id', $data->id)->update([
                        'code' => $param['code']
                    ]);
                } else {
                    $this->where('id', $data->id)->update([
                        'code' => HelperController::generateCompanyCode($data->id)
                    ]);
                }
            }

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }

    public function postEditCompany($param, $uid)
    {
        DB::transaction(function () use($param, $uid) {
            if($param['img_path'] != null || $param['img_path'] != '') {
                $this->where('uid', $uid)->where('delete', 0)->update([
                    'name' => urlencode($param['name']),
                    'code' => $param['code'],
                    'title' => $param['title'],
                    'img' => $param['img_path']
                ]);
            } else {
                $this->where('uid', $uid)->where('delete', 0)->update([
                    'name' => urlencode($param['name']),
                    'code' => $param['code'],
                    'title' => $param['title']
                ]);
            }
            
        });

        return $this->success_update_msg;
    }

    public function postDeleteCompany($uid)
    {
        DB::transaction(function () use($uid) {
            $this->where('uid', $uid)->where('delete', 0)->update([
                'delete' => 1
            ]);
        });

        return $this->success_delete_msg;
    }
}
