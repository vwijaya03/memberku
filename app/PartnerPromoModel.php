<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use App\PromoModel;
use Hash, DB, Log;

class PartnerPromoModel extends Model
{
    protected $table = 'partners_promos';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'user_uid', 'promo_uid', 'delete'];

    private $success_add_msg = 'Promo berhasil di tambahkan.';
    private $success_delete_msg = 'Promo berhasil di hapus.';

    public function getExistPartnerPromo($user_uid, $promo_uid)
    {
    	$promo = $this->select('partners_promos.promo_uid')
        ->where('partners_promos.user_uid', $user_uid)
        ->whereIn('partners_promos.promo_uid', $promo_uid)
        ->get();

        return $promo;
    }

    public function countAllActiveSelectedPromo($user_uid)
    {
        $count_selected_promo = $this->join('promos', 'promos.uid', '=', 'partners_promos.promo_uid')
        ->join('merchants', 'merchants.uid', '=', 'promos.merchant_uid')
        ->where('partners_promos.user_uid', $user_uid)
        ->where('promos.delete', 0)
        ->where('merchants.delete', 0)
        ->count('partners_promos.uid');

        return $count_selected_promo;
    }

    public function countAllFilteredActiveSelectedPromo($user_uid, $search)
    {
        $count_selected_promo = $this->join('promos', 'promos.uid', '=', 'partners_promos.promo_uid')
        ->join('merchants', 'merchants.uid', '=', 'promos.merchant_uid')
        ->where(function ($q) use($search) {
            $q->where('merchants.name', 'like', '%'.$search.'%');
            $q->orWhere('promos.title', 'like', '%'.$search.'%');
        })
        ->where('partners_promos.user_uid', $user_uid)
        ->where('promos.delete', 0)
        ->where('merchants.delete', 0)
        ->count('partners_promos.uid');

        return $count_selected_promo;
    }

    public function getSelectedPromo($user_uid, $start, $limit)
    {
        $promo = $this->select('merchants.name', 'promos.uid', 'promos.title', 'promos.description')
        ->join('promos', 'promos.uid', '=', 'partners_promos.promo_uid')
        ->join('merchants', 'merchants.uid', '=', 'promos.merchant_uid')
        ->where('partners_promos.user_uid', $user_uid)
        ->where('promos.delete', 0)
        ->where('merchants.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->get();

        return $promo;
    }

    public function getFilteredSelectedPromo($user_uid, $search, $start, $limit)
    {
        $promo = $this->select('merchants.name', 'promos.uid', 'promos.title', 'promos.description')
        ->join('promos', 'promos.uid', '=', 'partners_promos.promo_uid')
        ->join('merchants', 'merchants.uid', '=', 'promos.merchant_uid')
        ->where(function ($q) use($search) {
            $q->where('merchants.name', 'like', '%'.$search.'%');
            $q->orWhere('promos.title', 'like', '%'.$search.'%');
        })
        ->where('partners_promos.user_uid', $user_uid)
        ->where('promos.delete', 0)
        ->where('merchants.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->get();

        return $promo;
    }

    public function getSelectedPromoUID($user_uid)
    {
        $promo_uid = $this->select('partners_promos.uid')
        ->where('partners_promos.user_uid', $user_uid)
        ->get();

        return $promo_uid;
    }

    public function getForPublicSelectedPromo($user_uid)
    {
        $promo = $this->select('merchants.id as merchant_id', 'merchants.name', 'promos.id', 'promos.uid', 'promos.merchant_uid', 'promos.title', 'promos.description', 'promos.img', 'promos.harga_awal', 'promos.harga_akhir', 'promos.expired_date', 'promos.top_promo', 'promos.use_pin')
        ->join('promos', 'promos.uid', '=', 'partners_promos.promo_uid')
        ->join('merchants', 'merchants.uid', '=', 'promos.merchant_uid')
        ->where('partners_promos.user_uid', $user_uid)
        ->where('promos.delete', 0)
        ->where('merchants.delete', 0)
        ->orderBy('promos.sequence', 'asc')
        ->get();

        return $promo;
    }

    public function postAddPartnerPromo($promos)
    {
    	$final = DB::transaction(function () use($promos) {
            $this->insert($promos);

            return $this->success_add_msg;
        });

        return $final;
    }

    /* --------------------------------------------- Unselected Promo ----------------------------------------------- */

    public function countAllActiveUnselectedPromo($promo_uid)
    {
    	$promoModel = new PromoModel();

        $count_unselected_promo = $promoModel
        ->join('merchants', 'merchants.uid', '=', 'promos.merchant_uid')
        ->whereNotIn('promos.uid', $promo_uid)
        ->where('promos.delete', 0)
        ->where('merchants.delete', 0)
        ->count('promos.uid');

        return $count_unselected_promo;
    }

    public function countAllFilteredActiveUnselectedPromo($promo_uid, $search)
    {
    	$promoModel = new PromoModel();

        $count_unselected_promo = $promoModel
        ->join('merchants', 'merchants.uid', '=', 'promos.merchant_uid')
        ->where(function ($q) use($search) {
            $q->where('merchants.name', 'like', '%'.$search.'%');
            $q->orWhere('promos.title', 'like', '%'.$search.'%');
        })
        ->whereNotIn('promos.uid', $promo_uid)
        ->where('promos.delete', 0)
        ->where('merchants.delete', 0)
        ->count('promos.uid');

        return $count_unselected_promo;
    }

    // public function getUnselectedPromo($promo_uid, $start, $limit, $order, $dir)
    public function getUnselectedPromo($promo_uid, $start, $limit)
    {
    	$promoModel = new PromoModel();

        $promo = $promoModel->select('merchants.name', 'promos.uid', 'promos.title', 'promos.description')
        ->join('merchants', 'merchants.uid', '=', 'promos.merchant_uid')
        ->whereNotIn('promos.uid', $promo_uid)
        ->where('promos.delete', 0)
        ->where('merchants.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->get();

        return $promo;
    }

    public function getFilteredUnselectedPromo($promo_uid, $search, $start, $limit)
    {
    	$promoModel = new PromoModel();

        $promo = $promoModel->select('merchants.name', 'promos.uid', 'promos.title', 'promos.description')
        ->join('merchants', 'merchants.uid', '=', 'promos.merchant_uid')
        ->where(function ($q) use($search) {
            $q->where('merchants.name', 'like', '%'.$search.'%');
            $q->orWhere('promos.title', 'like', '%'.$search.'%');
        })
        ->whereNotIn('promos.uid', $promo_uid)
        ->where('promos.delete', 0)
        ->where('merchants.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->get();

        return $promo;
    }

    public function postDeletePartnerPromo($user_uid, $promo_uid)
    {
    	$final = DB::transaction(function () use($user_uid, $promo_uid) {
            $this->where('partners_promos.user_uid', $user_uid)
	        ->whereIn('partners_promos.promo_uid', $promo_uid)
	        ->delete();

            return $this->success_delete_msg;
        });

        return $final;
    }
}
