<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class MembershipModel extends Model
{
    protected $table = 'memberships';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'user_uid', 'nama_usaha', 'address', 'city', 'phone', 'fullname', 'card_number', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di proses.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function countAllActivePartnerMembership($user_uid)
    {
        $count_membership = $this->select('memberships.id')
        ->join('users', 'users.uid', '=', 'memberships.user_uid')
        ->where('memberships.user_uid', $user_uid)
        ->where('memberships.delete', 0)
        ->where('users.delete', 0)
        ->where('users.role', 'partner')
        ->count();

        return $count_membership;
    }

    public function countAllFilteredActivePartnerMembership($search, $user_uid)
    {
        $count_membership = $this->select('memberships.id')
        ->join('users', 'users.uid', '=', 'memberships.user_uid')
        ->where(function ($q) use($search) {
            $q->where('memberships.nama_usaha', 'like', '%'.$search.'%');
            $q->orWhere('memberships.address', 'like', '%'.$search.'%');
            $q->orWhere('memberships.city', 'like', '%'.$search.'%');
            $q->orWhere('memberships.phone', 'like', '%'.$search.'%');
            $q->orWhere('memberships.fullname', 'like', '%'.$search.'%');
            $q->orWhere('memberships.card_number', 'like', '%'.$search.'%');
        })
        ->where('memberships.user_uid', $user_uid)
        ->where('memberships.delete', 0)
        ->where('users.delete', 0)
        ->where('users.role', 'partner')
        ->count();

        return $count_membership;
    }

    public function getPartnerMembership($start, $limit, $order, $dir, $user_uid)
    {
        $membership = $this->select('users.fullname', 'memberships.id', 'memberships.uid', 'memberships.user_uid', 'memberships.nama_usaha', 'memberships.address', 'memberships.city', 'memberships.phone', 'memberships.fullname', 'memberships.card_number', 'memberships.created_at')
        ->join('users', 'users.uid', '=', 'memberships.user_uid')
        ->where('memberships.user_uid', $user_uid)
        ->where('memberships.delete', 0)
        ->where('users.delete', 0)
        ->where('users.role', 'partner')
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $membership;
    }
    
    public function getFilteredPartnerMembership($search, $start, $limit, $order, $dir, $user_uid)
    {
        $membership = $this->select('users.fullname', 'memberships.id', 'memberships.uid', 'memberships.user_uid', 'memberships.nama_usaha', 'memberships.address', 'memberships.city', 'memberships.phone', 'memberships.fullname', 'memberships.card_number', 'memberships.created_at')
        ->join('users', 'users.uid', '=', 'memberships.user_uid')
        ->where(function ($q) use($search) {
            $q->where('memberships.nama_usaha', 'like', '%'.$search.'%');
            $q->orWhere('memberships.address', 'like', '%'.$search.'%');
            $q->orWhere('memberships.city', 'like', '%'.$search.'%');
            $q->orWhere('memberships.phone', 'like', '%'.$search.'%');
            $q->orWhere('memberships.fullname', 'like', '%'.$search.'%');
            $q->orWhere('memberships.card_number', 'like', '%'.$search.'%');
        })
        ->where('memberships.user_uid', $user_uid)
        ->where('memberships.delete', 0)
        ->where('users.delete', 0)
        ->where('users.role', 'partner')
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $membership;
    }

    public function getOneMembershipByCardNumber($card_number)
    {
        $membership = $this->select('memberships.uid')
        ->where('memberships.card_number', $card_number)
        ->where('memberships.delete', 0)
        ->first();

        return $membership;
    }

    public function getOneMembershipByUID($uid)
    {
        $membership = $this->select('memberships.nama_usaha', 'memberships.address', 'memberships.city', 'memberships.phone', 'memberships.fullname', 'memberships.card_number')
        ->where('memberships.uid', $uid)
        ->where('memberships.delete', 0)
        ->first();

        return $membership;
    }

    public function postAddMembership($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('memberships'),
                'user_uid' => $param['user_uid'],
                'nama_usaha' => $param['nama_usaha'],
                'address' => $param['address'],
                'city' => $param['city'],
                'phone' => $param['phone'],
                'fullname' => $param['fullname'],
                'card_number' => $param['card_number'],
                'delete' => 0
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }

    public function postEditPartnerMembership($param, $id, $user_uid)
    {
        DB::transaction(function () use($param, $id, $user_uid) {
            $this->where('memberships.id', $id)->where('memberships.user_uid', $user_uid)->where('memberships.delete', 0)->update([
                'nama_usaha' => $param['nama_usaha'],
                'address' => $param['address'],
                'city' => $param['city'],
                'phone' => $param['phone'],
                'fullname' => $param['fullname']
            ]);
        });

        return $this->success_update_msg;
    }

    public function postDeletePartnerMembership($id, $user_uid)
    {
        DB::transaction(function () use($id, $user_uid) {
            $this->where('memberships.id', $id)->where('memberships.user_uid', $user_uid)->where('memberships.delete', 0)->update([
                'delete' => 1
            ]);
        });

        return $this->success_delete_msg;
    }
}
