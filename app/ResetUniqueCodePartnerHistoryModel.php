<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class ResetUniqueCodePartnerHistoryModel extends Model
{
    protected $table = 'reset_unique_code_partners_history';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'user_uid', 'reset_date', 'reset_by', 'reset_by_uid', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di proses.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function postAddResetUniqueCodePartnerHistory($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('reset_unique_code_partners_history'),
                'user_uid' => $param['user_uid'],
                'reset_date' => $param['reset_date'],
                'reset_by' => $param['reset_by'],
                'reset_by_uid' => $param['reset_by_uid'],
                'delete' => 0
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }
}
