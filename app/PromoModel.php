<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class PromoModel extends Model
{
    protected $table = 'promos';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'merchant_uid', 'title', 'description', 'img', 'harga_awal', 'harga_akhir', 'use_pin', 'expired_date', 'top_promo', 'sequence', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_update_sequence_msg = 'Urutan data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di proses.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function countAllPromo() 
    {
        $count_promo = $this->count('promos.id');

        return $count_promo;
    }

    public function countAllActivePromo()
    {
        $count_promo = $this->select('promos.id')
        ->join('merchants', 'merchants.uid', '=', 'promos.merchant_uid')
        ->where('promos.delete', 0)
        ->where('merchants.delete', 0)
        ->count();

        return $count_promo;
    }

    public function countAllFilteredActivePromo($search)
    {
        $count_promo = $this->select('promos.id')
        ->join('merchants', 'merchants.uid', '=', 'promos.merchant_uid')
        ->where(function ($q) use($search) {
            $q->where('merchants.name', 'like', '%'.$search.'%');
            $q->orWhere('promos.title', 'like', '%'.$search.'%');
            $q->orWhere('promos.description', 'like', '%'.$search.'%');
            $q->orWhere('promos.img', 'like', '%'.$search.'%');
            $q->orWhere('promos.harga_awal', 'like', '%'.$search.'%');
            $q->orWhere('promos.harga_akhir', 'like', '%'.$search.'%');
        })
        ->where('promos.delete', 0)
        ->where('merchants.delete', 0)
        ->count();

        return $count_promo;
    }

    public function getPromo($start, $limit, $order, $dir)
    {
        $promo = $this->select('merchants.id as merchant_id', 'merchants.name', 'promos.id', 'promos.uid', 'promos.merchant_uid', 'promos.title', 'promos.description', 'promos.img', 'promos.harga_awal', 'promos.harga_akhir', 'promos.use_pin', 'promos.sequence', 'promos.expired_date', 'promos.top_promo')
        ->join('merchants', 'merchants.uid', '=', 'promos.merchant_uid')
        ->where('promos.delete', 0)
        ->where('merchants.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $promo;
    }
    
    public function getFilteredPromo($search, $start, $limit, $order, $dir)
    {
        $promo = $this->select('merchants.id as merchant_id', 'merchants.name', 'promos.id', 'promos.uid', 'promos.merchant_uid', 'promos.title', 'promos.description', 'promos.img', 'promos.harga_awal', 'promos.harga_akhir', 'promos.use_pin', 'promos.sequence', 'promos.expired_date', 'promos.top_promo')
        ->join('merchants', 'merchants.uid', '=', 'promos.merchant_uid')
        ->where(function ($q) use($search) {
            $q->where('merchants.name', 'like', '%'.$search.'%');
            $q->orWhere('promos.title', 'like', '%'.$search.'%');
            $q->orWhere('promos.description', 'like', '%'.$search.'%');
            $q->orWhere('promos.img', 'like', '%'.$search.'%');
            $q->orWhere('promos.harga_awal', 'like', '%'.$search.'%');
            $q->orWhere('promos.harga_akhir', 'like', '%'.$search.'%');
        })
        ->where('promos.delete', 0)
        ->where('merchants.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $promo;
    }

    /* ------------------------------------- Promo Global Partner Di Halaman Benefit --------------------------- */

    public function countAllActiveSelectedPromoPartnerInBenefit($user_uid)
    {
        $count_promo = $this->select('promos.id')
        ->join('merchants', 'merchants.uid', '=', 'promos.merchant_uid')
        ->join('partners_promos', 'partners_promos.promo_uid', '=', 'promos.uid')
        ->where('partners_promos.user_uid', $user_uid)
        ->where('promos.delete', 0)
        ->where('merchants.delete', 0)
        ->count();

        return $count_promo;
    }

    public function countAllFilteredActiveSelectedPromoPartnerInBenefit($search, $user_uid)
    {
        $count_promo = $this->select('promos.id')
        ->join('merchants', 'merchants.uid', '=', 'promos.merchant_uid')
        ->join('partners_promos', 'partners_promos.promo_uid', '=', 'promos.uid')
        ->where(function ($q) use($search) {
            $q->where('merchants.name', 'like', '%'.$search.'%');
            $q->orWhere('promos.title', 'like', '%'.$search.'%');
            $q->orWhere('promos.description', 'like', '%'.$search.'%');
            $q->orWhere('promos.img', 'like', '%'.$search.'%');
            $q->orWhere('promos.harga_awal', 'like', '%'.$search.'%');
            $q->orWhere('promos.harga_akhir', 'like', '%'.$search.'%');
        })
        ->where('partners_promos.user_uid', $user_uid)
        ->where('promos.delete', 0)
        ->where('merchants.delete', 0)
        ->count();

        return $count_promo;
    }

    public function getSelectedPromoPartnerInBenefit($start, $limit, $order, $dir, $user_uid)
    {
        $promo = $this->select('merchants.id as merchant_id', 'merchants.name', 'promos.id', 'promos.uid', 'promos.merchant_uid', 'promos.title', 'promos.description', 'promos.img', 'promos.harga_awal', 'promos.harga_akhir', 'promos.use_pin', 'promos.sequence', 'promos.expired_date', 'promos.top_promo')
        ->join('merchants', 'merchants.uid', '=', 'promos.merchant_uid')
        ->join('partners_promos', 'partners_promos.promo_uid', '=', 'promos.uid')
        ->where('partners_promos.user_uid', $user_uid)
        ->where('promos.delete', 0)
        ->where('merchants.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $promo;
    }
    
    public function getFilteredSelectedPromoPartnerInBenefit($search, $start, $limit, $order, $dir, $user_uid)
    {
        $promo = $this->select('merchants.id as merchant_id', 'merchants.name', 'promos.id', 'promos.uid', 'promos.merchant_uid', 'promos.title', 'promos.description', 'promos.img', 'promos.harga_awal', 'promos.harga_akhir', 'promos.use_pin', 'promos.sequence', 'promos.expired_date', 'promos.top_promo')
        ->join('merchants', 'merchants.uid', '=', 'promos.merchant_uid')
        ->join('partners_promos', 'partners_promos.promo_uid', '=', 'promos.uid')
        ->where(function ($q) use($search) {
            $q->where('merchants.name', 'like', '%'.$search.'%');
            $q->orWhere('promos.title', 'like', '%'.$search.'%');
            $q->orWhere('promos.description', 'like', '%'.$search.'%');
            $q->orWhere('promos.img', 'like', '%'.$search.'%');
            $q->orWhere('promos.harga_awal', 'like', '%'.$search.'%');
            $q->orWhere('promos.harga_akhir', 'like', '%'.$search.'%');
        })
        ->where('partners_promos.user_uid', $user_uid)
        ->where('promos.delete', 0)
        ->where('merchants.delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $promo;
    }

    /* -------------------------------------------------------------------------------------------------------------- */

    public function getOnePromo($id)
    {
        $promo = $this->select('merchants.id as merchant_id', 'merchants.name', 'promos.id', 'promos.uid', 'promos.merchant_uid', 'promos.title', 'promos.description', 'promos.img', 'promos.harga_awal', 'promos.harga_akhir', 'promos.use_pin', 'promos.sequence', 'promos.expired_date', 'promos.top_promo')
        ->join('merchants', 'merchants.uid', '=', 'promos.merchant_uid')
        ->where('promos.id', $id)
        ->where('promos.delete', 0)
        ->where('merchants.delete', 0)
        ->first();

        return $promo;
    }

    public function getOnePromoByUID($uid)
    {
        $promo = $this->select('merchants.id as merchant_id', 'merchants.name', 'promos.id', 'promos.uid', 'promos.merchant_uid', 'promos.title', 'promos.description', 'promos.img', 'promos.harga_awal', 'promos.harga_akhir', 'promos.use_pin', 'promos.sequence', 'promos.expired_date', 'promos.top_promo')
        ->join('merchants', 'merchants.uid', '=', 'promos.merchant_uid')
        ->where('promos.uid', $uid)
        ->where('promos.delete', 0)
        ->where('merchants.delete', 0)
        ->first();

        return $promo;
    }

    public function verifyPromoExpiredDate($uid)
    {
        $promo = $this->select('promos.expired_date')
        ->where('promos.uid', $uid)
        ->where('promos.delete', 0)
        ->first();

        return $promo;
    }

    public function getTopPromo() {
        $promo = $this->select('merchants.name', 'promos.uid', 'promos.title')
        ->join('merchants', 'merchants.uid', '=', 'promos.merchant_uid')
        ->where('promos.top_promo', 'yes')
        ->where('promos.delete', 0)
        ->where('promos.expired_date', '>=', date('Y-m-d H:i:s'))
        ->where('merchants.delete', 0)
        ->get();

        return $promo;
    }

    public function getAllPromo() {
        $promo = $this->select('merchants.id as merchant_id', 'merchants.name', 'promos.id', 'promos.uid', 'promos.merchant_uid', 'promos.title', 'promos.description', 'promos.img', 'promos.harga_awal', 'promos.harga_akhir', 'promos.expired_date', 'promos.top_promo', 'promos.use_pin')
        ->join('merchants', 'merchants.uid', '=', 'promos.merchant_uid')
        ->where('promos.top_promo', 'yes')
        ->where('promos.delete', 0)
        ->where('promos.expired_date', '>=', date('Y-m-d H:i:s'))
        ->where('merchants.delete', 0)
        ->orderBy('promos.sequence', 'asc')
        ->get();

        return $promo;
    }

    public function getAllPromoForSettingPin() {
        $promo = $this->select('merchants.name', 'promos.uid', 'promos.title')
        ->join('merchants', 'merchants.uid', '=', 'promos.merchant_uid')
        ->where('promos.top_promo', 'yes')
        ->where('promos.delete', 0)
        ->where('promos.expired_date', '>=', date('Y-m-d H:i:s'))
        ->where('merchants.delete', 0)
        ->orderBy('promos.sequence', 'asc')
        ->get();

        return $promo;
    }

    public function postAddPromo($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('promos'),
                'merchant_uid' => $param['merchant_uid'],
                'title' => urlencode($param['title']),
                'description' => urlencode($param['description']),
                'img' => $param['img_path'],
                'harga_awal' => $param['harga_awal'],
                'harga_akhir' => $param['harga_akhir'],
                // 'pin' => $param['pin'],
                'use_pin' => $param['use_pin'],
                'expired_date' => $param['expired_date'],
                'top_promo' => $param['top_promo'],
                'sequence' => $param['sequence'],
                'delete' => 0
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }

    public function postEditPromo($param, $id)
    {
        DB::transaction(function () use($param, $id) {
            if($param['img_path'] != null || $param['img_path'] != '')
            {
                $this->where('promos.id', $id)->where('promos.delete', 0)->update([
                    'merchant_uid' => $param['merchant_uid'],
                    'title' => urlencode($param['title']),
                    'description' => urlencode($param['description']),
                    'img' => $param['img_path'],
                    'harga_awal' => $param['harga_awal'],
                    'harga_akhir' => $param['harga_akhir'],
                    'use_pin' => $param['use_pin'],
                    'expired_date' => $param['expired_date'],
                    'top_promo' => $param['top_promo']
                ]);
            }
            else
            {
                $this->where('promos.id', $id)->where('promos.delete', 0)->update([
                    'merchant_uid' => $param['merchant_uid'],
                    'title' => urlencode($param['title']),
                    'description' => urlencode($param['description']),
                    'harga_awal' => $param['harga_awal'],
                    'harga_akhir' => $param['harga_akhir'],
                    'pin' => $param['pin'],
                    'use_pin' => $param['use_pin'],
                    'expired_date' => $param['expired_date'],
                    'top_promo' => $param['top_promo']
                ]);
            }
            
        });

        return $this->success_update_msg;
    }

    public function postUpdateSequencePromo($param)
    {
        DB::transaction(function () use($param) {

            for ($i = 0; $i < count($param['promo_uid']); $i++) {
                
                $this->where('uid', $param['promo_uid'][$i])->where('promos.delete', 0)->update([
                    'sequence' => $param['new_position'][$i]
                ]);
            }
            
        });

        return $this->success_update_sequence_msg;
    }

    public function postDeletePromo($id)
    {
        DB::transaction(function () use($id) {
            $this->where('id', $id)->where('promos.delete', 0)->update([
                'delete' => 1
            ]);
        });

        return $this->success_delete_msg;
    }
}
