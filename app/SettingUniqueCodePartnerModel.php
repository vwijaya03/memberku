<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;
use Hash, DB, Log;

class SettingUniqueCodePartnerModel extends Model
{
    protected $table = 'setting_unique_code_partners';
	protected $primaryKey = 'id';
    protected $fillable = ['uid', 'user_uid', 'total_uc', 'max_total_uc', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di proses.';
    private $success_delete_msg = 'Data berhasil di hapus.';
    private $success_reset_msg = 'Data berhasil di reset.';

    public function getOne($user_uid)
    {
    	$setting_unique_code_partner = $this->select('id', 'uid', 'user_uid', 'total_uc', 'max_total_uc')
        ->where('user_uid', $user_uid)
        ->where('delete', 0)
        ->first();

        return $setting_unique_code_partner;
    }

    public function postAddSettingUniqueCodePartner($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'uid' => HelperController::uid('setting_unique_code_partners'),
                'user_uid' => $param['user_uid'],
                'total_uc' => 0,
                'max_total_uc' => $param['max_total_uc'],
                'delete' => 0
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }

    public function postEditSettingUniqueCodePartner($param, $user_uid)
    {
        DB::transaction(function () use($param, $user_uid) {
            $this->where('setting_unique_code_partners.user_uid', $user_uid)->where('setting_unique_code_partners.delete', 0)->update([
                'max_total_uc' => $param['max_total_uc']
            ]);
        });

        return $this->success_update_msg;
    }

    public function postEditSettingUniqueCodePartnerTotalUc($total_uc, $user_uid)
    {
        DB::transaction(function () use($total_uc, $user_uid) {
            $this->where('setting_unique_code_partners.user_uid', $user_uid)->where('setting_unique_code_partners.delete', 0)->update([
                'total_uc' => $total_uc
            ]);
        });

        return $this->success_update_msg;
    }

    public function postResetTotalUniqueCodePartner($user_uid)
    {
        DB::transaction(function () use($user_uid) {
            $this->where('setting_unique_code_partners.user_uid', $user_uid)->where('setting_unique_code_partners.delete', 0)->update([
                'total_uc' => 0
            ]);
        });

        return $this->success_reset_msg;
    }

    public function cmdResetTotalUniqueCode()
    {
        DB::transaction(function () {
            $this->where('setting_unique_code_partners.delete', 0)->update([
                'total_uc' => 0
            ]);
        });
    }
}